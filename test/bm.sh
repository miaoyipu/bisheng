for i in 1a1e 1b3f 1d6v 1m7d 1oba 1toj 1utn 1z3v 2a5b 2bfq 2d0k 2jg0 2qtn 2v00 2vhj 2xbv 2ydw 2yix 3cd5 3coz 3ekv 3gy3 3iqg 3jup 3ldp 3n35 3p5o 3s8n 4dkp 4g0y 1b32 1b3l 1jyq 1mfd 1qkb 1utl 1z3t 1zc9 2baj 2cbu 2hjb 2ogy 2uyn 2v3u 2vrj 2xdk 2ygf 3cct 3coy 3dsz 3ga5 3h78 3iw6 3kgu 3ms9 3nes 3rux 4agl 4g0p 4g0z
do
grep -i $i exp
../bin/bisheng -i ../example/input -p $i/$i'_complex.pdb' -l $i/$i'_ligand.mol2' > output.$i 

grep -i "AVERAGE FREE ENERGY=" output.$i | sed -e 's/AVERAGE FREE ENERGY=//g' | sed -e 's/KCAL\/MOL//g'
done
