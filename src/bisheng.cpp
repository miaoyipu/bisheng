//
//  bisheng.cpp
//  BiSheng
//
//  Created by Yipu Miao on 1/29/14.
//  Copyright (c) 2014 Yipu Miao. All rights reserved.
//

#include "bisheng.h"
using namespace std;

int main(int argc, char *argv[]){
    
    /***************************************************************************
     
     PART 0: PREPARE FILE FIRST
     
     ***************************************************************************/
    
    print_copyright();
    
    time_t rawtime;
    time ( &rawtime );
    
    cout << " START JOB AT " << ctime (&rawtime) << endl;
    
    // Default file name when user does not provide their own
    string inputFileName = "mt.in";
    string PDBFileName   = "mt.pdb";
    string ligandFileName= "mt.mol2";
    
    clock_t start_time, end_time;
    start_time = clock();
    
    for (int i = 1; i < argc - 1; i++) {
        if (strcmp(argv[i], "-i") == 0)  inputFileName  = argv[i+1];
        if (strcmp(argv[i], "-p") == 0)  PDBFileName    = argv[i+1];
        if (strcmp(argv[i], "-l") == 0)  ligandFileName = argv[i+1];
    }
    cout << " ---------------- FILE INFORMATION -----------------"<< endl;
    cout << "    INPUT FILE NAME : " << inputFileName <<endl;
    cout << "    PDB FILE NAME   : " << PDBFileName <<endl;
    cout << "    LIGAND FILE NAME: " << ligandFileName << endl;
    cout << endl;
    
    /***************************************************************************
     
       PART I: READ INPUT, PROTEIN AND LIGAND FILE
     
     ***************************************************************************/
    
    Input input(inputFileName);
    input.print();
    
    // Read PDB and ligand File
    Protein protein(PDBFileName);
    Ligand  ligand (ligandFileName);
    
    
    // We can eliminate non-sensentive protein atoms that distant from ligand, set cutoff in input file.
    protein.screen(ligand, input.getCutoff());
    
    end_time = clock();
    
    cout << " TIME TO PREPARE FILE = " << (end_time - start_time) / (double) CLOCKS_PER_SEC << " s" << endl;
    
    
    /***************************************************************************
     
       PART II: Start calculation depending on job card
     
     ***************************************************************************/
    
    if (input.getDocking()) {  // Docking
        Docking docking(protein, ligand, input);
    }else{                     // Free Energy Calculation
        Energy  energy (protein, ligand, input);
    }
    
    protein.clean();
    ligand.clean();
    
    // If it comes here, then we clean up the system successfully
    time ( &rawtime );
    cout << " COMPLETE JOB AT " << ctime (&rawtime) << endl;
}


void print_copyright(){
    cout << endl;
    cout << " =================================================== " << endl;
    cout << "                Bi Sheng                             " << endl;
    cout << " --------------------------------------------------- " << endl;
    cout << "     Copyright 2014 Michigan State University        " << endl;
    cout << "              Kenneth Merz Group"                     << endl;
    cout << endl;
    cout << "     Software Development:  Yipu Miao"                << endl;
    cout << "     Method Development  :  John Zheng Zheng"         << endl;
    cout << " ================================================== " << endl;
    cout << endl;
    
}