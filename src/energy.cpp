//
//  energy.cpp
//  BiSheng
//
//  Created by Yipu Miao on 11/13/14.
//  Copyright (c) 2014 Yipu Miao. All rights reserved.
//

#include "energy.h"

void print_title(){
    cout << "-----------------------------------------------------------------------------------------------------" <<endl;
    cout << "  RUN          dG              average(dG)          aver diff           SD                  SE "       <<endl;
    cout << "----------------------------------------------------------------------------------------------------"  <<endl;
    cout << std::fixed;
}

Energy :: Energy(Protein& protein, Ligand& ligand, Input& input){
    
    clock_t start_time, end_time;
    
    start_time = clock();
    
    /*
     Retrieve database for calculation:
     Currently, 9 databases are required which are:
     Proteins: Bond, Angle, Torsion, Non-Bond
     Ligend  : Bond, Angle, Torsion, Non-Bond
     And P-L : VDW
     
     */
    shared_ptr<Database> pro_bond_database     (new Database("PRO_BOND",    (BSDouble*)pro_bond_d,    (BSDouble*) pro_bond_f,
                                                                       PRO_BOND_D_W,    PRO_BOND_D_H,    PRO_BOND_TILE_W,    PRO_BOND_TILE_H,
                                                                       pro_bond_hashing,    1.0001) );
    
    shared_ptr<Database> pro_angle_database    (new Database("PRO_ANGLE",    (BSDouble*)pro_angle_d,  (BSDouble*) pro_angle_f,
                                                                       PRO_ANGLE_D_W,   PRO_ANGLE_D_H,   PRO_ANGLE_TILE_W,   PRO_ANGLE_TILE_H,
                                                                       pro_angle_hashing,   0.0100) );
    
    shared_ptr<Database> pro_torsion_database  (new Database("PRO_TORSION",  (BSDouble*)pro_torsion_d, (BSDouble*) pro_torsion_f,
                                                                       PRO_TORSION_D_W, PRO_TORSION_D_H, PRO_TORSION_TILE_W, PRO_TORSION_TILE_H,
                                                                       pro_torsion_hashing, 1.0001) );
    
    shared_ptr<Database> pro_non_database      (new Database("PRO_NON",      (BSDouble*)pro_non_d,     (BSDouble*) pro_non_f,
                                                                       PRO_NON_D_W,     PRO_NON_D_H,     PRO_NON_TILE_W,     PRO_NON_TILE_H,
                                                                       pro_non_hashing,     1.0001,
                                                                       pro_non_hashing_h,   pro_non_check_HB_hashing) );
    
    shared_ptr<Database> lig_bond_database     (new Database("LIG_BOND",    (BSDouble*)lig_bond_d,    (BSDouble*) lig_bond_f,
                                                                       LIG_BOND_D_W,    LIG_BOND_D_H,    LIG_BOND_TILE_W,    LIG_BOND_TILE_H,
                                                                       lig_bond_hashing,    1.0001) );
    
    shared_ptr<Database> lig_angle_database    (new Database("LIG_ANGLE",    (BSDouble*)lig_angle_d,  (BSDouble*) lig_angle_f,
                                                                       LIG_ANGLE_D_W,   LIG_ANGLE_D_H,   LIG_ANGLE_TILE_W,   LIG_ANGLE_TILE_H,
                                                                       lig_angle_hashing,   0.0101) );
    
    shared_ptr<Database> lig_torsion_database  (new Database("LIG_TORSION",  (BSDouble*)lig_torsion_d, (BSDouble*) lig_torsion_f,
                                                                       LIG_TORSION_D_W, LIG_TORSION_D_H, LIG_TORSION_TILE_W, LIG_TORSION_TILE_H,
                                                                       lig_torsion_hashing, 1.0001) );
    
    shared_ptr<Database> lig_non_database      (new Database("LIG_NON",      (BSDouble*)lig_non_d,     (BSDouble*) lig_non_f,
                                                                       LIG_NON_D_W,     LIG_NON_D_H,     LIG_NON_TILE_W,     LIG_NON_TILE_H,
                                                                       lig_non_hashing,     1.0001) );
    
    shared_ptr<Database> vdw_database          (new Database("VDW",          (BSDouble*)vdw_d,         (BSDouble*) vdw_f,
                                                                       VDW_D_W,         VDW_D_H,         VDW_TILE_W,         VDW_TILE_H,
                                                                       vdw_hashing,         1.0001,
                                                                       vdw_hashing_h,   vdw_check_HB_hashing) );
    
    
    /*
     Each calculated class will produce a result. And we executes several runs to get a stable result.
     Basically, we think average free energy is good enought to express the final result. The more runs
     you will have more accurate results. But we suggest to run 500-1000 runs with sample size equals to
     500-1000.
     
     Another option is to run until average free energy changes less than a tolerence, but this option is not
     recommended.
     */
    
    srand (unsigned(time(0)));
    
    Analysis analysis;
    
    if(input.getVerbose() > 1){
        print_title();
    }
    
    
    for (int i = 0; i < input.getMaxCycle(); i++) {
        
        Calculated result(protein, ligand, input.getWidth(), input.getHeight(),
                               pro_bond_database.get(), pro_angle_database.get(), pro_torsion_database.get(), pro_non_database.get(),
                               lig_bond_database.get(), lig_angle_database.get(), lig_torsion_database.get(), lig_non_database.get(),
                               vdw_database.get());
        
        analysis.add(result.get_dG());
        
        if (input.getVerbose() > 1){
            analysis.print_current();
        }
        
        this->dG = analysis.get_dG();
        
        if (fabs(sqrt(analysis.get_sd())/(i+1)) < input.getTol() && i > (input.getMinCycle() - 1) ) {
            if (input.getVerbose() > 1){
                cout << "----------------------------------------------------" <<endl << "REACH CONVERGE AT RUN = " << i << endl;
            }
            break;
        }
        
    }
    
    analysis.print_final(input);
    this->dG = analysis.get_dG();
    
    end_time = clock();
    if (input.getVerbose() > 1) {
        cout << "    TIME TO CALCULATE = " << setw(8) << setprecision(2) << (end_time - start_time) / (double) CLOCKS_PER_SEC << " S"
        << " ( AVERAGE "              << setw(8) << setprecision(2) << (end_time - start_time) / (double) CLOCKS_PER_SEC / analysis.get_converged_cyc()
        << " S/RUN )" << endl;
        cout << "--------------------------------------------------------------"<<endl;
    }
}