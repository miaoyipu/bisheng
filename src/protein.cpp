//
//  protein.cpp
//  BiSheng
//
//  Created by Yipu Miao on 2/3/14.
//  Copyright (c) 2014 Yipu Miao. All rights reserved.
//

#include "protein.h"


// Base class to express an atom

BSDouble XYZ :: Distance(XYZ* _xyz) const{
    BSDouble distance = 0.0e0;
    distance += pow((this->x - _xyz->x), 2);
    distance += pow((this->y - _xyz->y), 2);
    distance += pow((this->z - _xyz->z), 2);
    
    return sqrt(distance);
}


BSDouble XYZ :: Distance2(XYZ* _xyz) const{
    return  pow((this->x - _xyz->x), 2) +
            pow((this->y - _xyz->y), 2) +
            pow((this->z - _xyz->z), 2);
}

BSDouble XYZ :: Distance(XYZ& _xyz) const{
    BSDouble distance = 0.0e0;
    distance += pow((this->x - _xyz.x), 2);
    distance += pow((this->y - _xyz.y), 2);
    distance += pow((this->z - _xyz.z), 2);
    
    return sqrt(distance);
}


BSDouble XYZ :: Distance2(XYZ& _xyz) const{
    BSDouble distance = 0.0e0;
    distance += pow((this->x - _xyz.x), 2);
    distance += pow((this->y - _xyz.y), 2);
    distance += pow((this->z - _xyz.z), 2);
    return distance;
}


BSDouble XYZ :: Distance2(BSDouble x, BSDouble y, BSDouble z) const{
    BSDouble distance = 0.0e0;
    distance += pow((this->x - x), 2);
    distance += pow((this->y - y), 2);
    distance += pow((this->z - z), 2);
    return distance;
}

void Atom :: print(){
    cout << this->atomNum << " ATOM NAME = " << this->name1 << " " << this->atomName << " COOR: " << this->x \
    << " " << this->y << " " << this->z << " HB = " << this->hb << endl;
}

bool Atom :: checkHB(Atom* _xyz){
    bool result1 = ((this->hb == D) || (this->hb == D2) || (this->hb == DA));
    bool result2 = ((_xyz->hb == A) || (_xyz->hb == DA));
    bool result3 = ((_xyz->hb == D) || (_xyz->hb == D2) || (_xyz->hb == DA));
    bool result4 = ((this->hb == A) || (this->hb == DA));
    
    return ((result1 && result2) || (result3 && result4));
}


// Protein atom class that inherent atom class
ProteinAtom::ProteinAtom(string line){
    string _tmpString1, _tmpString2, _tmpString3 ;
    int i  = 0;
    size_t current;
    size_t next = -1;
    int atomType = -1; // 0: ATOM,  1: HETATM
    
    do
    {
        /*
         Current PDB read subroutine splits domain by space, so there will be a bug that if the atom
         name is too long so there is no space between next domain, it will read them as one. This can
         be easily revised later.
         */
        current = next + 1;
        next = line.find_first_of( " ", current );
        if (current != next) {
            if (i == 0 && line.substr( current, next - current).compare("ATOM") == 0 ) {
                atomType = 0;
            }else if(i == 0 && line.substr( current, next - current).compare("HETATM") == 0 ) {
                atomType = 1;
                this->chain = " ";
            }
            
            if (atomType != -1) {
                //            cout << line.substr( current, next - current ) << endl;
                if (i == 1) this->atomNum  = atoi(line.substr( current, next - current ).c_str());
                if (i == 3) this->res      = line.substr( current, next - current );
                if (i == 2) this->atomName = line.substr( current, next - current );
                
                /*
                 If it is an ATOM type
                 */
                if (atomType == 0) {
                    if (i == 5) this->chain   = line.substr( current, next - current );
                    if (i == 5) this->resSeq  = atoi(line.substr( current, next - current ).c_str());
                    if (i == 6) this->x       = atof(line.substr( current, next - current ).c_str());
                    if (i == 7) this->y       = atof(line.substr( current, next - current ).c_str());
                    if (i == 8) this->z       = atof(line.substr( current, next - current ).c_str());
                }
                /*
                 If it is a HETATM type
                 */
                if (atomType == 1) {
                    if (i == 4) this->resSeq = atoi(line.substr( current, next - current ).c_str());
                    if (i == 5) this->x = atof(line.substr( current, next - current ).c_str());
                    if (i == 6) this->y = atof(line.substr( current, next - current ).c_str());
                    if (i == 7) this->z = atof(line.substr( current, next - current ).c_str());
                }
            }
            i++;
        }
    }while (next != string::npos);
    
    
    /*
     Once atom information is read from input line, we can assign its atom typa and paramters
     such as atom radius.
     AssignValue will assign 
     =================================================
     AtomType               |   constant int
     Residue Sequence       |   BSInt
     Radius                 |   BSDouble
     H-Bond type            |   constant int
     Acceptor H-Bond angle  |   BSDouble
     Donor H-Bond angle     |   BSDouble
     charge                 |   BSDouble
     if it can form backbond|   BSInt
     =================================================
     */
    if (this->atomName.compare(0, 1, "H") == 0 ||this->atomName.compare(0, 1, "1") == 0 ||
        this->atomName.compare(0, 1, "2") == 0 ||this->atomName.compare(0, 1, "3") == 0 ) {
        AssignValue(H,    this->resSeq, 1.00e0, N_A, 0.0e0,   0.0e0, 0.0e0, 1);
    }else if (this->atomName.compare("CA") == 0){
        AssignValue(C_3,  this->resSeq, 1.94e0, N_A, 0.0e0,   0.0e0, 0.0e0, 1);
    }else if (this->atomName.compare("C") == 0 ){
        AssignValue(C_2,  this->resSeq, 1.90e0, N_A, 0.0e0,   0.0e0, 0.0e0, 1);
    }else if (this->atomName.compare("O") == 0 ){
        AssignValue(O_2,  this->resSeq, 1.66e0, A,   0.75*PI, 0.0e0, 0.0e0, 1);
    }else if (this->atomName.compare("N") == 0 ){
        AssignValue(N_AM, this->resSeq, 1.83e0, D,   0.0e0,   PI,    0.0e0, 1);
    }else if (this->atomName.compare(0, 2, "NE")  == 0 && this->res.compare("ARG") == 0){
        AssignValue(N_AM, this->resSeq, 1.83e0, D,   0.0e0,   PI,    0.0e0, 0);
    }else if (this->atomName.compare(0, 2, "CG")  == 0 && this->res.compare("ARG") == 0){
        AssignValue(C_3,  this->resSeq, 1.94e0, N_A, 0.0e0,   0.0e0, 0.0e0, 0);
    }else if (this->atomName.compare(0, 2, "CZ")  == 0 && this->res.compare("ARG") == 0){
        AssignValue(C_2,  this->resSeq, 1.90e0, N_A, 0.0e0,   0.0e0, 1.0e0, 0);
    }else if (this->atomName.compare(0, 3, "NH1") == 0 && this->res.compare("ARG") == 0){
        AssignValue(N_AM, this->resSeq, 1.83e0, D2,  0.0e0,   PI,    0.0e0, 0);
    }else if (this->atomName.compare(0, 3, "NH2") == 0 && this->res.compare("ARG") == 0){
        AssignValue(N_AM, this->resSeq, 1.83e0, D2,  0.0e0,   PI,    0.0e0, 0);
    }else if (this->atomName.compare(0, 2, "CD")  == 0 && this->res.compare("ARG") == 0){
        AssignValue(C_3,  this->resSeq, 1.94e0, N_A, 0.0e0,   0.0e0, 0.0e0, 0);
    }else if (this->atomName.compare(0, 3, "OD1") == 0 && this->res.compare("ASN") == 0){
        AssignValue(O_2,  this->resSeq, 1.66e0, A,   0.75*PI, 0.0e0, 0.0e0, 0);
    }else if (this->atomName.compare(0, 3, "ND2") == 0 && this->res.compare("ASN") == 0){
        AssignValue(N_AM, this->resSeq, 1.83e0, D2,  0.0e0,   PI,    0.0e0, 0);
    }else if (this->atomName.compare(0, 2, "CG")  == 0 && this->res.compare("ASN") == 0){
        AssignValue(C_2,  this->resSeq, 1.90e0, N_A, 0.0e0,   0.0e0, 0.0e0, 0);
    }else if (this->atomName.compare(0, 3, "OD1") == 0 && this->res.compare("ASP") == 0){
        AssignValue(O_2,  this->resSeq, 1.66e0, A,   0.75*PI, 0.0e0, -0.50, 0);
    }else if (this->atomName.compare(0, 3, "OD2") == 0 && this->res.compare("ASP") == 0){
        AssignValue(O_2,  this->resSeq, 1.66e0, A,   0.75*PI, 0.0e0, -0.50, 0);
    }else if (this->atomName.compare(0, 2, "CG")  == 0 && this->res.compare("ASP") == 0){
        AssignValue(C_2,  this->resSeq, 1.90e0, N_A, 0.0e0,   0.0e0, 0.0e0, 0);
    }else if (this->atomName.compare(0, 1, "S")   == 0 && this->res.compare("CYS") == 0){
        AssignValue(S_3,  this->resSeq, 2.09e0, DA,  0.6083*PI,PI,   0.087, 0);
    }else if (this->atomName.compare(0, 3, "OE1") == 0 && this->res.compare("GLN") == 0){
        AssignValue(O_2,  this->resSeq, 1.66e0, A,   0.75*PI, 0.0e0, 0.0e0, 0);
    }else if (this->atomName.compare(0, 3, "NE2") == 0 && this->res.compare("GLN") == 0){
        AssignValue(N_AM, this->resSeq, 1.87e0, D2,  0.0e0,   PI,    0.0e0, 0);
    }else if (this->atomName.compare(0, 2, "CG")  == 0 && this->res.compare("GLN") == 0){
        AssignValue(C_3,  this->resSeq, 1.94e0, N_A, 0.0e0,   0.0e0, 0.0e0, 0);
    }else if (this->atomName.compare(0, 2, "CD")  == 0 && this->res.compare("GLN") == 0){
        AssignValue(C_2,  this->resSeq, 1.90e0, N_A, 0.0e0,   0.0e0, 0.0e0, 0);
    }else if (this->atomName.compare(0, 3, "OE1") == 0 && this->res.compare("GLU") == 0){
        AssignValue(O_2,  this->resSeq, 1.66e0, A,   0.75*PI, 0.0e0, -0.50, 0);
    }else if (this->atomName.compare(0, 3, "OE2") == 0 && this->res.compare("GLU") == 0){
        AssignValue(O_2,  this->resSeq, 1.66e0, A,   0.75*PI, 0.0e0, -0.50, 0);
    }else if (this->atomName.compare(0, 2, "CG")  == 0 && this->res.compare("GLU") == 0){
        AssignValue(C_3,  this->resSeq, 1.94e0, N_A, 0.0e0,   0.0e0, 0.0e0, 0);
    }else if (this->atomName.compare(0, 2, "CD")  == 0 && this->res.compare("GLU") == 0){
        AssignValue(C_2,  this->resSeq, 1.90e0, N_A, 0.0e0,   0.0e0, 0.0e0, 0);
    }else if (this->atomName.compare(0, 3, "ND1") == 0 && this->res.compare("HIE") == 0){
        AssignValue(N_2,  this->resSeq, 1.87e0, N_A, 0.75*PI, PI,    0.0e0, 0);
    }else if (this->atomName.compare(0, 3, "CD2") == 0 && this->res.compare("HIE") == 0){
        AssignValue(C_2,  this->resSeq, 1.90e0, N_A, 0.0e0,   0.0e0, 0.0e0, 0);
    }else if (this->atomName.compare(0, 3, "CE1") == 0 && this->res.compare("HIE") == 0){
        AssignValue(C_2,  this->resSeq, 1.90e0, N_A, 0.0e0,   0.0e0, 0.0e0, 0);
    }else if (this->atomName.compare(0, 3, "NE2") == 0 && this->res.compare("HIE") == 0){
        AssignValue(N_PL3,this->resSeq, 1.87e0, D,   0.75*PI, 0.0e0, 0.0e0, 0);
    }else if (this->atomName.compare(0, 2, "CG")  == 0 && this->res.compare("HIE") == 0){
        AssignValue(C_2,  this->resSeq, 1.90e0, N_A, 0.0e0,   0.0e0, 0.0e0, 0);
    }else if (this->atomName.compare(0, 3, "ND1") == 0 && this->res.compare("HIS") == 0){
        AssignValue(N_PL3,this->resSeq, 1.87e0, D,   0.75*PI, PI,    1.00,  0);
    }else if (this->atomName.compare(0, 3, "CD2") == 0 && this->res.compare("HIS") == 0){
        AssignValue(C_2,  this->resSeq, 1.90e0, N_A, 0.0e0,   0.0e0, 0.0e0, 0);
    }else if (this->atomName.compare(0, 3, "CE1") == 0 && this->res.compare("HIS") == 0){
        AssignValue(C_2,  this->resSeq, 1.90e0, N_A, 0.0e0,   0.0e0, 0.0e0, 0);
    }else if (this->atomName.compare(0, 3, "NE2") == 0 && this->res.compare("HIS") == 0){
        AssignValue(N_2,  this->resSeq, 1.87e0, N_A, 0.75*PI, 0.0e0, 0.0e0, 0);
    }else if (this->atomName.compare(0, 2, "CG")  == 0 && this->res.compare("HIS") == 0){
        AssignValue(C_2,  this->resSeq, 1.90e0, N_A, 0.0e0,   0.0e0, 0.0e0, 0);
    }else if (this->atomName.compare(0, 3, "CG1") == 0 && this->res.compare("ILE") == 0){
        AssignValue(C_3,  this->resSeq, 1.94e0, N_A, 0.0e0,   0.0e0, 0.0e0, 0);
    }else if (this->atomName.compare(0, 3, "CG2") == 0 && this->res.compare("ILE") == 0){
        AssignValue(C_3,  this->resSeq, 1.94e0, N_A, 0.0e0,   0.0e0, 0.0e0, 0);
    }else if (this->atomName.compare(0, 3, "CD1") == 0 && this->res.compare("ILE") == 0){
        AssignValue(C_3,  this->resSeq, 1.94e0, N_A, 0.0e0,   0.0e0, 0.0e0, 0);
    }else if (this->atomName.compare(0, 3, "CD1") == 0 && this->res.compare("LEU") == 0){
        AssignValue(C_3,  this->resSeq, 1.94e0, N_A, 0.0e0,   0.0e0, 0.0e0, 0);
    }else if (this->atomName.compare(0, 3, "CD2") == 0 && this->res.compare("LEU") == 0){
        AssignValue(C_3,  this->resSeq, 1.94e0, N_A, 0.0e0,   0.0e0, 0.0e0, 0);
    }else if (this->atomName.compare(0, 2, "CG")  == 0 && this->res.compare("LEU") == 0){
        AssignValue(C_3,  this->resSeq, 1.94e0, N_A, 0.0e0,   0.0e0, 0.0e0, 0);
    }else if (this->atomName.compare(0, 2, "NZ")  == 0 && this->res.compare("LYS") == 0){
        AssignValue(N_4,  this->resSeq, 1.83e0, D2,  0.0e0,   PI,    1.00,  0);
    }else if (this->atomName.compare(0, 2, "CG")  == 0 && this->res.compare("LYS") == 0){
        AssignValue(C_3,  this->resSeq, 1.94e0, N_A, 0.0e0,   0.0e0, 0.0e0, 0);
    }else if (this->atomName.compare(0, 2, "CD")  == 0 && this->res.compare("LYS") == 0){
        AssignValue(C_3,  this->resSeq, 1.94e0, N_A, 0.0e0,   0.0e0, 0.0e0, 0);
    }else if (this->atomName.compare(0, 2, "CE")  == 0 && this->res.compare("MET") == 0){
        AssignValue(C_3,  this->resSeq, 1.94e0, N_A, 0.0e0,   0.0e0, 0.0e0, 0);
    }else if (this->atomName.compare(0, 1, "S")   == 0 && this->res.compare("MET") == 0){
        AssignValue(S_3,  this->resSeq, 2.09e0, A,   0.75*PI, 0.0e0, 0.0e0, 0);
    }else if (this->atomName.compare(0, 2, "CG")  == 0 && this->res.compare("MET") == 0){
        AssignValue(C_3,  this->resSeq, 1.94e0, N_A, 0.0e0,   0.0e0, 0.0e0, 0);
    }else if (this->atomName.compare(0, 2, "CG")  == 0 && this->res.compare("PHE") == 0){
        AssignValue(C_AR, this->resSeq, 1.85e0, N_A, 0.0e0,   0.0e0, 0.0e0, 0);
    }else if (this->atomName.compare(0, 3, "CD1") == 0 && this->res.compare("PHE") == 0){
        AssignValue(C_AR, this->resSeq, 1.85e0, N_A, 0.0e0,   0.0e0, 0.0e0, 0);
    }else if (this->atomName.compare(0, 3, "CD2") == 0 && this->res.compare("PHE") == 0){
        AssignValue(C_AR, this->resSeq, 1.85e0, N_A, 0.0e0,   0.0e0, 0.0e0, 0);
    }else if (this->atomName.compare(0, 3, "CE1") == 0 && this->res.compare("PHE") == 0){
        AssignValue(C_AR, this->resSeq, 1.85e0, N_A, 0.0e0,   0.0e0, 0.0e0, 0);
    }else if (this->atomName.compare(0, 3, "CE2") == 0 && this->res.compare("PHE") == 0){
        AssignValue(C_AR, this->resSeq, 1.85e0, N_A, 0.0e0,   0.0e0, 0.0e0, 0);
    }else if (this->atomName.compare(0, 3, "CZ")  == 0 && this->res.compare("PHE") == 0){
        AssignValue(C_AR, this->resSeq, 1.85e0, N_A, 0.0e0,   0.0e0, 0.0e0, 0);
    }else if (this->atomName.compare(0, 3, "CZ2") == 0 && this->res.compare("PHE") == 0){
        AssignValue(C_AR, this->resSeq, 1.85e0, N_A, 0.0e0,   0.0e0, 0.0e0, 0);
    }else if (this->atomName.compare(0, 2, "CD")  == 0 && this->res.compare("PRO") == 0){
        AssignValue(C_3,  this->resSeq, 1.94e0, N_A, 0.0e0,   0.0e0, 0.0e0, 0);
    }else if (this->atomName.compare(0, 2, "CG")  == 0 && this->res.compare("PRO") == 0){
        AssignValue(C_3,  this->resSeq, 1.94e0, N_A, 0.0e0,   0.0e0, 0.0e0, 0);
    }else if (this->atomName.compare(0, 2, "OG")  == 0 && this->res.compare("SER") == 0){
        AssignValue(O_3,  this->resSeq, 1.74e0, DA,  0.6083*PI,  PI, 0.0e0, 0);
    }else if (this->atomName.compare(0, 3, "OG1") == 0 && this->res.compare("THR") == 0){
        AssignValue(O_3,  this->resSeq, 1.66e0, DA,  0.6083*PI,  PI, 0.0e0, 0);
    }else if (this->atomName.compare(0, 3, "CG2") == 0 && this->res.compare("THR") == 0){
        AssignValue(C_3,  this->resSeq, 1.94e0, N_A, 0.0e0,   0.0e0, 0.0e0, 0);
    }else if (this->atomName.compare(0, 3, "CD1") == 0 && this->res.compare("TRP") == 0){
        AssignValue(C_2,  this->resSeq, 1.90e0, N_A, 0.0e0,   0.0e0, 0.0e0, 0);
    }else if (this->atomName.compare(0, 3, "CD2") == 0 && this->res.compare("TRP") == 0){
        AssignValue(C_2,  this->resSeq, 1.90e0, N_A, 0.0e0,   0.0e0, 0.0e0, 0);
    }else if (this->atomName.compare(0, 3, "NE1") == 0 && this->res.compare("TRP") == 0){
        AssignValue(N_PL3,this->resSeq, 1.87e0, D,   0.6083*PI,  PI, 0.0e0, 0);
    }else if (this->atomName.compare(0, 3, "CE1") == 0 && this->res.compare("TRP") == 0){
        AssignValue(C_2,  this->resSeq, 1.90e0, N_A, 0.0e0,   0.0e0, 0.0e0, 0);
    }else if (this->atomName.compare(0, 3, "CE2") == 0 && this->res.compare("TRP") == 0){
        AssignValue(C_2,  this->resSeq, 1.90e0, N_A, 0.0e0,   0.0e0, 0.0e0, 0);
    }else if (this->atomName.compare(0, 3, "CE3") == 0 && this->res.compare("TRP") == 0){
        AssignValue(C_AR, this->resSeq, 1.85e0, N_A, 0.0e0,   0.0e0, 0.0e0, 0);
    }else if (this->atomName.compare(0, 3, "CZ2") == 0 && this->res.compare("TRP") == 0){
        AssignValue(C_AR, this->resSeq, 1.85e0, N_A, 0.0e0,   0.0e0, 0.0e0, 0);
    }else if (this->atomName.compare(0, 3, "CZ3") == 0 && this->res.compare("TRP") == 0){
        AssignValue(C_AR, this->resSeq, 1.85e0, N_A, 0.0e0,   0.0e0, 0.0e0, 0);
    }else if (this->atomName.compare(0, 3, "CH2") == 0 && this->res.compare("TRP") == 0){
        AssignValue(C_AR, this->resSeq, 1.85e0, N_A, 0.0e0,   0.0e0, 0.0e0, 0);
    }else if (this->atomName.compare(0, 2, "CG")  == 0 && this->res.compare("TRP") == 0){
        AssignValue(C_2,  this->resSeq, 1.90e0, N_A, 0.0e0,   0.0e0, 0.0e0, 0);
    }else if (this->atomName.compare(0, 2, "CG")  == 0 && this->res.compare("TYR") == 0){
        AssignValue(C_AR, this->resSeq, 1.85e0, N_A, 0.0e0,   0.0e0, 0.0e0, 0);
    }else if (this->atomName.compare(0, 3, "CD1") == 0 && this->res.compare("TYR") == 0){
        AssignValue(C_AR, this->resSeq, 1.85e0, N_A, 0.0e0,   0.0e0, 0.0e0, 0);
    }else if (this->atomName.compare(0, 3, "CD2") == 0 && this->res.compare("TYR") == 0){
        AssignValue(C_AR, this->resSeq, 1.85e0, N_A, 0.0e0,   0.0e0, 0.0e0, 0);
    }else if (this->atomName.compare(0, 3, "CE1") == 0 && this->res.compare("TYR") == 0){
        AssignValue(C_AR, this->resSeq, 1.85e0, N_A, 0.0e0,   0.0e0, 0.0e0, 0);
    }else if (this->atomName.compare(0, 3, "CE2") == 0 && this->res.compare("TYR") == 0){
        AssignValue(C_AR, this->resSeq, 1.85e0, N_A, 0.0e0,   0.0e0, 0.0e0, 0);
    }else if (this->atomName.compare(0, 2, "CZ")  == 0 && this->res.compare("TYR") == 0){
        AssignValue(C_AR, this->resSeq, 1.85e0, N_A, 0.0e0,   0.0e0, 0.0e0, 0);
    }else if (this->atomName.compare(0, 2, "OH")  == 0 && this->res.compare("TYR") == 0){
        AssignValue(O_3,  this->resSeq, 1.74e0, DA,  0.6083*PI,  PI, 0.0e0, 0);
    }else if (this->atomName.compare(0, 3, "CG1") == 0 && this->res.compare("VAL") == 0){
        AssignValue(C_3,  this->resSeq, 1.94e0, N_A, 0.0e0,   0.0e0, 0.0e0, 0);
    }else if (this->atomName.compare(0, 3, "CG2") == 0 && this->res.compare("VAL") == 0){
        AssignValue(C_3,  this->resSeq, 1.94e0, N_A, 0.0e0,   0.0e0, 0.0e0, 0);
    }else if (this->atomName.compare(0, 3, "LPD") == 0){
        AssignValue(H,    this->resSeq, 1.00e0, N_A, 0.0e0,   0.0e0, 0.0e0, 0);
    }else if (this->atomName.compare(0, 3, "LPG") == 0){
        AssignValue(H,    this->resSeq, 1.00e0, N_A, 0.0e0,   0.0e0, 0.0e0, 0);
    }else if (this->atomName.compare(0, 2, "CB")  == 0){
        AssignValue(C_3,  this->resSeq, 1.94e0, N_A, 0.0e0,   0.0e0, 0.0e0, 0);
    }else if (this->atomName.compare(0, 2, "CE")  == 0){
        AssignValue(C_3,  this->resSeq, 1.94e0, N_A, 0.0e0,   0.0e0, 0.0e0, 0);
    }else if (this->atomName.compare(0, 3, "OXT") == 0){
        AssignValue(O_2,  this->resSeq, 1.66e0, A,   0.75*PI, 0.0e0, 0.0e0, 1);
    }else if (this->atomName.compare(0, 2, "ZN")  == 0 && this->res.compare("ZN") == 0){
        AssignValue(ZN,   ZN,           1.2e0, N_A,  0.0e0,   0.0e0, 2.0e0, 0);
    }
    
    if     (this->chain.compare(0, 1, "A") == 0) {this->chainID = CHAIN_A;}
    else if(this->chain.compare(0, 1, "B") == 0) {this->chainID = CHAIN_B;}
    else if(this->chain.compare(0, 1, "C") == 0) {this->chainID = CHAIN_C;}
    else if(this->chain.compare(0, 1, "D") == 0) {this->chainID = CHAIN_D;}
    else if(this->chain.compare(0, 1, "E") == 0) {this->chainID = CHAIN_E;}
    else if(this->chain.compare(0, 1, "F") == 0) {this->chainID = CHAIN_F;}
    else if(this->chain.compare(0, 1, "G") == 0) {this->chainID = CHAIN_G;}
    else if(this->chain.compare(0, 1, "H") == 0) {this->chainID = CHAIN_H;}
    else if(this->chain.compare(0, 1, "I") == 0) {this->chainID = CHAIN_I;}
    else if(this->chain.compare(0, 1, "J") == 0) {this->chainID = CHAIN_J;}
    else if(this->chain.compare(0, 1, "K") == 0) {this->chainID = CHAIN_K;}
    else if(this->chain.compare(0, 1, "L") == 0) {this->chainID = CHAIN_L;}
    else if(this->chain.compare(0, 1, "M") == 0) {this->chainID = CHAIN_M;}
    else if(this->chain.compare(0, 1, "N") == 0) {this->chainID = CHAIN_N;}
    else if(this->chain.compare(0, 1, "O") == 0) {this->chainID = CHAIN_O;}
    else if(this->chain.compare(0, 1, "P") == 0) {this->chainID = CHAIN_P;}
    else if(this->chain.compare(0, 1, "Q") == 0) {this->chainID = CHAIN_Q;}
    else if(this->chain.compare(0, 1, "R") == 0) {this->chainID = CHAIN_R;}
    else if(this->chain.compare(0, 1, "S") == 0) {this->chainID = CHAIN_S;}
    else if(this->chain.compare(0, 1, "T") == 0) {this->chainID = CHAIN_T;}
    else if(this->chain.compare(0, 1, "U") == 0) {this->chainID = CHAIN_U;}
    else if(this->chain.compare(0, 1, "V") == 0) {this->chainID = CHAIN_V;}
    else if(this->chain.compare(0, 1, "W") == 0) {this->chainID = CHAIN_W;}
    else if(this->chain.compare(0, 1, "X") == 0) {this->chainID = CHAIN_X;}
    else if(this->chain.compare(0, 1, "Y") == 0) {this->chainID = CHAIN_Y;}
    else if(this->chain.compare(0, 1, "Z") == 0) {this->chainID = CHAIN_Z;}
    else if(this->chain.compare(0, 1, "1") == 0) {this->chainID = CHAIN_1;}
    else if(this->chain.compare(0, 1, "2") == 0) {this->chainID = CHAIN_2;}
    
    
    
    //cout << this-> backbond << endl;
    //cout << this->atomNum << " "<< this->res << " " << this->atomName << " " << this->resSeq << " name1=" << \
    //this->name1 << " name2="<< this->name2 << " " << this->radius << " "<< this->hb << " "<< this->acceptor_hb_angle \
    //<< " "<< this->donor_hb_angle << " "<< this->charge << " "<<  this->backbond << " "<< endl;
    return;
}

int ProteinAtom :: AssignValue(BSInt _name1, BSInt _name2, BSDouble _radius, BSInt _hb, BSDouble _acceptor_hb_angle, \
                                    BSDouble _donor_hb_angle, BSDouble _charge, BSInt _backbond){
    this->name1                 = _name1;
    this->name2                 = _name2;
    this->radius                = _radius;
    this->hb                    = _hb;
    this->acceptor_hb_angle     = _acceptor_hb_angle;
    this->donor_hb_angle        = _donor_hb_angle;
    this->charge                = _charge;
    this->backbond              = _backbond;
    return 0;
}

bool ProteinAtom :: IsZinc(){
    return (atomName.compare("ZN") == 0);
}



// Ligand atom class that inherent atom class
LigandAtom :: LigandAtom(string line){
    string _tmpString1, _tmpString2, _tmpString3 ;
    int i  = 0;
    
    size_t current;
    size_t next = -1;
    
    do
    {
        current = next + 1;
        next = line.find_first_of( " ", current );
        
        if (current != next) {
            if (i == 0) this->atomNum  = atoi(line.substr( current, next - current ).c_str());
            if (i == 2) this->x        = atof(line.substr( current, next - current ).c_str());
            if (i == 3) this->y        = atof(line.substr( current, next - current ).c_str());
            if (i == 4) this->z        = atof(line.substr( current, next - current ).c_str());
            if (i == 5) this->atomName = line.substr( current, next - current );
            i++;
        }
    }while (next != string::npos);
    
    this->neib_H = 0; // No neib H first
    
    if     ( this->atomName.compare("H")     == 0) { AssignValue(H,      1.00, N_A,  0.0e0,      0.0e0, 1.0,  0.0e0);}
    else if( this->atomName.compare("C.3")   == 0) { AssignValue(C_3,    1.90, N_A,  0.0e0,      0.0e0, 12.0, 0.0e0);}
    else if( this->atomName.compare("C.2")   == 0) { AssignValue(C_2,    1.90, N_A,  0.0e0,      0.0e0, 12.0, 0.0e0);}
    else if( this->atomName.compare("C.1")   == 0) { AssignValue(C_1,    1.90, N_A,  0.0e0,      0.0e0, 12.0, 0.0e0);}
    else if( this->atomName.compare("C.ar")  == 0) { AssignValue(C_AR,   1.90, N_A,  0.0e0,      0.0e0, 12.0, 0.0e0);}
    else if( this->atomName.compare("C.cat") == 0) { AssignValue(C_CAT,  1.90, N_A,  0.0e0,      0.0e0, 12.0, 0.0e0);} // May not be used
    else if( this->atomName.compare("O.3")   == 0) { AssignValue(O_3,    1.86, A,    0.0e0,      PI,    16.0, 0.0e0);}
    else if( this->atomName.compare("O.2")   == 0) { AssignValue(O_2,    1.86, A,    0.0e0,      PI,    16.0, 0.0e0);}
    else if( this->atomName.compare("O.w")   == 0) { AssignValue(O_W,    1.86, A,    0.0e0,      0.0e0, 16.0, 0.0e0);} // May not be used
    else if( this->atomName.compare("O.co2") == 0) { AssignValue(O_2,    1.86, A,    0.0e0,      PI,    16.0, 0.0e0);}
    else if( this->atomName.compare("N.4")   == 0) { AssignValue(N_4,    1.86, N_A,  0.0e0,      PI,    14.0, 1.0e0);}
    else if( this->atomName.compare("N.3")   == 0) { AssignValue(N_3,    1.86, N_A,  0.0e0,      PI,    14.0, 0.0e0);}
    else if( this->atomName.compare("N.2")   == 0) { AssignValue(N_2,    1.86, N_A,  0.0e0,      PI,    14.0, 0.0e0);}
    else if( this->atomName.compare("N.1")   == 0) { AssignValue(N_1,    1.86, N_A,  0.0e0,      0.0e0, 14.0, 0.0e0);} // May not be used
    else if( this->atomName.compare("N.ar")  == 0) { AssignValue(N_AR,   1.86, N_A,  0.0e0,      0.0e0, 14.0, 0.0e0);} // May not be used
    else if( this->atomName.compare("N.am")  == 0) { AssignValue(N_AM,   1.86, N_A,  0.0e0,      0.0e0, 14.0, 0.0e0);}
    else if( this->atomName.compare("N.pl3") == 0) { AssignValue(N_PL3,  1.86, N_A,  0.0e0,      0.0e0, 14.0, 0.0e0);}
    else if( this->atomName.compare("S.3")   == 0) { AssignValue(S_3,    2.09, A,    0.75 * PI,  0.0e0, 32.0, 0.0e0);}
    else if( this->atomName.compare("S.2")   == 0) { AssignValue(S_2,    2.09, A,    0.75 * PI,  0.0e0, 32.0, 0.0e0);}
    else if( this->atomName.compare("S.o")   == 0) { AssignValue(S_O,    2.09, A,    0.75 * PI,  0.0e0, 32.0, 0.0e0);}
    else if( this->atomName.compare("S.o2")  == 0) { AssignValue(S_O2,   2.09, A,    0.75 * PI,  0.0e0, 32.0, 0.0e0);}
    else if( this->atomName.compare("F")     == 0) { AssignValue(F,      1.77, A,    0.0e0,      0.0e0, 19.0, 0.0e0);}
    else if( this->atomName.compare("Cl")    == 0) { AssignValue(CL,     2.00, A,    0.0e0,      0.0e0, 35.5, 0.0e0);}
    else if( this->atomName.compare("Br")    == 0) { AssignValue(BR,     2.20, A,    0.0e0,      0.0e0, 80.0, 0.0e0);}
    else if( this->atomName.compare("I")     == 0) { AssignValue(I,      2.40, A,    0.0e0,      0.0e0, 127.0,0.0e0);}
    else if( this->atomName.compare("P.3")   == 0) { AssignValue(P,      2.03, A,    0.0e0,      0.0e0, 31.0, 0.0e0);}
    
    return;
    
}


int LigandAtom :: AssignValue(BSInt _name1, BSDouble _radius, BSInt _hb, BSDouble _acceptor_hb_angle, BSDouble _donor_hb_angle, \
                                   BSDouble _mass, BSDouble _charge){
    this->name1             = _name1;
    this->radius            = _radius;
    this->hb                = _hb;
    this->acceptor_hb_angle = _acceptor_hb_angle;
    this->donor_hb_angle    = _donor_hb_angle;
    this->mass              = _mass;
    this->charge            = _charge;
    return 0;
}

void LigandAtom :: FindNeibH(LigandAtom* ligand_atom){
    if       (this->IsHBondN() && ligand_atom->IsH() && this->Distance(ligand_atom) < HBOND_DISTANCE) {
        this->neib_H = this->neib_H + 1;
        if (this->neib_H == 1) {
            this->hb             = D;
            this->donor_hb_angle = PI;
        }else if (this->neib_H == 2){
            this->hb             = D2;
            this->donor_hb_angle = PI;
        }
    }else if (this->IsHBondS() && ligand_atom->IsH() && this->Distance(ligand_atom) < HBOND_DISTANCE) {
        this->neib_H = this->neib_H + 1;
        if (this->neib_H == 1) {
            this->hb                    = DA;
            this->donor_hb_angle        = PI;
            this->acceptor_hb_angle     = 0.6083 * PI;
        }
        
    }else if (this->IsHBondO() && ligand_atom->IsH() && this->Distance(ligand_atom) < HBOND_DISTANCE) {
        this->hb                = DA;
        this->donor_hb_angle    = PI;
        this->acceptor_hb_angle = 0.6083 * PI;
    }
    return;
}

XYZ Protein :: get_center_of_mass(){
    BSDouble x = 0;
    BSDouble y = 0;
    BSDouble z = 0;
    int size = is_screen? effect_atoms.size(): atoms.size();
    if (size == 0) {
        return XYZ(0, 0, 0);
    }
    
    if (is_screen) {
        for (int i = 0; i < size; i++) {
            x += effect_atoms[i]->x;
            y += effect_atoms[i]->y;
            z += effect_atoms[i]->z;
        }
    }else{
        for (int i = 0; i < size; i++) {
            x += atoms[i]->x;
            y += atoms[i]->y;
            z += atoms[i]->z;
        }
    }
#ifdef DEBUG
    cout << " EFFECT PROTEIN ATOM SIZE = " << size << endl;
    cout << " CENTER OF MASS: ( " << x/size << " , " << y/size << " , " << z/size << ")" << endl;
#endif
    
    return XYZ(x/size, y/size, z/size);
}




XYZ Protein :: get_min_xyz(){
    int size = is_screen? effect_atoms.size() : atoms.size();
    if (size == 0) {
        return XYZ(0, 0, 0);
    }
    
    
    if (is_screen) {
        BSDouble x = effect_atoms[0]->x;
        BSDouble y = effect_atoms[0]->y;
        BSDouble z = effect_atoms[0]->z;
        
        for (int i = 0; i <effect_atoms.size(); i++) {
            x = min(x, effect_atoms[i]->x);
            y = min(y, effect_atoms[i]->y);
            z = min(z, effect_atoms[i]->z);
        }
        
#ifdef DEBUG
        cout << " MIN OF PROTEIN: ( " << x<< " , " << y << " , " << z << ")" << endl;
#endif
        return XYZ(x, y, z);
    }else{
        BSDouble x = atoms[0]->x;
        BSDouble y = atoms[0]->y;
        BSDouble z = atoms[0]->z;
        
        for (int i = 0; i <atoms.size(); i++) {
            x = min(x, atoms[i]->x);
            y = min(y, atoms[i]->y);
            z = min(z, atoms[i]->z);
        }
        
#ifdef DEBUG
        cout << " MIN OF PROTEIN: ( " << x<< " , " << y << " , " << z << ")" << endl;
#endif
        return XYZ(x, y, z);
    }
}

XYZ Protein :: get_max_xyz(){
    int size = is_screen? effect_atoms.size() : atoms.size();
    if (size == 0) {
        return XYZ(0, 0, 0);
    }
    
    
    if (is_screen) {
        BSDouble x = effect_atoms[0]->x;
        BSDouble y = effect_atoms[0]->y;
        BSDouble z = effect_atoms[0]->z;
        
        for (int i = 0; i <effect_atoms.size(); i++) {
            x = max(x, effect_atoms[i]->x);
            y = max(y, effect_atoms[i]->y);
            z = max(z, effect_atoms[i]->z);
        }
        
#ifdef DEBUG
        cout << " MAX OF PROTEIN: ( " << x<< " , " << y << " , " << z << ")" << endl;
#endif
        
        return XYZ(x, y, z);
    }else{
        BSDouble x = atoms[0]->x;
        BSDouble y = atoms[0]->y;
        BSDouble z = atoms[0]->z;
        
        for (int i = 0; i <atoms.size(); i++) {
            x = max(x, atoms[i]->x);
            y = max(y, atoms[i]->y);
            z = max(z, atoms[i]->z);
        }
        
#ifdef DEBUG
        cout << " MAX OF PROTEIN: ( " << x<< " , " << y << " , " << z << ")" << endl;
#endif
        return XYZ(x, y, z);
    }
}



Protein :: Protein(string PDBFileName){
    ifstream PDBFile;
    
    PDBFile.open(PDBFileName.c_str());
    string line;
    
    if (PDBFile.is_open()) {

        while (getline(PDBFile, line)) {
            if (line.compare(0, 5, "ATOM ") == 0) {
                ProteinAtom* _tmp = (ProteinAtom*)new ProteinAtom(line);
#ifdef DEBUG
                _tmp -> print();
#endif
                if (! _tmp->IsH()) {
                    atoms.push_back(_tmp);
                }else{
                    h_atoms.push_back(_tmp);
                }
            }
            
            if (line.compare(0, 7, "HETATM ") == 0) {
                // This atom will read but not be counted unless it is Zinc
                ProteinAtom* _tmp = (ProteinAtom*)new ProteinAtom(line);
                if (_tmp->IsZinc()) {
                    vector<BSDouble> _tmp2;
                    _tmp2.push_back(_tmp->x);
                    _tmp2.push_back(_tmp->y);
                    _tmp2.push_back(_tmp->z);
                    _tmp2.push_back(_tmp->getResSeq());
                    ZnAtom.push_back(_tmp2);
                }
                //SAFEDELETE(_tmp);
            }
            
        }
        //this->set_atom_size(atoms.size());
        //this->set_h_atom_size(h_atoms.size());
        this->zn_atom_num     = this -> ZnAtom.size();
        
        PDBFile.close();
        
        if (this->get_atom_size() == 0) BSEXIT("CAN NOT FIND PROTEIN ATOM");
        
        cout << " COMPLETE PDB READ:  HEAVY ATOM = "<< this->get_atom_size() <<"  H ATOM = " << this->get_h_atom_size() << endl;
        
    }else{
        BSEXIT("UNABLE TO OPEN PDB FILE");
    }
    
    //this->effect_atom_num = this -> get_atom_size();
    
#ifdef CPU_MEM
    cpu_total_mem += sizeof(ProteinAtom*) * this->get_atom_size();
    cpu_total_mem += sizeof(ProteinAtom*) * this->get_h_atom_size();
    cpu_total_mem += sizeof(BSDouble)          * 4 * zn_atom_num;
    PRINTCPU("ALLOCATE ATOM POINTER");
#endif
    
    is_screen = false;
}


void Protein :: screen(Ligand ligand, BSDouble cutoff){
  
	// Clear effect atom so that new screen could erase old result; 
	effect_atoms.clear();
 
    for (int i = 0; i < this->get_atom_size(); i++) {
        for (int j = 0; j < ligand.get_atom_size(); j++) {
            if (ligand.get_atom(j)->Distance(this->get_atom(i)) < cutoff) {
                effect_atoms.push_back(this->get_atom(i));
                break;
            }
        }
    }
    
    cout << " WITH CUTOFF DISTANCE = " << cutoff << " A, FIND EFFECT PROTEIN ATOM = " << get_effect_atom_size() << endl;
    
    if (get_effect_atom_size() == 0) {
        BSEXIT("CAN NOT FIND EFFECT ATOMS CLOSE TO LIGEND");
    }
    
    is_screen = true;
}


Ligand :: Ligand(string ligandFileName){
    
    ifstream ligandFile;
    
    string line;
    
    ligandFile.open(ligandFileName.c_str());
    if (ligandFile.is_open()) {
        while (getline(ligandFile, line)) {
            unordered_map<BSInt, LigandAtom*> mp;
            int num = 0;
            if (line.compare(0, 13, "@<TRIPOS>ATOM") == 0) {
                while (getline(ligandFile,line) && !(line.compare(0, 13, "@<TRIPOS>BOND") == 0)) {
                    LigandAtom* _tmp = (LigandAtom*) new LigandAtom(line);
                    mp[num++] = _tmp;
#ifdef DEBUG
                    _tmp -> print();
#endif
                    if (! _tmp -> IsH() ) {
                        atoms.push_back(_tmp);
                    }else{
                        h_atoms.push_back(_tmp);
                    }
                }
                if (this->get_atom_size() == 0) BSEXIT("CAN NOT FIND LIGEND ATOM");
            }
            
            for (int i = 0; i < atoms.size(); i++) {
                this->graph.addNode(atoms[i]);
            }
            if (line.compare(0, 13, "@<TRIPOS>BOND") == 0){
                while (getline(ligandFile,line) && !(line.compare(0, 21, "@<TRIPOS>SUBSTRUCTURE") == 0)) {
                    BSInt _a, _b, _c;
                    string _d;
                    istringstream iss(line);
                    iss >> _a >> _b >> _c >> _d ; // _a is the bond number, _b and _c are two nodes and _d is bond type(not used here)
                    if (! mp[_b - 1]->IsH() && ! mp[_c - 1]->IsH() ) { // only count non-H atoms
                        this->graph.connect(mp[_b - 1], mp[_c - 1]);
                    }
                }
#ifdef DEBUG
                for (int i = 0; i < atoms.size(); i++) {
                    atoms[i]->print();
                    cout << "CYCLE: " << this->graph.findCycle(atoms[i]) << endl;
                }
#endif
            }
        }
        ligandFile.close();
        cout << " COMPLETE LIGEND READ: HEAVY ATOM = "<< this->get_atom_size() << " H ATOM = " << this->get_h_atom_size() << endl;
    }else{
        BSEXIT("UNABLE TO OPEN LIGAND FILE");
    }
    
    // Find H-bond and adjust atom type value, this is dependent on other atoms.
    for (int i = 0; i < this->get_atom_size(); i++) {
        for (int j = 0; j < this->get_atom_size(); j++) {
            this->atoms[i]->FindNeibH(this->atoms[j]);
        }
    }
    
    for (int i = 0; i < this->get_atom_size(); i++) {
        this->saved_xyz.push_back(XYZ(this->atoms[i]->x, this->atoms[i]->y, this->atoms[i]->z) );
    }
    
#ifdef CPU_MEM
    cpu_total_mem += sizeof(LigandAtom*)  * this->get_atom_size();
    cpu_total_mem += sizeof(LigandAtom*)  * this->get_h_atom_size();
#endif
    
}


void Ligand :: recover_xyz(){
    for (int i = 0; i < this->get_atom_size(); i++) {
        this->atoms[i]->x = this->saved_xyz[i].x;
        this->atoms[i]->y = this->saved_xyz[i].y;
        this->atoms[i]->z = this->saved_xyz[i].z;
    }
}

void Ligand :: trans(Transformation& transformation){
    for (int i = 0; i < this->get_atom_size(); i++) {
        transformation.rotate(this->atoms[i]);
        transformation.move(this->atoms[i]);
    }
    
}
