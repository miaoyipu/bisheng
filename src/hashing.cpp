//
//  hashing.cpp
//  BiSheng
//
//  Created by Yipu Miao on 2/3/14.
//  Copyright (c) 2014 Yipu Miao. All rights reserved.
//

#include "hashing.h"
#include <sstream>
#include <iostream>
#include <iomanip>
#include <locale>

Database :: Database(string _name,  BSDouble* _data,
                               BSDouble* _data_f,
                               BSInt _width,  BSInt _height,
                               BSInt _tile_w, BSInt _tile_h,
                               BSInt _hashing[MAX_ATOM_TYPE][MAX_ATOM_TYPE],
                               BSDouble _tol) :
name(_name), data(_data), data_f(_data_f),
width(_width), height(_height), tile_w(_tile_w), tile_h(_tile_h), interval(SEARCH_CRITERIA),
tol(_tol)
{
    hasHBond = false;
    
    for (int i = 0; i < MAX_ATOM_TYPE; i ++) {
        for (int j= 0; j < MAX_ATOM_TYPE; j++) {
            hashing[i][j]    = _hashing[i][j];
            hashing_hb[i][j] = 0;
            check_hb[i][j]   = 0;
        }
    }
    /*
    cout << name << endl;
    cout << std::fixed;
    for (int i = 0; i < height; i++) {
        for (int j= 0; j < width; j++) {
            cout << std::setw(8) << std::setprecision(5)<< LOC2(data, i, j, height, width);
        }
        cout << endl;
    }*/
    
    
    
    this->init();
    
}

Database :: Database(string _name,  BSDouble* _data,
                               BSDouble* _data_f,
                               BSInt _width,  BSInt _height,
                               BSInt _tile_w, BSInt _tile_h,
                               BSInt _hashing[MAX_ATOM_TYPE][MAX_ATOM_TYPE],
                               BSDouble _tol,
                               BSInt _hashing_hb[MAX_ATOM_TYPE][MAX_ATOM_TYPE],
                               BSInt _check_hb[MAX_ATOM_TYPE][MAX_ATOM_TYPE]
                               ) :
name(_name), data(_data), data_f(_data_f),
width(_width), height(_height), tile_w(_tile_w), tile_h(_tile_h), interval(SEARCH_CRITERIA),
tol(_tol)
{
    hasHBond = true;
    
    for (int i = 0; i < MAX_ATOM_TYPE; i ++) {
        for (int j= 0; j < MAX_ATOM_TYPE; j++) {
            hashing[i][j] = _hashing[i][j];
            hashing_hb[i][j] = _hashing_hb[i][j];
            check_hb[i][j]   = _check_hb[i][j];
        }
    }
    
    this->init();
}

void Database :: init(){
    
    
    for (int i = 1; i < this->width; i ++) {
        this->begin[i] = LOC2(this->data, 0, 0, this->height, this->width);
        this->end[i]   = LOC2(this->data, this->height - 1, 0, this->height, this->width);
        
        for (int k = 0; k < this->height; k++) {
            if ( fabs(LOC2(this->data, k, i, this->height, this->width)) > this->tol) {
                this->begin[i] = LOC2(this->data, k, 0, this->height, this->width);
                break;
            }
        }
        
        for (int k = this->height - 1; k >= 0 ; k--) {
            if ( fabs(LOC2(this->data, k, i, this->height, this->width)) > this->tol) {
                this->end[i] = LOC2(this->data, k, 0, this->height, this->width);
                break;
            }
        }
    }
}