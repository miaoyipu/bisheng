//
//  io.cpp
//  BiSheng
//
//  Created by Yipu Miao on 5/1/14.
//  Copyright (c) 2014 Yipu Miao. All rights reserved.
//

#include "io.h"

Input :: Input(string inputFileName):
max_cycle(50), min_cycle(5), width(1000), height(100),
solv(false), tol(1.0e-100), docking(false), grid_dense(0.2),
resolution(0.1), cutoff(sqrt(32.000)), verbose(3){
    ifstream input_file;
    input_file.open(inputFileName.c_str());
    if (! input_file.is_open()) {
        cout <<"UNABLE TO OPEN INPUT FILE, USE DEFAULT VALUE" << endl;
        return;
    }
    
    string line;
    while (getline(input_file, line)) {
        
        size_t current;
        size_t next = -1;
        int i = 0;
        int para_type = 0;
        do
        {
            current = next + 1;
            next = line.find_first_of( " ", current );
            if (current != next) {
                if       (i == 0 && line.substr( current, next - current).compare("maxcycle") == 0 ) {
                    para_type = 1;
                }else if (i == 0 && line.substr( current, next - current).compare("run") == 0 ) {
                    para_type = 1;
                }else if (i == 0 && line.substr( current, next - current).compare("mincycle") == 0 ) {
                    para_type = 2;
                }else if (i == 0 && line.substr( current, next - current).compare("sample") == 0 ) {
                    para_type = 3;
                }else if (i == 0 && line.substr( current, next - current).compare("solv") == 0 ) {
                    para_type = 4;
                }else if (i == 0 && line.substr( current, next - current).compare("tol") == 0 ) {
                    para_type = 5;
                }else if (i == 0 && line.substr( current, next - current).compare("docking") == 0 ) {
                    para_type = 6;
                }else if (i == 0 && line.substr( current, next - current).compare("grid_dense") == 0 ) {
                    para_type = 7;
                }else if (i == 0 && line.substr( current, next - current).compare("resolution") == 0 ) {
                    para_type = 8;
                }else if (i == 0 && line.substr( current, next - current).compare("cutoff") == 0 ) {
                    para_type = 9;
                }else if (i == 0 && line.substr( current, next - current).compare("verbose") == 0 ) {
                    para_type = 10;
                }
                
                if (i != 0 ) {
                    if      (para_type == 1) {
                        this->max_cycle = atoi(line.substr( current, next - current ).c_str());
                    }else if(para_type == 2){
                        this->min_cycle = atoi(line.substr( current, next - current ).c_str());
                    }else if(para_type == 3){
                        this->width = atoi(line.substr( current, next - current ).c_str());
                    }else if(para_type == 4){
                        if (line.substr( current, next - current ).compare("y") == 0) {
                            this->solv = true;
                        }
                    }else if(para_type == 5){
                        this->tol = atof(line.substr( current, next - current ).c_str());
                    }else if(para_type == 6){
                        if (line.substr( current, next - current ).compare("y") == 0) {
                            this->docking = true;
                        }
                    }else if(para_type == 7){
                        this->grid_dense = atof(line.substr( current, next - current ).c_str());
                    }else if(para_type == 8){
                        this->resolution = atof(line.substr( current, next - current ).c_str());
                    }else if(para_type == 9){
                        this->cutoff     = atof(line.substr( current, next - current ).c_str());
                    }else if(para_type == 10){
                        this->min_cycle = atoi(line.substr( current, next - current ).c_str());
                    }
                }
                
                i++;
            }
        }while (next != string::npos);
        
    }
    input_file.close();
}

void Input :: print(){
    cout << " ------------------ INPUT PARAMETERS ---------------" << endl;
    cout << "    MAX RUN       = " << this->max_cycle << endl;
    //cout << "    MIN RUN       = " << this->min_cycle << endl;
    cout << "    SAMPLE NUMBER = " << this->width     << endl;
    cout << "    VERBOSE LEVEL = " << this->verbose   << endl;
    if (this->solv) {
        cout << "    INCLUDE SOVLENT EFFECT"<< endl;
    }else{
        cout << "    NO SOVLENT, GAS PHASE "<< endl;
    }
    cout << "    PROTEIN-LEGAND CUTOFF = " << setw(8) << setprecision(2) << this-> cutoff << " A " << endl;
    
    if (this->docking) {
        cout << "    PROTEIN-LIGAND DOCKING"<< endl;
        cout << "       GRID DENSE = " << setw(8) << setprecision(2) << this-> grid_dense << " A " << endl;
        cout << "       RESOLUTION = " << setw(8) << setprecision(2) << this-> resolution << " A " << endl;
    }else{
        cout << "    NO DOCKING, CAL ENERGY"<< endl;
    }
    
    
    //cout << "    CONV CRITEIA  = " << this->tol       << endl;
    cout << "----------------------------------------------------" << endl;
    cout << endl;
}