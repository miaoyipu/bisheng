//
//  docking.cpp
//  BiSheng
//
//  Created by Yipu Miao on 11/13/14.
//  Copyright (c) 2014 Yipu Miao. All rights reserved.
//

#include "docking.h"

bool compare_x (XYZ* l,XYZ* r) { return (l->x < r->x); }
bool compare_y (XYZ* l,XYZ* r) { return (l->y < r->y); }
bool compare_z (XYZ* l,XYZ* r) { return (l->z < r->z); }

/*
    Construct KD tree recursively from protein coordinates. 
    For this program, K = 3, which makes it as 3-D tree
 */
KDTreeNode* KDTree :: construct_KDTree(vector<XYZ*>& coor, int s, int e, int k){
    if (s > e) {
        return NULL;
    }
    
    switch (k) {
        case 0: sort(coor.begin() + s, coor.begin() + e + 1, compare_x); break;
        case 1: sort(coor.begin() + s, coor.begin() + e + 1, compare_y); break;
        case 2: sort(coor.begin() + s, coor.begin() + e + 1, compare_z); break;
        default: break;
    }
    
    int mid = (s+e)/2;
    int next_k = (k == 2)? 0 : k+1;
    
    KDTreeNode* root = new KDTreeNode(coor[mid], k);
    root -> left  = construct_KDTree(coor, s, mid - 1, next_k);
    root -> right = construct_KDTree(coor, mid + 1, e, next_k);
    
    return root;
    
}

/*
void print_kdtree(KDTreeNode* root){
    if  (root == NULL) return;
    cout << "KD: ";
    root->xyz->print();
    cout << "left : ";
    if (root->left) {
        root->left->xyz->print();
    }
    cout << "right: ";
    if (root->right) {
        root->right->xyz->print();
    }
    cout << endl;
    print_kdtree(root->left);
    print_kdtree(root->right);
    return;
}*/


/*
    A small modification of search subroutine of standard KD tree. 
    Here pass the targe coordinates and cutoff criteria, return false immediately when found nearest point smaller than
    cutoff criteria. Return true if this targe is distant enough from other atoms
    For the principal of KD-tree, please check wikipedia.
 */

bool KDTree :: cutoff(XYZ* xyz, BSDouble cutoff_c){
    KDTreeNode* current = kdtree_root;
    
    // Using a array to simulate stack, because it is fast
    KDTreeNode* search_path[1000];
    int c = 0;
    
    // Step 1. Find the starting search point and search path (a stack) by binary-like-tree search
    //stack<KDTreeNode*> search_path;
    while (current) {
        //search_path.push(current);
        search_path[c++] = current;
        switch (current->ki) {
            case 0: current = (xyz->x < current->xyz->x) ? current->left : current->right; break;
            case 1: current = (xyz->y < current->xyz->y) ? current->left : current->right; break;
            case 2: current = (xyz->z < current->xyz->z) ? current->left : current->right; break;
            default: break;
        }
    }
    
    
    BSDouble min_dist = INT_MAX;
    
    // Step 2. Trace back to search path and determine if further search is necessary
    //while (!search_path.empty()) {
    while (c != 0) {
        
        //current = search_path.top();
        //search_path.pop();
        current = search_path[--c];
        
        BSDouble dist = current->xyz->Distance2(xyz);
        if ( dist < cutoff_c) return false;
        if ( dist < min_dist) min_dist = dist;
        
        // Determine if further search is needed
        BSDouble d = 0.0;
        switch (current->ki) {
            case 0: d = xyz->x - current->xyz->x; break;
            case 1: d = xyz->y - current->xyz->y; break;
            case 2: d = xyz->z - current->xyz->z; break;
            default: break;
        }
        
        // Further search is needed
        if (d*d < min_dist) {
            current = (d < 0)? current->right : current->left; // Go to another area
            
            while (current) {
                //search_path.push(current);
                search_path[c++] = current;
                
                switch (current->ki) {
                    case 0: current = (xyz->x < current->xyz->x) ? current->left : current->right; break;
                    case 1: current = (xyz->y < current->xyz->y) ? current->left : current->right; break;
                    case 2: current = (xyz->z < current->xyz->z) ? current->left : current->right; break;
                    default: break;
                }
            }
        }
    }
    
    return min_dist > cutoff_c;
    
}



KDTree :: KDTree(vector<ProteinAtom*>& pro_coor){
    vector<XYZ*> coor;
    for (int i = 0; i < pro_coor.size(); i++)  coor.push_back(pro_coor[i]);
    kdtree_root = construct_KDTree(coor, 0, pro_coor.size() - 1,0);
}

// Combine two grid point into one
void GridPoint :: combine(GridPoint& j){
    this->increase_val(j.get_val());
    this->add_atom_num(j.get_atom_num());
    j.set_val(0.0);
}


// print a grid point information
void GridPoint :: print(){
    cout << " (" << xyz->x << " " << xyz->y << " " << xyz->z << ")  ATOM: ";
    for(unordered_set<BSInt>::iterator it = atomNum.begin(); it != atomNum.end(); ++it) cout << *it << " ";
    cout << "val = " << this->val << " " << xyz << endl;
}

// print a heat map information
void HeatMap :: print(){
    if  (grid_points.size() == 0) return;
    cout << "  " << setw(8) << name << " POINTS   = " << grid_points.size() << endl;
}

// print a match information
void Match :: print(){
    this->grid_point->print();
    this->atom->print();
    cout << endl;
}


// Constructor for three point match. It also generate a transformation based on this three point match
ThreePointMatch :: ThreePointMatch(Match* _match1, Match* _match2, Match* _match3): match1(_match1), match2(_match2), match3(_match3){
    
    
    Plane p1(_match1->get_grid_point()->get_XYZ(), _match2->get_grid_point()->get_XYZ(), _match3->get_grid_point()->get_XYZ() );
    Plane p2(_match1->get_atom(), _match2->get_atom(), _match3->get_atom());
    transformation.setRotate(p1.norm(), -p2.angle(p1));
    
    XYZ* new_xyz1 = new XYZ(_match1->get_atom()->x, _match1->get_atom()->y, _match1->get_atom()->z);
    XYZ* new_xyz2 = new XYZ(_match2->get_atom()->x, _match2->get_atom()->y, _match2->get_atom()->z);
    XYZ* new_xyz3 = new XYZ(_match3->get_atom()->x, _match3->get_atom()->y, _match3->get_atom()->z);
    
    transformation.rotate(new_xyz1);
    transformation.rotate(new_xyz2);
    transformation.rotate(new_xyz3);
    
    BSDouble mass_of_grid_points     =  _match1->get_grid_point()-> get_val() + _match2->get_grid_point()-> get_val() + _match3->get_grid_point()-> get_val();
    BSDouble center_of_grid_points_x = (  _match1->get_grid_point()->get_XYZ()->x * _match1->get_grid_point()-> get_val()
                                        + _match2->get_grid_point()->get_XYZ()->x * _match2->get_grid_point()-> get_val()
                                        + _match3->get_grid_point()->get_XYZ()->x * _match3->get_grid_point()-> get_val()) / mass_of_grid_points;
    BSDouble center_of_grid_points_y = (  _match1->get_grid_point()->get_XYZ()->y * _match1->get_grid_point()-> get_val()
                                        + _match2->get_grid_point()->get_XYZ()->y * _match2->get_grid_point()-> get_val()
                                        + _match3->get_grid_point()->get_XYZ()->y * _match3->get_grid_point()-> get_val()) / mass_of_grid_points;
    BSDouble center_of_grid_points_z = (  _match1->get_grid_point()->get_XYZ()->z * _match1->get_grid_point()-> get_val()
                                        + _match2->get_grid_point()->get_XYZ()->z * _match2->get_grid_point()-> get_val()
                                        + _match3->get_grid_point()->get_XYZ()->z * _match3->get_grid_point()-> get_val()) / mass_of_grid_points;
    
    BSDouble center_of_atoms_x = (new_xyz1->x + new_xyz2->x + new_xyz3->x)/3;
    BSDouble center_of_atoms_y = (new_xyz1->y + new_xyz2->y + new_xyz3->y)/3;
    BSDouble center_of_atoms_z = (new_xyz1->z + new_xyz2->z + new_xyz3->z)/3;
    
    transformation.setMove(XYZ(center_of_grid_points_x - center_of_atoms_x, center_of_grid_points_y - center_of_atoms_y, center_of_grid_points_z - center_of_atoms_z));
    
    delete(new_xyz1);
    delete(new_xyz2);
    delete(new_xyz3);
    
}

// Retrieve a match from three point match
Match* ThreePointMatch :: get_match(BSInt i){
    if (i == 0){
        return this->match1;
    }else if (i == 1){
        return this->match2;
    }else if (i == 2){
        return this->match3;
    }
    
    return NULL;
}

// Do real trans formation
void ThreePointMatch :: trans(XYZ* xyz){
    transformation.rotate(xyz);
    transformation.move(xyz);
}

// print three point atom information
void ThreePointMatch :: print_atoms(){
    cout << match1->get_atom()->get_atom_num() << ", ";
    cout << match2->get_atom()->get_atom_num() << ", ";
    cout << match3->get_atom()->get_atom_num();
}

// print three point match information

void ThreePointMatch :: print(){
    cout << "match 1";
    match1->print();
    cout << "match 2";
    match2->print();
    cout << "match 3";
    match3->print();
}

// Add a three point match to the poll
void MatchSet :: add_three_point_match(ThreePointMatch* _three_point_match){
    three_point_matches.push_back(ThreePointMatch(_three_point_match->get_match(0), _three_point_match->get_match(1), _three_point_match->get_match(2) ) );
}
void MatchSet :: add_three_point_match(Match* _match1, Match* _match2, Match* _match3){
    three_point_matches.push_back(ThreePointMatch(_match1, _match2, _match3));
}
// Add an atom and a grid point as a match to match set
void MatchSet :: add(BSInt _atom_type, GridPoint* _gridpoint, LigandAtom* _atom){
    matches.push_back(Match(_atom_type, _gridpoint, _atom));
}

// print match set
void MatchSet :: print(){
    cout << " " << setw(8) << " POINT TO ATOM MATCHES = " << matches.size() << endl;
    cout << " " << setw(8) << " THREE POINTS COMBINATION = " << three_point_matches.size() << endl;
}

/*
    Manual Hashing table, use own version rather than STL for fast execution
    include subroutine: hashing and unhashing (unhashing x, y, z)
 */

BSDouble Docking::unhashing_x(BSLong h){
    return (h/(dim_z * dim_y)) * grid_dense + min_xyz.x;
}


BSDouble Docking::unhashing_y(BSLong h){
    return ((h/dim_z)%dim_y) * grid_dense + min_xyz.y;
}


BSDouble Docking::unhashing_z(BSLong h){
    return min_xyz.z + h%dim_z * grid_dense;
}

BSLong Docking :: hashing(BSDouble x, BSDouble y, BSDouble z){
    int xx = (x - min_xyz.x)/grid_dense;
    int yy = (y - min_xyz.y)/grid_dense;
    int zz = (z - min_xyz.z)/grid_dense;
    return zz + ((yy) + (xx) * dim_y) * dim_z;
}

BSLong Docking :: hashing(XYZ* xyz){
    int xx = (xyz->x - min_xyz.x)/grid_dense;
    int yy = (xyz->y - min_xyz.y)/grid_dense;
    int zz = (xyz->z - min_xyz.z)/grid_dense;
    return zz + ((yy) + (xx) * dim_y) * dim_z;
}


BSLong Docking :: hashing(XYZ& xyz){
    int xx = (xyz.x - min_xyz.x)/grid_dense;
    int yy = (xyz.y - min_xyz.y)/grid_dense;
    int zz = (xyz.z - min_xyz.z)/grid_dense;
    return zz + ((yy) + (xx) * dim_y) * dim_z;
}

// Generate a set of grid point by giving a center(current) and hole radiu
void Docking :: generate_point_set(XYZ* current, vector<BSLong>& grid_point_set, BSDouble min_hole_size, BSDouble max_hole_size){
    
    for (BSDouble x = - max_hole_size; x <  max_hole_size; x += this->grid_dense) {
        for (BSDouble y = - max_hole_size; y <  max_hole_size; y += this->grid_dense) {
            for (BSDouble z = - max_hole_size; z <  max_hole_size; z += this->grid_dense) {
                BSDouble dist2 = x * x + y * y + z * z;
                if ( dist2 >= min_hole_size * min_hole_size && dist2 <= max_hole_size * max_hole_size ) {
                    BSLong hashing_value = hashing(current->x + x, current->y + y, current->z + z);
                    if (grid_map[hashing_value] == NULL) {
                        grid_map[hashing_value] = new XYZ(unhashing_x(hashing_value), unhashing_y(hashing_value), unhashing_z(hashing_value));
                    }
                    
                    grid_point_set.push_back(hashing_value);
                }
            }
        }
    }
}


// Filt candidate point set and append them to target_point_set
void Docking :: find_grid_point(Atom* current, vector<BSLong>& candidate_point_set, BSInt col, HeatMap& target_point_set){
    
    
    bool is_close = (current_distance <= (vdw_max[col].first) * (vdw_max[col].first));
    
    BSDouble l = vdw_max[col].first;
    BSDouble v = vdw_max[col].second;
    
    BSDouble min_distance2 = target_point_set.get_min_distance() * target_point_set.get_min_distance();
    
    for (int i = 0; i < candidate_point_set.size(); i++) {
        
        XYZ* candidate_xyz = grid_map[candidate_point_set[i]];
        if (!candidate_xyz) continue;
        
        BSDouble dist2 = candidate_xyz->Distance2(current);
        if (abs( sqrt(dist2) -  l ) <= resolution) {
            if ((is_close || ((dist2 + current_distance) > candidate_xyz->Distance2(protein_center_of_mass))) ) {
                if (kdtree.cutoff(candidate_xyz, min_distance2)) {
                    target_point_set.grid_points.push_back(GridPoint(current->get_atom_num(), v, candidate_xyz));
                }
            }
        }
        
    }
}


// Combine grid points with the same position but belongs to different atoms into one grid point
BSDouble Docking :: fold_point_set(HeatMap& grid_point_set){
    
    unordered_map<XYZ*, BSInt> mp;
    
    for (int i = 0; i < grid_point_set.size(); i++) {
        if (mp.find(grid_point_set.grid_points[i].get_XYZ()) == mp.end()) {
            mp[grid_point_set.grid_points[i].get_XYZ()] = i;
        }else{
            grid_point_set.grid_points[ mp[grid_point_set.grid_points[i].get_XYZ()] ].combine(grid_point_set.grid_points[i]);
        }
    }
    
    unordered_map<BSInt, BSDouble> max_v;
    unordered_map<BSInt, BSDouble> min_v;
    
    for (int i = 0; i < grid_point_set.size(); i++) {
        for (unordered_set<BSInt>::iterator it = grid_point_set.grid_points[i].get_atom_num().begin(); it != grid_point_set.grid_points[i].get_atom_num().end(); ++it){
            if (max_v.find(*it) == max_v.end()) {
                max_v[*it] = grid_point_set.grid_points[i].get_val();
                min_v[*it] = grid_point_set.grid_points[i].get_val();
            }else{
                max_v[*it] = max(max_v[*it], grid_point_set.grid_points[i].get_val());
                min_v[*it] = min(min_v[*it], grid_point_set.grid_points[i].get_val());
            }
        }
    }
    
    vector<GridPoint> new_set;
    for (int i = 0; i < grid_point_set.size(); i++) {
        for (unordered_set<BSInt>::iterator it = grid_point_set.grid_points[i].get_atom_num().begin(); it != grid_point_set.grid_points[i].get_atom_num().end(); ++it){
            //if (grid_point_set.grid_points[i].get_val() == max_v[*it] && grid_point_set.grid_points[i].get_val() != 0.0 && max_v[*it] - min_v[*it] > criteria) {
            if (grid_point_set.grid_points[i].get_val() > MAX( min_v[*it], max_v[*it] - criteria) && grid_point_set.grid_points[i].get_val() != 0.0) {
                new_set.push_back(GridPoint(*it, grid_point_set.grid_points[i].get_val(), grid_point_set.grid_points[i].get_XYZ()));
            }
        }
    }
    grid_point_set.grid_points = new_set;
    return grid_point_set.size();
}

// find atom-gridpoint match. If a ligand atom is close to a grid point that favor to the same atom type, then we call it a match.
// Meanwhile, after find a match, we select three of them, with no duplicated grid point and atoms, as a three-point-match.
void Docking :: find_matches(Ligand& ligand, MatchSet& match){
    for (int i = 0; i < ligand.atoms.size(); i++) {
        
        // Some heat map is with H-Bond and some are not, but we treat them the same.
        HeatMap* current_heatmap = NULL;
        HeatMap* current_heatmap_HB = NULL;
        
        switch (ligand.get_atom(i)->getAtomType()) {
            case O_2:
                current_heatmap    = &grid_point_O_2;
                current_heatmap_HB = &grid_point_O_2_HB;
                break;
            case O_3:
                current_heatmap    = &grid_point_O_3;
                current_heatmap_HB = &grid_point_O_3_HB;
                break;
            case C_AR:
                current_heatmap    = &grid_point_C_AR;
                break;
            case N_AM:
                current_heatmap    = &grid_point_N_AM;
                current_heatmap_HB = &grid_point_N_AM_HB;
                break;
            case N_PL3:
                current_heatmap    = &grid_point_N_PL_3;
                current_heatmap_HB = &grid_point_N_PL_3_HB;
                break;
            case N_4:
                current_heatmap    = &grid_point_N_4;
                break;
            default:
                break;
        }
        
        unordered_set<XYZ*> mp; // store matched xyz point to avoid duplication.
        
        if (current_heatmap) {
            for (int j = 0; j < current_heatmap->size(); j++) {
                if (current_heatmap->get_grid_point(j)->get_XYZ()->Distance2(ligand.get_atom(i)) < docking_distance2){
                    if (mp.find(current_heatmap->get_grid_point(j)->get_XYZ()) == mp.end()) {
                        match.add(current_heatmap->get_atom_type(), current_heatmap->get_grid_point(j), ligand.get_atom(i));
                        mp.insert(current_heatmap->get_grid_point(j)->get_XYZ());
                    }
                }
            }
        }
        
        if (current_heatmap_HB) {
            for (int j = 0; j < current_heatmap_HB->size(); j++) {
                if (current_heatmap_HB->get_grid_point(j)->get_XYZ()->Distance2(ligand.get_atom(i)) < docking_distance2){
                    if (mp.find(current_heatmap_HB->get_grid_point(j)->get_XYZ()) == mp.end()) {
                        match.add(current_heatmap_HB->get_atom_type(),current_heatmap_HB->get_grid_point(j), ligand.get_atom(i));
                        mp.insert(current_heatmap_HB->get_grid_point(j)->get_XYZ());
                    }
                }
            }
        }
        
    }
    
    // Go throughout all the matches and find three point matches.
    for (int i = 0; i < match.size() - 2; i ++){
        Match* match1 = match.get_match(i);
        
        for (int j = i + 1; j < match.size() - 1; j ++){
            Match* match2 = match.get_match(j);
            
            if (match1->get_atom() == match2->get_atom() ||
                match1->get_grid_point()->get_XYZ() == match2->get_grid_point()->get_XYZ()) continue;
            
            for(int k = j + 1; k < match.size(); k++){
                Match* match3 = match.get_match(k);
                if (match1->get_atom() == match3->get_atom() ||
                    match2->get_atom() == match3->get_atom() ||
                    match1->get_grid_point()->get_XYZ() == match3->get_grid_point()->get_XYZ() ||
                    match2->get_grid_point()->get_XYZ() == match3->get_grid_point()->get_XYZ()) continue;
                match.add_three_point_match(match1, match2, match3);
            }
        }
    }
    
    
    
}

/*
    Docking:
 
        Docking includes several stages in this program. 
        1.  The first part is to find heat-map
        2.  then it needs to find out heat-map and atom match, and according to these matches, we can find 3 points matches
        3.  With 3 points matches, we first do a rough calculation for new conformal and select some possible candidate
        4.  A fine calculation with these candidates.
 */

Docking :: Docking(Protein& protein, Ligand& ligand, Input& input):
    grid_dense(input.getGridDense()), max_hole(4.8), criteria(1.0), resolution(input.getResolution()), number_of_points(0),
    kdtree(protein.get_all_atom()),                       // Construct KD tree using protein atom coordinates
    docking_distance (0.50000),
    docking_threshold(0.0500),
    grid_point_Br       ("BR",      BR,     3.2),
    grid_point_Br_HB    ("BR HB",   BR_HB,  3.2),
    grid_point_C_1      ("C1",      C_1,    3.2),
    grid_point_C_2      ("C2",      C_2,    3.2),
    grid_point_C_3      ("C3",      C_3,    3.2),
    grid_point_C_AR     ("C AR",    C_AR,   3.2),
    grid_point_Cl       ("Cl",      CL,     3.2),
    grid_point_Cl_HB    ("Cl HB",   CL_HB,  3.2),
    grid_point_F        ("F",       F,      2.9),
    grid_point_F_HB     ("F HB",    F_HB,   2.9),
    grid_point_I        ("I",       I,      3.2),
    grid_point_N_2      ("N2 ",     N_2,    2.5),
    grid_point_N_4      ("N4",      N_4,    2.5),
    grid_point_N_AM     ("N AM",    N_AM,   2.5),
    grid_point_N_AM_HB  ("N AM HB", N_AM_HB,2.5),
    grid_point_N_AR     ("N AR",    N_AR,   2.5),
    grid_point_N_PL_3   ("N PL3",   N_PL3,  2.5),
    grid_point_N_PL_3_HB("N PL3 HB",N_PL3_HB,2.5),
    grid_point_O_2      ("O2",      O_2,    2.5),
    grid_point_O_2_HB   ("O2 HB",   O_2_HB, 2.5),
    grid_point_O_3      ("O3",      O_3,    2.5),
    grid_point_O_3_HB   ("O3 HB",   O_3_HB, 2.5),
    grid_point_S        ("S",       S,      3.2),
    grid_point_S_HB     ("S HB",    S_HB,   3.3),
    match_set()
{
    clock_t start_time, end_time, start_time_now;
    
    start_time = clock();
    
    protein_center_of_mass = protein.get_center_of_mass();
    
    min_xyz = protein.get_min_xyz();
    max_xyz = protein.get_max_xyz();
    
    min_xyz.move(-max_hole, -max_hole, -max_hole);
    max_xyz.move( max_hole,  max_hole,  max_hole);
    
    dim_x = (max_xyz.x - min_xyz.x)/grid_dense + 1;
    dim_y = (max_xyz.y - min_xyz.y)/grid_dense + 1;
    dim_z = (max_xyz.z - min_xyz.z)/grid_dense + 1;
    
    grid_map.reserve(dim_x*dim_y*dim_z);
    
    
#ifdef DEBUG
    cout << "GRID DIM " << dim_x << " " << dim_y << " " << dim_z << endl;
#endif
    
    number_of_points = 0;
    
    
    // Pre-process datebase information
    for (int i = 0; i < VDW_DOCKING_D_W; i++) {
        int index = (max_dist[i]-vdw_docking_d[0]) / (2*SEARCH_CRITERIA);
        vdw_max[i] = pair<BSDouble, BSDouble>(max_dist[i], LOC2(vdw_docking_d, index, i, VDW_DOCKING_D_H, VDW_DOCKING_D_W) );
    }
    
    /*
     ====================================
     1. Heat Map
     ====================================
        The first calculation is to calculate heat map from protein information for ligand to docking.
        This calculation is carreried for the whole space that presented by a large collection of 3-D grid points distrubuted around protein. For these points,
        Each atom in protein given a favored position for certein kind of atom type to be located. By gather all favored positions, we can conclude that at some
        grid point, a type of atom type is strongly favored because of protein atom around it. Here heat map is to calculated how favoer it is to locate an atom
        type at this position
     
     */
    for (int i = 0; i < protein.get_effect_atom_size(); i++){
        
        ProteinAtom* current = protein.get_atom(i);
        current_distance = current->Distance2(protein_center_of_mass);
        
        if (current->getName() == C_3) {
            vector<BSLong> grid_point_set;
            generate_point_set(current, grid_point_set, 4.255, 4.880); //4.78, 4.355
            
            find_grid_point(current, grid_point_set, 1, grid_point_C_3);
            find_grid_point(current, grid_point_set, 2, grid_point_C_2);
            find_grid_point(current, grid_point_set, 3, grid_point_C_AR);
            
        }else if ( current->getName() == C_2){
            vector<BSLong> grid_point_set;
            generate_point_set(current, grid_point_set, 4.505, 4.825); // 4.725, 4.605
            find_grid_point(current, grid_point_set, 2,  grid_point_C_3);
            find_grid_point(current, grid_point_set, 22, grid_point_C_AR);
            find_grid_point(current, grid_point_set, 21, grid_point_C_2);
            
        }else if ( current->getName() == C_AR){
            vector<BSLong> grid_point_set;
            generate_point_set(current, grid_point_set, 4.505, 4.880); // 4.78, 4.605
            find_grid_point(current, grid_point_set, 3,  grid_point_C_3);
            find_grid_point(current, grid_point_set, 22, grid_point_C_2);
            find_grid_point(current, grid_point_set, 32, grid_point_C_AR);
            
        }else if ( current->getName() == N_AM  ||
                  (current->getName() == N_3   && current->getHBD())){
            vector<BSLong> grid_point_set;
            generate_point_set(current, grid_point_set, 2.82, 4.13); // 4.03 2.92
            
            find_grid_point(current, grid_point_set, 67, grid_point_F);
            find_grid_point(current, grid_point_set, 68, grid_point_Cl);
            find_grid_point(current, grid_point_set, 69, grid_point_Br);
            find_grid_point(current, grid_point_set, 70, grid_point_I);
            find_grid_point(current, grid_point_set, 76, grid_point_S_HB);
            find_grid_point(current, grid_point_set, 77, grid_point_O_3_HB);
            find_grid_point(current, grid_point_set, 78, grid_point_O_2_HB);
            
        }else if ( current->getName() == N_PL3 && current->getHBD()){
            vector<BSLong> grid_point_set;
            generate_point_set(current, grid_point_set, 2.775, 4.135); // 4.035 2.875
            
            find_grid_point(current, grid_point_set, 67, grid_point_F);
            find_grid_point(current, grid_point_set, 68, grid_point_Cl);
            find_grid_point(current, grid_point_set, 69, grid_point_Br);
            find_grid_point(current, grid_point_set, 70, grid_point_I);
            find_grid_point(current, grid_point_set, 81, grid_point_O_3_HB);
            find_grid_point(current, grid_point_set, 82, grid_point_O_2_HB);
            find_grid_point(current, grid_point_set, 83, grid_point_S_HB);
            
        }else if ( current->getName() == O_2){
            vector<BSLong> grid_point_set;
            generate_point_set(current, grid_point_set, 2.580, 3.725); // 3.625, 2.68
            
            find_grid_point(current, grid_point_set, 48, grid_point_O_3_HB);
            find_grid_point(current, grid_point_set, 50, grid_point_F);
            find_grid_point(current, grid_point_set, 51, grid_point_Cl);
            find_grid_point(current, grid_point_set, 52, grid_point_Br);
            find_grid_point(current, grid_point_set, 60, grid_point_N_4);
            find_grid_point(current, grid_point_set, 78, grid_point_N_AM_HB);
            find_grid_point(current, grid_point_set, 82, grid_point_N_PL_3_HB);
            
        }else if ( current->getName() == O_3){
            vector<BSLong> grid_point_set;
            generate_point_set(current, grid_point_set, 2.385, 3.965); // 3.865, 2.485
            
            find_grid_point(current, grid_point_set, 46, grid_point_S);
            find_grid_point(current, grid_point_set, 47, grid_point_O_3_HB);
            find_grid_point(current, grid_point_set, 48, grid_point_O_2_HB);
            find_grid_point(current, grid_point_set, 50, grid_point_F);
            find_grid_point(current, grid_point_set, 51, grid_point_Cl);
            find_grid_point(current, grid_point_set, 52, grid_point_Br);
            find_grid_point(current, grid_point_set, 77, grid_point_N_AM_HB);
            find_grid_point(current, grid_point_set, 84, grid_point_N_4);
            find_grid_point(current, grid_point_set, 81, grid_point_N_PL_3_HB);
            
        }else if ( current->getName() == N_4){
            vector<BSLong> grid_point_set;
            generate_point_set(current, grid_point_set, 2.730, 4.130); // 4.03, 2.83
            
            find_grid_point(current, grid_point_set, 67, grid_point_F);
            find_grid_point(current, grid_point_set, 69, grid_point_Br);
            find_grid_point(current, grid_point_set, 68, grid_point_Cl);
            find_grid_point(current, grid_point_set, 76, grid_point_S);
            find_grid_point(current, grid_point_set, 84, grid_point_O_3_HB);
            find_grid_point(current, grid_point_set, 85, grid_point_O_2_HB);
        }
    }
    
    
    number_of_points += fold_point_set(grid_point_Br);
    number_of_points += fold_point_set(grid_point_Br_HB);
    number_of_points += fold_point_set(grid_point_C_1);
    number_of_points += fold_point_set(grid_point_C_2);
    number_of_points += fold_point_set(grid_point_C_3);
    number_of_points += fold_point_set(grid_point_C_AR);
    number_of_points += fold_point_set(grid_point_Cl);
    number_of_points += fold_point_set(grid_point_Cl_HB);
    number_of_points += fold_point_set(grid_point_F);
    number_of_points += fold_point_set(grid_point_F_HB);
    number_of_points += fold_point_set(grid_point_I);
    number_of_points += fold_point_set(grid_point_N_2);
    number_of_points += fold_point_set(grid_point_N_4);
    number_of_points += fold_point_set(grid_point_N_AM);
    number_of_points += fold_point_set(grid_point_N_AM_HB);
    number_of_points += fold_point_set(grid_point_N_AR);
    number_of_points += fold_point_set(grid_point_N_PL_3);
    number_of_points += fold_point_set(grid_point_N_PL_3_HB);
    number_of_points += fold_point_set(grid_point_O_2);
    number_of_points += fold_point_set(grid_point_O_2_HB);
    number_of_points += fold_point_set(grid_point_O_3);
    number_of_points += fold_point_set(grid_point_O_3_HB);
    number_of_points += fold_point_set(grid_point_S);
    number_of_points += fold_point_set(grid_point_S_HB);

    cout << endl;
    cout << " --------------------- " << endl;
    cout << " HEAT MAP CALCULATION " << endl;
    cout << " --------------------- " << endl;
    
    grid_point_Br.print();
    grid_point_Br_HB.print();
    grid_point_C_1.print();
    grid_point_C_2.print();
    grid_point_C_3.print();
    grid_point_C_AR.print();
    grid_point_Cl.print();
    grid_point_Cl_HB.print();
    grid_point_F.print();
    grid_point_F_HB.print();
    grid_point_I.print();
    grid_point_N_2.print();
    grid_point_N_4.print();
    grid_point_N_AM.print();
    grid_point_N_AM_HB.print();
    grid_point_N_AR.print();
    grid_point_N_PL_3.print();
    grid_point_N_PL_3_HB.print();
    grid_point_O_2.print();
    grid_point_O_2_HB.print();
    grid_point_O_3.print();
    grid_point_O_3_HB.print();
    grid_point_S.print();
    grid_point_S_HB.print();
    
    cout << " --------------------- " << endl;
    cout << "   TOTAL POINTS   = "   << number_of_points << endl;
    
    
    end_time = clock();
    cout << endl << "    TIME TO FIND HEAT POINTS = " << setw(8) << setprecision(2) << (end_time - start_time) / (double) CLOCKS_PER_SEC << " S" << endl << endl;
    
    start_time_now = clock();
    
    /*
    ====================================
    2. Find matches
    ====================================
        By giving ligand information and heat map information, we can begin to match ligand atom and heat map grid. If a heat map grid point saying centein kind of 
        atom type is favored to located at its position and its near by (can be set as docking distance, 0.5 A for default), there is an atom happend to located. Then we
        can match them. 
     
        Moreover, if three matches, we form a transformation from three atoms to three grid points. We will introduce transformation later.
    */
    
    
    cout << endl;
    cout << " --------------------- " << endl;
    cout << " DOCKING CALCULATION " << endl;
    cout << " --------------------- " << endl;
    docking_distance2 = docking_distance * docking_distance;
    
    find_matches(ligand, match_set); // find match do 2 things, find atom-gridpoint match and find 3 point matches.
    match_set.print();
    
    
    end_time = clock();
    cout << endl << "    TIME TO FIND MATCHES = " << setw(8) << setprecision(2) << (end_time - start_time_now) / (double) CLOCKS_PER_SEC << " S" << endl;
    
    /*
     ====================================
     3. Rough Filter
     ====================================
        First, we calculate original conformal. Then do a transformation for a ligand based on a three-point matches. then calculate new free energy, but a rough calculation
        We go through all three-point matches, and find out possible candidate. Because it is a rough calculation, with 1/10 sample size of original argument, so can not garentee
        candidate is positive, but can garentee eliminiated candiates are negetive (like bloom filter). 
        The filter is to eliminate conformals with free energy calculated even larger than original one by 2*docking_threshold more.
    */
    
    // Do a fine calculation for original conformal
    cout << endl << " ORIGINAL CONFORMAL CALCULATION" << endl;
    Energy  energy (protein, ligand, input);
    BSDouble old_energy = energy.get_dG();
    
    
    // Begin a rough calculation for all candidate with 1/10 sample size
    start_time_now = clock();
    cout << endl << " NEW CONFORMAL CALCULATION" << endl;
    cout << " ( Old dG = " << setw(18) << setprecision(10) << old_energy << " kcal/mol) " << endl;
    cout << " DO A ROUGH SEARCH FIRST. SET SAMPLE SIZE AS 1/10 OF REQUESTED     "  << endl;
    cout << " ----------------------------------------------------------------- "  << endl;
    cout << "  GOOD?   dG(kcal/mol)             delta      ligand docking atoms "  << endl;
    cout << " ----------------------------------------------------------------- "  << endl;
    
    // Set verbose equals 0 to avoid mass output
    BSInt saved_verbose = input.getVerbose();
    input.setVerbose(0);
    // Set small size size for rough search
    BSInt saved_run = input.getMaxCycle();
    input.setRun(saved_run/10);
    
    // Calculate all possible conformations.
    for (int i = 0; i < match_set.three_point_match_size(); i++) {
        
        // Trans the whole ligand with current three point match
        ligand.trans(match_set.get_three_point_match(i)->get_transformation());
        
        
        // Calculate new free energy with new conformal
        Energy  new_energy (protein, ligand, input);
        
        // A Bloom Filter, eliminate very negative candidate and store possible candidates to found_match_set
        // eliminate conformal with free energy value is larger than original by 2 * docking_threshold
        if (new_energy.get_dG() < (old_energy + docking_threshold * 2)) {
            cout << "   * ";
            cout << setw(18) << setprecision(10) << new_energy.get_dG() <<  "      ";
            cout << setw(15) << setprecision(10) << right<< new_energy.get_dG() - old_energy  << "      ";
            match_set.get_three_point_match(i)->print_atoms();
            cout << endl;
            found_match_set.add_three_point_match(match_set.get_three_point_match(i) );
        }else{
            cout << "     ";
            cout << setw(18) << setprecision(10) << new_energy.get_dG() <<  "      ";
            cout << setw(15) << setprecision(10) << right<< new_energy.get_dG() - old_energy  << "      " << endl;
        }
        
        
        // Recover original ligand atom position
        ligand.recover_xyz();
        
    }
    
    cout << " ----------------------------------------------------------------- "  << endl;
    
    if (found_match_set.three_point_match_size() == 0) {
        cout << " COULD NOT FOUND ANY CANDIDATES " << endl;
    }else{
        cout << " FOUND " << found_match_set.three_point_match_size() << " POSSIBLE CANDIDATES " << endl;
    }
    
    
    end_time = clock();
    
    cout << endl << "    TIME TO CALCULATE NEW CONFORMAL = " << setw(8) << setprecision(2) << (end_time - start_time_now) / (double) CLOCKS_PER_SEC << " S ( AVERAGE ";
    cout <<  (end_time - start_time_now) / (double) CLOCKS_PER_SEC / match_set.three_point_match_size() << " S PER CONFORMAL) " << endl;
    
    
    /*
     ====================================
     4. A fine calculation for all candidate
     ====================================
        similar calculation, but with fine sample size.
    */
    
    // Restore sample size.
    input.setRun(saved_run);
    
    
    if (found_match_set.three_point_match_size() != 0) {
        
        BSDouble min_energy = old_energy;
        
        start_time_now = clock();
        cout << endl << " REDO NEW CONFORMAL CALCULATION WITH FINE SAMPLE SIZE" << endl;
        cout << " ( Old dG = " << setw(18) << setprecision(10) << old_energy << " kcal/mol) " << endl;
        cout << " ----------------------------------------------------------------- "  << endl;
        cout << "          dG(kcal/mol)             delta      ligand docking atoms "  << endl;
        cout << " ----------------------------------------------------------------- "  << endl;
        
        // Calculate all possible conformations.
        for (int i = 0; i < found_match_set.three_point_match_size(); i++) {
            
            // trans the whole ligand with current three point match
            ligand.trans(match_set.get_three_point_match(i)->get_transformation());
            
            Energy  new_energy (protein, ligand, input);
            
            // Conformal must be fit into protein with free energy is smaller than original one by docking_threshold
            // Good conformal is stored into final_match_set.
            if (new_energy.get_dG() < old_energy - docking_threshold) {
                cout << "   * ";
                cout << setw(18) << setprecision(10) << new_energy.get_dG() << "      ";
                cout << setw(15) << setprecision(10) << right << new_energy.get_dG() - old_energy  << "      ";
                found_match_set.get_three_point_match(i)->print_atoms();
                cout << endl;
                final_match_set.add_three_point_match(match_set.get_three_point_match(i) );
                min_energy = MIN(new_energy.get_dG(), min_energy);
            }else{
                cout << "     ";
                cout << setw(18) << setprecision(10) << new_energy.get_dG() << "      ";
                cout << setw(15) << setprecision(10) << right << new_energy.get_dG() - old_energy << endl;
            }
            
            ligand.recover_xyz();
        }
        
        cout << " ----------------------------------------------------------------- "  << endl;
        end_time = clock();
        
        if (final_match_set.three_point_match_size() == 0) {
            cout << " COULD NOT FOUND ANY FINAL CANDIDATES " << endl;
        }else{
            cout << " FOUND " << final_match_set.three_point_match_size() << " FINAL CANDIDATES " << endl;
            cout << " LOWEST FREE EENERGY = " << setprecision(10) << min_energy << " kcal/mol ( delta = " << min_energy - old_energy << " ) " << endl;
        }
        
        cout << endl << "    TIME TO CALCULATE NEW CONFORMAL = " << setw(8) << setprecision(2) << (end_time - start_time_now) / (double) CLOCKS_PER_SEC << " S ( AVERAGE ";
        cout <<  (end_time - start_time_now) / (double) CLOCKS_PER_SEC / found_match_set.three_point_match_size() << " S PER CONFORMAL) " << endl;
        
    }
    // Restore original verbosity.
    input.setVerbose(saved_verbose);
    
    cout << endl << "    OVERALL TIME TO CALCULATE DOCKING = " << setw(8) << setprecision(2) << (end_time - start_time) / (double) CLOCKS_PER_SEC << " S" << endl;
    
    
    
}