//
//  graph.h
//  BiSheng
//
//  Created by Yipu Miao on 12/16/14.
//  Copyright (c) 2014 Yipu Miao. All rights reserved.
//

#ifndef __BiSheng__graph__
#define __BiSheng__graph__

#include <stdio.h>
#include "bisheng_common.h"

template <typename T> class Graph;

template <typename T>
class Cycle{
    BSInt            cycle_num;
    unordered_set<T> cycle_atom;
    friend class Graph<T>;
};


template <typename T>
class Graph {
    unordered_map<T, unordered_set<T> > graph;
    vector<Cycle<T> > cycle;
    
    
    // DFS version of detect cycle subroutine
    void findCycleUtil(T start, T current, unordered_set<T>& visited, BSInt& cyc_num){
        
        visited.insert(current);
        
        for(auto it = graph[current].begin(); it != graph[current].end(); it++){
            if (*it == start && visited.size() != 2) {
                cyc_num = visited.size();
                break;
            }
            if (visited.find(*it) != visited.end()) continue;
            findCycleUtil(start, *it, visited, cyc_num);
        }
        
        visited.erase(current);
        
        return;
    }
    
public:
    Graph(){};
    bool addNode(T node){
        graph.insert(make_pair(node, unordered_set<T>() ));
        return true;
    }
    
    bool connect(T node1, T node2){
        graph[node1].insert(node2);
        graph[node2].insert(node1);
        return true;
    }
    
    BSInt findCycle(T node){
        unordered_set<T> visited;
        int cyc_num = 0;
        findCycleUtil(node, node, visited, cyc_num);
        return cyc_num;
    }
    
    unordered_set<T>& getConnect(T node)    { return graph[node]; };
    BSInt             getConnectNum(T node) { return graph[node].size(); };
    
};

#endif /* defined(__BiSheng__graph__) */
