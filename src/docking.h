//
//  docking.h
//  BiSheng
//
//  Created by Yipu Miao on 11/13/14.
//  Copyright (c) 2014 Yipu Miao. All rights reserved.
//

#ifndef __BiSheng__docking__
#define __BiSheng__docking__

#include <stdio.h>
#include <iostream>
#include "bisheng_common.h"
#include "protein.h"
#include "energy.h"

#endif /* defined(__BiSheng__docking__) */


static BSDouble max_dist[] = { 2.000, 4.355, 4.675, 4.780, 4.095, 3.690, 3.660, 3.660, 4.645, 3.885,
                               3.785, 4.970, 3.910, 3.940, 4.750, 3.630, 4.045, 3.810, 3.810, 4.030,
                               4.150, 4.725, 4.605, 3.605, 3.795, 3.650, 3.980, 3.990, 3.935, 3.815,
                               3.730, 4.760, 4.615, 3.740, 3.770, 3.560, 3.905, 4.480, 3.905, 3.950,
                               4.860, 4.290, 4.115, 2.800, 4.795, 2.885, 3.865, 2.485, 2.680, 2.640,
                               3.440, 3.625, 3.625, 3.625, 3.705, 3.545, 3.710, 3.885, 4.745, 4.360,
                               2.830, 3.590, 3.415, 4.690, 3.285, 2.955, 3.880, 3.245, 3.770, 3.830,
                               3.800, 2.980, 4.350, 4.685, 4.695, 3.990, 4.030, 2.930, 2.920, 2.880,
                               3.675, 2.875, 2.875, 4.035, 2.935, 2.830, 2.790};

/*
 KD Tree class
    KD tree, here is 3-D tree is a date structur designed to determine the minimum distance from
    one point to a N-point in K dimension, here is 3-Dimension, space. The complexity is reduce 
    from O(N) to O(lgN) for one query.
    KD Tree is based on a binary three, with left, and right member, but also has parent information,
    additionally, a ki member, which specified as current dimension (0-2 in this case) this node split.
    For more information, please read more from WIKI or GOOGLE.
 */
class KDTreeNode{
public:
    int     ki;         // Dimension
    XYZ*    xyz;        // coordinate
    bool    is_left;    // if this node is left child of parent.
    KDTreeNode* left;
    KDTreeNode* right;
    KDTreeNode* parent;
    KDTreeNode(XYZ* _xyz, int _k): ki(_k), xyz(_xyz){}
};

class KDTree{
    KDTreeNode* kdtree_root;
    KDTreeNode* construct_KDTree(vector<XYZ*>& coor, int s, int e, int k);
public:
    KDTree(vector<ProteinAtom*>& coor);
    
    //BSDouble search(XYZ* xyz);
    bool        cutoff(XYZ* xyz, BSDouble cutoff_c);
};




/*
 Grid Point class
    is a class that point to a grid point and also marked with corresponding atoms and
    with a intensity val, the larger the value is, the more possible that the element, such as C2, Cl,
    can be placed on that point
 */

class GridPoint{
    unordered_set<BSInt> atomNum;   // Corresponding atoms, use hashing set to store for fast access
    BSDouble             val;       // Value
    XYZ*                 xyz;       // a grid point coordinate, for fast access, use a xyz point rather than xyz object
    
    void add_atom_num(BSInt _atomNum)                 { this->atomNum.insert(_atomNum); }
    void add_atom_num(unordered_set<BSInt>& _atomNum) {for(unordered_set<BSInt>::iterator it = _atomNum.begin(); it != _atomNum.end(); ++it) this->atomNum.insert(*it);}
    void increase_val(BSDouble _val)                  { this->val += _val;}
    
public:
    
    // Default constructor
    GridPoint(BSInt _atomNum, BSDouble _val, XYZ* _xyz) : val(_val), xyz(_xyz){
        this->atomNum.insert(_atomNum);
    };
    
    // Gettor and Settor
    XYZ*                    get_XYZ()                  { return this->xyz;}
    unordered_set<BSInt>&   get_atom_num()             { return this-> atomNum; }
    BSDouble                get_val()                  { return this->val;}
    void                    set_val(BSDouble _val)     { this->val = _val; }
    void                    combine(GridPoint& j);     // If two points are close, combine two points into one and mask the other point
    void                    print();
    
};

/*
    Heat map class
        is a collection of gridpoints, which indicates that for an atom_type, it will be more favor to place this
        atom_type to these gridpoints, but gridpoints has a value fiele, which suggest the extent of the favor.
 */
class HeatMap{
    string              name;
    BSInt               atom_type;
    BSDouble            min_distance;
    vector<GridPoint>   grid_points;
    friend class Docking;
public:
    
    HeatMap(string _name, BSInt _atom_type, BSDouble _min_distance) : name(_name), atom_type(_atom_type), min_distance(_min_distance){};
    BSDouble    get_min_distance()      { return min_distance;      }
    int         size()                  { return grid_points.size();}
    BSInt       get_atom_type()         { return atom_type;         }
    GridPoint*  get_grid_point(BSInt i) { return &(grid_points[i]); }
    void        print();
};


/*
    Match
        represent match between an atom (from ligand) and a grid point, with the same atom type
 */

class Match{
    BSInt               atom_type;
    GridPoint*          grid_point;
    LigandAtom*    atom;
public:
    Match(BSInt _atom_type, GridPoint* _grid_point, LigandAtom* _atom) : atom_type(_atom_type), grid_point(_grid_point), atom(_atom){};
    
    GridPoint*          get_grid_point(){ return this->grid_point;  }
    LigandAtom*         get_atom()      { return this->atom;        }
    void                print();
};

/*
    ThreePointMatch
        is a class that wrap three match, and these three matches has no duplicated atom, duplicated grid points. Then giving a three-point-match,
        we form a transformation. A transformation has move and rotation part, rotation part is to make atoms into the same plane with three grid points,
        and move part is to move the center of three atoms to the value center of three grid points. (But need to rotate first then move)
 */
class ThreePointMatch{
    Match*          match1;
    Match*          match2;
    Match*          match3;
    Transformation  transformation;
public:
    ThreePointMatch(Match* _match1, Match* _match2, Match* _match3);
    
    // Return a match.
    Match*      get_match(BSInt i);
    Transformation& get_transformation() {return this->transformation; }
    void        trans(XYZ* xyz); // do a real transformation to a xyz. Remember this will ruin original xyz, so be sure to back up.
    void        print_atoms();
    void        print();
};


/*
    MatchSet
        represent a set of match between an atom (from ligand) and a grid point, with the same atom type.
        and  a set of three point match
 */

class MatchSet{
    vector<Match>               matches;
    vector<ThreePointMatch>     three_point_matches;
    
public:
    MatchSet(){};
    
    Match*              get_match(BSInt i)            {return &matches[i];               }
    ThreePointMatch*    get_three_point_match(BSInt i){return &three_point_matches[i];   }
    BSInt               three_point_match_size()      {return three_point_matches.size();}
    BSInt               size()                        {return matches.size();            }
    void                add_three_point_match(ThreePointMatch* _three_point_match);
    void                add_three_point_match(Match* _match1, Match* _match2, Match* _match3);
    void                add(BSInt _atom_type, GridPoint* _gridpoint, LigandAtom* _atom);
    void                print();
};


class Docking {
    BSDouble                 grid_dense;                    // Grid dense
    BSDouble                 max_hole;                      // Max Hole size, search range
    BSDouble                 criteria;                      // Criteria to be large enough to be a heated point
    BSDouble                 resolution;                    // Resolution
    pair<BSDouble, BSDouble> vdw_max[VDW_DOCKING_D_W];      // Database to record best chance of placement
    XYZ                      protein_center_of_mass;        // Coordinate of the mass center of the protein
    XYZ                      min_xyz;                       // the min x, y and z coordinates of the protein
    XYZ                      max_xyz;                       // the max x, y and z coordinates of the protein
    int                      dim_x;                         // Dimension of X, Y and Z axises
    int                      dim_y;
    int                      dim_z;
    BSInt                    number_of_points;              // Total points found valid
    
    //vector<GridPoint>        grid_points;
    //unordered_map<BSLong, XYZ*> grid_map;
    vector<XYZ*>             grid_map;                      // A collection of valid points in space, works as a hashing table but use hashing value manually
    KDTree                   kdtree;
    
    BSDouble                 current_distance;              // A temp value to reperent current distance
    BSDouble                 docking_distance;              // A distance to determine if a atom can fit into a grid point. For example, if the distance between an O2 atom and a O2 grid point is within this value, then this atom favor to docking into this point.
    BSDouble                 docking_distance2;             // Squre of docking_distance, for fast compute use.
    BSDouble                 docking_threshold;             // Energy difference with original conformal that is good enough to be listed as a good docking.
    
    
    // Heat map for following atom_type:
    //  Br and Br-HB; C1, C2, C3 and C-Ar; Cl and Cl-HB;
    //  F and F-HB; I; N2, N4, N-AM and N-AM-HB, N-Ar, N-PL3 and N-PL3-HB;
    //  O2, O2-HB, O3, O3-HB; S, S-HB;
    HeatMap                  grid_point_Br;
    HeatMap                  grid_point_Br_HB;
    HeatMap                  grid_point_C_1;
    HeatMap                  grid_point_C_2;
    HeatMap                  grid_point_C_3;
    HeatMap                  grid_point_C_AR;
    HeatMap                  grid_point_Cl;
    HeatMap                  grid_point_Cl_HB;
    HeatMap                  grid_point_F;
    HeatMap                  grid_point_F_HB;
    HeatMap                  grid_point_I;
    HeatMap                  grid_point_N_2;
    HeatMap                  grid_point_N_4;
    HeatMap                  grid_point_N_AM;
    HeatMap                  grid_point_N_AM_HB;
    HeatMap                  grid_point_N_AR;
    HeatMap                  grid_point_N_PL_3;
    HeatMap                  grid_point_N_PL_3_HB;
    HeatMap                  grid_point_O_2;
    HeatMap                  grid_point_O_2_HB;
    HeatMap                  grid_point_O_3;
    HeatMap                  grid_point_O_3_HB;
    HeatMap                  grid_point_S;
    HeatMap                  grid_point_S_HB;
    
    MatchSet                 match_set;             // match set poll
    MatchSet                 found_match_set;       // first rough round candidate match set
    MatchSet                 final_match_set;       // found match set
    
    
    BSLong   hashing(XYZ* x);
    BSLong   hashing(XYZ& x);
    BSLong   hashing(BSDouble x, BSDouble y, BSDouble z);
    BSDouble unhashing_x(BSLong h);
    BSDouble unhashing_y(BSLong h);
    BSDouble unhashing_z(BSLong h);
    
    BSDouble fold_point_set(HeatMap& grid_point_set);
    void generate_point_set(XYZ* current, vector<BSLong>& candidate_point_set, BSDouble min_hole_size, BSDouble max_hole_size);
    void find_grid_point(Atom* current, vector<BSLong>& candidate_point_set, BSInt col, HeatMap& target_point_set);
    void find_matches(Ligand& ligand, MatchSet& match);
    
public:
    Docking(Protein& protein, Ligand& ligand, Input& input);
    ~Docking(){
        for(int i = 0; i < grid_map.size(); i++){
            SAFEDELETE(grid_map[i]);
        }
    }
    
};
