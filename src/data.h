//
//  data.h
//  BiSheng
//
//  Created by Yipu Miao on 4/16/14.
//  Copyright (c) 2014 Yipu Miao. All rights reserved.
//

#ifndef __BiSheng__data__
#define __BiSheng__data__

#include <iostream>
#include "bisheng_common.h"
#include "protein.h"
#include <stdlib.h>
#include <string>
#include <fstream>
#include <ostream>
#include <algorithm>
#include <cstdlib>
#include <random>
#include "io.h"


/*
    Base class for matrix manupulation
 */
class DataMat{
    BSInt       width;
    BSInt       height;
    friend class Calculated;
    
protected:
    BSDouble*   data;
    
public:
    
    DataMat();
    DataMat(BSInt _width, BSInt _height);
    DataMat(BSInt _width, BSInt _height, BSDouble* _data);
    DataMat(BSInt _width, BSInt _height, BSDouble _data);
    DataMat(const DataMat& rhs);
    
    ~DataMat();
    
#ifdef CPU_MEM
    BSInt CPUMem();
#endif
    
    // Operator override
    DataMat&   operator= (const DataMat& rhs);
    DataMat    operator+ (const DataMat& rhs);
    DataMat&   operator+=(const DataMat& rhs);
    DataMat    operator- (const DataMat& rhs);
    DataMat&   operator-=(const DataMat& rhs);
    DataMat    operator* (const DataMat& rhs);
    DataMat    operator+ (const BSDouble&     rhs);
    DataMat    operator- (const BSDouble&     rhs);
    DataMat    operator* (const BSDouble&     rhs);
    DataMat    operator/ (const BSDouble&     rhs);
    DataMat    operator^ (const BSDouble&     rhs);
    
    // Those are special operators, for C=A&BC, (i,j) = A(i,j) * B(i,j)
    DataMat    operator& (const DataMat& rhs);
    DataMat&   operator&=(const DataMat& rhs);
    
    // Easy access to element
    BSDouble       &operator()(const unsigned int &row, const unsigned int &col);
    const BSDouble &operator()(const unsigned int &row, const unsigned int &col) const;
    
    // Transpose a matrix
    DataMat    transpose();
    
    // Print the matrix information (not elegently)
    void            print();
    void            print(string title); // Nothing more but add a title on top of the matrix
    
    // Some fast and safe ways to set and get data from a matrix
    void            setData(BSInt i, BSInt j, BSDouble data);
    void            setData(BSInt i, BSDouble data);
    BSDouble        getData(BSInt i){return data[i];};
    BSDouble        getData(BSInt i, BSInt j){return LOC2(this->data, i, j, this->height, this->width);};
    
    // Get the matrix size info
    BSInt           getWidth() { return width; };
    BSInt           getHeight(){ return height; };
    BSInt           Size()     { return width*height; };
    
    // Some other methods
    void            Shuffle();
    void            Shrink();           // Normalization the matrix
    BSDouble        sum();              // Return the sum of all elements in this matrix
    BSDouble        max();
};

/*
    Tile matrix is the several matrix elements that extract from database based on test_value
    note, test matrix (for index) and tile matrix may not be the same one
 */
class TileMat : public DataMat{
    bool found;
public:
    TileMat(const BSDouble* _source_mat, BSInt _source_mat_w, BSInt _source_mat_h, \
                 BSInt source_col,            const BSDouble* _test_mat,                \
                 BSInt _test_mat_w,           BSInt _test_mat_h,      BSDouble test_value, \
                 BSInt tile_mat_w,            BSInt tile_mat_h);
    bool Found(){return this->found;};
    
};

/*
    The replica matrix is to dulicated a (shuffled) tile matrix and further used to get final matrix
    this matrix is elimiated in the latest version ( does not need to allocate a temp matrix to save 
    time) by using repMat method from calculated class
 */
class ReplicaMat{

public:
    DataMat pMat;
    DataMat fMat;
    ReplicaMat(TileMat* tile_mat,   TileMat* tile_mat2,\
                    BSInt  tile_mat_w,        BSInt tile_mat_h, \
                    BSInt  replica_mat_w,     BSInt replica_mat_h, \
                    bool   ifShuffled);
};


/*
    The core code to calculate the free energy
 */
class Calculated{
    DataMat partition_matrix ;
    DataMat partition_matrix_lig;
    DataMat partition_matrix_vdw;
    DataMat partition_matrix_ang;
    DataMat partition_matrix_non;
    DataMat freedom_matrix   ;
    DataMat freedom_matrix_lig  ;
    DataMat freedom_matrix_vdw  ;
    DataMat freedom_matrix_ang  ;
    DataMat freedom_matrix_non  ;
    
    BSDouble     dG;
    bool         isValid(Database* database, BSDouble dist, BSInt i, BSInt j);
    bool         isValid(Database* database, BSDouble dist, BSInt i, BSInt j, bool hb);
    void repMat(DataMat* pMat,      DataMat* fMat,
                TileMat* tile_mat,  TileMat* tile_mat2,\
                BSInt tile_mat_w,        BSInt tile_mat_h, \
                BSInt  replica_mat_w,    BSInt replica_mat_h,\
                bool   shuffled);
    
    
    class parameter{
        BSInt           para_source_w;
        BSInt           para_source_h;
        BSInt           hashing_index;
        BSInt           para_f_source_w;
        BSInt           para_f_source_h;
        BSInt           tile_size_w;
        BSInt           tile_size_h;
        const BSDouble* para_source;
        const BSDouble* para_f_source;
        
        parameter(Database* database, BSInt i, BSInt j);
        parameter(Database* database, BSInt i, BSInt j, bool hb);
        friend class Calculated;
        
    };
public:
    Calculated();
    Calculated(//ProteinAtom** protein_atom, BSInt size_effect_protein_atom,
                    //LigandAtom**  ligand_atom,  BSInt size_ligand_atom,
                    Protein& protein, Ligand& ligand,
                    BSInt matrix_width, BSInt matrix_height,
                    Database* pro_bond_database,
                    Database* pro_angle_database,
                    Database* pro_torsion_database,
                    Database* pro_non_database,
                    Database* lig_bond_database,
                    Database* lig_angle_database,
                    Database* lig_torsion_database,
                    Database* lig_non_database,
                    Database* vdw_database) ;
    
    BSDouble get_dG(){return this->dG;};
    
    
};


class Analysis {
    vector<BSDouble> rms;
    BSDouble         calculated;
    BSDouble         oldAver;
    BSInt            converged_cyc;
    BSDouble         max_energy;
    BSDouble         min_energy;
    BSDouble         current;
    BSDouble         sd;
    BSDouble         err;
    BSDouble         dG;
    
public:
    Analysis(): calculated(0), oldAver(0), converged_cyc(0), max_energy(-1000.0), min_energy(10000.0){};
    void print_current();
    void print_final(Input input);
    void add(BSDouble current);
    BSDouble get_sd(){
        return this->sd;
    }
    
    BSDouble get_dG(){
        return this->dG;
    }
    
    BSInt get_converged_cyc(){
        return this->converged_cyc;
    }
};


#endif /* defined(__BiSheng__data__) */
