//
//  energy.h
//  BiSheng
//
//  Created by Yipu Miao on 11/13/14.
//  Copyright (c) 2014 Yipu Miao. All rights reserved.
//

#ifndef __BiSheng__energy__
#define __BiSheng__energy__

#include <stdio.h>
#include <stdio.h>
#include <iostream>
#include "bisheng_common.h"
#include "protein.h"
#include "data.h"
#include "io.h"

class Energy {
    BSDouble dG;
public:
    Energy(Protein& protain, Ligand& ligand, Input& input);
    BSDouble get_dG(){
        return this->dG;
    }
};

#endif /* defined(__BiSheng__energy__) */
