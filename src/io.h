//
//  io.h
//  BiSheng
//
//  Created by Yipu Miao on 5/1/14.
//  Copyright (c) 2014 Yipu Miao. All rights reserved.
//

#ifndef __BiSheng__io__
#define __BiSheng__io__

#include <iostream>
#include "bisheng_common.h"
#include <fstream>

/*
 This class is used to deal with input files
 */
class Input {
    BSInt       max_cycle;  // Keyword: maxcycle/run XX
    BSInt       min_cycle;  // Keyword: mincycle XX
    BSInt       width;      // Keyword: sample XXXX
                            //          express the matrix width, can be understanded as sample size.
    BSInt       height;     // No keyword
    bool        solv;       // Keyword: solv y/n
                            //          express the solvation mode
    BSDouble    tol;        // Keyword: tol XX
                            //          express the convergence criteria
    bool        docking;    // Keyword: docking y/n
    BSDouble    grid_dense; // keyword: grid_dense (For docking)
    BSDouble    resolution; // keyword: resolution (For docking)
    BSDouble    cutoff;     // Keyword: cutoff. For protein-legand cutoff
    BSInt       verbose;    // Keyword: verbose. For output level, 0 is default, which output basic information
    
public:
    Input(string inputFileName);
    BSInt       getMaxCycle()   {return max_cycle;};
    BSInt       getMinCycle()   {return min_cycle;};
    BSInt       getWidth()      {return width;};
    BSInt       getHeight()     {return height;};
    bool        getSolv()       {return solv;};
    BSDouble    getTol()        {return tol;};
    bool        getDocking()    {return docking;};
    BSDouble    getGridDense()  {return grid_dense;};
    BSDouble    getResolution() {return resolution;};
    BSDouble    getCutoff()     {return cutoff;};
    BSInt       getVerbose()    {return verbose;};
    void        setVerbose(BSInt _verbose){
        this->verbose = _verbose;
    }
    void        setMaxCycle(BSInt _maxCycle){
        this->max_cycle = _maxCycle;
    }
    
    void        setRun(BSInt _maxCycle){
        this->max_cycle = _maxCycle;
    }
    void    print();
};
#endif /* defined(__BiSheng__io__) */
