//
//  data.cpp
//  BiSheng
//
//  Created by Yipu Miao on 4/16/14.
//  Copyright (c) 2014 Yipu Miao. All rights reserved.
//

#include "data.h"


DataMat :: DataMat(): width(1), height(1){
    this->data    = (BSDouble*) malloc(1 * 1 * sizeof(BSDouble));
    this->data[0] = 1.0;
}

// Data is not initilized, so be careful for this constructor
DataMat :: DataMat(BSInt _width, BSInt _height): width(_width), height(_height){
    
    this->data    = (BSDouble*) malloc(_width * _height * sizeof(BSDouble));
    if (!this->data) {
        BSEXIT("CAN NOT ALLOCATE MATRIX");
    }
    
    /*for (int i = 0; i < _width*_height; i++) {
        this->data[i] = 0.0e0;
    }*/
    
}

DataMat :: DataMat(BSInt _width, BSInt _height, BSDouble* _data): width(_width), height(_height){
    
    this->data    = (BSDouble*) malloc(_width * _height * sizeof(BSDouble));
    
    if (!this->data) {
        BSEXIT("CAN NOT ALLOCATE MATRIX");
    }
    
    for (int i = 0; i < _width*_height; i++) {
        this->data[i] = _data[i];
    }
    
}

DataMat :: DataMat(BSInt _width, BSInt _height, BSDouble _data): width(_width), height(_height){
    
    this->data    = (BSDouble*) malloc(_width * _height * sizeof(BSDouble));
    
    if (!this->data) {
        BSEXIT("CAN NOT ALLOCATE MATRIX");
    }
    
    
    for (int i = _width * _height; i--;) {
        this->data[i] = _data;
    }
}

DataMat :: DataMat(const DataMat& rhs){
    
    this->data    = (BSDouble*) malloc(rhs.height * rhs.width * sizeof(BSDouble));
    this->width   = rhs.width;
    this->height  = rhs.height;
    
    if (!this->data) {
        BSEXIT("CAN NOT ALLOCATE MATRIX");
    }
    
    for (int i = 0; i < this->width * this->height; i++) {
        this->data[i] = rhs.data[i];
    }
}

DataMat& DataMat::operator=(const DataMat& rhs){
    
    if (this == &rhs) {
        return *this; // Avod self-assignment
    }else{
        if (this->width != rhs.width || this->height != rhs.height) {
            this->~DataMat();
            this->data    = (BSDouble*) malloc(rhs.height * rhs.width * sizeof(BSDouble));
            this->width   = rhs.width;
            this->height  = rhs.height;
            
            if (!this->data) {
                BSEXIT("CAN NOT ALLOCATE MATRIX");
            }
        }
        
        for (int i = 0; i < this->width * this->height; i++) {
            this->data[i] = rhs.data[i];
        }
    }
    return *this;
}

BSDouble&  DataMat ::operator()(const unsigned int &row, const unsigned int &col){
    return LOC2(this->data, row, col, this->height, this->width);
}


const BSDouble& DataMat ::operator()(const unsigned int &row, const unsigned int &col) const{
    return LOC2(this->data, row, col, this->height, this->width);
}


DataMat& DataMat::operator+=(const DataMat &rhs){
    for (int i = 0; i < this->width * this->height; i++) {
        this->data[i] += rhs.data[i];
    }
    return *this;
}

DataMat DataMat::operator+(const DataMat &rhs){
    DataMat tmp(this->width, this->height, 0.0);
    for (int i = 0; i < this->width * this->height; i++) {
        tmp.data[i] = rhs.data[i] + this->data[i];
    }
    return tmp;
}

DataMat& DataMat::operator-=(const DataMat &rhs){
    for (int i = 0; i < this->width * this->height; i++) {
        this->data[i] -= rhs.data[i];
    }
    return *this;
}

DataMat DataMat::operator-(const DataMat &rhs){
    DataMat tmp(this->width, this->height, 0.0);
    for (int i = 0; i < this->width * this->height; i++) {
        tmp.data[i] = rhs.data[i] + this->data[i];
    }
    return tmp;
}

DataMat DataMat::operator*(const DataMat &rhs){
    if (this->height != rhs.width) {
        BSEXIT("UNMATCHED MATRIX MUTIPLICATION SIZE");
    }
    DataMat result(this->width, rhs.height, 0.0e0);
    
    for (int i = 0; i < result.width; i++) {
        for (int j = 0; j < result.height; j++) {
            for (int k = 0; k < this->height; k++) {
                result(i,j) += LOC2(this->data, i, k, this->height, this->width) * rhs(k, j);
            }
        }
    }
    
    return result;
}

DataMat DataMat::operator+(const BSDouble& rhs){
    DataMat tmp(*this);
    DataMat tmp2(this->width, this->height, rhs);
    return (tmp + tmp2);
}


DataMat DataMat::operator-(const BSDouble& rhs){
    DataMat tmp(*this);
    DataMat tmp2(this->width, this->height, rhs);
    return (tmp - tmp2);
}


DataMat DataMat::operator*(const BSDouble& rhs){
    DataMat tmp(*this);
    for (int i = 0; i < this->width * this->height; i++) {
        tmp.data[i] = this->data[i] * rhs;
    }
    
    return tmp;
}


DataMat DataMat::operator/(const BSDouble& rhs){
    DataMat tmp(*this);
    for (int i = 0; i < this->width * this->height; i++) {
        tmp.data[i] = this->data[i] / rhs;
    }
    
    return tmp;
}



DataMat DataMat::operator^(const BSDouble& rhs){
    DataMat tmp(*this);
    for (int i = 0; i < this->width * this->height; i++) {
        tmp.data[i] = pow(this->data[i], rhs);
    }
    
    return tmp;
}


DataMat& DataMat::operator&=(const DataMat &rhs){
    
    for (int i = 0; i < this->width * this->height; i++) {
        this->data[i] = this->data[i] * rhs.data[i];
    }
    return *this;
}

DataMat DataMat::operator&(const DataMat &rhs){
    DataMat tmp(this->width, this->height, 0.0);
    
    for (int i = 0; i < this->width * this->height; i++) {
        tmp.data[i] = rhs.data[i] * this->data[i];
    }
    return tmp;
}


DataMat DataMat::transpose(){
    DataMat result(this->height, this->width, 0.0);
    for (int i = 0; i < this->height; i++) {
        for (int j= 0; j<this->width; j++) {
            LOC2(result.data, j, i, this->width, this->height) = LOC2(this->data, i, j, this->height, this->width);
        }
    }
    
    return result;
}

void   DataMat::Shrink(){
    BSDouble sum = 0.0e0;
    for (int i = 0; i < this->height * this->width; i++) {
        sum += this->data[i];
    }
    for (int i = 0; i < this->height * this->width; i++) {
            this->data[i] = this->data[i] / sum;
    }
    
}

void DataMat :: print(){
    
    cout << "MATSIZE = " << this->width << " * " << this->height << endl;
    
    for (int i = 0; i < this->height; i++) {
        cout << i <<"  | ";
        for (int j = 0; j < this->width; j++) {
            cout << LOC2(this->data, i, j, this->height, this->width) << " ";
        }
        cout<<endl;
    }
}

void DataMat :: print(string title){
    cout << title << endl;
    this->print();
}

void DataMat :: setData(BSInt i, BSInt j, BSDouble _data){
    LOC2(this->data, i, j, this->height, this->width) = _data;
}


void DataMat :: setData(BSInt i, BSDouble _data){
    this->data[i] = _data;
}

#ifdef CPU_MEM
BSInt DataMat :: CPUMem(){
    return sizeof(BSDouble) * this->width * this->height;
}
#endif


void DataMat :: Shuffle(){
    //std::random_device rd;
    //std::mt19937 g(rd());
    //shuffle(this->data, this->data + this->width * this->height, g);
    random_shuffle(this->data, this->data + this->width * this->height);
}


BSDouble DataMat :: sum(){
    BSDouble sum = 0.0e0;
    for (int i = 0; i < this->height * this->width; i++) {
        sum += this->data[i];
    }
    
    return sum;
}

BSDouble DataMat :: max(){
    BSDouble max = -10000.0;
    for (int i = 0; i < this->height * this->width; i++) {
        if (abs(max) < abs(this->data[i])) {
            max = this->data[i];
        }
    }
    return max;
}

DataMat :: ~DataMat(){
    if (this->data) {
        free(this->data);
    }
}


TileMat :: TileMat(const BSDouble* _source_mat, BSInt _source_mat_w, BSInt _source_mat_h, \
                             BSInt source_col,            const BSDouble* _test_mat, \
                             BSInt _test_mat_w,           BSInt _test_mat_h,      BSDouble test_value,
                             BSInt tile_mat_w,            BSInt tile_mat_h) :
DataMat(tile_mat_w, tile_mat_h, 0.0e0)
{
    this->found = true;
    int found_col = -1;
    
    for (int i = 0; i < _test_mat_h; i++) {
        if (fabs(test_value - LOC2(_test_mat, i, 0, _test_mat_h, _test_mat_w)) < SEARCH_CRITERIA) {
            found_col = i;
            break;
        }
    }
    
    if (found_col == -1) {
        this->found = false;
        for (int i = 0; i < tile_mat_h; i++) {
            this->setData(i, 0 , 1.0);
        }
    }else{
        
        // If TILE_SIZE_H = 5, so the interval is 2,
        // The matrix column picked is [i-2, i+2]
        // If TILE_SIZE_H = 100, the interval is 49,
        // the matrix column picked is [i-49, i+50];
        int interval = (tile_mat_h - 1) / 2;
        
        int starting_row = found_col - interval;
        int end_row = found_col + ( tile_mat_h - interval);
        
        if (starting_row < 0) {
            starting_row += interval;
            end_row += interval;
        }
        
        if (end_row >= _source_mat_h) {
            starting_row -= ( tile_mat_h - interval);
            end_row -= ( tile_mat_h - interval);
        }
        
        for (int i = 0; i < tile_mat_h; i++) {
            this->setData(i, 0 , LOC2(_source_mat, i + starting_row, source_col, _source_mat_h, _source_mat_w));
        }
    }
    
}

/*
static unsigned int g_seed;
inline void fast_srand( int seed )
{
    g_seed = seed;
}
//fastrand routine returns one integer, similar output value range as C lib.
inline int fastrand()
{
    g_seed = (214013*g_seed+2531011);
    return (g_seed>>16)&0x7FFF;
}
*/

ReplicaMat :: ReplicaMat(TileMat* tile_mat,  TileMat* tile_mat2,\
                                   BSInt tile_mat_w,        BSInt tile_mat_h, \
                                   BSInt  replica_mat_w,    BSInt replica_mat_h,\
                                   bool   shuffled):
pMat(replica_mat_w, replica_mat_h),
fMat(replica_mat_w, replica_mat_h)
{

    
    int tilesize = tile_mat_h * tile_mat_w;
    if (! shuffled) {
        for (int i = 0; i < replica_mat_h * replica_mat_w; i++) {
            int j = i % tilesize;
            this->pMat.setData(i, tile_mat ->getData(j));
            this->fMat.setData(i, tile_mat2->getData(j));
        }
    }else{
        /*
        for (int i = 0; i < replica_mat_h * replica_mat_w; i=i+tilesize) {
            int k = rand() % tilesize;
            for (int j = 0; j < tilesize ; j++) {
                if ((k+j) >= tilesize) {
                    k=k-tilesize;
                }
                
                this->pMat.setData(i + j, tile_mat ->getData(k + j));
                this->fMat.setData(i + j, tile_mat2->getData(k + j));
            }
            
        }*/
        
        for (int i = 0; i < replica_mat_h * replica_mat_w; i++) {
            int j = rand() % tilesize;
            this->pMat.setData(i, tile_mat ->getData(j));
            this->fMat.setData(i, tile_mat2->getData(j));
        }
    }
    // Following is the old version of using permutation, but replaced with random access
    /*
    int tile_mat_h_repeat_time = (int) replica_mat_h / tile_mat_h;
    int tile_mat_w_repaet_time = (int) replica_mat_w / tile_mat_w;
    
    DataMat col(tile_mat_w, replica_mat_h, 0.0e0);
    
    
    for (int i = 0; i < tile_mat_h_repeat_time; i++) {
        for (int j = 0; j < tile_mat_h; j++) {
            for (int k = 0; k < tile_mat_w; k++) {
                if (((i * tile_mat_h + j) < replica_mat_h) && k < replica_mat_w) {
                    col.setData(i * tile_mat_h + j, k, (*tile_mat)(j, k));
                }
            }
        }
    }
    
    for (int c = 0; c < tile_mat_w_repaet_time; c++) {
        
        if (shuffled) col.Shuffle();
        
        for (int i = 0; i < replica_mat_h; i ++) {
            for (int j= 0; j < tile_mat_w; j++) {
                if ( (c * tile_mat_w + j) < replica_mat_w) {
                    this->setData( i, c * tile_mat_w + j, col(i, j));
                }
            }
            
        }
    }*/
}


/*
 This subroutine is to replicate tile matrix to a full matrix
 We further combine this step with tensor multiply so that a temp matrix
 is not necessary
 */

void Calculated :: repMat(DataMat* pMat,      DataMat* fMat,
       TileMat* tile_mat,  TileMat* tile_mat2,\
       BSInt tile_mat_w,        BSInt tile_mat_h, \
       BSInt  replica_mat_w,    BSInt replica_mat_h,\
       bool   shuffled){
    
    
    int tilesize = tile_mat_h * tile_mat_w;
    if (! shuffled) {
        for (int i = 0; i < replica_mat_h * replica_mat_w; i++) {
            int j = i % tilesize;
            pMat->data[i+j] = pMat->data[i+j] * (BSDouble) tile_mat ->getData(j);
            fMat->data[i+j] = fMat->data[i+j] * (BSDouble) tile_mat2->getData(j);
        }
    }else{
        
        for (int i = 0; i < replica_mat_h * replica_mat_w; i=i+tilesize) {
            int k = rand() % tilesize;
            for (int j = 0; j < tilesize ; j++) {
                if ((k+j) >= tilesize) {
                    k=k-tilesize;
                }
                pMat->data[i+j] = pMat->data[i+j] * (BSDouble) tile_mat ->getData(k + j);
                fMat->data[i+j] = fMat->data[i+j] * (BSDouble) tile_mat2->getData(k + j);
            }
            
        }
    }
    
}

// Inner class that eases coding
Calculated :: parameter :: parameter(Database* database, BSInt atom_i_name, BSInt atom_j_name){
    
    para_source       = database->data;
    para_source_h     = database->height;
    para_source_w     = database->width;
    para_f_source     = database->data_f;
    para_f_source_h   = database->height;
    para_f_source_w   = database->width;
    tile_size_w       = database->tile_w;
    tile_size_h       = database->tile_h;
    
    if (database->hasHBond) {
        if (foundIHashing(database->hashing_hb,  atom_i_name, atom_j_name)) {
            hashing_index     = searchIHashing(database->hashing_hb, atom_i_name, atom_j_name);
        }else{
            hashing_index     = searchIHashing(database->hashing, atom_i_name, atom_j_name);
        }
    }else{
        hashing_index     = searchIHashing(database->hashing, atom_i_name, atom_j_name);
    }
}


Calculated :: parameter :: parameter(Database* database, BSInt atom_i_name, BSInt atom_j_name, bool hb){
    
    para_source       = database->data;
    para_source_h     = database->height;
    para_source_w     = database->width;
    para_f_source     = database->data_f;
    para_f_source_h   = database->height;
    para_f_source_w   = database->width;
    tile_size_w       = database->tile_w;
    tile_size_h       = database->tile_h;
    
    if (database->hasHBond) {
        if (searchIHashing(database->check_hb, atom_i_name, atom_j_name) == 2 && hb) {
            hashing_index     = searchIHashing(database->hashing_hb, atom_i_name, atom_j_name);
        }else{
            hashing_index     = searchIHashing(database->hashing, atom_i_name, atom_j_name);
        }
    }else{
        hashing_index     = searchIHashing(database->hashing, atom_i_name, atom_j_name);
    }
}

// Normal length check
bool Calculated ::  isValid(Database* database, BSDouble dist, BSInt atom_i_name, BSInt atom_j_name){
    return (   foundIHashing(database->hashing,atom_i_name, atom_j_name) \
            && dist < database->end  [searchIHashing(database->hashing, atom_i_name, atom_j_name)] \
            && dist > database->begin[searchIHashing(database->hashing, atom_i_name, atom_j_name)]);
}

// Length check with h-bond:
// check_hb = 0: hashing table only has no HB info
//          = 1: hashing table only has HB info
//          = 2: hashing table has both HB and no-HB info
bool Calculated ::  isValid(Database* database, BSDouble dist, BSInt atom_i_name, BSInt atom_j_name, bool hb){
    if (database->hasHBond) {
        if (foundIHashing(database->hashing, atom_i_name, atom_j_name)){
            if (searchIHashing(database->check_hb, atom_i_name, atom_j_name) == 1) {
                return hb && dist > database->begin[searchIHashing(database->hashing,    atom_i_name, atom_j_name)] \
                          && dist < database->end  [searchIHashing(database->hashing,    atom_i_name, atom_j_name)];
            }else if(searchIHashing(database->check_hb, atom_i_name, atom_j_name) == 2 && hb){
                return (     dist < database->end  [searchIHashing(database->hashing_hb, atom_i_name, atom_j_name)] \
                          && dist > database->begin[searchIHashing(database->hashing_hb, atom_i_name, atom_j_name)]);
            }else{
                return (     dist < database->end  [searchIHashing(database->hashing,    atom_i_name, atom_j_name)] \
                          && dist > database->begin[searchIHashing(database->hashing,    atom_i_name, atom_j_name)]);
            }
            
        }
    }else{
        return (   foundIHashing(database->hashing,atom_i_name, atom_j_name) \
                && dist < database->end  [searchIHashing(database->hashing, atom_i_name, atom_j_name)] \
                && dist > database->begin[searchIHashing(database->hashing, atom_i_name, atom_j_name)]);
    }
    return false;
}




Calculated :: Calculated (//ProteinAtom** protein_atom, BSInt size_effect_protein_atom,
                                    //LigandAtom**  ligand_atom,  BSInt size_ligand_atom,
                                    Protein& protein,
                                    Ligand&  ligand,
                                    BSInt matrix_width, BSInt matrix_height,
                                    Database* pro_bond_database,
                                    Database* pro_angle_database,
                                    Database* pro_torsion_database,
                                    Database* pro_non_database,
                                    Database* lig_bond_database,
                                    Database* lig_angle_database,
                                    Database* lig_torsion_database,
                                    Database* lig_non_database,
                                    Database* vdw_database):

partition_matrix    (matrix_width, matrix_height, 1.0),
partition_matrix_lig(matrix_width, matrix_height, 1.0),
partition_matrix_vdw(matrix_width, matrix_height, 1.0),
partition_matrix_ang(matrix_width, matrix_height, 1.0),
partition_matrix_non(matrix_width, matrix_height, 1.0),
freedom_matrix      (matrix_width, matrix_height, 1.0),
freedom_matrix_lig  (matrix_width, matrix_height, 1.0),
freedom_matrix_vdw  (matrix_width, matrix_height, 1.0),
freedom_matrix_ang  (matrix_width, matrix_height, 1.0),
freedom_matrix_non  (matrix_width, matrix_height, 1.0)
{
    
    if (matrix_height % 100 != 0) {
        cout << "MATRIX SIZE MUST BE A MULIPLIER OF 100" << endl;
        matrix_height = 100 * (int) (matrix_height/100) ;
        cout << "CHANGE SIZE TO " << matrix_height << endl;
    }
    
#ifdef CPU_MEM
    
    int cpu_cost  = 0;
    cpu_cost += (partition_matrix.CPUMem() +
                 partition_matrix_lig.CPUMem() +
                 partition_matrix_vdw.CPUMem() +
                 partition_matrix_ang.CPUMem() +
                 partition_matrix_non.CPUMem());
    
    cpu_cost += (freedom_matrix.CPUMem() +
                 freedom_matrix_lig.CPUMem() +
                 freedom_matrix_vdw.CPUMem() +
                 freedom_matrix_ang.CPUMem() +
                 freedom_matrix_non.CPUMem());
    
    cout << " CPU ALLOCATE MEMORY FOR PARAM = " << (BSDouble) (CPU_MEM_SIZE) / 1024 / 1024 << " MBYTE" << endl;
    cout << " CPU ALLOCATE MEMORY FOR MATRIX= " << (BSDouble) (cpu_cost)  / 1024 / 1024 << " MBYTE" << endl;
    cpu_total_mem += (CPU_MEM_SIZE + cpu_cost);
    PRINTCPU("ALLOCATE PARAMTER MATRIX AND CALCULATION MATRIX");
#endif
    
    TileMat* pMat;
    TileMat* fMat;
    parameter*    para;
    /*
        The scaler is used to prevent numeric overflow especially single precision calculation (float).
        The scaler would effect the accurcy theoretically but can be ignored
     */
    BSInt         scale_time  = 0;          // scaler called time
    BSInt         scale_count = 0;          // scaler counter, call scale when the counter equals to scale freq
    BSInt         scale_freq  = SCALEFREQ;  // scaler freqency
    
    int size_effect_protein_atom = protein.get_effect_atom_size();
    int size_ligand_atom         = ligand.get_atom_size();
    
    /*
     ============================================================
     Protein-Protein Interaction
     ============================================================
     */
    
    for (int i = 0; i < size_effect_protein_atom; i ++) {
        for (int j = i + 1; j < size_effect_protein_atom; j++) {
            
            BSInt           atom_i_name = protein.get_atom(i)->getName();
            BSInt           atom_j_name = protein.get_atom(j)->getName();
            int contribution_type = -1;
            bool pFound = false;
            
            BSDouble        dist   = protein.get_atom(i)->Distance(protein.get_atom(j));
            
            /*
             ------------------------------------------------------------------
             First of all, let's find the parameters, which actually, find the source of parameters and parameter array size
             ------------------------------------------------------------------
             */
            
            if ( isValid(pro_bond_database, dist,          atom_i_name, atom_j_name)) {
                // Bond contribution
                para = new parameter(pro_bond_database,    atom_i_name, atom_j_name);
                contribution_type = PRO_BOND_TYPE;
            }else if(isValid(pro_angle_database, dist,     atom_i_name, atom_j_name)){
                // Angle contribution
                para = new parameter(pro_angle_database,   atom_i_name, atom_j_name);
                contribution_type = PRO_ANGLE_TYPE;
            }else if (isValid(pro_torsion_database, dist,  atom_i_name, atom_j_name)){
                // Torsion Contribution
                para = new parameter(pro_torsion_database, atom_i_name, atom_j_name);
                contribution_type = PRO_TORSION_TYPE;
            }else if(isValid(pro_non_database, dist,       atom_i_name, atom_j_name, protein.get_atom(i)->checkHB(protein.get_atom(j)))){
                // Non-bond contribution
                para = new parameter(pro_non_database,     atom_i_name, atom_j_name);
                contribution_type = PRO_NON_TYPE;
            }
            
            /*
             ------------------------------------------------------------------
             Then extra parameters from parameter array to form a tile.
             ------------------------------------------------------------------
             */
            
            if (contribution_type != -1 && para->hashing_index > 0) {
                pMat = new TileMat(para->para_source,   para->para_source_w,   para->para_source_h,   para->hashing_index, \
                                        para->para_source,   para->para_source_w,   para->para_source_h,   dist, \
                                        para->tile_size_w,   para->tile_size_h);
                fMat = new TileMat(para->para_f_source, para->para_f_source_w, para->para_f_source_h, para->hashing_index, \
                                        para->para_source,   para->para_f_source_w, para->para_f_source_h, dist, \
                                        para->tile_size_w,   para->tile_size_h);
                pFound = pMat -> Found();
            }
            
            
            
            /*
             Once we find the file, shuffly form the replica from tile
             */
            
            if (pFound) {
                
                // form the replica by duplicating file and shuffled for several times
                //ReplicaMat p_rep  (pMat, fMat,  para->tile_size_w, para->tile_size_h, matrix_width, matrix_height, true);
                
                // Tensor product to partition function and f
                switch (contribution_type) {
                    case PRO_BOND_TYPE:
                    case PRO_TORSION_TYPE:
                        repMat(&partition_matrix,     &freedom_matrix,     pMat, fMat,  para->tile_size_w, para->tile_size_h, matrix_width, matrix_height, true);
                        //partition_matrix     &= p_rep.pMat;
                        //freedom_matrix       &= p_rep.fMat;
                        break;
                    case PRO_ANGLE_TYPE:
                        repMat(&partition_matrix_ang, &freedom_matrix_ang, pMat, fMat,  para->tile_size_w, para->tile_size_h, matrix_width, matrix_height, true);
                        //partition_matrix_ang &= p_rep.pMat;
                        //freedom_matrix_ang   &= p_rep.fMat;
                        break;
                    case PRO_NON_TYPE:
                        repMat(&partition_matrix_non, &freedom_matrix_non, pMat, fMat,  para->tile_size_w, para->tile_size_h, matrix_width, matrix_height, true);
                        //partition_matrix_non &= p_rep.pMat;
                        //freedom_matrix_non   &= p_rep.fMat;
                        break;
                    default:
                        break;
                }
                
                SAFEDELETE(pMat);
                SAFEDELETE(fMat);
                SAFEDELETE(para);
                
                // Scale count is used to scale the matrix to prevent overflown
                scale_count++;
                if (scale_count > scale_freq) {
                    scale_count = 0;
                    
                    if (partition_matrix.max() > SCALELIMIT) {
                        partition_matrix = partition_matrix * SCALE;
                    }
                    if (partition_matrix_ang.max() > SCALELIMIT) {
                        partition_matrix_ang = partition_matrix_ang * SCALE;
                    }
                    if (partition_matrix_non.max() > SCALELIMIT) {
                        partition_matrix_non = partition_matrix_non * SCALE;
                    }
                    
                    if (freedom_matrix.max() > SCALELIMIT) {
                        freedom_matrix.Shrink();
                    }
                    if (freedom_matrix_ang.max() < 1.0/SCALELIMIT) {
                        freedom_matrix_ang.Shrink();
                    }
                    freedom_matrix_ang.Shrink();
                    if (freedom_matrix_non.max() > SCALELIMIT) {
                        freedom_matrix_non.Shrink();
                    }
                }
            }
        }
    }
    
    
    /*
     ============================================================
     Ligend-Ligend Interaction
     ============================================================
     */
    scale_count = 0;
    for (int i = 0; i < size_ligand_atom; i ++) {
        for (int j = i + 1; j < size_ligand_atom; j++) {
            
            
            BSInt           atom_i_name = ligand.get_atom(i)->getName();
            BSInt           atom_j_name = ligand.get_atom(j)->getName();
            int contribution_type = -1;
            bool pFound = false;
            BSDouble        dist   = ligand.get_atom(i)->Distance(ligand.get_atom(j));
            
            /*
             First of all, just like protein, let's find the parameters
             */
            
            if (isValid(lig_bond_database,          dist, atom_i_name, atom_j_name)) {
                // Bond contribution
                para = new parameter(lig_bond_database,   atom_i_name, atom_j_name);
                contribution_type = LIG_BOND_TYPE;
            }else if(isValid(lig_angle_database,    dist, atom_i_name, atom_j_name)){
                // Angle contribution
                para = new parameter(lig_angle_database,  atom_i_name, atom_j_name);
                contribution_type = LIG_ANGLE_TYPE;
            }else if (isValid(lig_torsion_database, dist, atom_i_name, atom_j_name)){
                // Torsion Contribution
                para = new parameter(lig_torsion_database,atom_i_name, atom_j_name);
                contribution_type = LIG_TORSION_TYPE;
            }else if (isValid(lig_non_database,     dist, atom_i_name, atom_j_name)){
                // Non-Bond Contribution
                para = new parameter(lig_non_database,    atom_i_name, atom_j_name);
                contribution_type = LIG_NON_TYPE;
            }
            
            /*
             Then extra parameters from parameter array to form a tile.
             */
            
            if (contribution_type != -1) {
                pMat = new TileMat(para->para_source,   para->para_source_w,   para->para_source_h,   para->hashing_index, \
                                        para->para_source,   para->para_source_w,   para->para_source_h,   dist, \
                                        para->tile_size_w,   para->tile_size_h);
                fMat = new TileMat(para->para_f_source, para->para_f_source_w, para->para_f_source_h, para->hashing_index, \
                                        para->para_source,   para->para_f_source_w, para->para_f_source_h, dist, \
                                        para->tile_size_w,   para->tile_size_h);
                pFound = pMat -> Found();
            }
            
            /*
             Once we find the file, shuffly form the replica from tile
             */
            
            if (pFound) {
                // form the replica by duplicating file and shuffled for several times
                //ReplicaMat p_rep  (pMat, fMat,  para->tile_size_w, para->tile_size_h, matrix_width, matrix_height, true);
                
                // Tensor product to partition function and f
                switch (contribution_type) {
                    case LIG_BOND_TYPE:
                    case LIG_ANGLE_TYPE:
                    case LIG_TORSION_TYPE:
                    case LIG_NON_TYPE:
                        repMat(&partition_matrix_lig, &freedom_matrix_lig, pMat, fMat,  para->tile_size_w, para->tile_size_h, matrix_width, matrix_height, true);
                        
                        //partition_matrix_lig  &= p_rep.pMat;
                        //freedom_matrix_lig    &= p_rep.fMat;
                        break;
                    default:
                        break;
                }
                
                SAFEDELETE(pMat);
                SAFEDELETE(fMat);
                SAFEDELETE(para);
                
                
                scale_count++;
                if (scale_count > scale_freq) {
                    scale_count = 0;
                    if (partition_matrix_lig.max() > SCALELIMIT) {
                        partition_matrix_lig = partition_matrix_lig * SCALE;
                    }
                    
                    
                    if (freedom_matrix_lig.max() < 1.0/SCALELIMIT) {
                        freedom_matrix_lig.Shrink();
                    }
                }
                
            }
            
        }
    }
    
    
    /*
     ============================================================
     Protein-Ligend Interaction (VDW)
     ============================================================
     */
    scale_count = 0;
    for (int i = 0; i < size_effect_protein_atom; i ++) {
        for (int j = 0; j < size_ligand_atom; j++) {
            
            BSInt           atom_i_name = protein.get_atom(i)->getName();
            BSInt           atom_j_name = ligand.get_atom(j)->getName();
            int contribution_type = -1;
            bool pFound = false;
            BSDouble        dist   = protein.get_atom(i)->Distance(ligand.get_atom(j));
            
            /*
             First of all, just like protein, let's find the parameters
             */
            if (isValid(vdw_database, dist, atom_i_name, atom_j_name, protein.get_atom(i)->checkHB(protein.get_atom(j)))){
                // VDW Contribution
                para = new parameter(vdw_database, atom_i_name, atom_j_name, protein.get_atom(i)->checkHB(ligand.get_atom(j)));
                contribution_type = VDW_TYPE;
            }
            
            /*
             Then extra parameters from parameter array to form a tile.
             */
            
            
            if (contribution_type != -1) {
                pMat = new TileMat(para->para_source,   para->para_source_w,   para->para_source_h,   para->hashing_index, \
                                        para->para_source,   para->para_source_w,   para->para_source_h,   dist, \
                                        para->tile_size_w,   para->tile_size_h);
                fMat = new TileMat(para->para_f_source, para->para_f_source_w, para->para_f_source_h, para->hashing_index, \
                                        para->para_source,   para->para_f_source_w, para->para_f_source_h, dist, \
                                        para->tile_size_w,   para->tile_size_h);
                pFound = pMat -> Found();
            }
            
            /*
             Once we find the file, shuffly form the replica from tile
             */
            
            if (pFound) {
                // form the replica by duplicating file and shuffled for several times
                //ReplicaMat p_rep  (pMat, fMat,  para->tile_size_w, para->tile_size_h, matrix_width, matrix_height, true);
                
                // Tensor product to partition function and f
                switch (contribution_type) {
                    case VDW_TYPE:
                        repMat(&partition_matrix_vdw, &freedom_matrix_vdw, pMat, fMat,  para->tile_size_w, para->tile_size_h, matrix_width, matrix_height, true);
 //                       partition_matrix_vdw  &= p_rep.pMat;
 //                       freedom_matrix_vdw    &= p_rep.fMat;
                        break;
                    default:
                        break;
                }
                
                SAFEDELETE(pMat);
                SAFEDELETE(fMat);
                SAFEDELETE(para);
                
                
                scale_count++;
                if (scale_count > scale_freq) {
                    scale_count = 0;
                    
                    if (partition_matrix_vdw.max() > SCALELIMIT) {
                        partition_matrix_vdw = partition_matrix_vdw * SCALE;
                        scale_time++;
                    }
                    
                    if (freedom_matrix_vdw.max() > SCALELIMIT) {
                        freedom_matrix_vdw.Shrink();
                    }
                }
                
            }
        }
    }
    
    DataMat freedom_matrix_pro_lig (matrix_width, matrix_height, 1.0);
    DataMat freedom_matrix_pro     (matrix_width, matrix_height, 1.0);
    
#ifdef CPU_MEM
    cpu_cost = (freedom_matrix_pro_lig.CPUMem() +
                freedom_matrix_pro.CPUMem());
    cpu_total_mem += cpu_cost;
    PRINTCPU("ALLOCATE FINAL MATRIX");
#endif
    
    freedom_matrix.Shrink();
    freedom_matrix_ang.Shrink();
    freedom_matrix_non.Shrink();
    freedom_matrix_vdw.Shrink();
    freedom_matrix_lig.Shrink();
    
    
    freedom_matrix_pro     = freedom_matrix      & freedom_matrix_ang & freedom_matrix_non;
    freedom_matrix_pro.Shrink();
    freedom_matrix_pro_lig = freedom_matrix_pro  & freedom_matrix_lig & freedom_matrix_vdw;
    freedom_matrix_pro_lig.Shrink();
    
    
    BSDouble max_com = (freedom_matrix_pro_lig    &((partition_matrix      ^ bo)
                                                  & (partition_matrix_ang  ^ ao)
                                                  & (partition_matrix_lig  ^ ao)
                                                  & (partition_matrix_non  ^ co)
                                                  & (partition_matrix_vdw  ^ co))).sum();
    BSDouble max_pro = (freedom_matrix_pro        &((partition_matrix      ^ bo)
                                                  & (partition_matrix_ang  ^ ao)
                                                  & (partition_matrix_non  ^ co))).sum();
    BSDouble max_lig = (freedom_matrix_lig        &((partition_matrix_lig  ^ ao))).sum();
    
    
    this->dG = -log10 (max_com / (max_pro * max_lig)) + co * scale_time * log10(ONEOVERSCALE);
    
}

/*
    analysor print current calculated cycle result.
 */

void Analysis :: print_current(){
    cout << " "<< setw(4) << converged_cyc << " " << setw(18) << setprecision(10) << current   << " " \
    << setw(18) << setprecision(10) << calculated/(converged_cyc) << " " \
    << setw(18) << setprecision(10) << err                        << " "\
    << setw(18) << setprecision(10) << sqrt(sd/(converged_cyc))   << " "
    << setw(18) << setprecision(10) << sqrt(sd)/(converged_cyc)   << endl;
    
}

/*
    analysor add one more cycle and combine old result into new. 
    Generate new average value, error with last cycle, standard deviation, and update cycle number.
 */

void Analysis :: add(BSDouble _current){
    
    current = _current;
    
    calculated += current;
    err = oldAver - calculated/(converged_cyc + 1); // difference with last cycle
    
    // Update rms and standard deviation (SD)
    rms.push_back(current);
    int i = rms.size();
    sd = 0.0;
    for (int j = 0; j < i; j++) {
        sd += pow((rms[j] - calculated/(i+1)),2);
    }
    
    
    oldAver = calculated/(i+1);
    this->dG = oldAver;
    
    // Update max and min value.
    if (rms.back() > max_energy) max_energy = rms.back();
    if (rms.back() < min_energy) min_energy = rms.back();
    
    // Update cycle number
    converged_cyc++;
    
}

/*
    analysor print the last cycle. Include average free energy, error bar, SD and SE.
 */
void Analysis :: print_final(Input input){
    
    BSDouble aver = calculated/converged_cyc;
    
    calculated = 0;
    for (int i = 0; i < converged_cyc; i++) {
        calculated += pow((rms[i] - aver),2);
    }
    sort (rms.begin(), rms.begin() + converged_cyc);
    
    BSDouble weighted_average = 0;
    BSDouble weight = 1.0;
    BSDouble total_weight = 0.0;
    for (int i = 0; i <= converged_cyc/2; i++) {
        total_weight = total_weight + weight * 2;
        weighted_average = weighted_average + weight * (rms[i] + rms[converged_cyc - i - 1]);
        weight=weight + 2.0;
    }
    if (input.getVerbose() > 1){
        cout << "----------------------------------------------------------------------------------------------------"  <<endl;
        cout << setw(18) << setprecision(10) << "    FREE ENERGY        =   " << aver << " KCAL/MOL"            << endl;
        cout << "    ERROR              = + " << max_energy - aver              << endl;
        cout << "                         - " << aver - min_energy              << endl;
        cout << "    WEIGHTED AVERAGE   =   " << weighted_average / total_weight<< endl;
        cout << "    MEDIAN             =   " << rms[converged_cyc/2]           << endl;
        cout << "    STANDARD DEVIATION =   " << sqrt(calculated/converged_cyc) << endl;
        cout << "    STANDARD ERROR     =   " << sqrt(calculated/converged_cyc)/sqrt(converged_cyc)
        << endl;
        cout << "----------------------------------------------------------------------------------------------------"  <<endl;
    }
    
    this->dG = aver;
}



