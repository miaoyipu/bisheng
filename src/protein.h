//
//  protein.h
//  BiSheng
//
//  Created by Yipu Miao on 2/3/14.
//  Copyright (c) 2014 Yipu Miao. All rights reserved.
//

/*
        Atom (Abstact)                     - (compose) ->   Molecule (Abstract)
        |                  \     (inherence)                       |               \ (inherence)
        |                  LigandAtom      - (compose) ->     |                Ligand
        ProteinAtom                        - (compose) ->    Protein
 
 */

#ifndef __BiSheng__protein__
#define __BiSheng__protein__

#include <iostream>
#include "bisheng_common.h"
#include <sstream>
#include <string>
#include "graph.h"

class XYZ {
public:
    BSDouble    x;
    BSDouble    y;
    BSDouble    z;
    XYZ() : x(0.0), y(0.0), z(0.0){};
    XYZ(BSDouble _x, BSDouble _y, BSDouble _z): x(_x),  y(_y),  z(_z) { };
    void move(BSDouble _x, BSDouble _y, BSDouble _z){
        x += _x;
        y += _y;
        z += _z;
    }
    void print(){
        cout << " ( " << x << " , " << y << " , " << z << " )";
    }
    
    // Return the distance between two atoms and its squre
    BSDouble Distance (XYZ* _xyz) const;
    BSDouble Distance2(XYZ* _xyz) const;
    BSDouble Distance (XYZ& _xyz) const;
    BSDouble Distance2(XYZ& _xyz) const;
    BSDouble Distance2(BSDouble x, BSDouble y, BSDouble z) const;
};

class Plane{
public:
    BSDouble a;
    BSDouble b;
    BSDouble c;
    BSDouble d;
    
    
    Plane(): a(0.0),b(0.0),c(0.0),d(0.0) {};
    
    // A plane determined by three points
    Plane(XYZ* xyz1, XYZ* xyz2, XYZ* xyz3){
        a = (xyz2->y - xyz1->y) * (xyz3->z - xyz1->z) - (xyz3->y - xyz1->y) * (xyz2->z - xyz1->z);
        b = (xyz2->z - xyz1->z) * (xyz3->x - xyz1->x) - (xyz3->z - xyz1->z) * (xyz2->x - xyz1->x);
        c = (xyz2->x - xyz1->x) * (xyz3->y - xyz1->y) - (xyz3->x - xyz1->x) * (xyz2->y - xyz1->y);
        BSDouble dd = sqrt( a * a + b * b + c * c);
        
        if (dd == 0.0){
            d = 0;
            return;
        }
        
        // Normalize a, b, c. And (a, b, c) is norm of the plane
        a = a / dd;
        b = b / dd;
        c = c / dd;
        
        d = 0 - a * xyz1->x - b * xyz1->y - c * xyz1->z;
        
    }
    
    XYZ norm(){
        return XYZ(a, b, c);
    }
    
    BSDouble normalization(){
        return sqrt(a*a + b*b + c*c);
    }
    BSDouble angle(Plane& s){
        return acos((a * s.a + b * s.b + c * s.c) / (this->normalization() * s.normalization() ) );
    }
    
};

class Transformation{
    BSDouble        trans_mat[4][4];
public:
    Transformation(){
        trans_mat[0][0] = 1;    trans_mat[0][1] = 0;    trans_mat[0][2] = 0;    trans_mat[0][3] = 0;
        trans_mat[1][0] = 0;    trans_mat[1][1] = 1;    trans_mat[1][2] = 0;    trans_mat[1][3] = 0;
        trans_mat[2][0] = 0;    trans_mat[2][1] = 0;    trans_mat[2][2] = 1;    trans_mat[2][3] = 0;
        trans_mat[3][0] = 0;    trans_mat[3][1] = 0;    trans_mat[3][2] = 0;    trans_mat[3][3] = 1;
    }
    
    void setMove(XYZ xyz){
        trans_mat[3][0] = xyz.x;
        trans_mat[3][1] = xyz.y;
        trans_mat[3][2] = xyz.z;
    }
    
    // Rotate around the vector "one" (normalized) for theta degree
    // the transformation matrix can be expressed as:
    //  exp([ 0             -z * theta       y * theta
    //        z * theta     0               -x * theta
    //       -y * theta      x * theta      0]
    
    void setRotate(XYZ one, BSDouble theta){
        BSDouble norm = sqrt(one.x * one.x + one.y * one.y + one.z * one.z);
        BSDouble x = one.x/norm;
        BSDouble y = one.y/norm;
        BSDouble z = one.z/norm;
        BSDouble cos_theta = cos(theta);
        BSDouble sin_theta = sin(theta);
        trans_mat[0][0] = cos_theta + (1 - cos_theta) * x * x;        trans_mat[0][1] = (1 - cos_theta) * x * y - sin_theta * z;    trans_mat[0][2] = (1 - cos_theta) * x * z + sin_theta * y;
        trans_mat[1][0] = (1 - cos_theta) * y * x + sin_theta * z;    trans_mat[1][1] = cos_theta + (1 - cos_theta) * y * y;        trans_mat[1][2] = (1 - cos_theta) * y * z - sin_theta * x;
        trans_mat[2][0] = (1 - cos_theta) * z * x - sin_theta * y;    trans_mat[2][1] = (1 - cos_theta) * z * y + sin_theta * x;    trans_mat[2][2] = cos_theta + (1 - cos_theta) * z * z;
    }
    
    // (x',y',z') = (x, y, z) * M
    void move(XYZ* xyz){
        xyz->x += trans_mat[3][0];
        xyz->y += trans_mat[3][1];
        xyz->z += trans_mat[3][2];
    }
    
    void rotate(XYZ* xyz){
        xyz->x = xyz->x * trans_mat[0][0] + xyz->y * trans_mat[1][0] + xyz->z * trans_mat[2][0];
        xyz->y = xyz->x * trans_mat[0][1] + xyz->y * trans_mat[1][1] + xyz->z * trans_mat[2][1];
        xyz->z = xyz->x * trans_mat[0][2] + xyz->y * trans_mat[1][2] + xyz->z * trans_mat[2][2];
    }
};

/*
    ==============================================================
        Base class for atom, works for protein and ligand both
    ==============================================================
 */
class Atom : public XYZ{
protected:
    BSInt       name1;      // Real Atom type rather than atom name that PDB provides
    string      atomName;
    BSInt       atomNum;
    BSDouble    radius;     // Radius
    BSInt       hb;         // hbond type
    BSDouble    acceptor_hb_angle;
    BSDouble    donor_hb_angle;
    BSDouble    charge;     // Atom charge
    
public:
    
    //Atom(): x(0.0), y(0.0), z(0.0), name1(DUMMY){ };
    //Atom(BSDouble _x, BSDouble _y, BSDouble _z): x(_x),  y(_y),  z(_z), name1(DUMMY) { };
    
    // return if this atom type is Hydrogen or N/S that can form H-bond
    bool IsH(){
        return (this->name1 == H);
    };
    
    bool IsHBondN(){
        return ((this->name1 == N_3) || (this->name1 == N_2) \
                ||(this->name1 == N_AM)|| (this->name1 == N_PL3));
    };
    
    bool IsHBondS(){
        return ((this->name1 == S_3) || (this->name1 == S_2));
    };
    
    bool IsHBondO(){
        return (this->name1 == O_3);
    };
    
    BSInt getHB(){
        return this->hb;
    };
    
    bool getHBD(){
        return (this->hb == D) || (this->hb == D2) || (this->hb == DA);
    }
    
    BSInt    getName()        { return this->name1; };
    BSInt    getAtomType()    { return this->name1; };
    void     print();
    bool     checkHB(Atom* _xyz);
    BSDouble getRadius()      { return this->radius;};
    BSInt    get_atom_num()   { return this->atomNum;};
};


/*
    ==============================================================
        Protein Atom type
        important members include chain information
    ==============================================================
 */

class ProteinAtom : public Atom{
    /* 
        Inherented
    ===============================
    BSDouble    x;
    BSDouble    y;
    BSDouble    z;
    BSInt       name1;      // Real Atom type rather than atom name that PDB provides
    string      atomName;
    BSInt       atomNum;
    BSDouble    radius;     // Radius
    BSInt       hb;         // Hydrogen bond type
    BSDouble    acceptor_hb_angle;
    BSDouble    donor_hb_angle;
    BSDouble    charge;     // Atom charge
    */
    
    // Not used
    //BSDouble    occupancy;
    
    string      res;
    BSInt       resSeq;
    string      chain;
    
    BSInt       name2;      // Sequence name, same with resSeq
    BSInt       backbond;   // Backbond? 1 or 0
    BSInt       chainID;    // Chain ID, depends on res variables
public:
    // default and constructor based on input line
    ProteinAtom(string inputLine);
    
    
    // Assign Result depends on atom types
    int AssignValue(BSInt       _name1, \
                    BSInt       _name2, \
                    BSDouble    _radius, \
                    BSInt       _hb, \
                    BSDouble    _acceptor_hb_angle, \
                    BSDouble    _donor_hb_angle, \
                    BSDouble    _charge, \
                    BSInt       _backbond);
  
    bool IsZinc();
    BSInt getResSeq(){ return this->resSeq; }
};

/*
    ==============================================================
        ligand atom type
    ==============================================================
 */
class LigandAtom :  public Atom {
    /*
     Inherented
     ===============================
     BSDouble    x;
     BSDouble    y;
     BSDouble    z;
     BSInt       name1;      // Real Atom type rather than atom name that PDB provides
     string      atomName;
     BSInt       atomNum;
     BSDouble    radius;     // Radius
     BSInt       hb;         // Hydrogen bond type
     BSDouble    acceptor_hb_angle;
     BSDouble    donor_hb_angle;
     BSDouble    charge;     // Atom charge
     */
    
    BSDouble    mass;       // Atom mass
    BSInt       neib_H;     // Neiberhoud H

public:
    // default and constructor based on input line
    LigandAtom(string inputLine);
    
    int AssignValue(BSInt       _name1, \
                    BSDouble    _radius, \
                    BSInt       _hb, \
                    BSDouble    _acceptor_hb_angle, \
                    BSDouble    _donor_hb_angle, \
                    BSDouble    _mass, \
                    BSDouble    _charge);
    
    void FindNeibH(LigandAtom* ligand_atom);
    
};
/*
class GridPoint : public Atom{
    BSInt   value;
public:
    void set_val(BSInt _value) { this->value = _value;};
    BSInt get_val()            {return this->value;};
};
*/


template <class T >
class Molecule {
    int atom_num;
    int h_atom_num;
    
public:
    
    vector<T*> atoms;
    vector<T*> h_atoms;
    
    int get_atom_size(){ return atoms.size(); }
    
    int get_h_atom_size(){ return h_atoms.size(); }
    
    vector<T*>& get_atom(){ return this->atoms; }
    
    T* get_atom(int i){
        if (i > atoms.size()) { return NULL; }else{ return atoms[i]; }
    }
    
    void super_clean(){
        for (int i = 0; i < atoms.size(); i++) SAFEDELETE(atoms[i]);
        for (int i = 0; i < h_atoms.size(); i++) SAFEDELETE(h_atoms[i]);
    }
    
    virtual void clean(){
        super_clean();
    }
};



class Ligand : public Molecule<LigandAtom>{
    Graph<LigandAtom*> graph;
    vector<XYZ>             saved_xyz;
public:
    Ligand(string ligandFileName);
    void recover_xyz();
    void trans(Transformation& transformation);
};



class Protein : public Molecule<ProteinAtom>{
    int zn_atom_num;
    bool is_screen;
    vector<ProteinAtom*> effect_atoms;
    vector<vector<BSDouble> > ZnAtom;
    
public:
    Protein(string PDBFileName);
    
    // The following are same
    vector<ProteinAtom*>& get_all_atom(){
        return this->atoms;
    }
    vector<ProteinAtom*>& get_atom(){
        return this->atoms;
    }
    
    vector<ProteinAtom*>& get_effect_atom(){
        return this->effect_atoms;
    }
    
    
    XYZ get_min_xyz();
    XYZ get_max_xyz();
    
    int get_effect_atom_size(){ return effect_atoms.size(); }
    int get_all_atom_size()   { return this->get_atom_size(); };
    
    
    
    ProteinAtom* get_atom(int i){
        if (! is_screen) {
            if (i > this->get_atom_size())    { return NULL; }else{ return this->get_all_atom()[i]; }
        }else{
            if (i > this->effect_atoms.size()) { return NULL; }else{ return effect_atoms[i];}
        }
    }
    
    ProteinAtom* get_all_atom(int i){
         if (i > this->get_atom_size()) {
             return NULL;
         }else{
             return this->get_all_atom()[i];
         }
    }
    
    
    XYZ get_center_of_mass();
    
    void screen(Ligand ligand, BSDouble cutoff);
    
    
};

#endif /* defined(__BiSheng__protein__) */
