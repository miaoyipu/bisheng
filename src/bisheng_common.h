//
//  bisheng_common.h
//  BiSheng
//
//  Created by Yipu Miao on 2/3/14.
//  Copyright (c) 2014 Yipu Miao. All rights reserved.
//

#ifndef BiSheng_bisheng_common_h
#define BiSheng_bisheng_common_h



#ifdef CPU_MEM
static int cpu_total_mem = 0;
#endif

#include <math.h>
#include <stdlib.h> 
#include <ctime>
#include <algorithm>
#include <vector>
#include <iomanip>
#include <memory>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <cstdlib>
#include <string.h>
#include <cmath>
#include <unordered_map>
#include <unordered_set>
#include <queue>
#include <stack>

using namespace std;

#ifndef BSSINGLE
typedef double  BSDouble;
typedef float   BSSingle;
typedef int     BSInt;
typedef long long int BSLong;
#else
typedef float   BSDouble;
typedef float   BSSingle;
typedef int     BSInt;
typedef long long int BSLong;
#endif

// Size limitation
static const int       MAX_PROTEIN_ATOM     = 100000;
static const int       MAX_PROTEIN_H_ATOM   = 30000;
static const int       MAX_LIGEND_ATOM      = 30000;
static const int       MAX_LIGEND_H_ATOM    = 5000;
static const int       MAXZN                = 10;
static const int       MAX_ATOM_TYPE        = 38;
static const int       MAX_CYCLE            = 500000;
static const int       MIN_CYCLE            = 5;

static const int       PRO_BOND_TILE_H      = 5;
static const int       PRO_BOND_TILE_W      = 1;
static const int       PRO_ANGLE_TILE_H     = 5;
static const int       PRO_ANGLE_TILE_W     = 1;
static const int       PRO_TORSION_TILE_H   = 10;
static const int       PRO_TORSION_TILE_W   = 1;
static const int       PRO_NON_TILE_H       = 100;
static const int       PRO_NON_TILE_W       = 1;
static const int       LIG_BOND_TILE_H      = 5;
static const int       LIG_BOND_TILE_W      = 1;
static const int       LIG_ANGLE_TILE_H     = 5;
static const int       LIG_ANGLE_TILE_W     = 1;
static const int       LIG_TORSION_TILE_H   = 10;
static const int       LIG_TORSION_TILE_W   = 1;
static const int       LIG_NON_TILE_H       = 100;
static const int       LIG_NON_TILE_W       = 1;
static const int       VDW_TILE_H           = 100;
static const int       VDW_TILE_W           = 1;

// Size defination
// PRO/SIZE * BOND/ANGLE/TORSION/NON * D/F * W/H
//= 2       * 4                      * 2   * 2    = 32 + VDW*D/F*W/H = 36
static const int       PRO_BOND_D_W         = 17;
static const int       PRO_BOND_D_H         = 221;
static const int       PRO_BOND_F_W         = 17;
static const int       PRO_BOND_F_H         = 221;
static const int       PRO_ANGLE_D_W        = 30;
static const int       PRO_ANGLE_D_H        = 175;
static const int       PRO_ANGLE_F_W        = 30;
static const int       PRO_ANGLE_F_H        = 175;
static const int       PRO_TORSION_D_W      = 26;
static const int       PRO_TORSION_D_H      = 494;
static const int       PRO_TORSION_F_W      = 26;
static const int       PRO_TORSION_F_H      = 494;
static const int       PRO_NON_D_W          = 45;
static const int       PRO_NON_D_H          = 537;
static const int       PRO_NON_F_W          = 45;
static const int       PRO_NON_F_H          = 537;
static const int       LIG_BOND_D_W         = 41;
static const int       LIG_BOND_D_H         = 259;
static const int       LIG_BOND_F_W         = 41;
static const int       LIG_BOND_F_H         = 259;
static const int       LIG_ANGLE_D_W        = 46;
static const int       LIG_ANGLE_D_H        = 177;
static const int       LIG_ANGLE_F_W        = 46;
static const int       LIG_ANGLE_F_H        = 177;
static const int       LIG_TORSION_D_W      = 50;
static const int       LIG_TORSION_D_H      = 494;
static const int       LIG_TORSION_F_W      = 50;
static const int       LIG_TORSION_F_H      = 494;
static const int       LIG_NON_D_W          = 48;
static const int       LIG_NON_D_H          = 543;
static const int       LIG_NON_F_W          = 48;
static const int       LIG_NON_F_H          = 543;
static const int       VDW_D_W              = 85;
static const int       VDW_D_H              = 764;
static const int       VDW_F_W              = 85;
static const int       VDW_F_H              = 764;

static const int       VDW_DOCKING_D_W      = 87;
static const int       VDW_DOCKING_D_H      = 796;
static const int       VDW_DOCKING_F_W      = 87;
static const int       VDW_DOCKING_F_H      = 796;


static const int       CPU_MEM_SIZE         = sizeof(BSDouble) * (  (PRO_BOND_D_W    * PRO_BOND_D_H) \
                                                                  + (PRO_BOND_F_W    * PRO_BOND_F_H) \
                                                                  + (PRO_ANGLE_F_W   * PRO_ANGLE_F_H) \
                                                                  + (PRO_ANGLE_D_W   * PRO_ANGLE_D_H) \
                                                                  + (PRO_TORSION_F_W * PRO_TORSION_F_H) \
                                                                  + (PRO_TORSION_D_W * PRO_TORSION_D_H) \
                                                                  + (PRO_NON_F_W     * PRO_NON_F_H) \
                                                                  + (PRO_NON_D_W     * PRO_NON_D_H) \
                                                                  + (LIG_BOND_D_W    * LIG_BOND_D_H) \
                                                                  + (LIG_BOND_F_W    * LIG_BOND_F_H) \
                                                                  + (LIG_ANGLE_F_W   * LIG_ANGLE_F_H) \
                                                                  + (LIG_ANGLE_D_W   * LIG_ANGLE_D_H) \
                                                                  + (LIG_TORSION_F_W * LIG_TORSION_F_H) \
                                                                  + (LIG_TORSION_D_W * LIG_TORSION_D_H) \
                                                                  + (LIG_NON_F_W     * LIG_NON_F_H) \
                                                                  + (LIG_NON_D_W     * LIG_NON_D_H) \
                                                                  + (VDW_D_W         * VDW_D_H) \
                                                                  + (VDW_F_W         * VDW_F_H) \
                                                                  );


// Const
static const BSDouble    PI             = 3.14159265358979323846264338327950288419716939937510;
static const BSDouble    R              = 1.9872041; // kcal/mol
static const BSDouble    T              = 298.00;    // Temp
static const BSDouble    ONE            = 1.0e0;
static const BSDouble    ZERO           = 0.0e0;
static const BSDouble    SCALE          = 1.0e-30;
#ifdef BSSINGLE
static const BSDouble    SCALELIMIT     = 1.0;
static const BSDouble    SCALEFREQ      = 30;
#else
static const BSDouble    SCALELIMIT     = 1.0e10;
static const BSDouble    SCALEFREQ      = 1000;
#endif
static const BSDouble    ONEOVERSCALE   = 1.0 / SCALE;

// Parameters
static const BSDouble    HBOND_DISTANCE       =        1.60e0;

//static const BSDouble    INTERACTION_CUTOFF   = sqrt(100000.00e0);
//static const BSDouble    INTERACTION_CUTOFF   = sqrt(32.00e0);

static const BSDouble    EFFECT_BOND_LENGTH   =  sqrt(3.24e0);
static const BSDouble    SEARCH_CRITERIA      =  0.0025e0;
//static const BSDouble    DESOLVATION_DISTANCE =        5.00e0;
//static const BSDouble    DESOLVATION_DISTANCE2=        3.00e0;
static const BSDouble    DATABASE_CUTOFF      =  1.0e-6;
//static const BSDouble    DOCKING_DISTANCE     =  0.5000;


// Special type
static const int DUMMY  =  -1;
static const int UNKNOWN=  -2;

// Organic Atom Types
static const int C_3    =   1;
static const int C_2    =   2;
static const int C_1    =   3;
static const int C_AR   =   4;
static const int O_3    =   5;
static const int O_2    =   6;
static const int O_W    =   7;
static const int N_3    =   8;
static const int N_2    =   9;
static const int N_1    =   10;
static const int N_AR   =   11;
static const int N_AM   =   12;
static const int N_PL3  =   13;
static const int S_3    =   14;
static const int S_2    =   15;
static const int S_O    =   16;
static const int S_O2   =   17;
static const int F      =   18;
static const int CL     =   19;
static const int BR     =   20;
static const int I      =   21;
static const int P      =   22;
static const int H      =   23;
static const int C_CAT  =   24;
static const int O_CO2  =   25;
static const int N_4    =   26;
static const int S      =   27;
static const int BR_HB  =   220;
static const int CL_HB  =   221;
static const int F_HB   =   222;
static const int N_AM_HB=   222;
static const int N_PL3_HB   =   223;
static const int O_2_HB =   224;
static const int O_3_HB =   225;
static const int S_HB   =   226;



// Metal Atom Types
static const int LI     =   27;
static const int NA     =   28;
static const int K      =   29;
static const int MG     =   30;
static const int CAA    =   31;
static const int AL     =   32;
static const int MN     =   33;
static const int FE     =   34;
static const int CO     =   35;
static const int NI     =   36;
static const int CU     =   37;
static const int ZN     =   38;

// H-Bond types
static const int N_A    =   0;
static const int D      =   1;
static const int D2     =   2;
static const int A      =   3;
static const int DA     =   4;

// Residue Types
static const int ALA    =   101;
static const int ARG    =   102;
static const int ASN    =   103;
static const int ASP    =   104;
static const int CYS    =   105;
static const int GLN    =   106;
static const int GLU    =   107;
static const int GLY    =   108;
static const int HIS    =   109;
static const int ILE    =   110;
static const int LEU    =   111;
static const int LYS    =   112;
static const int MET    =   113;
static const int PHE    =   114;
static const int PRO    =   115;
static const int SER    =   116;
static const int THR    =   117;
static const int TRP    =   118;
static const int TYR    =   119;
static const int VAL    =   120;

// Type
static const int PRO_BOND_TYPE    = 1;
static const int PRO_ANGLE_TYPE   = 2;
static const int PRO_TORSION_TYPE = 3;
static const int PRO_NON_TYPE     = 4;
static const int LIG_BOND_TYPE    = 5;
static const int LIG_ANGLE_TYPE   = 6;
static const int LIG_TORSION_TYPE = 7;
static const int LIG_NON_TYPE     = 8;
static const int VDW_TYPE         = 9;


// ELEMENT SYMBOLS
static const string CARBON      =   "C";
static const string OXYGEN      =   "O";
static const string NITROGEN    =   "N";
static const string SULFER      =   "S";
static const string FLUORIN     =   "F";
static const string CHLORINE    =   "Cl";
static const string BROMINE     =   "Br";
static const string IODINE      =   "I";
static const string PHOSPHOR    =   "P";
static const string HYDROGEN    =   "H";


// CHAIN SYMBOLS
static const int CHAIN_A = 1;
static const int CHAIN_B = 2;
static const int CHAIN_C = 3;
static const int CHAIN_D = 4;
static const int CHAIN_E = 5;
static const int CHAIN_F = 6;
static const int CHAIN_G = 7;
static const int CHAIN_H = 8;
static const int CHAIN_I = 9;
static const int CHAIN_J = 10;
static const int CHAIN_K = 11;
static const int CHAIN_L = 12;
static const int CHAIN_M = 13;
static const int CHAIN_N = 14;
static const int CHAIN_O = 15;
static const int CHAIN_P = 16;
static const int CHAIN_Q = 17;
static const int CHAIN_R = 18;
static const int CHAIN_S = 19;
static const int CHAIN_T = 20;
static const int CHAIN_U = 21;
static const int CHAIN_V = 22;
static const int CHAIN_W = 23;
static const int CHAIN_X = 24;
static const int CHAIN_Y = 25;
static const int CHAIN_Z = 26;
static const int CHAIN_1 = 1;
static const int CHAIN_2 = 2;

// SCALE FACTOR

static const BSDouble co = 0.06;
static const BSDouble ao = 0.03;
static const BSDouble bo = 0.028;
static const BSDouble eo = 0.01;
static const BSDouble fo = -0.4;


// Macro for two- and three- dimension array, d1,d2 and d3 are the dimension and i1,i2 and i3 are the indices
#define LOC2(A,i1,i2,d1,d2)  A[i2+(i1)*(d2)]
#define LOC3(A,i1,i2,i3,d1,d2,d3) A[i3+((i2)+(i1)*(d2))*(d3)]
//#define LOC4(A,i1,i2,i3,i4,d1,d2,d3,d4) A[i4+(i3+((i2)+(i1)*(d2))*(d3))*(d4)]

#define MAX(A,B)    (A>=B?A:B)
#define MIN(A,B)    (A<B?A:B)

#include "param.h"
#include "hashing.h"
#include "io.h"


#define BSEXIT(s)\
{\
    cout << "(FATAL ERROR): " << s << endl; \
    exit(-1); \
}

#define SAFEDELETE(s)\
{\
    if( (s) != NULL ) delete (s); (s) = NULL; \
}

#ifdef CPU_MEM

#define PRINTCPU(s)\
{\
cout<<"=== TOTAL CPU " << (BSDouble) cpu_total_mem /1024/1024<< " MBytes  == ACTION: " <<s<<" ==="<<endl;\
}
#endif


#endif



