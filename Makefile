#!/bin/sh

MAKEIN = ./make.in
include $(MAKEIN)

#----------------------
# src file location
#----------------------
srcfolder = ./src

#----------------------
# obj file location
#----------------------
objfolder = ./obj

#----------------------
# exe file location
#----------------------
exefolder = ./bin

#----------------------
# library file location
#----------------------
libfolder = ./lib

#----------------------
# Source file list
#----------------------
srcfile   = bisheng.cpp data.cpp docking.cpp energy.cpp hashing.cpp io.cpp protein.cpp graph.cpp

main:
	cd $(srcfolder) && $(CPP) $(CFLAG) $(CFLAGSINGE) -o ../$(exefolder)/bisheng $(srcfile)
	cd ..
debug:
	cd $(srcfolder) && $(CPP) $(CFLAG) $(CFLAGSINGE) -DDEBUG -o ../$(exefolder)/bisheng $(srcfile)
	cd ..
clean:
	rm $(exefolder)/bisheng $(srcfolder)/*.o
