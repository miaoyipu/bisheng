#include <stdlib.h>
#include <iomanip>
#include <iostream>
using namespace std;

int main(){
	int w = 87;
	int h = 796;
    cout << fixed;
	int k = 0;
	for(int i = 0; i < h; i++){
		k = 0;
		for(int j = 0; j < w; j++){
		float b;
		cin >> b;
		cout << setw(10) << setprecision(5) << b << ",";
		k++;
		if (k%15 ==0 ) cout << endl;
		}
		cout << endl;
		
	}
}
