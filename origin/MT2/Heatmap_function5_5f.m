function out = Heatmap_function2_1(latom,centroidb,protential_protein)
%latom=ligand;
max_dist=[0
4.355
4.675
4.78
4.095
3.69
3.66
3.66
4.645
3.885
3.785
4.97
3.91
3.94
4.75
3.63
4.045
3.81
3.81
4.03
4.15
4.725
4.605
3.605
3.795
3.65
3.98
3.99
3.935
3.815
3.73
4.76
4.615
3.74
3.77
3.56
3.905
4.48
3.905
3.95
4.86
4.29
4.115
2.8
4.795
2.885
3.865
2.485
2.68
2.64
3.44
3.625
3.625
3.625
3.705
3.545
3.71
3.885
4.745
4.36
2.83
3.59
3.415
4.69
3.285
2.955
3.88
3.245
3.77
3.83
3.8
2.98
4.35
4.685
4.695
3.99
4.03
2.93
2.92
2.88
3.675
2.875
2.875
4.035
2.935
2.83
2.79
];

size_protential_protein=size(protential_protein);

protein_atom_num=[1:1:size_protential_protein(1,1)];

protential_protein(:,7)=transpose(protein_atom_num);
protein_H=protential_protein(protential_protein(:,4)==23,:);

size_protein_H=size(protein_H);

protential_protein(protential_protein(:,4)==23,:)=[];

size_protential_protein=size(protential_protein);



centroid=latom;


size_centroid=size(centroid);
size_centroidb=size(centroidb);

cent_p=[sum(centroidb(:,1))/size_centroidb(1,1),sum(centroidb(:,2))/size_centroidb(1,1),sum(centroidb(:,3))/size_centroidb(1,1)];


run vdw_para_final_v;

size_vdw_matrix=size(vdw_matrix);
clear vdw_max
for i=2:1:size_vdw_matrix(1,2)
    [m1 m2]=find(vdw_matrix(:,1)==max_dist(i,1));
    if length(m1)==size_vdw_matrix(1,1)
        [m1 m2]=find(vdw_f(:,i)==max(vdw_f(:,i)));
        vdw_max(i,1)=vdw_f(m1(1,1),1);
        vdw_max(i,2)=vdw_matrix(m1(1,1),i);
    else
        vdw_max(i,1)=vdw_matrix(m1(1,1),1);
        vdw_max(i,2)=vdw_matrix(m1(1,1),i);
    end
end
    

     
N_A=0;
D=1;
D2=2;
A=3;
DA=4;

C_3=1;
C_2=2;
C_1=3;
C_ar=4;
O_3=5;
O_2=6;
O_w=7;
N_3=8;
N_2=9;
N_1=10;
N_ar=11;
N_am=12;
N_pl3=13;
S_3=14;
S_2=15;
S_o=16;
S_o2=17;
F=18;
Cl=19;
Br=20;
I=21;
P=22;
H=23;
C_cat=24;
O_co2=25;
N_4=26;

Matr_c2=[];
Matr_c3=[];
Matr_car=[];
Matr_n2=[];
Matr_clHB=[];
Matr_o2HB=[];
Matr_brHB=[];
Matr_fHB=[];
Matr_o3HB=[];
Matr_sHB=[];
Matr_n4=[];
Matr_npl3=[];
Matr_s=[];
Matr_nar=[];
Matr_br=[];
Matr_c1=[];
Matr_cl=[];
Matr_f=[];
Matr_namHB=[];
Matr_npl3HB=[];
Matr_nam=[];
Matr_o2=[];
Matr_o3=[];
Matr_i=[];

grid_d=0.2;

for zz=1:1:size_centroidb(1,1)
    clear sig_x sig_y sig_z grid_coor grid_coor1r

    if centroidb(zz,4)==1
        max_x=max(centroidb(zz,1))+4.8;
        min_x=min(centroidb(zz,1))-4.8;
        max_y=max(centroidb(zz,2))+4.8;
        min_y=min(centroidb(zz,2))-4.8;
        max_z=max(centroidb(zz,3))+4.8;
        min_z=min(centroidb(zz,3))-4.8;
        sig_x=transpose(min_x:grid_d:max_x);
        sig_y=transpose(min_y:grid_d:max_y);
        sig_z=transpose(min_z:grid_d:max_z);
        
        size_sig_x=size(sig_x);
        size_sig_y=size(sig_y);
        size_sig_z=size(sig_z);
    
    
    for i=1:1:size_sig_x(1,1)
        grid_coor((i-1)*size_sig_y(1,1)*size_sig_z(1,1)+1:i*size_sig_y(1,1)*size_sig_z(1,1),1)=sig_x(i,1);
    end
    
    for i=1:1:size_sig_y(1,1)
        grid_coor1r((i-1)*size_sig_z(1,1)+1:i*size_sig_z(1,1),1)=sig_y(i,1);
    end
    
    grid_coor(:,2)=repmat(grid_coor1r,size_sig_x(1,1),1);
    grid_coor(:,3)=repmat(sig_z,size_sig_x(1,1)*size_sig_y(1,1),1);
    grid_coor(:,4)=zz;
    size_grid_coor=size(grid_coor);
    
    if norm(centroidb(zz,1:3)-cent_p(1,1:3))<=vdw_max(4,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(4,1))<=0.1,:);
    elseif norm(centroidb(zz,1:3)-cent_p(1,1:3))>vdw_max(4,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(4,1))<=0.1&    sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 + (centroidb(zz,1)-cent_p(1,1)).^2+(centroidb(zz,2)-cent_p(1,2)).^2+(centroidb(zz,3)-cent_p(1,3)).^2 )> sqrt( (cent_p(1,1)-grid_coor(:,1)).^2+(cent_p(1,2)-grid_coor(:,2)).^2+(cent_p(1,3)-grid_coor(:,3)).^2 ),:);
    end
    
    for pk=1:1:size_protential_protein(1,1)
        clear pku grid_test Matr centr_a;
        P_sh(:,5)=0;
        
        pku(:,1)=(P_sh(:,1)-protential_protein(pk,1)).^2;
        pku(:,2)=(P_sh(:,2)-protential_protein(pk,2)).^2;
        pku(:,3)=(P_sh(:,3)-protential_protein(pk,3)).^2;
        
        
%        pku(:,4)=(pku(:,1)+pku(:,2)+pku(:,3)).^0.5;
        pku(((pku(:,1)+pku(:,2)+pku(:,3)).^0.5<=3.2),4)=1;        
        P_sh(pku(:,4)==1,5)=1;

        P_sh(P_sh(:,5)==1,:)=[];

    end



    P_sh(:,5)=[];


    size_P_sh=size(P_sh);
    size_Matr_car=size(Matr_car);
    Matr_car(1+size_Matr_car(1,1):size_Matr_car(1,1)+size_P_sh(1,1),1:4)=P_sh;

    Matr_car(1+size_Matr_car(1,1):size_Matr_car(1,1)+size_P_sh(1,1),5)=vdw_max(4,2);
    
    
    
    if norm(centroidb(zz,1:3)-cent_p(1,1:3))<=vdw_max(2,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(2,1))<=0.1,:);
    elseif norm(centroidb(zz,1:3)-cent_p(1,1:3))>vdw_max(2,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(2,1))<=0.1&    sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 + (centroidb(zz,1)-cent_p(1,1)).^2+(centroidb(zz,2)-cent_p(1,2)).^2+(centroidb(zz,3)-cent_p(1,3)).^2 )> sqrt( (cent_p(1,1)-grid_coor(:,1)).^2+(cent_p(1,2)-grid_coor(:,2)).^2+(cent_p(1,3)-grid_coor(:,3)).^2 ),:);
    end
    
    for pk=1:1:size_protential_protein(1,1)
        clear pku grid_test Matr centr_a;
        P_sh(:,5)=0;
        
        pku(:,1)=(P_sh(:,1)-protential_protein(pk,1)).^2;
        pku(:,2)=(P_sh(:,2)-protential_protein(pk,2)).^2;
        pku(:,3)=(P_sh(:,3)-protential_protein(pk,3)).^2;
        
        
%        pku(:,4)=(pku(:,1)+pku(:,2)+pku(:,3)).^0.5;
        pku(((pku(:,1)+pku(:,2)+pku(:,3)).^0.5<=3.2),4)=1;        
        P_sh(pku(:,4)==1,5)=1;

        P_sh(P_sh(:,5)==1,:)=[];

    end



    P_sh(:,5)=[];


    size_P_sh=size(P_sh);
    size_Matr_c3=size(Matr_c3);
    Matr_c3(1+size_Matr_c3(1,1):size_Matr_c3(1,1)+size_P_sh(1,1),1:4)=P_sh;

    Matr_c3(1+size_Matr_c3(1,1):size_Matr_c3(1,1)+size_P_sh(1,1),5)=vdw_max(2,2);
    
    
    
    if norm(centroidb(zz,1:3)-cent_p(1,1:3))<=vdw_max(3,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(3,1))<=0.1,:);
    elseif norm(centroidb(zz,1:3)-cent_p(1,1:3))>vdw_max(3,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(3,1))<=0.1&    sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 + (centroidb(zz,1)-cent_p(1,1)).^2+(centroidb(zz,2)-cent_p(1,2)).^2+(centroidb(zz,3)-cent_p(1,3)).^2 )> sqrt( (cent_p(1,1)-grid_coor(:,1)).^2+(cent_p(1,2)-grid_coor(:,2)).^2+(cent_p(1,3)-grid_coor(:,3)).^2 ),:);
    end
    
    for pk=1:1:size_protential_protein(1,1)
        clear pku grid_test Matr centr_a;
        P_sh(:,5)=0;
        
        pku(:,1)=(P_sh(:,1)-protential_protein(pk,1)).^2;
        pku(:,2)=(P_sh(:,2)-protential_protein(pk,2)).^2;
        pku(:,3)=(P_sh(:,3)-protential_protein(pk,3)).^2;
        
        
%        pku(:,4)=(pku(:,1)+pku(:,2)+pku(:,3)).^0.5;
        pku(((pku(:,1)+pku(:,2)+pku(:,3)).^0.5<=3.2),4)=1;        
        P_sh(pku(:,4)==1,5)=1;

        P_sh(P_sh(:,5)==1,:)=[];

    end



    P_sh(:,5)=[];


    size_P_sh=size(P_sh);
    size_Matr_c2=size(Matr_c2);
    Matr_c2(1+size_Matr_c2(1,1):size_Matr_c2(1,1)+size_P_sh(1,1),1:4)=P_sh;

    Matr_c2(1+size_Matr_c2(1,1):size_Matr_c2(1,1)+size_P_sh(1,1),5)=vdw_max(3,2);
    
    
    
    
    
    
    
    
    
    
    
    
    elseif centroidb(zz,4)==2
        max_x=max(centroidb(:,1))+4.7;
        min_x=min(centroidb(:,1))-4.7;
        max_y=max(centroidb(:,2))+4.7;
        min_y=min(centroidb(:,2))-4.7;
        max_z=max(centroidb(:,3))+4.7;
        min_z=min(centroidb(:,3))-4.7;
        sig_x=transpose(min_x:grid_d:max_x);
        sig_y=transpose(min_y:grid_d:max_y);
        sig_z=transpose(min_z:grid_d:max_z);
        
        size_sig_x=size(sig_x);
        size_sig_y=size(sig_y);
        size_sig_z=size(sig_z);
    
    
    for i=1:1:size_sig_x(1,1)
        grid_coor((i-1)*size_sig_y(1,1)*size_sig_z(1,1)+1:i*size_sig_y(1,1)*size_sig_z(1,1),1)=sig_x(i,1);
    end
    
    for i=1:1:size_sig_y(1,1)
        grid_coor1r((i-1)*size_sig_z(1,1)+1:i*size_sig_z(1,1),1)=sig_y(i,1);
    end
    
    grid_coor(:,2)=repmat(grid_coor1r,size_sig_x(1,1),1);
    grid_coor(:,3)=repmat(sig_z,size_sig_x(1,1)*size_sig_y(1,1),1);
    grid_coor(:,4)=zz;
    size_grid_coor=size(grid_coor);
    
    if norm(centroidb(zz,1:3)-cent_p(1,1:3))<=vdw_max(23,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(23,1))<=0.1,:);
    elseif norm(centroidb(zz,1:3)-cent_p(1,1:3))>vdw_max(23,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(23,1))<=0.1&    sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 + (centroidb(zz,1)-cent_p(1,1)).^2+(centroidb(zz,2)-cent_p(1,2)).^2+(centroidb(zz,3)-cent_p(1,3)).^2 )> sqrt( (cent_p(1,1)-grid_coor(:,1)).^2+(cent_p(1,2)-grid_coor(:,2)).^2+(cent_p(1,3)-grid_coor(:,3)).^2 ),:);
    end


    for pk=1:1:size_protential_protein(1,1)
        clear pku grid_test Matr centr_a;
        P_sh(:,5)=0;
        
        pku(:,1)=(P_sh(:,1)-protential_protein(pk,1)).^2;
        pku(:,2)=(P_sh(:,2)-protential_protein(pk,2)).^2;
        pku(:,3)=(P_sh(:,3)-protential_protein(pk,3)).^2;
        
        
%        pku(:,4)=(pku(:,1)+pku(:,2)+pku(:,3)).^0.5;
        pku(((pku(:,1)+pku(:,2)+pku(:,3)).^0.5<=3.2),4)=1;        
        P_sh(pku(:,4)==1,5)=1;

        P_sh(P_sh(:,5)==1,:)=[];

    end


    P_sh(:,5)=[];
    size_P_sh=size(P_sh);
    size_Matr_car=size(Matr_car);
    Matr_car(1+size_Matr_car(1,1):size_Matr_car(1,1)+size_P_sh(1,1),1:4)=P_sh;

    Matr_car(1+size_Matr_car(1,1):size_Matr_car(1,1)+size_P_sh(1,1),5)=vdw_max(23,2);
    
    
    
    
    if norm(centroidb(zz,1:3)-cent_p(1,1:3))<=vdw_max(3,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(3,1))<=0.1,:);
    elseif norm(centroidb(zz,1:3)-cent_p(1,1:3))>vdw_max(3,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(3,1))<=0.1&    sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 + (centroidb(zz,1)-cent_p(1,1)).^2+(centroidb(zz,2)-cent_p(1,2)).^2+(centroidb(zz,3)-cent_p(1,3)).^2 )> sqrt( (cent_p(1,1)-grid_coor(:,1)).^2+(cent_p(1,2)-grid_coor(:,2)).^2+(cent_p(1,3)-grid_coor(:,3)).^2 ),:);
    end


    for pk=1:1:size_protential_protein(1,1)
        clear pku grid_test Matr centr_a;
        P_sh(:,5)=0;
        
        pku(:,1)=(P_sh(:,1)-protential_protein(pk,1)).^2;
        pku(:,2)=(P_sh(:,2)-protential_protein(pk,2)).^2;
        pku(:,3)=(P_sh(:,3)-protential_protein(pk,3)).^2;
        
        
%        pku(:,4)=(pku(:,1)+pku(:,2)+pku(:,3)).^0.5;
        pku(((pku(:,1)+pku(:,2)+pku(:,3)).^0.5<=3.2),4)=1;        
        P_sh(pku(:,4)==1,5)=1;

        P_sh(P_sh(:,5)==1,:)=[];

    end


    P_sh(:,5)=[];
    size_P_sh=size(P_sh);
    size_Matr_c3=size(Matr_c3);
    Matr_c3(1+size_Matr_c3(1,1):size_Matr_c3(1,1)+size_P_sh(1,1),1:4)=P_sh;

    Matr_c3(1+size_Matr_c3(1,1):size_Matr_c3(1,1)+size_P_sh(1,1),5)=vdw_max(3,2);
    
    
    if norm(centroidb(zz,1:3)-cent_p(1,1:3))<=vdw_max(22,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(22,1))<=0.1,:);
    elseif norm(centroidb(zz,1:3)-cent_p(1,1:3))>vdw_max(22,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(22,1))<=0.1&    sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 + (centroidb(zz,1)-cent_p(1,1)).^2+(centroidb(zz,2)-cent_p(1,2)).^2+(centroidb(zz,3)-cent_p(1,3)).^2 )> sqrt( (cent_p(1,1)-grid_coor(:,1)).^2+(cent_p(1,2)-grid_coor(:,2)).^2+(cent_p(1,3)-grid_coor(:,3)).^2 ),:);
    end


    for pk=1:1:size_protential_protein(1,1)
        clear pku grid_test Matr centr_a;
        P_sh(:,5)=0;
        
        pku(:,1)=(P_sh(:,1)-protential_protein(pk,1)).^2;
        pku(:,2)=(P_sh(:,2)-protential_protein(pk,2)).^2;
        pku(:,3)=(P_sh(:,3)-protential_protein(pk,3)).^2;
        
        
%        pku(:,4)=(pku(:,1)+pku(:,2)+pku(:,3)).^0.5;
        pku(((pku(:,1)+pku(:,2)+pku(:,3)).^0.5<=3.2),4)=1;        
        P_sh(pku(:,4)==1,5)=1;

        P_sh(P_sh(:,5)==1,:)=[];

    end


    P_sh(:,5)=[];
    size_P_sh=size(P_sh);
    size_Matr_c2=size(Matr_c2);
    Matr_c2(1+size_Matr_c2(1,1):size_Matr_c2(1,1)+size_P_sh(1,1),1:4)=P_sh;

    Matr_c2(1+size_Matr_c2(1,1):size_Matr_c2(1,1)+size_P_sh(1,1),5)=vdw_max(22,2);
    
    
    
    
    
    
    
    elseif centroidb(zz,4)==4
        max_x=max(centroidb(:,1))+4.8;
        min_x=min(centroidb(:,1))-4.8;
        max_y=max(centroidb(:,2))+4.8;
        min_y=min(centroidb(:,2))-4.8;
        max_z=max(centroidb(:,3))+4.8;
        min_z=min(centroidb(:,3))-4.8;
        sig_x=transpose(min_x:grid_d:max_x);
        sig_y=transpose(min_y:grid_d:max_y);
        sig_z=transpose(min_z:grid_d:max_z);
        
        size_sig_x=size(sig_x);
        size_sig_y=size(sig_y);
        size_sig_z=size(sig_z);
    
    
    for i=1:1:size_sig_x(1,1)
        grid_coor((i-1)*size_sig_y(1,1)*size_sig_z(1,1)+1:i*size_sig_y(1,1)*size_sig_z(1,1),1)=sig_x(i,1);
    end
    
    for i=1:1:size_sig_y(1,1)
        grid_coor1r((i-1)*size_sig_z(1,1)+1:i*size_sig_z(1,1),1)=sig_y(i,1);
    end
    
    grid_coor(:,2)=repmat(grid_coor1r,size_sig_x(1,1),1);
    grid_coor(:,3)=repmat(sig_z,size_sig_x(1,1)*size_sig_y(1,1),1);
    grid_coor(:,4)=zz;
    size_grid_coor=size(grid_coor);
    
    if norm(centroidb(zz,1:3)-cent_p(1,1:3))<=vdw_max(4,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(4,1))<=0.1,:);
    elseif norm(centroidb(zz,1:3)-cent_p(1,1:3))>vdw_max(4,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(4,1))<=0.1&    sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 + (centroidb(zz,1)-cent_p(1,1)).^2+(centroidb(zz,2)-cent_p(1,2)).^2+(centroidb(zz,3)-cent_p(1,3)).^2 )> sqrt( (cent_p(1,1)-grid_coor(:,1)).^2+(cent_p(1,2)-grid_coor(:,2)).^2+(cent_p(1,3)-grid_coor(:,3)).^2 ),:);
    end
    
    for pk=1:1:size_protential_protein(1,1)
        clear pku grid_test Matr centr_a;
        P_sh(:,5)=0;
        
        pku(:,1)=(P_sh(:,1)-protential_protein(pk,1)).^2;
        pku(:,2)=(P_sh(:,2)-protential_protein(pk,2)).^2;
        pku(:,3)=(P_sh(:,3)-protential_protein(pk,3)).^2;
        
        
%        pku(:,4)=(pku(:,1)+pku(:,2)+pku(:,3)).^0.5;
        pku(((pku(:,1)+pku(:,2)+pku(:,3)).^0.5<=3.2),4)=1;        
        P_sh(pku(:,4)==1,5)=1;

        P_sh(P_sh(:,5)==1,:)=[];

    end


    P_sh(:,5)=[];


    size_P_sh=size(P_sh);
    size_Matr_c3=size(Matr_c3);
    Matr_c3(1+size_Matr_c3(1,1):size_Matr_c3(1,1)+size_P_sh(1,1),1:4)=P_sh;

    Matr_c3(1+size_Matr_c3(1,1):size_Matr_c3(1,1)+size_P_sh(1,1),5)=vdw_max(4,2);
    
    
    if norm(centroidb(zz,1:3)-cent_p(1,1:3))<=vdw_max(33,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(33,1))<=0.1,:);
    elseif norm(centroidb(zz,1:3)-cent_p(1,1:3))>vdw_max(33,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(33,1))<=0.1&    sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 + (centroidb(zz,1)-cent_p(1,1)).^2+(centroidb(zz,2)-cent_p(1,2)).^2+(centroidb(zz,3)-cent_p(1,3)).^2 )> sqrt( (cent_p(1,1)-grid_coor(:,1)).^2+(cent_p(1,2)-grid_coor(:,2)).^2+(cent_p(1,3)-grid_coor(:,3)).^2 ),:);
    end

    for pk=1:1:size_protential_protein(1,1)
        clear pku grid_test Matr centr_a;
        P_sh(:,5)=0;
        
        pku(:,1)=(P_sh(:,1)-protential_protein(pk,1)).^2;
        pku(:,2)=(P_sh(:,2)-protential_protein(pk,2)).^2;
        pku(:,3)=(P_sh(:,3)-protential_protein(pk,3)).^2;
        
        
%        pku(:,4)=(pku(:,1)+pku(:,2)+pku(:,3)).^0.5;
        pku(((pku(:,1)+pku(:,2)+pku(:,3)).^0.5<=3.2),4)=1;        
        P_sh(pku(:,4)==1,5)=1;

        P_sh(P_sh(:,5)==1,:)=[];

    end


    P_sh(:,5)=[];

    size_P_sh=size(P_sh);
    size_P_sh=size(P_sh);
    size_Matr_car=size(Matr_car);
    Matr_car(1+size_Matr_car(1,1):size_Matr_car(1,1)+size_P_sh(1,1),1:4)=P_sh;

    Matr_car(1+size_Matr_car(1,1):size_Matr_car(1,1)+size_P_sh(1,1),5)=vdw_max(33,2);
    
    
    
    if norm(centroidb(zz,1:3)-cent_p(1,1:3))<=vdw_max(23,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(23,1))<=0.1,:);
    elseif norm(centroidb(zz,1:3)-cent_p(1,1:3))>vdw_max(23,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(23,1))<=0.1&    sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 + (centroidb(zz,1)-cent_p(1,1)).^2+(centroidb(zz,2)-cent_p(1,2)).^2+(centroidb(zz,3)-cent_p(1,3)).^2 )> sqrt( (cent_p(1,1)-grid_coor(:,1)).^2+(cent_p(1,2)-grid_coor(:,2)).^2+(cent_p(1,3)-grid_coor(:,3)).^2 ),:);
    end

    for pk=1:1:size_protential_protein(1,1)
        clear pku grid_test Matr centr_a;
        P_sh(:,5)=0;
        
        pku(:,1)=(P_sh(:,1)-protential_protein(pk,1)).^2;
        pku(:,2)=(P_sh(:,2)-protential_protein(pk,2)).^2;
        pku(:,3)=(P_sh(:,3)-protential_protein(pk,3)).^2;
        
        
%        pku(:,4)=(pku(:,1)+pku(:,2)+pku(:,3)).^0.5;
        pku(((pku(:,1)+pku(:,2)+pku(:,3)).^0.5<=3.2),4)=1;        
        P_sh(pku(:,4)==1,5)=1;

        P_sh(P_sh(:,5)==1,:)=[];

    end


    P_sh(:,5)=[];

    size_P_sh=size(P_sh);
    size_P_sh=size(P_sh);
    size_Matr_c2=size(Matr_c2);
    Matr_c2(1+size_Matr_c2(1,1):size_Matr_c2(1,1)+size_P_sh(1,1),1:4)=P_sh;

    Matr_c2(1+size_Matr_c2(1,1):size_Matr_c2(1,1)+size_P_sh(1,1),5)=vdw_max(23,2);
    
    
    
    
    
    elseif (centroidb(zz,4)==12|centroidb(zz,4)==8)&(centroidb(zz,6)==1|centroidb(zz,6)==2|centroidb(zz,6)==4)
        max_x=max(centroidb(zz,1))+4.0;
        min_x=min(centroidb(zz,1))-4.0;
        max_y=max(centroidb(zz,2))+4.0;
        min_y=min(centroidb(zz,2))-4.0;
        max_z=max(centroidb(zz,3))+4.0;
        min_z=min(centroidb(zz,3))-4.0;
        sig_x=transpose(min_x:grid_d:max_x);
        sig_y=transpose(min_y:grid_d:max_y);
        sig_z=transpose(min_z:grid_d:max_z);
        
        size_sig_x=size(sig_x);
        size_sig_y=size(sig_y);
        size_sig_z=size(sig_z);
    
    
    for i=1:1:size_sig_x(1,1)
        grid_coor((i-1)*size_sig_y(1,1)*size_sig_z(1,1)+1:i*size_sig_y(1,1)*size_sig_z(1,1),1)=sig_x(i,1);
    end
    
    for i=1:1:size_sig_y(1,1)
        grid_coor1r((i-1)*size_sig_z(1,1)+1:i*size_sig_z(1,1),1)=sig_y(i,1);
    end
    
    grid_coor(:,2)=repmat(grid_coor1r,size_sig_x(1,1),1);
    grid_coor(:,3)=repmat(sig_z,size_sig_x(1,1)*size_sig_y(1,1),1);
    grid_coor(:,4)=zz;
    size_grid_coor=size(grid_coor);
    
    
    
    if norm(centroidb(zz,1:3)-cent_p(1,1:3))<=vdw_max(69,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(69,1))<=0.1,:);
    elseif norm(centroidb(zz,1:3)-cent_p(1,1:3))>vdw_max(69,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(69,1))<=0.1&    sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 + (centroidb(zz,1)-cent_p(1,1)).^2+(centroidb(zz,2)-cent_p(1,2)).^2+(centroidb(zz,3)-cent_p(1,3)).^2 )> sqrt( (cent_p(1,1)-grid_coor(:,1)).^2+(cent_p(1,2)-grid_coor(:,2)).^2+(cent_p(1,3)-grid_coor(:,3)).^2 ),:);
    end

    
    for pk=1:1:size_protential_protein(1,1)
        clear pku grid_test Matr centr_a;
        P_sh(:,5)=0;
        
        pku(:,1)=(P_sh(:,1)-protential_protein(pk,1)).^2;
        pku(:,2)=(P_sh(:,2)-protential_protein(pk,2)).^2;
        pku(:,3)=(P_sh(:,3)-protential_protein(pk,3)).^2;
        
        
%        pku(:,4)=(pku(:,1)+pku(:,2)+pku(:,3)).^0.5;
        pku(((pku(:,1)+pku(:,2)+pku(:,3)).^0.5<=3.2),4)=1;        
        P_sh(pku(:,4)==1,5)=1;

        P_sh(P_sh(:,5)==1,:)=[];

    end
    
    P_sh(:,5)=[];
    size_P_sh=size(P_sh);
    size_Matr_cl=size(Matr_cl);
    Matr_cl(1+size_Matr_cl(1,1):size_Matr_cl(1,1)+size_P_sh(1,1),1:4)=P_sh;

    Matr_cl(1+size_Matr_cl(1,1):size_Matr_cl(1,1)+size_P_sh(1,1),5)=vdw_max(69,2);
    
    
    
    
    if norm(centroidb(zz,1:3)-cent_p(1,1:3))<=vdw_max(68,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(68,1))<=0.1,:);
    elseif norm(centroidb(zz,1:3)-cent_p(1,1:3))>vdw_max(68,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(68,1))<=0.1&    sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 + (centroidb(zz,1)-cent_p(1,1)).^2+(centroidb(zz,2)-cent_p(1,2)).^2+(centroidb(zz,3)-cent_p(1,3)).^2 )> sqrt( (cent_p(1,1)-grid_coor(:,1)).^2+(cent_p(1,2)-grid_coor(:,2)).^2+(cent_p(1,3)-grid_coor(:,3)).^2 ),:);
    end
    
    for pk=1:1:size_protential_protein(1,1)
        clear pku grid_test Matr centr_a;
        P_sh(:,5)=0;
        
        pku(:,1)=(P_sh(:,1)-protential_protein(pk,1)).^2;
        pku(:,2)=(P_sh(:,2)-protential_protein(pk,2)).^2;
        pku(:,3)=(P_sh(:,3)-protential_protein(pk,3)).^2;
        
        
%        pku(:,4)=(pku(:,1)+pku(:,2)+pku(:,3)).^0.5;
        pku(((pku(:,1)+pku(:,2)+pku(:,3)).^0.5<=2.9),4)=1;        
        P_sh(pku(:,4)==1,5)=1;

        P_sh(P_sh(:,5)==1,:)=[];

    end
 
    P_sh(:,5)=[];
    size_P_sh=size(P_sh);
    size_Matr_f=size(Matr_f);
    Matr_f(1+size_Matr_f(1,1):size_Matr_f(1,1)+size_P_sh(1,1),1:4)=P_sh;

    Matr_f(1+size_Matr_f(1,1):size_Matr_f(1,1)+size_P_sh(1,1),5)=vdw_max(68,2);
    
    
    
    if norm(centroidb(zz,1:3)-cent_p(1,1:3))<=vdw_max(70,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(70,1))<=0.1,:);
    elseif norm(centroidb(zz,1:3)-cent_p(1,1:3))>vdw_max(70,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(70,1))<=0.1&    sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 + (centroidb(zz,1)-cent_p(1,1)).^2+(centroidb(zz,2)-cent_p(1,2)).^2+(centroidb(zz,3)-cent_p(1,3)).^2 )> sqrt( (cent_p(1,1)-grid_coor(:,1)).^2+(cent_p(1,2)-grid_coor(:,2)).^2+(cent_p(1,3)-grid_coor(:,3)).^2 ),:);
    end
    
    for pk=1:1:size_protential_protein(1,1)
        clear pku grid_test Matr centr_a;
        P_sh(:,5)=0;
        
        pku(:,1)=(P_sh(:,1)-protential_protein(pk,1)).^2;
        pku(:,2)=(P_sh(:,2)-protential_protein(pk,2)).^2;
        pku(:,3)=(P_sh(:,3)-protential_protein(pk,3)).^2;
        
        
%        pku(:,4)=(pku(:,1)+pku(:,2)+pku(:,3)).^0.5;
        pku(((pku(:,1)+pku(:,2)+pku(:,3)).^0.5<=3.2),4)=1;        
        P_sh(pku(:,4)==1,5)=1;

        P_sh(P_sh(:,5)==1,:)=[];

    end
 
    P_sh(:,5)=[];
    size_P_sh=size(P_sh);
    size_Matr_br=size(Matr_br);
    Matr_br(1+size_Matr_br(1,1):size_Matr_br(1,1)+size_P_sh(1,1),1:4)=P_sh;

    Matr_br(1+size_Matr_br(1,1):size_Matr_br(1,1)+size_P_sh(1,1),5)=vdw_max(70,2);
    
    
    if norm(centroidb(zz,1:3)-cent_p(1,1:3))<=vdw_max(71,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(71,1))<=0.1,:);
    elseif norm(centroidb(zz,1:3)-cent_p(1,1:3))>vdw_max(71,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(71,1))<=0.1&    sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 + (centroidb(zz,1)-cent_p(1,1)).^2+(centroidb(zz,2)-cent_p(1,2)).^2+(centroidb(zz,3)-cent_p(1,3)).^2 )> sqrt( (cent_p(1,1)-grid_coor(:,1)).^2+(cent_p(1,2)-grid_coor(:,2)).^2+(cent_p(1,3)-grid_coor(:,3)).^2 ),:);
    end
    
    for pk=1:1:size_protential_protein(1,1)
        clear pku grid_test Matr centr_a;
        P_sh(:,5)=0;
        
        pku(:,1)=(P_sh(:,1)-protential_protein(pk,1)).^2;
        pku(:,2)=(P_sh(:,2)-protential_protein(pk,2)).^2;
        pku(:,3)=(P_sh(:,3)-protential_protein(pk,3)).^2;
        
        
%        pku(:,4)=(pku(:,1)+pku(:,2)+pku(:,3)).^0.5;
        pku(((pku(:,1)+pku(:,2)+pku(:,3)).^0.5<=3.2),4)=1;        
        P_sh(pku(:,4)==1,5)=1;

        P_sh(P_sh(:,5)==1,:)=[];

    end
 
    P_sh(:,5)=[];
    size_P_sh=size(P_sh);
    size_Matr_i=size(Matr_i);
    Matr_i(1+size_Matr_i(1,1):size_Matr_i(1,1)+size_P_sh(1,1),1:4)=P_sh;

    Matr_i(1+size_Matr_i(1,1):size_Matr_i(1,1)+size_P_sh(1,1),5)=vdw_max(71,2);
    
    
    
    
    if norm(centroidb(zz,1:3)-cent_p(1,1:3))<=vdw_max(79,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(79,1))<=0.1,:);
    elseif norm(centroidb(zz,1:3)-cent_p(1,1:3))>vdw_max(79,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(79,1))<=0.1&    sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 + (centroidb(zz,1)-cent_p(1,1)).^2+(centroidb(zz,2)-cent_p(1,2)).^2+(centroidb(zz,3)-cent_p(1,3)).^2 )> sqrt( (cent_p(1,1)-grid_coor(:,1)).^2+(cent_p(1,2)-grid_coor(:,2)).^2+(cent_p(1,3)-grid_coor(:,3)).^2 ),:);
    end
    
    for pk=1:1:size_protential_protein(1,1)
        clear pku grid_test Matr centr_a;
        P_sh(:,5)=0;
        
        pku(:,1)=(P_sh(:,1)-protential_protein(pk,1)).^2;
        pku(:,2)=(P_sh(:,2)-protential_protein(pk,2)).^2;
        pku(:,3)=(P_sh(:,3)-protential_protein(pk,3)).^2;
        
        
%        pku(:,4)=(pku(:,1)+pku(:,2)+pku(:,3)).^0.5;
        pku(((pku(:,1)+pku(:,2)+pku(:,3)).^0.5<=2.5),4)=1;        
        P_sh(pku(:,4)==1,5)=1;

        P_sh(P_sh(:,5)==1,:)=[];

    end

 
    P_sh(:,5)=[];

    size_P_sh=size(P_sh);
    size_Matr_o2HB=size(Matr_o2HB);
    Matr_o2HB(1+size_Matr_o2HB(1,1):size_Matr_o2HB(1,1)+size_P_sh(1,1),1:4)=P_sh;

    Matr_o2HB(1+size_Matr_o2HB(1,1):size_Matr_o2HB(1,1)+size_P_sh(1,1),5)=vdw_max(79,2);
    
    

    if norm(centroidb(zz,1:3)-cent_p(1,1:3))<=vdw_max(78,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(78,1))<=0.1,:);
    elseif norm(centroidb(zz,1:3)-cent_p(1,1:3))>vdw_max(78,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(78,1))<=0.1&    sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 + (centroidb(zz,1)-cent_p(1,1)).^2+(centroidb(zz,2)-cent_p(1,2)).^2+(centroidb(zz,3)-cent_p(1,3)).^2 )> sqrt( (cent_p(1,1)-grid_coor(:,1)).^2+(cent_p(1,2)-grid_coor(:,2)).^2+(cent_p(1,3)-grid_coor(:,3)).^2 ),:);
    end
    
    for pk=1:1:size_protential_protein(1,1)
        clear pku grid_test Matr centr_a;
        P_sh(:,5)=0;
        
        pku(:,1)=(P_sh(:,1)-protential_protein(pk,1)).^2;
        pku(:,2)=(P_sh(:,2)-protential_protein(pk,2)).^2;
        pku(:,3)=(P_sh(:,3)-protential_protein(pk,3)).^2;
        
        
%        pku(:,4)=(pku(:,1)+pku(:,2)+pku(:,3)).^0.5;
        pku(((pku(:,1)+pku(:,2)+pku(:,3)).^0.5<=2.5),4)=1;        
        P_sh(pku(:,4)==1,5)=1;

        P_sh(P_sh(:,5)==1,:)=[];

    end

    P_sh(:,5)=[];

    size_P_sh=size(P_sh);
    size_Matr_o3HB=size(Matr_o3HB);
    Matr_o3HB(1+size_Matr_o3HB(1,1):size_Matr_o3HB(1,1)+size_P_sh(1,1),1:4)=P_sh;

    Matr_o3HB(1+size_Matr_o3HB(1,1):size_Matr_o3HB(1,1)+size_P_sh(1,1),5)=vdw_max(78,2);
    
    
    if norm(centroidb(zz,1:3)-cent_p(1,1:3))<=vdw_max(77,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(77,1))<=0.1,:);
    elseif norm(centroidb(zz,1:3)-cent_p(1,1:3))>vdw_max(77,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(77,1))<=0.1&    sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 + (centroidb(zz,1)-cent_p(1,1)).^2+(centroidb(zz,2)-cent_p(1,2)).^2+(centroidb(zz,3)-cent_p(1,3)).^2 )> sqrt( (cent_p(1,1)-grid_coor(:,1)).^2+(cent_p(1,2)-grid_coor(:,2)).^2+(cent_p(1,3)-grid_coor(:,3)).^2 ),:);
    end
    for pk=1:1:size_protential_protein(1,1)
        clear pku grid_test Matr centr_a;
        P_sh(:,5)=0;
        
        pku(:,1)=(P_sh(:,1)-protential_protein(pk,1)).^2;
        pku(:,2)=(P_sh(:,2)-protential_protein(pk,2)).^2;
        pku(:,3)=(P_sh(:,3)-protential_protein(pk,3)).^2;
        
        
%        pku(:,4)=(pku(:,1)+pku(:,2)+pku(:,3)).^0.5;
        pku(((pku(:,1)+pku(:,2)+pku(:,3)).^0.5<=3.3),4)=1;        
        P_sh(pku(:,4)==1,5)=1;

        P_sh(P_sh(:,5)==1,:)=[];

    end

 
    P_sh(:,5)=[];
    size_P_sh=size(P_sh);
    size_Matr_sHB=size(Matr_sHB);
    Matr_sHB(1+size_Matr_sHB(1,1):size_Matr_sHB(1,1)+size_P_sh(1,1),1:4)=P_sh;

    Matr_sHB(1+size_Matr_sHB(1,1):size_Matr_sHB(1,1)+size_P_sh(1,1),5)=vdw_max(77,2);
    
    
    elseif (centroidb(zz,4)==13)&(centroidb(zz,6)==1|centroidb(zz,6)==2|centroidb(zz,6)==4)
        max_x=max(centroidb(zz,1))+4.5;
        min_x=min(centroidb(zz,1))-4.5;
        max_y=max(centroidb(zz,2))+4.5;
        min_y=min(centroidb(zz,2))-4.5;
        max_z=max(centroidb(zz,3))+4.5;
        min_z=min(centroidb(zz,3))-4.5;
        sig_x=transpose(min_x:grid_d:max_x);
        sig_y=transpose(min_y:grid_d:max_y);
        sig_z=transpose(min_z:grid_d:max_z);
        
        size_sig_x=size(sig_x);
        size_sig_y=size(sig_y);
        size_sig_z=size(sig_z);
    
    
    for i=1:1:size_sig_x(1,1)
        grid_coor((i-1)*size_sig_y(1,1)*size_sig_z(1,1)+1:i*size_sig_y(1,1)*size_sig_z(1,1),1)=sig_x(i,1);
    end
    
    for i=1:1:size_sig_y(1,1)
        grid_coor1r((i-1)*size_sig_z(1,1)+1:i*size_sig_z(1,1),1)=sig_y(i,1);
    end
    
    grid_coor(:,2)=repmat(grid_coor1r,size_sig_x(1,1),1);
    grid_coor(:,3)=repmat(sig_z,size_sig_x(1,1)*size_sig_y(1,1),1);
    grid_coor(:,4)=zz;
    size_grid_coor=size(grid_coor);
    
    if norm(centroidb(zz,1:3)-cent_p(1,1:3))<=vdw_max(83,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(83,1))<=0.1,:);
    elseif norm(centroidb(zz,1:3)-cent_p(1,1:3))>vdw_max(83,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(83,1))<=0.1&    sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 + (centroidb(zz,1)-cent_p(1,1)).^2+(centroidb(zz,2)-cent_p(1,2)).^2+(centroidb(zz,3)-cent_p(1,3)).^2 )> sqrt( (cent_p(1,1)-grid_coor(:,1)).^2+(cent_p(1,2)-grid_coor(:,2)).^2+(cent_p(1,3)-grid_coor(:,3)).^2 ),:);
    end

    for pk=1:1:size_protential_protein(1,1)
        clear pku grid_test Matr centr_a;
        P_sh(:,5)=0;
        
        pku(:,1)=(P_sh(:,1)-protential_protein(pk,1)).^2;
        pku(:,2)=(P_sh(:,2)-protential_protein(pk,2)).^2;
        pku(:,3)=(P_sh(:,3)-protential_protein(pk,3)).^2;
        
        
%        pku(:,4)=(pku(:,1)+pku(:,2)+pku(:,3)).^0.5;
        pku(((pku(:,1)+pku(:,2)+pku(:,3)).^0.5<=2.5),4)=1;        
        P_sh(pku(:,4)==1,5)=1;

        P_sh(P_sh(:,5)==1,:)=[];

    end
 
    P_sh(:,5)=[];

    size_P_sh=size(P_sh);
    size_Matr_o2HB=size(Matr_o2HB);
    Matr_o2HB(1+size_Matr_o2HB(1,1):size_Matr_o2HB(1,1)+size_P_sh(1,1),1:4)=P_sh;

    Matr_o2HB(1+size_Matr_o2HB(1,1):size_Matr_o2HB(1,1)+size_P_sh(1,1),5)=vdw_max(83,2);

    
    if norm(centroidb(zz,1:3)-cent_p(1,1:3))<=vdw_max(82,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(82,1))<=0.1,:);
    elseif norm(centroidb(zz,1:3)-cent_p(1,1:3))>vdw_max(82,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(82,1))<=0.1&    sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 + (centroidb(zz,1)-cent_p(1,1)).^2+(centroidb(zz,2)-cent_p(1,2)).^2+(centroidb(zz,3)-cent_p(1,3)).^2 )> sqrt( (cent_p(1,1)-grid_coor(:,1)).^2+(cent_p(1,2)-grid_coor(:,2)).^2+(cent_p(1,3)-grid_coor(:,3)).^2 ),:);
    end

    for pk=1:1:size_protential_protein(1,1)
        clear pku grid_test Matr centr_a;
        P_sh(:,5)=0;
        
        pku(:,1)=(P_sh(:,1)-protential_protein(pk,1)).^2;
        pku(:,2)=(P_sh(:,2)-protential_protein(pk,2)).^2;
        pku(:,3)=(P_sh(:,3)-protential_protein(pk,3)).^2;
        
        
%        pku(:,4)=(pku(:,1)+pku(:,2)+pku(:,3)).^0.5;
        pku(((pku(:,1)+pku(:,2)+pku(:,3)).^0.5<=2.5),4)=1;        
        P_sh(pku(:,4)==1,5)=1;

        P_sh(P_sh(:,5)==1,:)=[];

    end
 
    P_sh(:,5)=[];

    size_P_sh=size(P_sh);
    size_Matr_o3HB=size(Matr_o3HB);
    Matr_o3HB(1+size_Matr_o3HB(1,1):size_Matr_o3HB(1,1)+size_P_sh(1,1),1:4)=P_sh;

    Matr_o3HB(1+size_Matr_o3HB(1,1):size_Matr_o3HB(1,1)+size_P_sh(1,1),5)=vdw_max(82,2);
    
    
    if norm(centroidb(zz,1:3)-cent_p(1,1:3))<=vdw_max(84,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(84,1))<=0.1,:);
    elseif norm(centroidb(zz,1:3)-cent_p(1,1:3))>vdw_max(84,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(84,1))<=0.1&    sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 + (centroidb(zz,1)-cent_p(1,1)).^2+(centroidb(zz,2)-cent_p(1,2)).^2+(centroidb(zz,3)-cent_p(1,3)).^2 )> sqrt( (cent_p(1,1)-grid_coor(:,1)).^2+(cent_p(1,2)-grid_coor(:,2)).^2+(cent_p(1,3)-grid_coor(:,3)).^2 ),:);
    end

    for pk=1:1:size_protential_protein(1,1)
        clear pku grid_test Matr centr_a;
        P_sh(:,5)=0;
        
        pku(:,1)=(P_sh(:,1)-protential_protein(pk,1)).^2;
        pku(:,2)=(P_sh(:,2)-protential_protein(pk,2)).^2;
        pku(:,3)=(P_sh(:,3)-protential_protein(pk,3)).^2;
        
        
%        pku(:,4)=(pku(:,1)+pku(:,2)+pku(:,3)).^0.5;
        pku(((pku(:,1)+pku(:,2)+pku(:,3)).^0.5<=3.3),4)=1;        
        P_sh(pku(:,4)==1,5)=1;

        P_sh(P_sh(:,5)==1,:)=[];

    end


    P_sh(:,5)=[];

    size_P_sh=size(P_sh);
    size_Matr_sHB=size(Matr_sHB);
    Matr_sHB(1+size_Matr_sHB(1,1):size_Matr_sHB(1,1)+size_P_sh(1,1),1:4)=P_sh;

    Matr_sHB(1+size_Matr_sHB(1,1):size_Matr_sHB(1,1)+size_P_sh(1,1),5)=vdw_max(84,2);
    
    
    
    
    if norm(centroidb(zz,1:3)-cent_p(1,1:3))<=vdw_max(69,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(69,1))<=0.1,:);
    elseif norm(centroidb(zz,1:3)-cent_p(1,1:3))>vdw_max(69,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(69,1))<=0.1&    sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 + (centroidb(zz,1)-cent_p(1,1)).^2+(centroidb(zz,2)-cent_p(1,2)).^2+(centroidb(zz,3)-cent_p(1,3)).^2 )> sqrt( (cent_p(1,1)-grid_coor(:,1)).^2+(cent_p(1,2)-grid_coor(:,2)).^2+(cent_p(1,3)-grid_coor(:,3)).^2 ),:);
    end

    
    for pk=1:1:size_protential_protein(1,1)
        clear pku grid_test Matr centr_a;
        P_sh(:,5)=0;
        
        pku(:,1)=(P_sh(:,1)-protential_protein(pk,1)).^2;
        pku(:,2)=(P_sh(:,2)-protential_protein(pk,2)).^2;
        pku(:,3)=(P_sh(:,3)-protential_protein(pk,3)).^2;
        
        
%        pku(:,4)=(pku(:,1)+pku(:,2)+pku(:,3)).^0.5;
        pku(((pku(:,1)+pku(:,2)+pku(:,3)).^0.5<=3.2),4)=1;        
        P_sh(pku(:,4)==1,5)=1;

        P_sh(P_sh(:,5)==1,:)=[];

    end
    
    P_sh(:,5)=[];
    size_P_sh=size(P_sh);
    size_Matr_cl=size(Matr_cl);
    Matr_cl(1+size_Matr_cl(1,1):size_Matr_cl(1,1)+size_P_sh(1,1),1:4)=P_sh;

    Matr_cl(1+size_Matr_cl(1,1):size_Matr_cl(1,1)+size_P_sh(1,1),5)=vdw_max(69,2);
    
    
    
    
    if norm(centroidb(zz,1:3)-cent_p(1,1:3))<=vdw_max(68,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(68,1))<=0.1,:);
    elseif norm(centroidb(zz,1:3)-cent_p(1,1:3))>vdw_max(68,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(68,1))<=0.1&    sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 + (centroidb(zz,1)-cent_p(1,1)).^2+(centroidb(zz,2)-cent_p(1,2)).^2+(centroidb(zz,3)-cent_p(1,3)).^2 )> sqrt( (cent_p(1,1)-grid_coor(:,1)).^2+(cent_p(1,2)-grid_coor(:,2)).^2+(cent_p(1,3)-grid_coor(:,3)).^2 ),:);
    end
    
    for pk=1:1:size_protential_protein(1,1)
        clear pku grid_test Matr centr_a;
        P_sh(:,5)=0;
        
        pku(:,1)=(P_sh(:,1)-protential_protein(pk,1)).^2;
        pku(:,2)=(P_sh(:,2)-protential_protein(pk,2)).^2;
        pku(:,3)=(P_sh(:,3)-protential_protein(pk,3)).^2;
        
        
%        pku(:,4)=(pku(:,1)+pku(:,2)+pku(:,3)).^0.5;
        pku(((pku(:,1)+pku(:,2)+pku(:,3)).^0.5<=2.9),4)=1;        
        P_sh(pku(:,4)==1,5)=1;

        P_sh(P_sh(:,5)==1,:)=[];

    end
 
    P_sh(:,5)=[];
    size_P_sh=size(P_sh);
    size_Matr_f=size(Matr_f);
    Matr_f(1+size_Matr_f(1,1):size_Matr_f(1,1)+size_P_sh(1,1),1:4)=P_sh;

    Matr_f(1+size_Matr_f(1,1):size_Matr_f(1,1)+size_P_sh(1,1),5)=vdw_max(68,2);
    
    
    
    if norm(centroidb(zz,1:3)-cent_p(1,1:3))<=vdw_max(70,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(70,1))<=0.1,:);
    elseif norm(centroidb(zz,1:3)-cent_p(1,1:3))>vdw_max(70,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(70,1))<=0.1&    sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 + (centroidb(zz,1)-cent_p(1,1)).^2+(centroidb(zz,2)-cent_p(1,2)).^2+(centroidb(zz,3)-cent_p(1,3)).^2 )> sqrt( (cent_p(1,1)-grid_coor(:,1)).^2+(cent_p(1,2)-grid_coor(:,2)).^2+(cent_p(1,3)-grid_coor(:,3)).^2 ),:);
    end
    
    for pk=1:1:size_protential_protein(1,1)
        clear pku grid_test Matr centr_a;
        P_sh(:,5)=0;
        
        pku(:,1)=(P_sh(:,1)-protential_protein(pk,1)).^2;
        pku(:,2)=(P_sh(:,2)-protential_protein(pk,2)).^2;
        pku(:,3)=(P_sh(:,3)-protential_protein(pk,3)).^2;
        
        
%        pku(:,4)=(pku(:,1)+pku(:,2)+pku(:,3)).^0.5;
        pku(((pku(:,1)+pku(:,2)+pku(:,3)).^0.5<=3.2),4)=1;        
        P_sh(pku(:,4)==1,5)=1;

        P_sh(P_sh(:,5)==1,:)=[];

    end
 
    P_sh(:,5)=[];
    size_P_sh=size(P_sh);
    size_Matr_br=size(Matr_br);
    Matr_br(1+size_Matr_br(1,1):size_Matr_br(1,1)+size_P_sh(1,1),1:4)=P_sh;

    Matr_br(1+size_Matr_br(1,1):size_Matr_br(1,1)+size_P_sh(1,1),5)=vdw_max(70,2);
    
    
    if norm(centroidb(zz,1:3)-cent_p(1,1:3))<=vdw_max(71,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(71,1))<=0.1,:);
    elseif norm(centroidb(zz,1:3)-cent_p(1,1:3))>vdw_max(71,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(71,1))<=0.1&    sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 + (centroidb(zz,1)-cent_p(1,1)).^2+(centroidb(zz,2)-cent_p(1,2)).^2+(centroidb(zz,3)-cent_p(1,3)).^2 )> sqrt( (cent_p(1,1)-grid_coor(:,1)).^2+(cent_p(1,2)-grid_coor(:,2)).^2+(cent_p(1,3)-grid_coor(:,3)).^2 ),:);
    end
    
    for pk=1:1:size_protential_protein(1,1)
        clear pku grid_test Matr centr_a;
        P_sh(:,5)=0;
        
        pku(:,1)=(P_sh(:,1)-protential_protein(pk,1)).^2;
        pku(:,2)=(P_sh(:,2)-protential_protein(pk,2)).^2;
        pku(:,3)=(P_sh(:,3)-protential_protein(pk,3)).^2;
        
        
%        pku(:,4)=(pku(:,1)+pku(:,2)+pku(:,3)).^0.5;
        pku(((pku(:,1)+pku(:,2)+pku(:,3)).^0.5<=3.2),4)=1;        
        P_sh(pku(:,4)==1,5)=1;

        P_sh(P_sh(:,5)==1,:)=[];

    end
 
    P_sh(:,5)=[];
    size_P_sh=size(P_sh);
    size_Matr_i=size(Matr_i);
    Matr_i(1+size_Matr_i(1,1):size_Matr_i(1,1)+size_P_sh(1,1),1:4)=P_sh;

    Matr_i(1+size_Matr_i(1,1):size_Matr_i(1,1)+size_P_sh(1,1),5)=vdw_max(71,2);
    
    
    
    
    elseif (centroidb(zz,4)==6)
        max_x=max(centroidb(zz,1))+4.5;
        min_x=min(centroidb(zz,1))-4.5;
        max_y=max(centroidb(zz,2))+4.5;
        min_y=min(centroidb(zz,2))-4.5;
        max_z=max(centroidb(zz,3))+4.5;
        min_z=min(centroidb(zz,3))-4.5;
        sig_x=transpose(min_x:grid_d:max_x);
        sig_y=transpose(min_y:grid_d:max_y);
        sig_z=transpose(min_z:grid_d:max_z);
        
        size_sig_x=size(sig_x);
        size_sig_y=size(sig_y);
        size_sig_z=size(sig_z);
    
    
    for i=1:1:size_sig_x(1,1)
        grid_coor((i-1)*size_sig_y(1,1)*size_sig_z(1,1)+1:i*size_sig_y(1,1)*size_sig_z(1,1),1)=sig_x(i,1);
    end
    
    for i=1:1:size_sig_y(1,1)
        grid_coor1r((i-1)*size_sig_z(1,1)+1:i*size_sig_z(1,1),1)=sig_y(i,1);
    end
    
    grid_coor(:,2)=repmat(grid_coor1r,size_sig_x(1,1),1);
    grid_coor(:,3)=repmat(sig_z,size_sig_x(1,1)*size_sig_y(1,1),1);
    grid_coor(:,4)=zz;
    size_grid_coor=size(grid_coor);
    
    
    if norm(centroidb(zz,1:3)-cent_p(1,1:3))<=vdw_max(53,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(53,1))<=0.1,:);
    elseif norm(centroidb(zz,1:3)-cent_p(1,1:3))>vdw_max(53,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(53,1))<=0.1&    sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 + (centroidb(zz,1)-cent_p(1,1)).^2+(centroidb(zz,2)-cent_p(1,2)).^2+(centroidb(zz,3)-cent_p(1,3)).^2 )> sqrt( (cent_p(1,1)-grid_coor(:,1)).^2+(cent_p(1,2)-grid_coor(:,2)).^2+(cent_p(1,3)-grid_coor(:,3)).^2 ),:);
    end

    for pk=1:1:size_protential_protein(1,1)
        clear pku grid_test Matr centr_a;
        P_sh(:,5)=0;
        
        pku(:,1)=(P_sh(:,1)-protential_protein(pk,1)).^2;
        pku(:,2)=(P_sh(:,2)-protential_protein(pk,2)).^2;
        pku(:,3)=(P_sh(:,3)-protential_protein(pk,3)).^2;
        
        
%        pku(:,4)=(pku(:,1)+pku(:,2)+pku(:,3)).^0.5;
        pku(((pku(:,1)+pku(:,2)+pku(:,3)).^0.5<=3.2),4)=1;        
        P_sh(pku(:,4)==1,5)=1;

        P_sh(P_sh(:,5)==1,:)=[];

    end

 
    P_sh(:,5)=[];

    size_P_sh=size(P_sh);
    size_Matr_br=size(Matr_br);
    Matr_br(1+size_Matr_br(1,1):size_Matr_br(1,1)+size_P_sh(1,1),1:4)=P_sh;

    Matr_br(1+size_Matr_br(1,1):size_Matr_br(1,1)+size_P_sh(1,1),5)=vdw_max(53,2);
    
    if norm(centroidb(zz,1:3)-cent_p(1,1:3))<=vdw_max(52,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(52,1))<=0.1,:);
    elseif norm(centroidb(zz,1:3)-cent_p(1,1:3))>vdw_max(52,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(52,1))<=0.1&    sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 + (centroidb(zz,1)-cent_p(1,1)).^2+(centroidb(zz,2)-cent_p(1,2)).^2+(centroidb(zz,3)-cent_p(1,3)).^2 )> sqrt( (cent_p(1,1)-grid_coor(:,1)).^2+(cent_p(1,2)-grid_coor(:,2)).^2+(cent_p(1,3)-grid_coor(:,3)).^2 ),:);
    end

    for pk=1:1:size_protential_protein(1,1)
        clear pku grid_test Matr centr_a;
        P_sh(:,5)=0;
        
        pku(:,1)=(P_sh(:,1)-protential_protein(pk,1)).^2;
        pku(:,2)=(P_sh(:,2)-protential_protein(pk,2)).^2;
        pku(:,3)=(P_sh(:,3)-protential_protein(pk,3)).^2;
        
        
%        pku(:,4)=(pku(:,1)+pku(:,2)+pku(:,3)).^0.5;
        pku(((pku(:,1)+pku(:,2)+pku(:,3)).^0.5<=3.2),4)=1;        
        P_sh(pku(:,4)==1,5)=1;

        P_sh(P_sh(:,5)==1,:)=[];

    end
 
    P_sh(:,5)=[];
    size_P_sh=size(P_sh);
    size_Matr_cl=size(Matr_cl);
    Matr_cl(1+size_Matr_cl(1,1):size_Matr_cl(1,1)+size_P_sh(1,1),1:4)=P_sh;

    Matr_cl(1+size_Matr_cl(1,1):size_Matr_cl(1,1)+size_P_sh(1,1),5)=vdw_max(52,2);
    
    
    if norm(centroidb(zz,1:3)-cent_p(1,1:3))<=vdw_max(51,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(51,1))<=0.1,:);
    elseif norm(centroidb(zz,1:3)-cent_p(1,1:3))>vdw_max(51,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(51,1))<=0.1&    sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 + (centroidb(zz,1)-cent_p(1,1)).^2+(centroidb(zz,2)-cent_p(1,2)).^2+(centroidb(zz,3)-cent_p(1,3)).^2 )> sqrt( (cent_p(1,1)-grid_coor(:,1)).^2+(cent_p(1,2)-grid_coor(:,2)).^2+(cent_p(1,3)-grid_coor(:,3)).^2 ),:);
    end
    
    
    for pk=1:1:size_protential_protein(1,1)
        clear pku grid_test Matr centr_a;
        P_sh(:,5)=0;
        
        pku(:,1)=(P_sh(:,1)-protential_protein(pk,1)).^2;
        pku(:,2)=(P_sh(:,2)-protential_protein(pk,2)).^2;
        pku(:,3)=(P_sh(:,3)-protential_protein(pk,3)).^2;
        
        
%        pku(:,4)=(pku(:,1)+pku(:,2)+pku(:,3)).^0.5;
        pku(((pku(:,1)+pku(:,2)+pku(:,3)).^0.5<=2.9),4)=1;        
        P_sh(pku(:,4)==1,5)=1;

        P_sh(P_sh(:,5)==1,:)=[];

    end


 
    P_sh(:,5)=[];

    size_P_sh=size(P_sh);
    size_Matr_f=size(Matr_f);
    Matr_f(1+size_Matr_f(1,1):size_Matr_f(1,1)+size_P_sh(1,1),1:4)=P_sh;

    Matr_f(1+size_Matr_f(1,1):size_Matr_f(1,1)+size_P_sh(1,1),5)=vdw_max(51,2);
    
    
    if norm(centroidb(zz,1:3)-cent_p(1,1:3))<=vdw_max(61,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(61,1))<=0.1,:);
    elseif norm(centroidb(zz,1:3)-cent_p(1,1:3))>vdw_max(61,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(61,1))<=0.1&    sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 + (centroidb(zz,1)-cent_p(1,1)).^2+(centroidb(zz,2)-cent_p(1,2)).^2+(centroidb(zz,3)-cent_p(1,3)).^2 )> sqrt( (cent_p(1,1)-grid_coor(:,1)).^2+(cent_p(1,2)-grid_coor(:,2)).^2+(cent_p(1,3)-grid_coor(:,3)).^2 ),:);
    end


    for pk=1:1:size_protential_protein(1,1)
        clear pku grid_test Matr centr_a;
        P_sh(:,5)=0;
        
        pku(:,1)=(P_sh(:,1)-protential_protein(pk,1)).^2;
        pku(:,2)=(P_sh(:,2)-protential_protein(pk,2)).^2;
        pku(:,3)=(P_sh(:,3)-protential_protein(pk,3)).^2;
        
        
%        pku(:,4)=(pku(:,1)+pku(:,2)+pku(:,3)).^0.5;
        pku(((pku(:,1)+pku(:,2)+pku(:,3)).^0.5<=2.5),4)=1;        
        P_sh(pku(:,4)==1,5)=1;

        P_sh(P_sh(:,5)==1,:)=[];

    end
 
    P_sh(:,5)=[];

    size_P_sh=size(P_sh);
    size_Matr_n4=size(Matr_n4);
    Matr_n4(1+size_Matr_n4(1,1):size_Matr_n4(1,1)+size_P_sh(1,1),1:4)=P_sh;

    Matr_n4(1+size_Matr_n4(1,1):size_Matr_n4(1,1)+size_P_sh(1,1),5)=vdw_max(61,2);
    
    
    if norm(centroidb(zz,1:3)-cent_p(1,1:3))<=vdw_max(79,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(79,1))<=0.1,:);
    elseif norm(centroidb(zz,1:3)-cent_p(1,1:3))>vdw_max(79,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(79,1))<=0.1&    sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 + (centroidb(zz,1)-cent_p(1,1)).^2+(centroidb(zz,2)-cent_p(1,2)).^2+(centroidb(zz,3)-cent_p(1,3)).^2 )> sqrt( (cent_p(1,1)-grid_coor(:,1)).^2+(cent_p(1,2)-grid_coor(:,2)).^2+(cent_p(1,3)-grid_coor(:,3)).^2 ),:);
    end

    for pk=1:1:size_protential_protein(1,1)
        clear pku grid_test Matr centr_a;
        P_sh(:,5)=0;
        
        pku(:,1)=(P_sh(:,1)-protential_protein(pk,1)).^2;
        pku(:,2)=(P_sh(:,2)-protential_protein(pk,2)).^2;
        pku(:,3)=(P_sh(:,3)-protential_protein(pk,3)).^2;
        
        
%        pku(:,4)=(pku(:,1)+pku(:,2)+pku(:,3)).^0.5;
        pku(((pku(:,1)+pku(:,2)+pku(:,3)).^0.5<=2.5),4)=1;        
        P_sh(pku(:,4)==1,5)=1;

        P_sh(P_sh(:,5)==1,:)=[];

    end

 
    P_sh(:,5)=[];

    size_P_sh=size(P_sh);
    size_Matr_namHB=size(Matr_namHB);
    Matr_namHB(1+size_Matr_namHB(1,1):size_Matr_namHB(1,1)+size_P_sh(1,1),1:4)=P_sh;

    Matr_namHB(1+size_Matr_namHB(1,1):size_Matr_namHB(1,1)+size_P_sh(1,1),5)=vdw_max(79,2);
    
    if norm(centroidb(zz,1:3)-cent_p(1,1:3))<=vdw_max(83,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(83,1))<=0.1,:);
    elseif norm(centroidb(zz,1:3)-cent_p(1,1:3))>vdw_max(83,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(83,1))<=0.1&    sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 + (centroidb(zz,1)-cent_p(1,1)).^2+(centroidb(zz,2)-cent_p(1,2)).^2+(centroidb(zz,3)-cent_p(1,3)).^2 )> sqrt( (cent_p(1,1)-grid_coor(:,1)).^2+(cent_p(1,2)-grid_coor(:,2)).^2+(cent_p(1,3)-grid_coor(:,3)).^2 ),:);
    end

    for pk=1:1:size_protential_protein(1,1)
        clear pku grid_test Matr centr_a;
        P_sh(:,5)=0;
        
        pku(:,1)=(P_sh(:,1)-protential_protein(pk,1)).^2;
        pku(:,2)=(P_sh(:,2)-protential_protein(pk,2)).^2;
        pku(:,3)=(P_sh(:,3)-protential_protein(pk,3)).^2;
        
        
%        pku(:,4)=(pku(:,1)+pku(:,2)+pku(:,3)).^0.5;
        pku(((pku(:,1)+pku(:,2)+pku(:,3)).^0.5<=2.5),4)=1;        
        P_sh(pku(:,4)==1,5)=1;

        P_sh(P_sh(:,5)==1,:)=[];

    end

 
    P_sh(:,5)=[];

    size_P_sh=size(P_sh);
    size_Matr_npl3HB=size(Matr_npl3HB);
    Matr_npl3HB(1+size_Matr_npl3HB(1,1):size_Matr_npl3HB(1,1)+size_P_sh(1,1),1:4)=P_sh;

    Matr_npl3HB(1+size_Matr_npl3HB(1,1):size_Matr_npl3HB(1,1)+size_P_sh(1,1),5)=vdw_max(83,2);

    
    if norm(centroidb(zz,1:3)-cent_p(1,1:3))<=vdw_max(49,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(49,1))<=0.1,:);
    elseif norm(centroidb(zz,1:3)-cent_p(1,1:3))>vdw_max(49,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(49,1))<=0.1&    sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 + (centroidb(zz,1)-cent_p(1,1)).^2+(centroidb(zz,2)-cent_p(1,2)).^2+(centroidb(zz,3)-cent_p(1,3)).^2 )> sqrt( (cent_p(1,1)-grid_coor(:,1)).^2+(cent_p(1,2)-grid_coor(:,2)).^2+(cent_p(1,3)-grid_coor(:,3)).^2 ),:);
    end
    for pk=1:1:size_protential_protein(1,1)
        clear pku grid_test Matr centr_a;
        P_sh(:,5)=0;
        
        pku(:,1)=(P_sh(:,1)-protential_protein(pk,1)).^2;
        pku(:,2)=(P_sh(:,2)-protential_protein(pk,2)).^2;
        pku(:,3)=(P_sh(:,3)-protential_protein(pk,3)).^2;
        
        
%        pku(:,4)=(pku(:,1)+pku(:,2)+pku(:,3)).^0.5;
        pku(((pku(:,1)+pku(:,2)+pku(:,3)).^0.5<=2.5),4)=1;        
        P_sh(pku(:,4)==1,5)=1;

        P_sh(P_sh(:,5)==1,:)=[];

    end
 
    P_sh(:,5)=[];

    size_P_sh=size(P_sh);
    size_Matr_o3HB=size(Matr_o3HB);
    Matr_o3HB(1+size_Matr_o3HB(1,1):size_Matr_o3HB(1,1)+size_P_sh(1,1),1:4)=P_sh;

    Matr_o3HB(1+size_Matr_o3HB(1,1):size_Matr_o3HB(1,1)+size_P_sh(1,1),5)=vdw_max(49,2);
    
    
    
    elseif (centroidb(zz,4)==5)
        max_x=max(centroidb(zz,1))+4.5;
        min_x=min(centroidb(zz,1))-4.5;
        max_y=max(centroidb(zz,2))+4.5;
        min_y=min(centroidb(zz,2))-4.5;
        max_z=max(centroidb(zz,3))+4.5;
        min_z=min(centroidb(zz,3))-4.5;
        sig_x=transpose(min_x:grid_d:max_x);
        sig_y=transpose(min_y:grid_d:max_y);
        sig_z=transpose(min_z:grid_d:max_z);
        
        size_sig_x=size(sig_x);
        size_sig_y=size(sig_y);
        size_sig_z=size(sig_z);
    
    
    for i=1:1:size_sig_x(1,1)
        grid_coor((i-1)*size_sig_y(1,1)*size_sig_z(1,1)+1:i*size_sig_y(1,1)*size_sig_z(1,1),1)=sig_x(i,1);
    end
    
    for i=1:1:size_sig_y(1,1)
        grid_coor1r((i-1)*size_sig_z(1,1)+1:i*size_sig_z(1,1),1)=sig_y(i,1);
    end
    
    grid_coor(:,2)=repmat(grid_coor1r,size_sig_x(1,1),1);
    grid_coor(:,3)=repmat(sig_z,size_sig_x(1,1)*size_sig_y(1,1),1);
    grid_coor(:,4)=zz;
    size_grid_coor=size(grid_coor);
    
    if norm(centroidb(zz,1:3)-cent_p(1,1:3))<=vdw_max(52,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(52,1))<=0.1,:);
    elseif norm(centroidb(zz,1:3)-cent_p(1,1:3))>vdw_max(52,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(52,1))<=0.1&    sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 + (centroidb(zz,1)-cent_p(1,1)).^2+(centroidb(zz,2)-cent_p(1,2)).^2+(centroidb(zz,3)-cent_p(1,3)).^2 )> sqrt( (cent_p(1,1)-grid_coor(:,1)).^2+(cent_p(1,2)-grid_coor(:,2)).^2+(cent_p(1,3)-grid_coor(:,3)).^2 ),:);
    end

    for pk=1:1:size_protential_protein(1,1)
        clear pku grid_test Matr centr_a;
        P_sh(:,5)=0;
        
        pku(:,1)=(P_sh(:,1)-protential_protein(pk,1)).^2;
        pku(:,2)=(P_sh(:,2)-protential_protein(pk,2)).^2;
        pku(:,3)=(P_sh(:,3)-protential_protein(pk,3)).^2;
        
        
%        pku(:,4)=(pku(:,1)+pku(:,2)+pku(:,3)).^0.5;
        pku(((pku(:,1)+pku(:,2)+pku(:,3)).^0.5<=3.2),4)=1;        
        P_sh(pku(:,4)==1,5)=1;

        P_sh(P_sh(:,5)==1,:)=[];

    end
 
    P_sh(:,5)=[];
    size_P_sh=size(P_sh);
    size_Matr_cl=size(Matr_cl);
    Matr_cl(1+size_Matr_cl(1,1):size_Matr_cl(1,1)+size_P_sh(1,1),1:4)=P_sh;

    Matr_cl(1+size_Matr_cl(1,1):size_Matr_cl(1,1)+size_P_sh(1,1),5)=vdw_max(52,2);
    
    
    if norm(centroidb(zz,1:3)-cent_p(1,1:3))<=vdw_max(51,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(51,1))<=0.1,:);
    elseif norm(centroidb(zz,1:3)-cent_p(1,1:3))>vdw_max(51,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(51,1))<=0.1&    sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 + (centroidb(zz,1)-cent_p(1,1)).^2+(centroidb(zz,2)-cent_p(1,2)).^2+(centroidb(zz,3)-cent_p(1,3)).^2 )> sqrt( (cent_p(1,1)-grid_coor(:,1)).^2+(cent_p(1,2)-grid_coor(:,2)).^2+(cent_p(1,3)-grid_coor(:,3)).^2 ),:);
    end
    for pk=1:1:size_protential_protein(1,1)
        clear pku grid_test Matr centr_a;
        P_sh(:,5)=0;
        
        pku(:,1)=(P_sh(:,1)-protential_protein(pk,1)).^2;
        pku(:,2)=(P_sh(:,2)-protential_protein(pk,2)).^2;
        pku(:,3)=(P_sh(:,3)-protential_protein(pk,3)).^2;
        
        
%        pku(:,4)=(pku(:,1)+pku(:,2)+pku(:,3)).^0.5;
        pku(((pku(:,1)+pku(:,2)+pku(:,3)).^0.5<=2.9),4)=1;        
        P_sh(pku(:,4)==1,5)=1;

        P_sh(P_sh(:,5)==1,:)=[];

    end
 
    P_sh(:,5)=[];
    size_P_sh=size(P_sh);
    size_Matr_f=size(Matr_f);
    Matr_f(1+size_Matr_f(1,1):size_Matr_f(1,1)+size_P_sh(1,1),1:4)=P_sh;

    Matr_f(1+size_Matr_f(1,1):size_Matr_f(1,1)+size_P_sh(1,1),5)=vdw_max(51,2);
    
    
    
    if norm(centroidb(zz,1:3)-cent_p(1,1:3))<=vdw_max(53,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(53,1))<=0.1,:);
    elseif norm(centroidb(zz,1:3)-cent_p(1,1:3))>vdw_max(53,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(53,1))<=0.1&    sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 + (centroidb(zz,1)-cent_p(1,1)).^2+(centroidb(zz,2)-cent_p(1,2)).^2+(centroidb(zz,3)-cent_p(1,3)).^2 )> sqrt( (cent_p(1,1)-grid_coor(:,1)).^2+(cent_p(1,2)-grid_coor(:,2)).^2+(cent_p(1,3)-grid_coor(:,3)).^2 ),:);
    end

    for pk=1:1:size_protential_protein(1,1)
        clear pku grid_test Matr centr_a;
        P_sh(:,5)=0;
        
        pku(:,1)=(P_sh(:,1)-protential_protein(pk,1)).^2;
        pku(:,2)=(P_sh(:,2)-protential_protein(pk,2)).^2;
        pku(:,3)=(P_sh(:,3)-protential_protein(pk,3)).^2;
        
        
%        pku(:,4)=(pku(:,1)+pku(:,2)+pku(:,3)).^0.5;
        pku(((pku(:,1)+pku(:,2)+pku(:,3)).^0.5<=3.2),4)=1;        
        P_sh(pku(:,4)==1,5)=1;

        P_sh(P_sh(:,5)==1,:)=[];

    end

 
    P_sh(:,5)=[];

    size_P_sh=size(P_sh);
    size_Matr_br=size(Matr_br);
    Matr_br(1+size_Matr_br(1,1):size_Matr_br(1,1)+size_P_sh(1,1),1:4)=P_sh;

    Matr_br(1+size_Matr_br(1,1):size_Matr_br(1,1)+size_P_sh(1,1),5)=vdw_max(53,2);
    
    
    
    
    
    
    
    
    
    
    if norm(centroidb(zz,1:3)-cent_p(1,1:3))<=vdw_max(85,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(85,1))<=0.1,:);
    elseif norm(centroidb(zz,1:3)-cent_p(1,1:3))>vdw_max(85,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(85,1))<=0.1&    sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 + (centroidb(zz,1)-cent_p(1,1)).^2+(centroidb(zz,2)-cent_p(1,2)).^2+(centroidb(zz,3)-cent_p(1,3)).^2 )> sqrt( (cent_p(1,1)-grid_coor(:,1)).^2+(cent_p(1,2)-grid_coor(:,2)).^2+(cent_p(1,3)-grid_coor(:,3)).^2 ),:);
    end

    for pk=1:1:size_protential_protein(1,1)
        clear pku grid_test Matr centr_a;
        P_sh(:,5)=0;
        
        pku(:,1)=(P_sh(:,1)-protential_protein(pk,1)).^2;
        pku(:,2)=(P_sh(:,2)-protential_protein(pk,2)).^2;
        pku(:,3)=(P_sh(:,3)-protential_protein(pk,3)).^2;
        
        
%        pku(:,4)=(pku(:,1)+pku(:,2)+pku(:,3)).^0.5;
        pku(((pku(:,1)+pku(:,2)+pku(:,3)).^0.5<=2.5),4)=1;        
        P_sh(pku(:,4)==1,5)=1;

        P_sh(P_sh(:,5)==1,:)=[];

    end
 
    P_sh(:,5)=[];
    size_P_sh=size(P_sh);
    size_Matr_n4=size(Matr_n4);
    Matr_n4(1+size_Matr_n4(1,1):size_Matr_n4(1,1)+size_P_sh(1,1),1:4)=P_sh;

    Matr_n4(1+size_Matr_n4(1,1):size_Matr_n4(1,1)+size_P_sh(1,1),5)=vdw_max(85,2);
    
    
    if norm(centroidb(zz,1:3)-cent_p(1,1:3))<=vdw_max(78,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(78,1))<=0.1,:);
    elseif norm(centroidb(zz,1:3)-cent_p(1,1:3))>vdw_max(78,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(78,1))<=0.1&    sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 + (centroidb(zz,1)-cent_p(1,1)).^2+(centroidb(zz,2)-cent_p(1,2)).^2+(centroidb(zz,3)-cent_p(1,3)).^2 )> sqrt( (cent_p(1,1)-grid_coor(:,1)).^2+(cent_p(1,2)-grid_coor(:,2)).^2+(cent_p(1,3)-grid_coor(:,3)).^2 ),:);
    end

    for pk=1:1:size_protential_protein(1,1)
        clear pku grid_test Matr centr_a;
        P_sh(:,5)=0;
        
        pku(:,1)=(P_sh(:,1)-protential_protein(pk,1)).^2;
        pku(:,2)=(P_sh(:,2)-protential_protein(pk,2)).^2;
        pku(:,3)=(P_sh(:,3)-protential_protein(pk,3)).^2;
        
        
%        pku(:,4)=(pku(:,1)+pku(:,2)+pku(:,3)).^0.5;
        pku(((pku(:,1)+pku(:,2)+pku(:,3)).^0.5<=2.5),4)=1;        
        P_sh(pku(:,4)==1,5)=1;

        P_sh(P_sh(:,5)==1,:)=[];

    end
 
    P_sh(:,5)=[];

    size_P_sh=size(P_sh);
    size_Matr_namHB=size(Matr_namHB);
    Matr_namHB(1+size_Matr_namHB(1,1):size_Matr_namHB(1,1)+size_P_sh(1,1),1:4)=P_sh;

    Matr_namHB(1+size_Matr_namHB(1,1):size_Matr_namHB(1,1)+size_P_sh(1,1),5)=vdw_max(78,2);
    
    if norm(centroidb(zz,1:3)-cent_p(1,1:3))<=vdw_max(82,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(82,1))<=0.1,:);
    elseif norm(centroidb(zz,1:3)-cent_p(1,1:3))>vdw_max(82,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(82,1))<=0.1&    sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 + (centroidb(zz,1)-cent_p(1,1)).^2+(centroidb(zz,2)-cent_p(1,2)).^2+(centroidb(zz,3)-cent_p(1,3)).^2 )> sqrt( (cent_p(1,1)-grid_coor(:,1)).^2+(cent_p(1,2)-grid_coor(:,2)).^2+(cent_p(1,3)-grid_coor(:,3)).^2 ),:);
    end

    for pk=1:1:size_protential_protein(1,1)
        clear pku grid_test Matr centr_a;
        P_sh(:,5)=0;
        
        pku(:,1)=(P_sh(:,1)-protential_protein(pk,1)).^2;
        pku(:,2)=(P_sh(:,2)-protential_protein(pk,2)).^2;
        pku(:,3)=(P_sh(:,3)-protential_protein(pk,3)).^2;
        
        
%        pku(:,4)=(pku(:,1)+pku(:,2)+pku(:,3)).^0.5;
        pku(((pku(:,1)+pku(:,2)+pku(:,3)).^0.5<=2.5),4)=1;        
        P_sh(pku(:,4)==1,5)=1;

        P_sh(P_sh(:,5)==1,:)=[];

    end
 
    P_sh(:,5)=[];
    size_P_sh=size(P_sh);
    size_Matr_npl3HB=size(Matr_npl3HB);
    Matr_npl3HB(1+size_Matr_npl3HB(1,1):size_Matr_npl3HB(1,1)+size_P_sh(1,1),1:4)=P_sh;

    Matr_npl3HB(1+size_Matr_npl3HB(1,1):size_Matr_npl3HB(1,1)+size_P_sh(1,1),5)=vdw_max(82,2);

    
    
    if norm(centroidb(zz,1:3)-cent_p(1,1:3))<=vdw_max(49,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(49,1))<=0.1,:);
    elseif norm(centroidb(zz,1:3)-cent_p(1,1:3))>vdw_max(49,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(49,1))<=0.1&    sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 + (centroidb(zz,1)-cent_p(1,1)).^2+(centroidb(zz,2)-cent_p(1,2)).^2+(centroidb(zz,3)-cent_p(1,3)).^2 )> sqrt( (cent_p(1,1)-grid_coor(:,1)).^2+(cent_p(1,2)-grid_coor(:,2)).^2+(cent_p(1,3)-grid_coor(:,3)).^2 ),:);
    end
    for pk=1:1:size_protential_protein(1,1)
        clear pku grid_test Matr centr_a;
        P_sh(:,5)=0;
        
        pku(:,1)=(P_sh(:,1)-protential_protein(pk,1)).^2;
        pku(:,2)=(P_sh(:,2)-protential_protein(pk,2)).^2;
        pku(:,3)=(P_sh(:,3)-protential_protein(pk,3)).^2;
        
        
%        pku(:,4)=(pku(:,1)+pku(:,2)+pku(:,3)).^0.5;
        pku(((pku(:,1)+pku(:,2)+pku(:,3)).^0.5<=2.5),4)=1;        
        P_sh(pku(:,4)==1,5)=1;

        P_sh(P_sh(:,5)==1,:)=[];

    end
 
    P_sh(:,5)=[];

    size_P_sh=size(P_sh);
    size_Matr_o2HB=size(Matr_o2HB);
    Matr_o2HB(1+size_Matr_o2HB(1,1):size_Matr_o2HB(1,1)+size_P_sh(1,1),1:4)=P_sh;

    Matr_o2HB(1+size_Matr_o2HB(1,1):size_Matr_o2HB(1,1)+size_P_sh(1,1),5)=vdw_max(49,2);
    
    
    if norm(centroidb(zz,1:3)-cent_p(1,1:3))<=vdw_max(48,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(48,1))<=0.1,:);
    elseif norm(centroidb(zz,1:3)-cent_p(1,1:3))>vdw_max(48,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(48,1))<=0.1&    sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 + (centroidb(zz,1)-cent_p(1,1)).^2+(centroidb(zz,2)-cent_p(1,2)).^2+(centroidb(zz,3)-cent_p(1,3)).^2 )> sqrt( (cent_p(1,1)-grid_coor(:,1)).^2+(cent_p(1,2)-grid_coor(:,2)).^2+(cent_p(1,3)-grid_coor(:,3)).^2 ),:);
    end
    for pk=1:1:size_protential_protein(1,1)
        clear pku grid_test Matr centr_a;
        P_sh(:,5)=0;
        
        pku(:,1)=(P_sh(:,1)-protential_protein(pk,1)).^2;
        pku(:,2)=(P_sh(:,2)-protential_protein(pk,2)).^2;
        pku(:,3)=(P_sh(:,3)-protential_protein(pk,3)).^2;
        
        
%        pku(:,4)=(pku(:,1)+pku(:,2)+pku(:,3)).^0.5;
        pku(((pku(:,1)+pku(:,2)+pku(:,3)).^0.5<=2.5),4)=1;        
        P_sh(pku(:,4)==1,5)=1;

        P_sh(P_sh(:,5)==1,:)=[];

    end
 
    P_sh(:,5)=[];

    size_P_sh=size(P_sh);
    size_Matr_o3HB=size(Matr_o3HB);
    Matr_o3HB(1+size_Matr_o3HB(1,1):size_Matr_o3HB(1,1)+size_P_sh(1,1),1:4)=P_sh;

    Matr_o3HB(1+size_Matr_o3HB(1,1):size_Matr_o3HB(1,1)+size_P_sh(1,1),5)=vdw_max(48,2);
    
    
    
    if norm(centroidb(zz,1:3)-cent_p(1,1:3))<=vdw_max(47,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(47,1))<=0.1,:);
    elseif norm(centroidb(zz,1:3)-cent_p(1,1:3))>vdw_max(47,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(47,1))<=0.1&    sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 + (centroidb(zz,1)-cent_p(1,1)).^2+(centroidb(zz,2)-cent_p(1,2)).^2+(centroidb(zz,3)-cent_p(1,3)).^2 )> sqrt( (cent_p(1,1)-grid_coor(:,1)).^2+(cent_p(1,2)-grid_coor(:,2)).^2+(cent_p(1,3)-grid_coor(:,3)).^2 ),:);
    end
    for pk=1:1:size_protential_protein(1,1)
        clear pku grid_test Matr centr_a;
        P_sh(:,5)=0;
        
        pku(:,1)=(P_sh(:,1)-protential_protein(pk,1)).^2;
        pku(:,2)=(P_sh(:,2)-protential_protein(pk,2)).^2;
        pku(:,3)=(P_sh(:,3)-protential_protein(pk,3)).^2;
        
        
%        pku(:,4)=(pku(:,1)+pku(:,2)+pku(:,3)).^0.5;
        pku(((pku(:,1)+pku(:,2)+pku(:,3)).^0.5<=3.2),4)=1;        
        P_sh(pku(:,4)==1,5)=1;

        P_sh(P_sh(:,5)==1,:)=[];

    end
 
    P_sh(:,5)=[];

    size_P_sh=size(P_sh);
    size_Matr_s=size(Matr_s);
    Matr_s(1+size_Matr_s(1,1):size_Matr_s(1,1)+size_P_sh(1,1),1:4)=P_sh;

    Matr_s(1+size_Matr_s(1,1):size_Matr_s(1,1)+size_P_sh(1,1),5)=vdw_max(47,2);
    
    
    
    elseif (centroidb(zz,4)==26)
        max_x=max(centroidb(zz,1))+4.0;
        min_x=min(centroidb(zz,1))-4.0;
        max_y=max(centroidb(zz,2))+4.0;
        min_y=min(centroidb(zz,2))-4.0;
        max_z=max(centroidb(zz,3))+4.0;
        min_z=min(centroidb(zz,3))-4.0;
        sig_x=transpose(min_x:grid_d:max_x);
        sig_y=transpose(min_y:grid_d:max_y);
        sig_z=transpose(min_z:grid_d:max_z);
        
        size_sig_x=size(sig_x);
        size_sig_y=size(sig_y);
        size_sig_z=size(sig_z);
    
    
    for i=1:1:size_sig_x(1,1)
        grid_coor((i-1)*size_sig_y(1,1)*size_sig_z(1,1)+1:i*size_sig_y(1,1)*size_sig_z(1,1),1)=sig_x(i,1);
    end
    
    for i=1:1:size_sig_y(1,1)
        grid_coor1r((i-1)*size_sig_z(1,1)+1:i*size_sig_z(1,1),1)=sig_y(i,1);
    end
    
    grid_coor(:,2)=repmat(grid_coor1r,size_sig_x(1,1),1);
    grid_coor(:,3)=repmat(sig_z,size_sig_x(1,1)*size_sig_y(1,1),1);
    grid_coor(:,4)=zz;
    size_grid_coor=size(grid_coor);
    
    if norm(centroidb(zz,1:3)-cent_p(1,1:3))<=vdw_max(85,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(85,1))<=0.1,:);
    elseif norm(centroidb(zz,1:3)-cent_p(1,1:3))>vdw_max(85,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(85,1))<=0.1&    sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 + (centroidb(zz,1)-cent_p(1,1)).^2+(centroidb(zz,2)-cent_p(1,2)).^2+(centroidb(zz,3)-cent_p(1,3)).^2 )> sqrt( (cent_p(1,1)-grid_coor(:,1)).^2+(cent_p(1,2)-grid_coor(:,2)).^2+(cent_p(1,3)-grid_coor(:,3)).^2 ),:);
    end
    for pk=1:1:size_protential_protein(1,1)
        clear pku grid_test Matr centr_a;
        P_sh(:,5)=0;
        
        pku(:,1)=(P_sh(:,1)-protential_protein(pk,1)).^2;
        pku(:,2)=(P_sh(:,2)-protential_protein(pk,2)).^2;
        pku(:,3)=(P_sh(:,3)-protential_protein(pk,3)).^2;
        
        
%        pku(:,4)=(pku(:,1)+pku(:,2)+pku(:,3)).^0.5;
        pku(((pku(:,1)+pku(:,2)+pku(:,3)).^0.5<=2.5),4)=1;        
        P_sh(pku(:,4)==1,5)=1;

        P_sh(P_sh(:,5)==1,:)=[];

    end
 
    P_sh(:,5)=[];

    size_P_sh=size(P_sh);
    size_Matr_o3HB=size(Matr_o3HB);
    Matr_o3HB(1+size_Matr_o3HB(1,1):size_Matr_o3HB(1,1)+size_P_sh(1,1),1:4)=P_sh;

    Matr_o3HB(1+size_Matr_o3HB(1,1):size_Matr_o3HB(1,1)+size_P_sh(1,1),5)=vdw_max(85,2);
    
    
    
    if norm(centroidb(zz,1:3)-cent_p(1,1:3))<=vdw_max(86,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(86,1))<=0.1,:);
    elseif norm(centroidb(zz,1:3)-cent_p(1,1:3))>vdw_max(86,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(86,1))<=0.1&    sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 + (centroidb(zz,1)-cent_p(1,1)).^2+(centroidb(zz,2)-cent_p(1,2)).^2+(centroidb(zz,3)-cent_p(1,3)).^2 )> sqrt( (cent_p(1,1)-grid_coor(:,1)).^2+(cent_p(1,2)-grid_coor(:,2)).^2+(cent_p(1,3)-grid_coor(:,3)).^2 ),:);
    end
    for pk=1:1:size_protential_protein(1,1)
        clear pku grid_test Matr centr_a;
        P_sh(:,5)=0;
        
        pku(:,1)=(P_sh(:,1)-protential_protein(pk,1)).^2;
        pku(:,2)=(P_sh(:,2)-protential_protein(pk,2)).^2;
        pku(:,3)=(P_sh(:,3)-protential_protein(pk,3)).^2;
        
        
%        pku(:,4)=(pku(:,1)+pku(:,2)+pku(:,3)).^0.5;
        pku(((pku(:,1)+pku(:,2)+pku(:,3)).^0.5<=2.5),4)=1;        
        P_sh(pku(:,4)==1,5)=1;

        P_sh(P_sh(:,5)==1,:)=[];

    end
 
    P_sh(:,5)=[];

    size_P_sh=size(P_sh);
    size_Matr_o2HB=size(Matr_o2HB);
    Matr_o2HB(1+size_Matr_o2HB(1,1):size_Matr_o2HB(1,1)+size_P_sh(1,1),1:4)=P_sh;

    Matr_o2HB(1+size_Matr_o2HB(1,1):size_Matr_o2HB(1,1)+size_P_sh(1,1),5)=vdw_max(86,2);
    
    
    if norm(centroidb(zz,1:3)-cent_p(1,1:3))<=vdw_max(70,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(70,1))<=0.1,:);
    elseif norm(centroidb(zz,1:3)-cent_p(1,1:3))>vdw_max(70,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(70,1))<=0.1&    sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 + (centroidb(zz,1)-cent_p(1,1)).^2+(centroidb(zz,2)-cent_p(1,2)).^2+(centroidb(zz,3)-cent_p(1,3)).^2 )> sqrt( (cent_p(1,1)-grid_coor(:,1)).^2+(cent_p(1,2)-grid_coor(:,2)).^2+(cent_p(1,3)-grid_coor(:,3)).^2 ),:);
    end

    for pk=1:1:size_protential_protein(1,1)
        clear pku grid_test Matr centr_a;
        P_sh(:,5)=0;
        
        pku(:,1)=(P_sh(:,1)-protential_protein(pk,1)).^2;
        pku(:,2)=(P_sh(:,2)-protential_protein(pk,2)).^2;
        pku(:,3)=(P_sh(:,3)-protential_protein(pk,3)).^2;
        
        
%        pku(:,4)=(pku(:,1)+pku(:,2)+pku(:,3)).^0.5;
        pku(((pku(:,1)+pku(:,2)+pku(:,3)).^0.5<=3.2),4)=1;        
        P_sh(pku(:,4)==1,5)=1;

        P_sh(P_sh(:,5)==1,:)=[];

    end

 
    P_sh(:,5)=[];

    size_P_sh=size(P_sh);
    size_Matr_br=size(Matr_br);
    Matr_br(1+size_Matr_br(1,1):size_Matr_br(1,1)+size_P_sh(1,1),1:4)=P_sh;

    Matr_br(1+size_Matr_br(1,1):size_Matr_br(1,1)+size_P_sh(1,1),5)=vdw_max(70,2);
    
    if norm(centroidb(zz,1:3)-cent_p(1,1:3))<=vdw_max(69,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(69,1))<=0.1,:);
    elseif norm(centroidb(zz,1:3)-cent_p(1,1:3))>vdw_max(69,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(69,1))<=0.1&    sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 + (centroidb(zz,1)-cent_p(1,1)).^2+(centroidb(zz,2)-cent_p(1,2)).^2+(centroidb(zz,3)-cent_p(1,3)).^2 )> sqrt( (cent_p(1,1)-grid_coor(:,1)).^2+(cent_p(1,2)-grid_coor(:,2)).^2+(cent_p(1,3)-grid_coor(:,3)).^2 ),:);
    end

    for pk=1:1:size_protential_protein(1,1)
        clear pku grid_test Matr centr_a;
        P_sh(:,5)=0;
        
        pku(:,1)=(P_sh(:,1)-protential_protein(pk,1)).^2;
        pku(:,2)=(P_sh(:,2)-protential_protein(pk,2)).^2;
        pku(:,3)=(P_sh(:,3)-protential_protein(pk,3)).^2;
        
        
%        pku(:,4)=(pku(:,1)+pku(:,2)+pku(:,3)).^0.5;
        pku(((pku(:,1)+pku(:,2)+pku(:,3)).^0.5<=3.2),4)=1;        
        P_sh(pku(:,4)==1,5)=1;

        P_sh(P_sh(:,5)==1,:)=[];

    end
 
    P_sh(:,5)=[];
    size_P_sh=size(P_sh);
    size_Matr_cl=size(Matr_cl);
    Matr_cl(1+size_Matr_cl(1,1):size_Matr_cl(1,1)+size_P_sh(1,1),1:4)=P_sh;

    Matr_cl(1+size_Matr_cl(1,1):size_Matr_cl(1,1)+size_P_sh(1,1),5)=vdw_max(69,2);
    
    
    if norm(centroidb(zz,1:3)-cent_p(1,1:3))<=vdw_max(68,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(68,1))<=0.1,:);
    elseif norm(centroidb(zz,1:3)-cent_p(1,1:3))>vdw_max(68,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(68,1))<=0.1&    sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 + (centroidb(zz,1)-cent_p(1,1)).^2+(centroidb(zz,2)-cent_p(1,2)).^2+(centroidb(zz,3)-cent_p(1,3)).^2 )> sqrt( (cent_p(1,1)-grid_coor(:,1)).^2+(cent_p(1,2)-grid_coor(:,2)).^2+(cent_p(1,3)-grid_coor(:,3)).^2 ),:);
    end
    
    
    for pk=1:1:size_protential_protein(1,1)
        clear pku grid_test Matr centr_a;
        P_sh(:,5)=0;
        
        pku(:,1)=(P_sh(:,1)-protential_protein(pk,1)).^2;
        pku(:,2)=(P_sh(:,2)-protential_protein(pk,2)).^2;
        pku(:,3)=(P_sh(:,3)-protential_protein(pk,3)).^2;
        
        
%        pku(:,4)=(pku(:,1)+pku(:,2)+pku(:,3)).^0.5;
        pku(((pku(:,1)+pku(:,2)+pku(:,3)).^0.5<=2.9),4)=1;        
        P_sh(pku(:,4)==1,5)=1;

        P_sh(P_sh(:,5)==1,:)=[];

    end


 
    P_sh(:,5)=[];

    size_P_sh=size(P_sh);
    size_Matr_f=size(Matr_f);
    Matr_f(1+size_Matr_f(1,1):size_Matr_f(1,1)+size_P_sh(1,1),1:4)=P_sh;

    Matr_f(1+size_Matr_f(1,1):size_Matr_f(1,1)+size_P_sh(1,1),5)=vdw_max(68,2);
    
    
    
    if norm(centroidb(zz,1:3)-cent_p(1,1:3))<=vdw_max(77,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(77,1))<=0.1,:);
    elseif norm(centroidb(zz,1:3)-cent_p(1,1:3))>vdw_max(77,1)
        P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 )- vdw_max(77,1))<=0.1&    sqrt( (grid_coor(:,1)-centroidb(zz,1)).^2+(grid_coor(:,2)-centroidb(zz,2)).^2+(grid_coor(:,3)-centroidb(zz,3)).^2 + (centroidb(zz,1)-cent_p(1,1)).^2+(centroidb(zz,2)-cent_p(1,2)).^2+(centroidb(zz,3)-cent_p(1,3)).^2 )> sqrt( (cent_p(1,1)-grid_coor(:,1)).^2+(cent_p(1,2)-grid_coor(:,2)).^2+(cent_p(1,3)-grid_coor(:,3)).^2 ),:);
    end
    
    
    for pk=1:1:size_protential_protein(1,1)
        clear pku grid_test Matr centr_a;
        P_sh(:,5)=0;
        
        pku(:,1)=(P_sh(:,1)-protential_protein(pk,1)).^2;
        pku(:,2)=(P_sh(:,2)-protential_protein(pk,2)).^2;
        pku(:,3)=(P_sh(:,3)-protential_protein(pk,3)).^2;
        
        
%        pku(:,4)=(pku(:,1)+pku(:,2)+pku(:,3)).^0.5;
        pku(((pku(:,1)+pku(:,2)+pku(:,3)).^0.5<=3.2),4)=1;        
        P_sh(pku(:,4)==1,5)=1;

        P_sh(P_sh(:,5)==1,:)=[];

    end


 
    P_sh(:,5)=[];

    size_P_sh=size(P_sh);
    size_Matr_s=size(Matr_s);
    Matr_s(1+size_Matr_s(1,1):size_Matr_s(1,1)+size_P_sh(1,1),1:4)=P_sh;

    Matr_s(1+size_Matr_s(1,1):size_Matr_s(1,1)+size_P_sh(1,1),5)=vdw_max(77,2);
    
    
    
    
    
    
    
    
    end
    
    
    
    
    
    
    
    
    
    
    
    
end


size_Matr_c2=size(Matr_c2);
size_Matr_c3=size(Matr_c3);
size_Matr_car=size(Matr_car);
size_Matr_n2=size(Matr_n2);
size_Matr_clHB=size(Matr_clHB);
size_Matr_o2HB=size(Matr_o2HB);
size_Matr_brHB=size(Matr_brHB);
size_Matr_fHB=size(Matr_fHB);
size_Matr_o3HB=size(Matr_o3HB);
size_Matr_sHB=size(Matr_sHB);
size_Matr_n4=size(Matr_n4);
size_Matr_npl3=size(Matr_npl3);
size_Matr_s=size(Matr_s);
size_Matr_nar=size(Matr_nar);
size_Matr_br=size(Matr_br);
size_Matr_c1=size(Matr_c1);
size_Matr_cl=size(Matr_cl);
size_Matr_f=size(Matr_f);
size_Matr_namHB=size(Matr_namHB);
size_Matr_npl3HB=size(Matr_npl3HB);
size_Matr_nam=size(Matr_nam);
size_Matr_o2=size(Matr_o2);
size_Matr_o3=size(Matr_o3);
size_Matr_i=size(Matr_i);






if size_Matr_c2(1,1)~=0
    Matr_c2=sortrows(Matr_c2,[1 2 3]);
end
if size_Matr_c3(1,1)~=0
Matr_c3=sortrows(Matr_c3,[1 2 3]);
end
if size_Matr_car(1,1)~=0
Matr_car=sortrows(Matr_car,[1 2 3]);
end
if size_Matr_n2(1,1)~=0
Matr_n2=sortrows(Matr_n2,[1 2 3]);
end
if size_Matr_clHB(1,1)~=0
Matr_clHB=sortrows(Matr_clHB,[1 2 3]);
end
if size_Matr_o2HB(1,1)~=0
Matr_o2HB=sortrows(Matr_o2HB,[1 2 3]);
end
if size_Matr_brHB(1,1)~=0
Matr_brHB=sortrows(Matr_brHB,[1 2 3]);
end
if size_Matr_fHB(1,1)~=0
Matr_fHB=sortrows(Matr_fHB,[1 2 3]);
end
if size_Matr_o3HB(1,1)~=0
Matr_o3HB=sortrows(Matr_o3HB,[1 2 3]);
end
if size_Matr_sHB(1,1)~=0
Matr_sHB=sortrows(Matr_sHB,[1 2 3]);
end
if size_Matr_n4(1,1)~=0
Matr_n4=sortrows(Matr_n4,[1 2 3]);
end
if size_Matr_npl3(1,1)~=0
Matr_npl3=sortrows(Matr_npl3,[1 2 3]);
end

if size_Matr_s(1,1)~=0
Matr_s=sortrows(Matr_s,[1 2 3]);
end
if size_Matr_nar(1,1)~=0
Matr_nar=sortrows(Matr_nar,[1 2 3]);
end

if size_Matr_br(1,1)~=0
Matr_br=sortrows(Matr_br,[1 2 3]);
end
if size_Matr_c1(1,1)~=0
Matr_c1=sortrows(Matr_c1,[1 2 3]);
end
if size_Matr_cl(1,1)~=0
Matr_cl=sortrows(Matr_cl,[1 2 3]);
end
if size_Matr_f(1,1)~=0
Matr_f=sortrows(Matr_f,[1 2 3]);
end

if size_Matr_namHB(1,1)~=0
Matr_namHB=sortrows(Matr_namHB,[1 2 3]);
end
if size_Matr_npl3HB(1,1)~=0
Matr_npl3HB=sortrows(Matr_npl3HB,[1 2 3]);
end
if size_Matr_nam(1,1)~=0
Matr_nam=sortrows(Matr_nam,[1 2 3]);
end
if size_Matr_o2(1,1)~=0
Matr_o2=sortrows(Matr_o2,[1 2 3]);
end
if size_Matr_o3(1,1)~=0
Matr_o3=sortrows(Matr_o3,[1 2 3]);
end
if size_Matr_i(1,1)~=0
Matr_i=sortrows(Matr_i,[1 2 3]);
end


i=1;
while i<size_Matr_c2(1,1)-1
    size_Matr_c2=size(Matr_c2);
    if i>=size_Matr_c2(1,1)-1
        break
    
    elseif abs(Matr_c2(i,1)-Matr_c2(i+1,1))<grid_d&abs(Matr_c2(i,2)-Matr_c2(i+1,2))<grid_d&abs(Matr_c2(i,3)-Matr_c2(i+1,3))<grid_d
        
        
        Matr_c2(i,5)=Matr_c2(i,5)+Matr_c2(i+1,5);
        Matr_c2(i+1,:)=[];
        size_Matr_c2=size(Matr_c2);
    else
        i=i+1;
    end
end
    
i=1;
while i<size_Matr_c3(1,1)-1
    size_Matr_c3=size(Matr_c3);
    if i>=size_Matr_c3(1,1)-1
        break
    
    elseif abs(Matr_c3(i,1)-Matr_c3(i+1,1))<grid_d&abs(Matr_c3(i,2)-Matr_c3(i+1,2))<grid_d&abs(Matr_c3(i,3)-Matr_c3(i+1,3))<grid_d
        Matr_c3(i,5)=Matr_c3(i,5)+Matr_c3(i+1,5);
        Matr_c3(i+1,:)=[];
        size_Matr_c3=size(Matr_c3);
    else
        i=i+1;
    end
end

i=1;
while i<size_Matr_car(1,1)-1
    size_Matr_car=size(Matr_car);
    if i>=size_Matr_car(1,1)-1
        break
    
    elseif abs(Matr_car(i,1)-Matr_car(i+1,1))<grid_d&abs(Matr_car(i,2)-Matr_car(i+1,2))<grid_d&abs(Matr_car(i,3)-Matr_car(i+1,3))<grid_d
        Matr_car(i,5)=Matr_car(i,5)+Matr_car(i+1,5);
        Matr_car(i+1,:)=[];
        size_Matr_car=size(Matr_car);
    else
        i=i+1;
    end
end


i=1;
while i<size_Matr_n2(1,1)-1
    size_Matr_n2=size(Matr_n2);
    if i>=size_Matr_n2(1,1)-1
        break
    
    elseif abs(Matr_n2(i,1)-Matr_n2(i+1,1))<grid_d&abs(Matr_n2(i,2)-Matr_n2(i+1,2))<grid_d&abs(Matr_n2(i,3)-Matr_n2(i+1,3))<grid_d
        Matr_n2(i,5)=Matr_n2(i,5)+Matr_n2(i+1,5);
        Matr_n2(i+1,:)=[];
        size_Matr_n2=size(Matr_n2);
    else
        i=i+1;
    end
end




i=1;
while i<size_Matr_clHB(1,1)-1
    size_Matr_clHB=size(Matr_clHB);
    if i>=size_Matr_clHB(1,1)-1
        break
    
    elseif abs(Matr_clHB(i,1)-Matr_clHB(i+1,1))<grid_d&abs(Matr_clHB(i,2)-Matr_clHB(i+1,2))<grid_d&abs(Matr_clHB(i,3)-Matr_clHB(i+1,3))<grid_d
        Matr_clHB(i,5)=Matr_clHB(i,5)+Matr_clHB(i+1,5);
        Matr_clHB(i+1,:)=[];
        size_Matr_clHB=size(Matr_clHB);
    else
        i=i+1;
    end
end



i=1;
while i<size_Matr_o2HB(1,1)-1
    size_Matr_o2HB=size(Matr_o2HB);
    if i>=size_Matr_o2HB(1,1)-1
        break
    
    elseif abs(Matr_o2HB(i,1)-Matr_o2HB(i+1,1))<grid_d&abs(Matr_o2HB(i,2)-Matr_o2HB(i+1,2))<grid_d&abs(Matr_o2HB(i,3)-Matr_o2HB(i+1,3))<grid_d
        Matr_o2HB(i,5)=Matr_o2HB(i,5)+Matr_o2HB(i+1,5);
        Matr_o2HB(i+1,:)=[];
        size_Matr_o2HB=size(Matr_o2HB);
    else
        i=i+1;
    end
end


i=1;
while i<size_Matr_brHB(1,1)-1
    size_Matr_brHB=size(Matr_brHB);
    if i>=size_Matr_brHB(1,1)-1
        break
    
    elseif abs(Matr_brHB(i,1)-Matr_brHB(i+1,1))<grid_d&abs(Matr_brHB(i,2)-Matr_brHB(i+1,2))<grid_d&abs(Matr_brHB(i,3)-Matr_brHB(i+1,3))<grid_d
        Matr_brHB(i,5)=Matr_brHB(i,5)+Matr_brHB(i+1,5);
        Matr_brHB(i+1,:)=[];
        size_Matr_brHB=size(Matr_brHB);
    else
        i=i+1;
    end
end


i=1;
while i<size_Matr_fHB(1,1)-1
    size_Matr_fHB=size(Matr_fHB);
    if i>=size_Matr_fHB(1,1)-1
        break
    
    elseif abs(Matr_fHB(i,1)-Matr_fHB(i+1,1))<grid_d&abs(Matr_fHB(i,2)-Matr_fHB(i+1,2))<grid_d&abs(Matr_fHB(i,3)-Matr_fHB(i+1,3))<grid_d
        Matr_fHB(i,5)=Matr_fHB(i,5)+Matr_fHB(i+1,5);
        Matr_fHB(i+1,:)=[];
        size_Matr_fHB=size(Matr_fHB);
    else
        i=i+1;
    end
end


i=1;
while i<size_Matr_o3HB(1,1)-1
    size_Matr_o3HB=size(Matr_o3HB);
    if i>=size_Matr_o3HB(1,1)-1
        break
    
    elseif abs(Matr_o3HB(i,1)-Matr_o3HB(i+1,1))<grid_d&abs(Matr_o3HB(i,2)-Matr_o3HB(i+1,2))<grid_d&abs(Matr_o3HB(i,3)-Matr_o3HB(i+1,3))<grid_d
        Matr_o3HB(i,5)=Matr_o3HB(i,5)+Matr_o3HB(i+1,5);
        Matr_o3HB(i+1,:)=[];
        size_Matr_o3HB=size(Matr_o3HB);
    else
        i=i+1;
    end
end



i=1;
while i<size_Matr_sHB(1,1)-1
    size_Matr_sHB=size(Matr_sHB);
    if i>=size_Matr_sHB(1,1)-1
        break
    
    elseif abs(Matr_sHB(i,1)-Matr_sHB(i+1,1))<grid_d&abs(Matr_sHB(i,2)-Matr_sHB(i+1,2))<grid_d&abs(Matr_sHB(i,3)-Matr_sHB(i+1,3))<grid_d
        Matr_sHB(i,5)=Matr_sHB(i,5)+Matr_sHB(i+1,5);
        Matr_sHB(i+1,:)=[];
        size_Matr_sHB=size(Matr_sHB);
    else
        i=i+1;
    end
end


i=1;
while i<size_Matr_n4(1,1)-1
    size_Matr_n4=size(Matr_n4);
    if i>=size_Matr_n4(1,1)-1
        break
    
    elseif abs(Matr_n4(i,1)-Matr_n4(i+1,1))<grid_d&abs(Matr_n4(i,2)-Matr_n4(i+1,2))<grid_d&abs(Matr_n4(i,3)-Matr_n4(i+1,3))<grid_d
        Matr_n4(i,5)=Matr_n4(i,5)+Matr_n4(i+1,5);
        Matr_n4(i+1,:)=[];
        size_Matr_n4=size(Matr_n4);
    else
        i=i+1;
    end
end




i=1;
while i<size_Matr_npl3(1,1)-1
    size_Matr_npl3=size(Matr_npl3);
    if i>=size_Matr_npl3(1,1)-1
        break
    
    elseif abs(Matr_npl3(i,1)-Matr_npl3(i+1,1))<grid_d&abs(Matr_npl3(i,2)-Matr_npl3(i+1,2))<grid_d&abs(Matr_npl3(i,3)-Matr_npl3(i+1,3))<grid_d
        Matr_npl3(i,5)=Matr_npl3(i,5)+Matr_npl3(i+1,5);
        Matr_npl3(i+1,:)=[];
        size_Matr_npl3=size(Matr_npl3);
    else
        i=i+1;
    end
end


i=1;
while i<size_Matr_s(1,1)-1
    size_Matr_s=size(Matr_s);
    if i>=size_Matr_s(1,1)-1
        break
    
    elseif abs(Matr_s(i,1)-Matr_s(i+1,1))<grid_d&abs(Matr_s(i,2)-Matr_s(i+1,2))<grid_d&abs(Matr_s(i,3)-Matr_s(i+1,3))<grid_d
        Matr_s(i,5)=Matr_s(i,5)+Matr_s(i+1,5);
        Matr_s(i+1,:)=[];
        size_Matr_s=size(Matr_s);
    else
        i=i+1;
    end
end



i=1;
while i<size_Matr_nar(1,1)-1
    size_Matr_nar=size(Matr_nar);
    if i>=size_Matr_nar(1,1)-1
        break
    
    elseif abs(Matr_nar(i,1)-Matr_nar(i+1,1))<grid_d&abs(Matr_nar(i,2)-Matr_nar(i+1,2))<grid_d&abs(Matr_nar(i,3)-Matr_nar(i+1,3))<grid_d
        Matr_nar(i,5)=Matr_nar(i,5)+Matr_nar(i+1,5);
        Matr_nar(i+1,:)=[];
        size_Matr_nar=size(Matr_nar);
    else
        i=i+1;
    end
end




i=1;
while i<size_Matr_br(1,1)-1
    size_Matr_br=size(Matr_br);
    if i>=size_Matr_br(1,1)-1
        break
    
    elseif abs(Matr_br(i,1)-Matr_br(i+1,1))<grid_d&abs(Matr_br(i,2)-Matr_br(i+1,2))<grid_d&abs(Matr_br(i,3)-Matr_br(i+1,3))<grid_d
        Matr_br(i,5)=Matr_br(i,5)+Matr_br(i+1,5);
        Matr_br(i+1,:)=[];
        size_Matr_br=size(Matr_br);
    else
        i=i+1;
    end
end





i=1;
while i<size_Matr_cl(1,1)-1
    size_Matr_cl=size(Matr_cl);
    if i>=size_Matr_cl(1,1)-1
        break
    
    elseif abs(Matr_cl(i,1)-Matr_cl(i+1,1))<grid_d&abs(Matr_cl(i,2)-Matr_cl(i+1,2))<grid_d&abs(Matr_cl(i,3)-Matr_cl(i+1,3))<grid_d
        Matr_cl(i,5)=Matr_cl(i,5)+Matr_cl(i+1,5);
        Matr_cl(i+1,:)=[];
        size_Matr_cl=size(Matr_cl);
    else
        i=i+1;
    end
end




i=1;
while i<size_Matr_f(1,1)-1
    size_Matr_f=size(Matr_f);
    if i>=size_Matr_f(1,1)-1
        break
    
    elseif abs(Matr_f(i,1)-Matr_f(i+1,1))<grid_d&abs(Matr_f(i,2)-Matr_f(i+1,2))<grid_d&abs(Matr_f(i,3)-Matr_f(i+1,3))<grid_d
        Matr_f(i,5)=Matr_f(i,5)+Matr_f(i+1,5);
        Matr_f(i+1,:)=[];
        size_Matr_f=size(Matr_f);
    else
        i=i+1;
    end
end




i=1;
while i<size_Matr_namHB(1,1)-1
    size_Matr_namHB=size(Matr_namHB);
    if i>=size_Matr_namHB(1,1)-1
        break
    
    elseif abs(Matr_namHB(i,1)-Matr_namHB(i+1,1))<grid_d&abs(Matr_namHB(i,2)-Matr_namHB(i+1,2))<grid_d&abs(Matr_namHB(i,3)-Matr_namHB(i+1,3))<grid_d
        Matr_namHB(i,5)=Matr_namHB(i,5)+Matr_namHB(i+1,5);
        Matr_namHB(i+1,:)=[];
        size_Matr_namHB=size(Matr_namHB);
    else
        i=i+1;
    end
end






i=1;
while i<size_Matr_npl3HB(1,1)-1
    size_Matr_npl3HB=size(Matr_npl3HB);
    if i>=size_Matr_npl3HB(1,1)-1
        break
    
    elseif abs(Matr_npl3HB(i,1)-Matr_npl3HB(i+1,1))<grid_d&abs(Matr_npl3HB(i,2)-Matr_npl3HB(i+1,2))<grid_d&abs(Matr_npl3HB(i,3)-Matr_npl3HB(i+1,3))<grid_d
        Matr_npl3HB(i,5)=Matr_npl3HB(i,5)+Matr_npl3HB(i+1,5);
        Matr_npl3HB(i+1,:)=[];
        size_Matr_npl3HB=size(Matr_npl3HB);
    else
        i=i+1;
    end
end







i=1;
while i<size_Matr_nam(1,1)-1
    size_Matr_nam=size(Matr_nam);
    if i>=size_Matr_nam(1,1)-1
        break
    
    elseif abs(Matr_nam(i,1)-Matr_nam(i+1,1))<grid_d&abs(Matr_nam(i,2)-Matr_nam(i+1,2))<grid_d&abs(Matr_nam(i,3)-Matr_nam(i+1,3))<grid_d
        Matr_nam(i,5)=Matr_nam(i,5)+Matr_nam(i+1,5);
        Matr_nam(i+1,:)=[];
        size_Matr_nam=size(Matr_nam);
    else
        i=i+1;
    end
end





i=1;
while i<size_Matr_o2(1,1)-1
    size_Matr_o2=size(Matr_o2);
    if i>=size_Matr_o2(1,1)-1
        break
    
    elseif abs(Matr_o2(i,1)-Matr_o2(i+1,1))<grid_d&abs(Matr_o2(i,2)-Matr_o2(i+1,2))<grid_d&abs(Matr_o2(i,3)-Matr_o2(i+1,3))<grid_d
        Matr_o2(i,5)=Matr_o2(i,5)+Matr_o2(i+1,5);
        Matr_o2(i+1,:)=[];
        size_Matr_o2=size(Matr_o2);
    else
        i=i+1;
    end
end





i=1;
while i<size_Matr_o3(1,1)-1
    size_Matr_o3=size(Matr_o3);
    if i>=size_Matr_o3(1,1)-1
        break
    
    elseif abs(Matr_o3(i,1)-Matr_o3(i+1,1))<grid_d&abs(Matr_o3(i,2)-Matr_o3(i+1,2))<grid_d&abs(Matr_o3(i,3)-Matr_o3(i+1,3))<grid_d
        Matr_o3(i,5)=Matr_o3(i,5)+Matr_o3(i+1,5);
        Matr_o3(i+1,:)=[];
        size_Matr_o3=size(Matr_o3);
    else
        i=i+1;
    end
end





i=1;
while i<size_Matr_i(1,1)-1
    size_Matr_i=size(Matr_i);
    if i>=size_Matr_i(1,1)-1
        break
    
    elseif abs(Matr_i(i,1)-Matr_i(i+1,1))<grid_d&abs(Matr_i(i,2)-Matr_i(i+1,2))<grid_d&abs(Matr_i(i,3)-Matr_i(i+1,3))<grid_d
        Matr_i(i,5)=Matr_i(i,5)+Matr_i(i+1,5);
        Matr_i(i+1,:)=[];
        size_Matr_i=size(Matr_i);
    else
        i=i+1;
    end
end



if size_Matr_c2(1,1)~=0
    Matr_c2=sortrows(Matr_c2,[4 1 2 3]);
end
if size_Matr_c3(1,1)~=0
Matr_c3=sortrows(Matr_c3,[4 1 2 3]);
end
if size_Matr_car(1,1)~=0
Matr_car=sortrows(Matr_car,[4 1 2 3]);
end
if size_Matr_n2(1,1)~=0
Matr_n2=sortrows(Matr_n2,[4 1 2 3]);
end
if size_Matr_clHB(1,1)~=0
Matr_clHB=sortrows(Matr_clHB,[4 1 2 3]);
end
if size_Matr_o2HB(1,1)~=0
Matr_o2HB=sortrows(Matr_o2HB,[4 1 2 3]);
end
if size_Matr_brHB(1,1)~=0
Matr_brHB=sortrows(Matr_brHB,[4 1 2 3]);
end
if size_Matr_fHB(1,1)~=0
Matr_fHB=sortrows(Matr_fHB,[4 1 2 3]);
end
if size_Matr_o3HB(1,1)~=0
Matr_o3HB=sortrows(Matr_o3HB,[4 1 2 3]);
end
if size_Matr_sHB(1,1)~=0
Matr_sHB=sortrows(Matr_sHB,[4 1 2 3]);
end
if size_Matr_n4(1,1)~=0
Matr_n4=sortrows(Matr_n4,[4 1 2 3]);
end
if size_Matr_npl3(1,1)~=0
Matr_npl3=sortrows(Matr_npl3,[4 1 2 3]);
end

if size_Matr_s(1,1)~=0
Matr_s=sortrows(Matr_s,[4 1 2 3]);
end
if size_Matr_nar(1,1)~=0
Matr_nar=sortrows(Matr_nar,[4 1 2 3]);
end

if size_Matr_br(1,1)~=0
Matr_br=sortrows(Matr_br,[4 1 2 3]);
end
if size_Matr_c1(1,1)~=0
Matr_c1=sortrows(Matr_c1,[4 1 2 3]);
end
if size_Matr_cl(1,1)~=0
Matr_cl=sortrows(Matr_cl,[4 1 2 3]);
end
if size_Matr_f(1,1)~=0
Matr_f=sortrows(Matr_f,[4 1 2 3]);
end

if size_Matr_namHB(1,1)~=0
Matr_namHB=sortrows(Matr_namHB,[4 1 2 3]);
end
if size_Matr_npl3HB(1,1)~=0
Matr_npl3HB=sortrows(Matr_npl3HB,[4 1 2 3]);
end
if size_Matr_nam(1,1)~=0
Matr_nam=sortrows(Matr_nam,[4 1 2 3]);
end
if size_Matr_o2(1,1)~=0
Matr_o2=sortrows(Matr_o2,[4 1 2 3]);
end
if size_Matr_o3(1,1)~=0
Matr_o3=sortrows(Matr_o3,[4 1 2 3]);
end
if size_Matr_i(1,1)~=0
Matr_i=sortrows(Matr_i,[4 1 2 3]);
end



if size_Matr_c2(1,1)~=0
    i=1;
    Matr_c2r=[];
    while i<size_centroidb(1,1)
        Matr_c2p=Matr_c2(Matr_c2(:,4)==i,:);
        size_Matr_c2p=size(Matr_c2p);
        
        i=i+1;
        if length(Matr_c2p)~=0
            Matr_c2pp=Matr_c2p(Matr_c2p(:,5)==max(Matr_c2p(:,5)),:);
            size_Matr_c2pp=size(Matr_c2pp);
            size_Matr_c2r=size(Matr_c2r);
            Matr_c2r(size_Matr_c2r(1,1)+1:size_Matr_c2r(1,1)+size_Matr_c2pp(1,1),:)=Matr_c2pp;
        end
    end

Matr_c2=Matr_c2r;
size_Matr_c2=size(Matr_c2);

if size_Matr_c2(1,1)>100
    Matr_c2=Matr_c2(Matr_c2(:,5)>=min(Matr_c2(:,5))+1,:);
    size_Matr_c2=size(Matr_c2);

end


end  
    
    
    
if size_Matr_c3(1,1)~=0
    i=1;
    Matr_c3r=[];
    while i<size_centroidb(1,1)
        Matr_c3p=Matr_c3(Matr_c3(:,4)==i,:);
        size_Matr_c3p=size(Matr_c3p);
        
        i=i+1;
        if length(Matr_c3p)~=0
            Matr_c3pp=Matr_c3p(Matr_c3p(:,5)==max(Matr_c3p(:,5)),:);
            size_Matr_c3pp=size(Matr_c3pp);
            size_Matr_c3r=size(Matr_c3r);
            Matr_c3r(size_Matr_c3r(1,1)+1:size_Matr_c3r(1,1)+size_Matr_c3pp(1,1),:)=Matr_c3pp;
        end
    end

Matr_c3=Matr_c3r;
size_Matr_c3=size(Matr_c3);

if size_Matr_c2(1,1)>100
    Matr_c2=Matr_c2(Matr_c2(:,5)>=min(Matr_c2(:,5))+1,:);
    size_Matr_c2=size(Matr_c2);

end

end

if size_Matr_car(1,1)~=0
    i=1;
    Matr_carr=[];
    while i<size_centroidb(1,1)
        Matr_carp=Matr_car(Matr_car(:,4)==i,:);
        size_Matr_carp=size(Matr_carp);
        
        i=i+1;
        if length(Matr_carp)~=0
            Matr_carpp=Matr_carp(Matr_carp(:,5)==max(Matr_carp(:,5)),:);
            size_Matr_carpp=size(Matr_carpp);
            size_Matr_carr=size(Matr_carr);
            Matr_carr(size_Matr_carr(1,1)+1:size_Matr_carr(1,1)+size_Matr_carpp(1,1),:)=Matr_carpp;
        end
    end

Matr_car=Matr_carr;
size_Matr_car=size(Matr_car);

if size_Matr_car(1,1)>100
    Matr_car=Matr_car(Matr_car(:,5)>=min(Matr_car(:,5))+1,:);
    size_Matr_car=size(Matr_car);

end







%Matr_car=sortrows(Matr_car,[5 4 1 2 3]);

%if size_Matr_car(1,1)<20
%    Matr_car=Matr_car;
%elseif size_Matr_car(1,1)>=20
%    Matr_car=Matr_car(1:20,:);
%end

%size_Matr_car=size(Matr_car);

end   



if size_Matr_n2(1,1)~=0
    i=1;
    Matr_n2r=[];
    while i<size_centroidb(1,1)
        Matr_n2p=Matr_n2(Matr_n2(:,4)==i,:);
        size_Matr_n2p=size(Matr_n2p);
        
        i=i+1;
        if length(Matr_n2p)~=0
            Matr_n2pp=Matr_n2p(Matr_n2p(:,5)==max(Matr_n2p(:,5)),:);
            size_Matr_n2pp=size(Matr_n2pp);
            size_Matr_n2r=size(Matr_n2r);
            Matr_n2r(size_Matr_n2r(1,1)+1:size_Matr_n2r(1,1)+size_Matr_n2pp(1,1),:)=Matr_n2pp;
        end
    end

Matr_n2=Matr_n2r;
size_Matr_n2=size(Matr_n2);

if size_Matr_n2(1,1)>100
    Matr_n2=Matr_n2(Matr_n2(:,5)>=min(Matr_n2(:,5))+1,:);
    size_Matr_n2=size(Matr_n2);

end

end   
if size_Matr_clHB(1,1)~=0
    i=1;
    Matr_clHBr=[];
    while i<size_centroidb(1,1)
        Matr_clHBp=Matr_clHB(Matr_clHB(:,4)==i,:);
        size_Matr_clHBp=size(Matr_clHBp);
        
        i=i+1;
        if length(Matr_clHBp)~=0
            Matr_clHBpp=Matr_clHBp(Matr_clHBp(:,5)==max(Matr_clHBp(:,5)),:);
            size_Matr_clHBpp=size(Matr_clHBpp);
            size_Matr_clHBr=size(Matr_clHBr);
            Matr_clHBr(size_Matr_clHBr(1,1)+1:size_Matr_clHBr(1,1)+size_Matr_clHBpp(1,1),:)=Matr_clHBpp;
        end
    end

Matr_clHB=Matr_clHBr;
size_Matr_clHB=size(Matr_clHB);

if size_Matr_clHB(1,1)>100
    Matr_clHB=Matr_clHB(Matr_clHB(:,5)>=min(Matr_clHB(:,5))+1,:);
    size_Matr_clHB=size(Matr_clHB);

end

end  
if size_Matr_o2HB(1,1)~=0
    i=1;
    Matr_o2HBr=[];
    while i<size_centroidb(1,1)
        Matr_o2HBp=Matr_o2HB(Matr_o2HB(:,4)==i,:);
        size_Matr_o2HBp=size(Matr_o2HBp);
        
        i=i+1;
        if length(Matr_o2HBp)~=0
            Matr_o2HBpp=Matr_o2HBp(Matr_o2HBp(:,5)==max(Matr_o2HBp(:,5)),:);
            size_Matr_o2HBpp=size(Matr_o2HBpp);
            size_Matr_o2HBr=size(Matr_o2HBr);
            Matr_o2HBr(size_Matr_o2HBr(1,1)+1:size_Matr_o2HBr(1,1)+size_Matr_o2HBpp(1,1),:)=Matr_o2HBpp;
        end
    end

Matr_o2HB=Matr_o2HBr;
size_Matr_o2HB=size(Matr_o2HB);

if size_Matr_o2HB(1,1)>100
    Matr_o2HB=Matr_o2HB(Matr_o2HB(:,5)>=min(Matr_o2HB(:,5))+1,:);
    size_Matr_o2HB=size(Matr_o2HB);

end

end


if size_Matr_brHB(1,1)~=0
    i=1;
    Matr_brHBr=[];
    while i<size_centroidb(1,1)
        Matr_brHBp=Matr_brHB(Matr_brHB(:,4)==i,:);
        size_Matr_brHBp=size(Matr_brHBp);
        
        i=i+1;
        if length(Matr_brHBp)~=0
            Matr_brHBpp=Matr_brHBp(Matr_brHBp(:,5)==max(Matr_brHBp(:,5)),:);
            size_Matr_brHBpp=size(Matr_brHBpp);
            size_Matr_brHBr=size(Matr_brHBr);
            Matr_brHBr(size_Matr_brHBr(1,1)+1:size_Matr_brHBr(1,1)+size_Matr_brHBpp(1,1),:)=Matr_brHBpp;
        end
    end

Matr_brHB=Matr_brHBr;
size_Matr_brHB=size(Matr_brHB);

if size_Matr_brHB(1,1)>100
    Matr_brHB=Matr_brHB(Matr_brHB(:,5)>=min(Matr_brHB(:,5))+1,:);
    size_Matr_brHB=size(Matr_brHB);

end

end
if size_Matr_fHB(1,1)~=0
    i=1;
    Matr_fHBr=[];
    while i<size_centroidb(1,1)
        Matr_fHBp=Matr_fHB(Matr_fHB(:,4)==i,:);
        size_Matr_fHBp=size(Matr_fHBp);
        
        i=i+1;
        if length(Matr_fHBp)~=0
            Matr_fHBpp=Matr_fHBp(Matr_fHBp(:,5)==max(Matr_fHBp(:,5)),:);
            size_Matr_fHBpp=size(Matr_fHBpp);
            size_Matr_fHBr=size(Matr_fHBr);
            Matr_fHBr(size_Matr_fHBr(1,1)+1:size_Matr_fHBr(1,1)+size_Matr_fHBpp(1,1),:)=Matr_fHBpp;
        end
    end

Matr_fHB=Matr_fHBr;
size_Matr_fHB=size(Matr_fHB);

if size_Matr_fHB(1,1)>100
    Matr_fHB=Matr_fHB(Matr_fHB(:,5)>=min(Matr_fHB(:,5))+1,:);
    size_Matr_fHB=size(Matr_fHB);

end

end

if size_Matr_o3HB(1,1)~=0
    i=1;
    Matr_o3HBr=[];
    while i<size_centroidb(1,1)
        Matr_o3HBp=Matr_o3HB(Matr_o3HB(:,4)==i,:);
        size_Matr_o3HBp=size(Matr_o3HBp);
        
        i=i+1;
        if length(Matr_o3HBp)~=0
            Matr_o3HBpp=Matr_o3HBp(Matr_o3HBp(:,5)==max(Matr_o3HBp(:,5)),:);
            size_Matr_o3HBpp=size(Matr_o3HBpp);
            size_Matr_o3HBr=size(Matr_o3HBr);
            Matr_o3HBr(size_Matr_o3HBr(1,1)+1:size_Matr_o3HBr(1,1)+size_Matr_o3HBpp(1,1),:)=Matr_o3HBpp;
        end
    end

Matr_o3HB=Matr_o3HBr;
size_Matr_o3HB=size(Matr_o3HB);

if size_Matr_o3HB(1,1)>100
    Matr_o3HB=Matr_o3HB(Matr_o3HB(:,5)>=min(Matr_o3HB(:,5))+1,:);
    size_Matr_o3HB=size(Matr_o3HB);

end

end

if size_Matr_sHB(1,1)~=0
    i=1;
    Matr_sHBr=[];
    while i<size_centroidb(1,1)
        Matr_sHBp=Matr_sHB(Matr_sHB(:,4)==i,:);
        size_Matr_sHBp=size(Matr_sHBp);
        
        i=i+1;
        if length(Matr_sHBp)~=0
            Matr_sHBpp=Matr_sHBp(Matr_sHBp(:,5)==max(Matr_sHBp(:,5)),:);
            size_Matr_sHBpp=size(Matr_sHBpp);
            size_Matr_sHBr=size(Matr_sHBr);
            Matr_sHBr(size_Matr_sHBr(1,1)+1:size_Matr_sHBr(1,1)+size_Matr_sHBpp(1,1),:)=Matr_sHBpp;
        end
    end

Matr_sHB=Matr_sHBr;
size_Matr_sHB=size(Matr_sHB);

if size_Matr_sHB(1,1)>100
    Matr_sHB=Matr_sHB(Matr_sHB(:,5)>=min(Matr_sHB(:,5))+1,:);
    size_Matr_sHB=size(Matr_sHB);

end

end

if size_Matr_n4(1,1)~=0
    i=1;
    Matr_n4r=[];
    while i<size_centroidb(1,1)
        Matr_n4p=Matr_n4(Matr_n4(:,4)==i,:);
        size_Matr_n4p=size(Matr_n4p);
        
        i=i+1;
        if length(Matr_n4p)~=0
            Matr_n4pp=Matr_n4p(Matr_n4p(:,5)==max(Matr_n4p(:,5)),:);
            size_Matr_n4pp=size(Matr_n4pp);
            size_Matr_n4r=size(Matr_n4r);
            Matr_n4r(size_Matr_n4r(1,1)+1:size_Matr_n4r(1,1)+size_Matr_n4pp(1,1),:)=Matr_n4pp;
        end
    end

Matr_n4=Matr_n4r;
size_Matr_n4=size(Matr_n4);

if size_Matr_n4(1,1)>100
    Matr_n4=Matr_n4(Matr_n4(:,5)>=min(Matr_n4(:,5))+1,:);
    size_Matr_n4=size(Matr_n4);

end

end

if size_Matr_npl3(1,1)~=0
    i=1;
    Matr_npl3r=[];
    while i<size_centroidb(1,1)
        Matr_npl3p=Matr_npl3(Matr_npl3(:,4)==i,:);
        size_Matr_npl3p=size(Matr_npl3p);
        
        i=i+1;
        if length(Matr_npl3p)~=0
            Matr_npl3pp=Matr_npl3p(Matr_npl3p(:,5)==max(Matr_npl3p(:,5)),:);
            size_Matr_npl3pp=size(Matr_npl3pp);
            size_Matr_npl3r=size(Matr_npl3r);
            Matr_npl3r(size_Matr_npl3r(1,1)+1:size_Matr_npl3r(1,1)+size_Matr_npl3pp(1,1),:)=Matr_npl3pp;
        end
    end

Matr_npl3=Matr_npl3r;
size_Matr_npl3=size(Matr_npl3);

if size_Matr_npl3(1,1)>100
    Matr_npl3=Matr_npl3(Matr_npl3(:,5)>=min(Matr_npl3(:,5))+1,:);
    size_Matr_npl3=size(Matr_npl3);

end

end

if size_Matr_s(1,1)~=0
    i=1;
    Matr_sr=[];
    while i<size_centroidb(1,1)
        Matr_sp=Matr_s(Matr_s(:,4)==i,:);
        size_Matr_sp=size(Matr_sp);
        
        i=i+1;
        if length(Matr_sp)~=0
            Matr_spp=Matr_sp(Matr_sp(:,5)==max(Matr_sp(:,5)),:);
            size_Matr_spp=size(Matr_spp);
            size_Matr_sr=size(Matr_sr);
            Matr_sr(size_Matr_sr(1,1)+1:size_Matr_sr(1,1)+size_Matr_spp(1,1),:)=Matr_spp;
        end
    end

Matr_s=Matr_sr;
size_Matr_s=size(Matr_s);

if size_Matr_s(1,1)>100
    Matr_s=Matr_s(Matr_s(:,5)>=min(Matr_s(:,5))+1,:);
    size_Matr_s=size(Matr_s);

end

end
if size_Matr_nar(1,1)~=0
    i=1;
    Matr_narr=[];
    while i<size_centroidb(1,1)
        Matr_narp=Matr_nar(Matr_nar(:,4)==i,:);
        size_Matr_narp=size(Matr_narp);
        
        i=i+1;
        if length(Matr_narp)~=0
            Matr_narpp=Matr_narp(Matr_narp(:,5)==max(Matr_narp(:,5)),:);
            size_Matr_narpp=size(Matr_narpp);
            size_Matr_narr=size(Matr_narr);
            Matr_narr(size_Matr_narr(1,1)+1:size_Matr_narr(1,1)+size_Matr_narpp(1,1),:)=Matr_narpp;
        end
    end

Matr_nar=Matr_narr;
size_Matr_nar=size(Matr_nar);

if size_Matr_nar(1,1)>100
    Matr_nar=Matr_nar(Matr_nar(:,5)>=min(Matr_nar(:,5))+1,:);
    size_Matr_nar=size(Matr_nar);

end

end

if size_Matr_br(1,1)~=0
    i=1;
    Matr_brr=[];
    while i<size_centroidb(1,1)
        Matr_brp=Matr_br(Matr_br(:,4)==i,:);
        size_Matr_brp=size(Matr_brp);
        
        i=i+1;
        if length(Matr_brp)~=0
            Matr_brpp=Matr_brp(Matr_brp(:,5)==max(Matr_brp(:,5)),:);
            size_Matr_brpp=size(Matr_brpp);
            size_Matr_brr=size(Matr_brr);
            Matr_brr(size_Matr_brr(1,1)+1:size_Matr_brr(1,1)+size_Matr_brpp(1,1),:)=Matr_brpp;
        end
    end

Matr_br=Matr_brr;
size_Matr_br=size(Matr_br);

if size_Matr_br(1,1)>100
    Matr_br=Matr_br(Matr_br(:,5)>=min(Matr_br(:,5))+1,:);
    size_Matr_br=size(Matr_br);

end

end
if size_Matr_c1(1,1)~=0
    i=1;
    Matr_c1r=[];
    while i<size_centroidb(1,1)
        Matr_c1p=Matr_c1(Matr_c1(:,4)==i,:);
        size_Matr_c1p=size(Matr_c1p);
        
        i=i+1;
        if length(Matr_c1p)~=0
            Matr_c1pp=Matr_c1p(Matr_c1p(:,5)==max(Matr_c1p(:,5)),:);
            size_Matr_c1pp=size(Matr_c1pp);
            size_Matr_c1r=size(Matr_c1r);
            Matr_c1r(size_Matr_c1r(1,1)+1:size_Matr_c1r(1,1)+size_Matr_c1pp(1,1),:)=Matr_c1pp;
        end
    end

Matr_c1=Matr_c1r;
size_Matr_c1=size(Matr_c1);

if size_Matr_c1(1,1)>100
    Matr_c1=Matr_c1(Matr_c1(:,5)>=min(Matr_c1(:,5))+1,:);
    size_Matr_c1=size(Matr_c1);

end

end
if size_Matr_cl(1,1)~=0
    i=1;
    Matr_clr=[];
    while i<size_centroidb(1,1)
        Matr_clp=Matr_cl(Matr_cl(:,4)==i,:);
        size_Matr_clp=size(Matr_clp);
        
        i=i+1;
        if length(Matr_clp)~=0
            Matr_clpp=Matr_clp(Matr_clp(:,5)==max(Matr_clp(:,5)),:);
            size_Matr_clpp=size(Matr_clpp);
            size_Matr_clr=size(Matr_clr);
            Matr_clr(size_Matr_clr(1,1)+1:size_Matr_clr(1,1)+size_Matr_clpp(1,1),:)=Matr_clpp;
        end
    end

Matr_cl=Matr_clr;
size_Matr_cl=size(Matr_cl);

if size_Matr_cl(1,1)>100
    Matr_cl=Matr_cl(Matr_cl(:,5)>=min(Matr_cl(:,5))+1,:);
    size_Matr_cl=size(Matr_cl);

end

end
if size_Matr_f(1,1)~=0
    i=1;
    Matr_fr=[];
    while i<size_centroidb(1,1)
        Matr_fp=Matr_f(Matr_f(:,4)==i,:);
        size_Matr_fp=size(Matr_fp);
        
        i=i+1;
        if length(Matr_fp)~=0
            Matr_fpp=Matr_fp(Matr_fp(:,5)==max(Matr_fp(:,5)),:);
            size_Matr_fpp=size(Matr_fpp);
            size_Matr_fr=size(Matr_fr);
            Matr_fr(size_Matr_fr(1,1)+1:size_Matr_fr(1,1)+size_Matr_fpp(1,1),:)=Matr_fpp;
        end
    end

Matr_f=Matr_fr;
size_Matr_f=size(Matr_f);

if size_Matr_f(1,1)>100
    Matr_f=Matr_f(Matr_f(:,5)>=min(Matr_f(:,5))+1,:);
    size_Matr_f=size(Matr_f);

end

end
if size_Matr_namHB(1,1)~=0
    i=1;
    Matr_namHBr=[];
    while i<size_centroidb(1,1)
        Matr_namHBp=Matr_namHB(Matr_namHB(:,4)==i,:);
        size_Matr_namHBp=size(Matr_namHBp);
        
        i=i+1;
        if length(Matr_namHBp)~=0
            Matr_namHBpp=Matr_namHBp(Matr_namHBp(:,5)==max(Matr_namHBp(:,5)),:);
            size_Matr_namHBpp=size(Matr_namHBpp);
            size_Matr_namHBr=size(Matr_namHBr);
            Matr_namHBr(size_Matr_namHBr(1,1)+1:size_Matr_namHBr(1,1)+size_Matr_namHBpp(1,1),:)=Matr_namHBpp;
        end
    end

Matr_namHB=Matr_namHBr;
size_Matr_namHB=size(Matr_namHB);

if size_Matr_namHB(1,1)>100
    Matr_namHB=Matr_namHB(Matr_namHB(:,5)>=min(Matr_namHB(:,5))+1,:);
    size_Matr_namHB=size(Matr_namHB);

end

end
if size_Matr_npl3HB(1,1)~=0
    i=1;
    Matr_npl3HBr=[];
    while i<size_centroidb(1,1)
        Matr_npl3HBp=Matr_npl3HB(Matr_npl3HB(:,4)==i,:);
        size_Matr_npl3HBp=size(Matr_npl3HBp);
        
        i=i+1;
        if length(Matr_npl3HBp)~=0
            Matr_npl3HBpp=Matr_npl3HBp(Matr_npl3HBp(:,5)==max(Matr_npl3HBp(:,5)),:);
            size_Matr_npl3HBpp=size(Matr_npl3HBpp);
            size_Matr_npl3HBr=size(Matr_npl3HBr);
            Matr_npl3HBr(size_Matr_npl3HBr(1,1)+1:size_Matr_npl3HBr(1,1)+size_Matr_npl3HBpp(1,1),:)=Matr_npl3HBpp;
        end
    end

Matr_npl3HB=Matr_npl3HBr;
size_Matr_npl3HB=size(Matr_npl3HB);

if size_Matr_npl3HB(1,1)>100
    Matr_npl3HB=Matr_npl3HB(Matr_npl3HB(:,5)>=min(Matr_npl3HB(:,5))+1,:);
    size_Matr_npl3HB=size(Matr_npl3HB);

end

end
if size_Matr_nam(1,1)~=0
    i=1;
    Matr_namr=[];
    while i<size_centroidb(1,1)
        Matr_namp=Matr_nam(Matr_nam(:,4)==i,:);
        size_Matr_namp=size(Matr_namp);
        
        i=i+1;
        if length(Matr_namp)~=0
            Matr_nampp=Matr_namp(Matr_namp(:,5)==max(Matr_namp(:,5)),:);
            size_Matr_nampp=size(Matr_nampp);
            size_Matr_namr=size(Matr_namr);
            Matr_namr(size_Matr_namr(1,1)+1:size_Matr_namr(1,1)+size_Matr_nampp(1,1),:)=Matr_nampp;
        end
    end

Matr_nam=Matr_namr;
size_Matr_nam=size(Matr_nam);

if size_Matr_nam(1,1)>100
    Matr_nam=Matr_nam(Matr_nam(:,5)>=min(Matr_nam(:,5))+1,:);
    size_Matr_nam=size(Matr_nam);

end

end
if size_Matr_o2(1,1)~=0
    i=1;
    Matr_o2r=[];
    while i<size_centroidb(1,1)
        Matr_o2p=Matr_o2(Matr_o2(:,4)==i,:);
        size_Matr_o2p=size(Matr_o2p);
        
        i=i+1;
        if length(Matr_o2p)~=0
            Matr_o2pp=Matr_o2p(Matr_o2p(:,5)==max(Matr_o2p(:,5)),:);
            size_Matr_o2pp=size(Matr_o2pp);
            size_Matr_o2r=size(Matr_o2r);
            Matr_o2r(size_Matr_o2r(1,1)+1:size_Matr_o2r(1,1)+size_Matr_o2pp(1,1),:)=Matr_o2pp;
        end
    end

Matr_o2=Matr_o2r;
size_Matr_o2=size(Matr_o2);

if size_Matr_o2(1,1)>100
    Matr_o2=Matr_o2(Matr_o2(:,5)>=min(Matr_o2(:,5))+1,:);
    size_Matr_o2=size(Matr_o2);

end

end
if size_Matr_o3(1,1)~=0
    i=1;
    Matr_o3r=[];
    while i<size_centroidb(1,1)
        Matr_o3p=Matr_o3(Matr_o3(:,4)==i,:);
        size_Matr_o3p=size(Matr_o3p);
        
        i=i+1;
        if length(Matr_o3p)~=0
            Matr_o3pp=Matr_o3p(Matr_o3p(:,5)==max(Matr_o3p(:,5)),:);
            size_Matr_o3pp=size(Matr_o3pp);
            size_Matr_o3r=size(Matr_o3r);
            Matr_o3r(size_Matr_o3r(1,1)+1:size_Matr_o3r(1,1)+size_Matr_o3pp(1,1),:)=Matr_o3pp;
        end
    end

Matr_o3=Matr_o3r;
size_Matr_o3=size(Matr_o3);

if size_Matr_o3(1,1)>100
    Matr_o3=Matr_o3(Matr_o3(:,5)>=min(Matr_o3(:,5))+1,:);
    size_Matr_o3=size(Matr_o3);

end

end
if size_Matr_i(1,1)~=0
    i=1;
    Matr_ir=[];
    while i<size_centroidb(1,1)
        Matr_ip=Matr_i(Matr_i(:,4)==i,:);
        size_Matr_ip=size(Matr_ip);
        
        i=i+1;
        if length(Matr_ip)~=0
            Matr_ipp=Matr_ip(Matr_ip(:,5)==max(Matr_ip(:,5)),:);
            size_Matr_ipp=size(Matr_ipp);
            size_Matr_ir=size(Matr_ir);
            Matr_ir(size_Matr_ir(1,1)+1:size_Matr_ir(1,1)+size_Matr_ipp(1,1),:)=Matr_ipp;
        end
    end

Matr_i=Matr_ir;
size_Matr_i=size(Matr_i);

if size_Matr_i(1,1)>100
    Matr_i=Matr_i(Matr_i(:,5)>=min(Matr_i(:,5))+1,:);
    size_Matr_i=size(Matr_i);

end

end

Matr_br=[Matr_br
    Matr_brHB];
Matr_br=sortrows(Matr_br,[4 1 2 3]);
size_Matr_br=size(Matr_br);


Matr_cl=[Matr_cl
    Matr_clHB];
Matr_cl=sortrows(Matr_cl,[4 1 2 3]);
size_Matr_cl=size(Matr_cl);

Matr_o2=[Matr_o2
    Matr_o2HB];
Matr_o2=sortrows(Matr_o2,[4 1 2 3]);
size_Matr_o2=size(Matr_o2);


size_M=[size_Matr_br(1,1)
    size_Matr_cl(1,1)
    %size_Matr_c3(1,1)
    %size_Matr_c2(1,1)
    size_Matr_car(1,1)
    size_Matr_n4(1,1)
    size_Matr_nam(1,1)
    size_Matr_namHB(1,1)
    size_Matr_npl3(1,1)
    size_Matr_npl3HB(1,1)
    size_Matr_o2(1,1)
    size_Matr_o3(1,1)
    size_Matr_o3HB(1,1)
    size_Matr_s(1,1)
    size_Matr_sHB(1,1)
    ];

M_num=max(size_M);

if size_Matr_br(1,1)<M_num
    Matr_br(size_Matr_br+1:M_num,1:5)=0;
end

if size_Matr_cl(1,1)<M_num
    Matr_cl(size_Matr_cl+1:M_num,1:5)=0;
end


%if size_Matr_c3(1,1)<M_num
%    Matr_c3(size_Matr_c3+1:M_num,1:5)=0;
%end


%if size_Matr_c2(1,1)<M_num
%    Matr_c2(size_Matr_c2+1:M_num,1:5)=0;
%end



if size_Matr_car(1,1)<M_num
    Matr_car(size_Matr_car+1:M_num,1:5)=0;
end



if size_Matr_n4(1,1)<M_num
    Matr_n4(size_Matr_n4+1:M_num,1:5)=0;
end


if size_Matr_nam(1,1)<M_num
    Matr_nam(size_Matr_nam+1:M_num,1:5)=0;
end


if size_Matr_namHB(1,1)<M_num
    Matr_namHB(size_Matr_namHB+1:M_num,1:5)=0;
end


if size_Matr_npl3(1,1)<M_num
    Matr_npl3(size_Matr_npl3+1:M_num,1:5)=0;
end


if size_Matr_npl3HB(1,1)<M_num
    Matr_npl3HB(size_Matr_npl3HB+1:M_num,1:5)=0;
end



if size_Matr_o2(1,1)<M_num
    Matr_o2(size_Matr_o2+1:M_num,1:5)=0;
end


if size_Matr_o3(1,1)<M_num
    Matr_o3(size_Matr_o3+1:M_num,1:5)=0;
end



if size_Matr_o3HB(1,1)<M_num
    Matr_o3HB(size_Matr_o3HB+1:M_num,1:5)=0;
end


if size_Matr_s(1,1)<M_num
    Matr_s(size_Matr_s+1:M_num,1:5)=0;
end

if size_Matr_sHB(1,1)<M_num
    Matr_sHB(size_Matr_sHB+1:M_num,1:5)=0;
end

%out=[Matr_br Matr_cl Matr_c3 Matr_c2 Matr_car Matr_n4 Matr_nam Matr_namHB Matr_npl3 Matr_npl3HB Matr_o2 Matr_o3 Matr_o3HB Matr_s Matr_sHB];

out=[Matr_br Matr_cl Matr_car Matr_n4 Matr_nam Matr_namHB Matr_npl3 Matr_npl3HB Matr_o2 Matr_o3 Matr_o3HB Matr_s Matr_sHB];








