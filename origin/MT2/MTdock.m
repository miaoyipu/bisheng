clear all


tic



ligand_dir=[ strcat('/Users/miao/Workspace/BiSheng/example/3h78_ligand.mol2')];

% Enter protein pdb file directory for blur
protein_dir=[ strcat('/Users/miao/Workspace/BiSheng/example/3h78_pocket.pdb')];

% Enter ligand mol2 file directory for blur
%ligand_dir=[ strcat('E:\pdbbind_v2013\v2013\2zo3\2zo3_ligand.mol2')];

% Enter protein pdb file directory for blur
%protein_dir=[ strcat('E:\pdbbind_v2013\v2013\2zo3\2zo3_protein.pdb')];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

big=1;

asdf=0

best_struc=[];


clear training_protein_dir;
clear training_ligand_dir;
clear PDBData1;
clear protein_atom1;
clear protein_atom2;
clear protein_atom3;
clear protein_atomname;
clear protein_res;
clear protein_resseq;
clear protein_occupancy;
clear protein_atom_f;
clear protein_atom;
clear fidin;
clear tline;
clear atom;
clear name_str;
clear mass;
clear protein;
clear protein_name;
clear protein_atom_radius;
clear protein_atom_hb;
clear protein_acceptor_hb_angle;
clear protein_donor_hb_angle;
clear protein_atom_charge;
clear protein_backbone;
clear protein_chainID;
clear protein_resseq;
clear protein_atom_num;
clear protential_protein;
clear atom_conj_num;
clear name;
clear atom_radius;
clear atom_hb;
clear acceptor_hb_angle;
clear donor_hb_angle;
clear atom_mass;
clear atom_charge;
clear total_atom_mass;
clear neib_H;
clear atom_hb;
clear donor_hb_angle;
clear acceptor_hb_angle;
clear ligand;
clear ligand_atom_num;
clear protein_H;
clear protein_pointerB;
clear protein_pointerB_list;
clear protein_pocket_partB;
clear protein_pocket_partBB;
clear protein_starterA;
clear protein_starterB;
clear protein_starterAA;
clear protein_pocket_partA;
clear protein_pocket_partB;
clear protein_pocketA_g;
clear protein_starterA_pointer;
clear protein_pocketA_lili	;
clear protein_pocketB_g;
clear protein_starterB;
clear protein_starterB_pointer;
clear protein_pocketB_lili;
clear protein_pocketA_fragment;
clear protein_pocketA_fragment_pointer;
clear protein_pocketB_fragment;
clear protein_pocketB_fragment_pointer;
clear protein_complex_fragment;
clear protein_A_pointer;
clear protein_B_pointer;
clear protein_A_pointerb;
clear protein_B_pointerb;
clear rll;
clear rpp;
clear protein_list_resseq;
clear protein_list_resname;
clear protein_pocketA_output_name1;
clear protein_A_pointer_starter;
clear protein_B_pointer_starter;
clear protein_pocketA_output_name1;
clear protein_pocketB_output_name1;
clear protein_pocketA_output_name11;
clear protein_pocketA_output_name11_pointer	;
clear protein_pocketA_output_name_f;
clear qq;
clear protein_pocketB_output_name11;
clear protein_pocketB_output_name11_pointer;
clear protein_pocketB_output_name_f;
clear output_info;
clear aaa;
clear bbb;
clear ddd;
clear eee;
clear output_info_resname;
clear output_info_ligand_atomname;
clear output_info_protein_atomname;
clear ligand_desolvation_atom;
clear ligand_in_desovation_distance_atom;
clear grid_coor;
clear grid_property;
clear grid;
clear tmp_grid_x1;
clear tmp_grid_x2;
clear tmp_grid_y1;
clear tmp_grid_y2;
clear tmp_grid_z1;
clear tmp_grid_z2;
clear surface_grid;
clear desolvation_surface_grid;
clear final_desolvation_surface_grid;

%protein atoms initialization


PDBData1 = pdbread(protein_dir(1,:));
protein_atom1=[PDBData1.Model.Atom.X];
protein_atom2=[PDBData1.Model.Atom.Y];
protein_atom3=[PDBData1.Model.Atom.Z];
protein_atomname={PDBData1.Model.Atom.AtomName};
protein_res={PDBData1.Model.Atom.resName};
protein_resseq=[PDBData1.Model.Atom.resSeq];
protein_occupancy=[PDBData1.Model.Atom.occupancy];

%protein_atom_f=[transpose(protein_atom1),transpose(protein_atom2),transpose(protein_atom3),transpose(protein_resseq)];
protein_atom=[transpose(protein_atom1),transpose(protein_atom2(2,:)),transpose(protein_atom3(2,:))];


C_3=1;
C_2=2;
C_1=3;
C_ar=4;
O_3=5;
O_2=6;
O_w=7;
N_3=8;
N_2=9;
N_1=10;
N_ar=11;
N_am=12;
N_pl3=13;
S_3=14;
S_2=15;
S_o=16;
S_o2=17;
F=18;
Cl=19;
Br=20;
I=21;
P=22;
H=23;
C_cat=24;
O_co2=25;
N_4=26;

LI   = 27; 
NA   = 28;	
K    = 29 ;    
MG   = 30  ;  
CAA   = 31	;
AL   = 32	;
MN   = 33  	;
FE   = 34    ;
CO   = 35    ;
NI   = 36    ;
CU   = 37    ;
ZN   = 38    ;
        
        
N_A=0;
D=1;
D2=2;
A=3;
DA=4;
        
ALA=101;
ARG=102;
ASN=103;
ASP=104;
CYS=105;
GLN=106;
GLU=107;
GLY=108;
HIS=109;
ILE=110;
LEU=111;
LYS=112;
MET=113;
PHE=114;
PRO=115;
SER=116;
THR=117;
TRP=118;
TYR=119;
VAL=120;



carbon='C';
oxygen='O';
nitrogen='N';
sulfer='S';
fluorin='F';
chlorine='Cl';
bromine='Br';
iodine='I';
phosphor='P';
hydrogen='H';




for i=1:length(protein_atom1)
    if (strncmp(PDBData1.Model.Atom(1,i).AtomName(1), hydrogen, 1)==1)|(strncmp(PDBData1.Model.Atom(1,i).AtomName(1), '1', 1)==1)|(strncmp(PDBData1.Model.Atom(1,i).AtomName(1), '2', 1)==1)|(strncmp(PDBData1.Model.Atom(1,i).AtomName(1), '3', 1)==1)
        protein_name(i,1)=H;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1;
        protein_atom_hb(i)=N_A;
        protein_acceptor_hb_angle(i)=0;
        protein_donor_hb_angle(i)=0;
        protein_atom_charge(i)=0;
        protein_backbone(i)=1;
        
        
        
        
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'CA', 2)==1)
        protein_name(i,1)=C_3;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.94;
        protein_atom_hb(i)=N_A;
        protein_acceptor_hb_angle(i)=0;
        protein_donor_hb_angle(i)=0;
        protein_atom_charge(i)=0;
        protein_backbone(i)=1;
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'C', 1)==1)&(length(PDBData1.Model.Atom(1,i).AtomName)==1)
        protein_name(i,1)=C_2;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.90;
        protein_atom_hb(i)=N_A;
        protein_acceptor_hb_angle(i)=0;
        protein_donor_hb_angle(i)=0;
        protein_atom_charge(i)=0;
        protein_backbone(i)=1;
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'O', 1)==1)&(length(PDBData1.Model.Atom(1,i).AtomName)==1)
        protein_name(i,1)=O_2;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.66;
        protein_atom_hb(i)=A;
        protein_acceptor_hb_angle(i)=0.75*pi;
        protein_donor_hb_angle(i)=0;
        protein_atom_charge(i)=0;
        protein_backbone(i)=1;
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'N', 1)==1)&(length(PDBData1.Model.Atom(1,i).AtomName)==1)
        protein_name(i,1)=N_am;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.83;
        protein_atom_hb(i)=D;
        protein_acceptor_hb_angle(i)=0;
        protein_donor_hb_angle(i)=pi;
        protein_atom_charge(i)=0;
        protein_backbone(i)=1;

        
        
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'NE', 2)==1)&(strncmp(PDBData1.Model.Atom(1,i).resName, 'ARG', 3)==1)
        protein_name(i,1)=N_pl3;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.83;
        protein_atom_hb(i)=D;
        protein_acceptor_hb_angle(i)=0;
        protein_donor_hb_angle(i)=pi;
        protein_atom_charge(i)=0;
        protein_backbone(i)=0;
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'CG', 2)==1)&(strncmp(PDBData1.Model.Atom(1,i).resName, 'ARG', 3)==1)
        protein_name(i,1)=C_3;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.94;
        protein_atom_hb(i)=N_A;
        protein_acceptor_hb_angle(i)=0;
        protein_donor_hb_angle(i)=0;
        protein_atom_charge(i)=0;
        protein_backbone(i)=0;
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'CZ', 2)==1)&(strncmp(PDBData1.Model.Atom(1,i).resName, 'ARG', 3)==1)
        protein_name(i,1)=C_2;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.90;
        protein_atom_hb(i)=N_A;
        protein_acceptor_hb_angle(i)=0;
        protein_donor_hb_angle(i)=0;
        protein_atom_charge(i)=1;
        protein_backbone(i)=0;
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'NH1', 3)==1)&(strncmp(PDBData1.Model.Atom(1,i).resName, 'ARG', 3)==1)
        protein_name(i,1)=N_pl3;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.83;
        protein_atom_hb(i)=D2;
        protein_acceptor_hb_angle(i)=0;
        protein_donor_hb_angle(i)=pi;
        protein_atom_charge(i)=0;
        protein_backbone(i)=0;
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'NH2', 3)==1)&(strncmp(PDBData1.Model.Atom(1,i).resName, 'ARG', 3)==1)
        protein_name(i,1)=N_pl3;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.83;
        protein_atom_hb(i)=D2;
        protein_acceptor_hb_angle(i)=0;
        protein_donor_hb_angle(i)=pi;
        protein_atom_charge(i)=0;
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'CD', 2)==1)&(strncmp(PDBData1.Model.Atom(1,i).resName, 'ARG', 3)==1)
        protein_name(i,1)=C_3;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.94;
        protein_atom_hb(i)=N_A;
        protein_acceptor_hb_angle(i)=0;
        protein_donor_hb_angle(i)=0;
        protein_atom_charge(i)=0;
        protein_backbone(i)=0;
        

    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'OD1', 3)==1)&(strncmp(PDBData1.Model.Atom(1,i).resName, 'ASN', 3)==1)
        protein_name(i,1)=O_2;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.66;
        protein_atom_hb(i)=A;
        protein_acceptor_hb_angle(i)=0.75*pi;
        protein_donor_hb_angle(i)=0;
        protein_atom_charge(i)=0;
        protein_backbone(i)=0;
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'ND2', 3)==1)&(strncmp(PDBData1.Model.Atom(1,i).resName, 'ASN', 3)==1)
        protein_name(i,1)=N_am;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.83;
        protein_atom_hb(i)=D2;
        protein_acceptor_hb_angle(i)=0;
        protein_donor_hb_angle(i)=pi;
        protein_atom_charge(i)=0;
        protein_backbone(i)=0;
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'CG', 2)==1)&(strncmp(PDBData1.Model.Atom(1,i).resName, 'ASN', 3)==1)
        protein_name(i,1)=C_2;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.90;
        protein_atom_hb(i)=N_A;
        protein_acceptor_hb_angle(i)=0;
        protein_donor_hb_angle(i)=0;
        protein_atom_charge(i)=0;
        protein_backbone(i)=0;
        
        
        
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'OD1', 3)==1)&(strncmp(PDBData1.Model.Atom(1,i).resName, 'ASP', 3)==1)
        protein_name(i,1)=O_2;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.66;
        protein_atom_hb(i)=A;
        protein_acceptor_hb_angle(i)=0.75*pi;
        protein_donor_hb_angle(i)=0;
        protein_atom_charge(i)=-0.5;
        protein_backbone(i)=0;
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'OD2', 3)==1)&(strncmp(PDBData1.Model.Atom(1,i).resName, 'ASP', 3)==1)
        protein_name(i,1)=O_2;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.66;
        protein_atom_hb(i)=A;
        protein_acceptor_hb_angle(i)=0.75*pi;
        protein_donor_hb_angle(i)=0;
        protein_atom_charge(i)=-0.5;
        protein_backbone(i)=0;
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'CG', 2)==1)&(strncmp(PDBData1.Model.Atom(1,i).resName, 'ASP', 3)==1)
        protein_name(i,1)=C_2;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.90;
        protein_atom_hb(i)=N_A;
        protein_acceptor_hb_angle(i)=0;
        protein_donor_hb_angle(i)=0;
        protein_atom_charge(i)=0;
        protein_backbone(i)=0;
        
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName(1), 'S', 1)==1)&(strncmp(PDBData1.Model.Atom(1,i).resName, 'CYS', 3)==1)
        protein_name(i,1)=S_3;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=2.09;
        protein_atom_hb(i)=DA;
        protein_acceptor_hb_angle(i)=0.6083*pi;
        protein_donor_hb_angle(i)=pi;
        protein_atom_charge(i)=0.087;
        protein_backbone(i)=0;
        
        
        
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'OE1', 3)==1)&(strncmp(PDBData1.Model.Atom(1,i).resName, 'GLN', 3)==1)
        protein_name(i,1)=O_2;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.66;
        protein_atom_hb(i)=A;
        protein_acceptor_hb_angle(i)=0.75*pi;
        protein_donor_hb_angle(i)=0;
        protein_atom_charge(i)=0;
        protein_backbone(i)=0;
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'NE2', 3)==1)&(strncmp(PDBData1.Model.Atom(1,i).resName, 'GLN', 3)==1)
        protein_name(i,1)=N_am;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.87;
        protein_atom_hb(i)=D2;
        protein_acceptor_hb_angle(i)=0;
        protein_donor_hb_angle(i)=pi;
        protein_atom_charge(i)=0;
        protein_backbone(i)=0;
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'CG', 2)==1)&(strncmp(PDBData1.Model.Atom(1,i).resName, 'GLN', 3)==1)
        protein_name(i,1)=C_3;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.94;
        protein_atom_hb(i)=N_A;
        protein_acceptor_hb_angle(i)=0;
        protein_donor_hb_angle(i)=0;
        protein_atom_charge(i)=0;
        protein_backbone(i)=0;
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'CD', 2)==1)&(strncmp(PDBData1.Model.Atom(1,i).resName, 'GLN', 3)==1)
        protein_name(i,1)=C_2;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.90;
        protein_atom_hb(i)=N_A;
        protein_acceptor_hb_angle(i)=0;
        protein_donor_hb_angle(i)=0;
        protein_atom_charge(i)=0;
        protein_backbone(i)=0;
        
        
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'OE1', 3)==1)&(strncmp(PDBData1.Model.Atom(1,i).resName, 'GLU', 3)==1)
        protein_name(i,1)=O_2;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.66;
        protein_atom_hb(i)=A;
        protein_acceptor_hb_angle(i)=0.75*pi;
        protein_donor_hb_angle(i)=0;
        protein_atom_charge(i)=-0.5;
        protein_backbone(i)=0;
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'OE2', 3)==1)&(strncmp(PDBData1.Model.Atom(1,i).resName, 'GLU', 3)==1)
        protein_name(i,1)=O_2;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.66;
        protein_atom_hb(i)=A;
        protein_acceptor_hb_angle(i)=0.75*pi;
        protein_donor_hb_angle(i)=0;
        protein_atom_charge(i)=-0.5;
        protein_backbone(i)=0;
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'CG', 2)==1)&(strncmp(PDBData1.Model.Atom(1,i).resName, 'GLU', 3)==1)
        protein_name(i,1)=C_3;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.94;
        protein_atom_hb(i)=N_A;
        protein_acceptor_hb_angle(i)=0;
        protein_donor_hb_angle(i)=0;
        protein_atom_charge(i)=0;
        protein_backbone(i)=0;
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'CD', 2)==1)&(strncmp(PDBData1.Model.Atom(1,i).resName, 'GLU', 3)==1)
        protein_name(i,1)=C_2;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.90;
        protein_atom_hb(i)=N_A;
        protein_acceptor_hb_angle(i)=0;
        protein_donor_hb_angle(i)=0;
        protein_atom_charge(i)=0;
        protein_backbone(i)=0;
        
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'ND1', 3)==1)&(strncmp(PDBData1.Model.Atom(1,i).resName, 'HIE', 3)==1)
        protein_name(i,1)=N_ar;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.87;
        protein_atom_hb(i)=N_A;
        protein_acceptor_hb_angle(i)=0.75*pi;
        protein_donor_hb_angle(i)=pi;
        protein_atom_charge(i)=0;
        protein_backbone(i)=0;
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'CD2', 3)==1)&(strncmp(PDBData1.Model.Atom(1,i).resName, 'HIE', 3)==1)
        protein_name(i,1)=C_2;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.9;
        protein_atom_hb(i)=N_A;
        protein_acceptor_hb_angle(i)=0;
        protein_donor_hb_angle(i)=0;
        protein_atom_charge(i)=0;
        protein_backbone(i)=0;
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'CE1', 3)==1)&(strncmp(PDBData1.Model.Atom(1,i).resName, 'HIE', 3)==1)
        protein_name(i,1)=C_2;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.9;
        protein_atom_hb(i)=N_A;
        protein_acceptor_hb_angle(i)=0;
        protein_donor_hb_angle(i)=0;
        protein_atom_charge(i)=0;
        protein_backbone(i)=0;
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'NE2', 3)==1)&(strncmp(PDBData1.Model.Atom(1,i).resName, 'HIE', 3)==1)
        protein_name(i,1)=N_pl3;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.87;
        protein_atom_hb(i)=D;
        protein_acceptor_hb_angle(i)=0.75*pi;
        protein_donor_hb_angle(i)=0;
        protein_atom_charge(i)=0;
        protein_backbone(i)=0;
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'CG', 2)==1)&(strncmp(PDBData1.Model.Atom(1,i).resName, 'HIE', 3)==1)
        protein_name(i,1)=C_2;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.90;
        protein_atom_hb(i)=N_A;
        protein_acceptor_hb_angle(i)=0;
        protein_donor_hb_angle(i)=0;
        protein_atom_charge(i)=0;
        protein_backbone(i)=0;
        
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'ND1', 3)==1)&(strncmp(PDBData1.Model.Atom(1,i).resName, 'HIS', 3)==1)
        protein_name(i,1)=N_pl3;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.87;
        protein_atom_hb(i)=D;
        protein_acceptor_hb_angle(i)=0.75*pi;
        protein_donor_hb_angle(i)=pi;
        protein_atom_charge(i)=1;
        protein_backbone(i)=0;
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'CD2', 3)==1)&(strncmp(PDBData1.Model.Atom(1,i).resName, 'HIS', 3)==1)
        protein_name(i,1)=C_2;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.9;
        protein_atom_hb(i)=N_A;
        protein_acceptor_hb_angle(i)=0;
        protein_donor_hb_angle(i)=0;
        protein_atom_charge(i)=0;
        protein_backbone(i)=0;
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'CE1', 3)==1)&(strncmp(PDBData1.Model.Atom(1,i).resName, 'HIS', 3)==1)
        protein_name(i,1)=C_2;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.9;
        protein_atom_hb(i)=N_A;
        protein_acceptor_hb_angle(i)=0;
        protein_donor_hb_angle(i)=0;
        protein_atom_charge(i)=0;
        protein_backbone(i)=0;
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'NE2', 3)==1)&(strncmp(PDBData1.Model.Atom(1,i).resName, 'HIS', 3)==1)
        protein_name(i,1)=N_pl3;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.87;
        protein_atom_hb(i)=D;
        protein_acceptor_hb_angle(i)=0.75*pi;
        protein_donor_hb_angle(i)=0;
        protein_atom_charge(i)=0;
        protein_backbone(i)=0;
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'CG', 2)==1)&(strncmp(PDBData1.Model.Atom(1,i).resName, 'HIS', 3)==1)
        protein_name(i,1)=C_2;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.90;
        protein_atom_hb(i)=N_A;
        protein_acceptor_hb_angle(i)=0;
        protein_donor_hb_angle(i)=0;
        protein_atom_charge(i)=0;
        protein_backbone(i)=0;
        
        
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'CG1', 3)==1)&(strncmp(PDBData1.Model.Atom(1,i).resName, 'ILE', 3)==1)
        protein_name(i,1)=C_3;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.94;
        protein_atom_hb(i)=N_A;
        protein_acceptor_hb_angle(i)=0;
        protein_donor_hb_angle(i)=0;
        protein_atom_charge(i)=0;
        protein_backbone(i)=0;
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'CG2', 3)==1)&(strncmp(PDBData1.Model.Atom(1,i).resName, 'ILE', 3)==1)
        protein_name(i,1)=C_3;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.94;
        protein_atom_hb(i)=N_A;
        protein_acceptor_hb_angle(i)=0;
        protein_donor_hb_angle(i)=0;
        protein_atom_charge(i)=0;
        protein_backbone(i)=0;
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'CD1', 3)==1)&(strncmp(PDBData1.Model.Atom(1,i).resName, 'ILE', 3)==1)
        protein_name(i,1)=C_3;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.94;
        protein_atom_hb(i)=N_A;
        protein_acceptor_hb_angle(i)=0;
        protein_donor_hb_angle(i)=0;
        protein_atom_charge(i)=0;
        protein_backbone(i)=0;
        
        
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'CD1', 3)==1)&(strncmp(PDBData1.Model.Atom(1,i).resName, 'LEU', 3)==1)
        protein_name(i,1)=C_3;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.94;
        protein_atom_hb(i)=N_A;
        protein_acceptor_hb_angle(i)=0;
        protein_donor_hb_angle(i)=0;
        protein_atom_charge(i)=0;
        protein_backbone(i)=0;
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'CD2', 3)==1)&(strncmp(PDBData1.Model.Atom(1,i).resName, 'LEU', 3)==1)
        protein_name(i,1)=C_3;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.94;
        protein_atom_hb(i)=N_A;
        protein_acceptor_hb_angle(i)=0;
        protein_donor_hb_angle(i)=0;
        protein_atom_charge(i)=0;
        protein_backbone(i)=0;
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'CG', 2)==1)&(strncmp(PDBData1.Model.Atom(1,i).resName, 'LEU', 3)==1)
        protein_name(i,1)=C_3;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.94;
        protein_atom_hb(i)=N_A;
        protein_acceptor_hb_angle(i)=0;
        protein_donor_hb_angle(i)=0;
        protein_atom_charge(i)=0;
        protein_backbone(i)=0;
        
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'NZ', 2)==1)&(strncmp(PDBData1.Model.Atom(1,i).resName, 'LYS', 3)==1)
        protein_name(i,1)=N_4;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.83;
        protein_atom_hb(i)=D2;
        protein_acceptor_hb_angle(i)=0;
        protein_donor_hb_angle(i)=pi;
        protein_atom_charge(i)=1;
        protein_backbone(i)=0;
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'CG', 2)==1)&(strncmp(PDBData1.Model.Atom(1,i).resName, 'LYS', 3)==1)
        protein_name(i,1)=C_3;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.94;
        protein_atom_hb(i)=N_A;
        protein_acceptor_hb_angle(i)=0;
        protein_donor_hb_angle(i)=0;
        protein_atom_charge(i)=0;
        protein_backbone(i)=0;
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'CD', 2)==1)&(strncmp(PDBData1.Model.Atom(1,i).resName, 'LYS', 3)==1)
        protein_name(i,1)=C_3;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.94;
        protein_atom_hb(i)=N_A;
        protein_acceptor_hb_angle(i)=0;
        protein_donor_hb_angle(i)=0;
        protein_atom_charge(i)=0;
        protein_backbone(i)=0;
        
        
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'CE', 2)==1)&(strncmp(PDBData1.Model.Atom(1,i).resName, 'MET', 3)==1)
        protein_name(i,1)=C_3;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.94;
        protein_atom_hb(i)=N_A;
        protein_acceptor_hb_angle(i)=0;
        protein_donor_hb_angle(i)=0;
        protein_atom_charge(i)=0;
        protein_backbone(i)=0;
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName(1), 'S', 1)==1)&(strncmp(PDBData1.Model.Atom(1,i).resName, 'MET', 3)==1)
        protein_name(i,1)=S_3;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=2.09;
        protein_atom_hb(i)=A;
        protein_acceptor_hb_angle(i)=0.75*pi;
        protein_donor_hb_angle(i)=0;
        protein_atom_charge(i)=0;
        protein_backbone(i)=0;
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'CG', 2)==1)&(strncmp(PDBData1.Model.Atom(1,i).resName, 'MET', 3)==1)
        protein_name(i,1)=C_3;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.94;
        protein_atom_hb(i)=N_A;
        protein_acceptor_hb_angle(i)=0;
        protein_donor_hb_angle(i)=0;
        protein_atom_charge(i)=0;
        protein_backbone(i)=0;
        
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'CG', 2)==1)&(strncmp(PDBData1.Model.Atom(1,i).resName, 'PHE', 3)==1)
        protein_name(i,1)=C_ar;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.85;
        protein_atom_hb(i)=N_A;
        protein_acceptor_hb_angle(i)=0;
        protein_donor_hb_angle(i)=0;
        protein_atom_charge(i)=0;
        protein_backbone(i)=0;
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'CD1', 3)==1)&(strncmp(PDBData1.Model.Atom(1,i).resName, 'PHE', 3)==1)
        protein_name(i,1)=C_ar;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.85;
        protein_atom_hb(i)=N_A;
        protein_acceptor_hb_angle(i)=0;
        protein_donor_hb_angle(i)=0;
        protein_atom_charge(i)=0;
        protein_backbone(i)=0;
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'CD2', 3)==1)&(strncmp(PDBData1.Model.Atom(1,i).resName, 'PHE', 3)==1)
        protein_name(i,1)=C_ar;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.85;
        protein_atom_hb(i)=N_A;
        protein_acceptor_hb_angle(i)=0;
        protein_donor_hb_angle(i)=0;
        protein_atom_charge(i)=0;
        protein_backbone(i)=0;
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'CE1', 3)==1)&(strncmp(PDBData1.Model.Atom(1,i).resName, 'PHE', 3)==1)
        protein_name(i,1)=C_ar;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.85;
        protein_atom_hb(i)=N_A;
        protein_acceptor_hb_angle(i)=0;
        protein_donor_hb_angle(i)=0;
        protein_atom_charge(i)=0;
        protein_backbone(i)=0;
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'CE2', 3)==1)&(strncmp(PDBData1.Model.Atom(1,i).resName, 'PHE', 3)==1)
        protein_name(i,1)=C_ar;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.85;
        protein_atom_hb(i)=N_A;
        protein_acceptor_hb_angle(i)=0;
        protein_donor_hb_angle(i)=0;
        protein_atom_charge(i)=0;
        protein_backbone(i)=0;
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'CZ', 2)==1)&(strncmp(PDBData1.Model.Atom(1,i).resName, 'PHE', 3)==1)
        protein_name(i,1)=C_ar;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.85;
        protein_atom_hb(i)=N_A;
        protein_acceptor_hb_angle(i)=0;
        protein_donor_hb_angle(i)=0;
        protein_atom_charge(i)=0;
        protein_backbone(i)=0;
        
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'CD', 2)==1)&(strncmp(PDBData1.Model.Atom(1,i).resName, 'PRO', 3)==1)
        protein_name(i,1)=C_3;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.94;
        protein_atom_hb(i)=N_A;
        protein_acceptor_hb_angle(i)=0;
        protein_donor_hb_angle(i)=0;
        protein_atom_charge(i)=0;
        protein_backbone(i)=0;
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'CG', 2)==1)&(strncmp(PDBData1.Model.Atom(1,i).resName, 'PRO', 3)==1)
        protein_name(i,1)=C_3;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.94;
        protein_atom_hb(i)=N_A;
        protein_acceptor_hb_angle(i)=0;
        protein_donor_hb_angle(i)=0;
        protein_atom_charge(i)=0;
        protein_backbone(i)=0;
        
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'OG', 2)==1)&(strncmp(PDBData1.Model.Atom(1,i).resName, 'SER', 3)==1)
        protein_name(i,1)=O_3;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.74;
        protein_atom_hb(i)=DA;
        protein_acceptor_hb_angle(i)=0.6083*pi;
        protein_donor_hb_angle(i)=pi;
        protein_atom_charge(i)=0;
        protein_backbone(i)=0;
        
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'OG1', 3)==1)&(strncmp(PDBData1.Model.Atom(1,i).resName, 'THR', 3)==1)
        protein_name(i,1)=O_3;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.66;
        protein_atom_hb(i)=DA;
        protein_acceptor_hb_angle(i)=0.6083*pi;
        protein_donor_hb_angle(i)=pi;
        protein_atom_charge(i)=0;
        protein_backbone(i)=0;
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'CG2', 3)==1)&(strncmp(PDBData1.Model.Atom(1,i).resName, 'THR', 3)==1)
        protein_name(i,1)=C_3;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.94;
        protein_atom_hb(i)=N_A;
        protein_acceptor_hb_angle(i)=0;
        protein_donor_hb_angle(i)=0;
        protein_atom_charge(i)=0;
        protein_backbone(i)=0;
        
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'CD1', 3)==1)&(strncmp(PDBData1.Model.Atom(1,i).resName, 'TRP', 3)==1)
        protein_name(i,1)=C_2;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.90;
        protein_atom_hb(i)=N_A;
        protein_acceptor_hb_angle(i)=0;
        protein_donor_hb_angle(i)=0;
        protein_atom_charge(i)=0;
        protein_backbone(i)=0;
        
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'CD2', 3)==1)&(strncmp(PDBData1.Model.Atom(1,i).resName, 'TRP', 3)==1)
        protein_name(i,1)=C_2;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.90;
        protein_atom_hb(i)=N_A;
        protein_acceptor_hb_angle(i)=0;
        protein_donor_hb_angle(i)=0;
        protein_atom_charge(i)=0;
        protein_backbone(i)=0;
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'NE1', 3)==1)&(strncmp(PDBData1.Model.Atom(1,i).resName, 'TRP', 3)==1)
        protein_name(i,1)=N_pl3;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.87;
        protein_atom_hb(i)=D;
        protein_acceptor_hb_angle(i)=0.6083*pi;
        protein_donor_hb_angle(i)=pi;
        protein_atom_charge(i)=0;
        protein_backbone(i)=0;
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'CD1', 3)==1)&(strncmp(PDBData1.Model.Atom(1,i).resName, 'TRP', 3)==1)
        protein_name(i,1)=C_2;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.90;
        protein_atom_hb(i)=N_A;
        protein_acceptor_hb_angle(i)=0;
        protein_donor_hb_angle(i)=0;
        protein_atom_charge(i)=0;
        protein_backbone(i)=0;
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'CE2', 3)==1)&(strncmp(PDBData1.Model.Atom(1,i).resName, 'TRP', 3)==1)
        protein_name(i,1)=C_2;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.90;
        protein_atom_hb(i)=N_A;
        protein_acceptor_hb_angle(i)=0;
        protein_donor_hb_angle(i)=0;
        protein_atom_charge(i)=0;
        protein_backbone(i)=0;
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'CE3', 3)==1)&(strncmp(PDBData1.Model.Atom(1,i).resName, 'TRP', 3)==1)
        protein_name(i,1)=C_ar;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.85;
        protein_atom_hb(i)=N_A;
        protein_acceptor_hb_angle(i)=0;
        protein_donor_hb_angle(i)=0;
        protein_atom_charge(i)=0;
        protein_backbone(i)=0;
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'CZ2', 3)==1)&(strncmp(PDBData1.Model.Atom(1,i).resName, 'TRP', 3)==1)
        protein_name(i,1)=C_ar;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.85;
        protein_atom_hb(i)=N_A;
        protein_acceptor_hb_angle(i)=0;
        protein_donor_hb_angle(i)=0;
        protein_atom_charge(i)=0;
        protein_backbone(i)=0;
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'CZ3', 3)==1)&(strncmp(PDBData1.Model.Atom(1,i).resName, 'TRP', 3)==1)
        protein_name(i,1)=C_ar;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.85;
        protein_atom_hb(i)=N_A;
        protein_acceptor_hb_angle(i)=0;
        protein_donor_hb_angle(i)=0;
        protein_atom_charge(i)=0;
        protein_backbone(i)=0;
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'CH2', 3)==1)&(strncmp(PDBData1.Model.Atom(1,i).resName, 'TRP', 3)==1)
        protein_name(i,1)=C_ar;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.85;
        protein_atom_hb(i)=N_A;
        protein_acceptor_hb_angle(i)=0;
        protein_donor_hb_angle(i)=0;
        protein_atom_charge(i)=0;
        protein_backbone(i)=0;
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'CG', 2)==1)&(strncmp(PDBData1.Model.Atom(1,i).resName, 'TRP', 3)==1)
        protein_name(i,1)=C_2;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.90;
        protein_atom_hb(i)=N_A;
        protein_acceptor_hb_angle(i)=0;
        protein_donor_hb_angle(i)=0;
        protein_atom_charge(i)=0;
        protein_backbone(i)=0;
        
        
        
        
        
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'CG', 2)==1)&(strncmp(PDBData1.Model.Atom(1,i).resName, 'TYR', 3)==1)
        protein_name(i,1)=C_ar;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.85;
        protein_atom_hb(i)=N_A;
        protein_acceptor_hb_angle(i)=0;
        protein_donor_hb_angle(i)=0;
        protein_atom_charge(i)=0;
        protein_backbone(i)=0;
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'CD1', 3)==1)&(strncmp(PDBData1.Model.Atom(1,i).resName, 'TYR', 3)==1)
        protein_name(i,1)=C_ar;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.85;
        protein_atom_hb(i)=N_A;
        protein_acceptor_hb_angle(i)=0;
        protein_donor_hb_angle(i)=0;
        protein_atom_charge(i)=0;
        protein_backbone(i)=0;
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'CD2', 3)==1)&(strncmp(PDBData1.Model.Atom(1,i).resName, 'TYR', 3)==1)
        protein_name(i,1)=C_ar;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.85;
        protein_atom_hb(i)=N_A;
        protein_acceptor_hb_angle(i)=0;
        protein_donor_hb_angle(i)=0;
        protein_atom_charge(i)=0;
        protein_backbone(i)=0;
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'CE1', 3)==1)&(strncmp(PDBData1.Model.Atom(1,i).resName, 'TYR', 3)==1)
        protein_name(i,1)=C_ar;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.85;
        protein_atom_hb(i)=N_A;
        protein_acceptor_hb_angle(i)=0;
        protein_donor_hb_angle(i)=0;
        protein_atom_charge(i)=0;
        protein_backbone(i)=0;
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'CE2', 3)==1)&(strncmp(PDBData1.Model.Atom(1,i).resName, 'TYR', 3)==1)
        protein_name(i,1)=C_ar;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.85;
        protein_atom_hb(i)=N_A;
        protein_acceptor_hb_angle(i)=0;
        protein_donor_hb_angle(i)=0;
        protein_atom_charge(i)=0;
        protein_backbone(i)=0;
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'CZ', 2)==1)&(strncmp(PDBData1.Model.Atom(1,i).resName, 'TYR', 3)==1)
        protein_name(i,1)=C_ar;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.85;
        protein_atom_hb(i)=N_A;
        protein_acceptor_hb_angle(i)=0;
        protein_donor_hb_angle(i)=0;
        protein_atom_charge(i)=0;
        protein_backbone(i)=0;
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'OH', 2)==1)&(strncmp(PDBData1.Model.Atom(1,i).resName, 'TYR', 3)==1)
        protein_name(i,1)=O_3;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.74;
        protein_atom_hb(i)=DA;
        protein_acceptor_hb_angle(i)=0.6083*pi;
        protein_donor_hb_angle(i)=pi;
        protein_atom_charge(i)=0;
        protein_backbone(i)=0;
        
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'CG1', 3)==1)&(strncmp(PDBData1.Model.Atom(1,i).resName, 'VAL', 3)==1)
        protein_name(i,1)=C_3;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.94;
        protein_atom_hb(i)=N_A;
        protein_acceptor_hb_angle(i)=0;
        protein_donor_hb_angle(i)=0;
        protein_atom_charge(i)=0;
        protein_backbone(i)=0;
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'CG2', 3)==1)&(strncmp(PDBData1.Model.Atom(1,i).resName, 'VAL', 3)==1)
        protein_name(i,1)=C_3;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.94;
        protein_atom_hb(i)=N_A;
        protein_acceptor_hb_angle(i)=0;
        protein_donor_hb_angle(i)=0;
        protein_atom_charge(i)=0;
        protein_backbone(i)=0;
        
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'LPD', 3)==1)
        protein_name(i,1)=H;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1;
        protein_atom_hb(i)=N_A;
        protein_acceptor_hb_angle(i)=0;
        protein_donor_hb_angle(i)=0;
        protein_atom_charge(i)=0;
        protein_backbone(i)=0;
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'LPG', 3)==1)
        protein_name(i,1)=H;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1;
        protein_atom_hb(i)=N_A;
        protein_acceptor_hb_angle(i)=0;
        protein_donor_hb_angle(i)=0;
        protein_atom_charge(i)=0;
        protein_backbone(i)=0;
        
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'CB', 2)==1)
        protein_name(i,1)=C_3     ;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.94;
        protein_atom_hb(i)=N_A;
        protein_acceptor_hb_angle(i)=0;
        protein_donor_hb_angle(i)=0;
        protein_atom_charge(i)=0;
        protein_backbone(i)=0;

    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'CE', 2)==1)
        protein_name(i,1)=C_3;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.94;
        protein_atom_hb(i)=N_A;
        protein_acceptor_hb_angle(i)=0;
        protein_donor_hb_angle(i)=0;
        protein_atom_charge(i)=0;
        protein_backbone(i)=0;
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'OXT', 3)==1)
        protein_name(i,1)=O_2 ;
        protein_name(i,2)=protein_resseq(i);
        protein_atom_radius(i)=1.66;
        protein_atom_hb(i)=A;
        protein_acceptor_hb_angle(i)=0.75*pi;
        protein_donor_hb_angle(i)=0;
        protein_atom_charge(i)=0;
        protein_backbone(i)=1;
        
        
    elseif (strncmp(PDBData1.Model.Atom(1,i).AtomName, 'ZN', 2)==1)&(strncmp(PDBData1.Model.Atom(1,i).resName, 'ZN', 2)==1)
        protein_name(i,1)=ZN;
        protein_name(i,2)=ZN;
        protein_atom_radius(i)=1.2;
        protein_atom_hb(i)=N_A;
        protein_acceptor_hb_angle(i)=0;
        protein_donor_hb_angle(i)=0;
        protein_atom_charge(i)=2;
        protein_backbone(i)=0;
    end
        
    if (strncmp(PDBData1.Model.Atom(1,i).chainID, 'A', 1)==1)
        protein_chainID(i)=1;
    elseif (strncmp(PDBData1.Model.Atom(1,i).chainID, 'B', 1)==1)
        protein_chainID(i)=2;
    elseif (strncmp(PDBData1.Model.Atom(1,i).chainID, 'C', 1)==1)
        protein_chainID(i)=3;
    elseif (strncmp(PDBData1.Model.Atom(1,i).chainID, 'D', 1)==1)
        protein_chainID(i)=4;
    elseif (strncmp(PDBData1.Model.Atom(1,i).chainID, 'E', 1)==1)
        protein_chainID(i)=5;
    elseif (strncmp(PDBData1.Model.Atom(1,i).chainID, 'F', 1)==1)
        protein_chainID(i)=6;
    elseif (strncmp(PDBData1.Model.Atom(1,i).chainID, 'G', 1)==1)
        protein_chainID(i)=7;
    elseif (strncmp(PDBData1.Model.Atom(1,i).chainID, 'H', 1)==1)
        protein_chainID(i)=8;
    elseif (strncmp(PDBData1.Model.Atom(1,i).chainID, 'I', 1)==1)
        protein_chainID(i)=9;
    elseif (strncmp(PDBData1.Model.Atom(1,i).chainID, 'J', 1)==1)
        protein_chainID(i)=10;
    elseif (strncmp(PDBData1.Model.Atom(1,i).chainID, 'K', 1)==1)
        protein_chainID(i)=11;
    elseif (strncmp(PDBData1.Model.Atom(1,i).chainID, 'L', 1)==1)
        protein_chainID(i)=12;
    elseif (strncmp(PDBData1.Model.Atom(1,i).chainID, 'M', 1)==1)
        protein_chainID(i)=13;
    elseif (strncmp(PDBData1.Model.Atom(1,i).chainID, 'N', 1)==1)
        protein_chainID(i)=14;
    elseif (strncmp(PDBData1.Model.Atom(1,i).chainID, 'O', 1)==1)
        protein_chainID(i)=15;
    elseif (strncmp(PDBData1.Model.Atom(1,i).chainID, 'P', 1)==1)
        protein_chainID(i)=16;
    elseif (strncmp(PDBData1.Model.Atom(1,i).chainID, 'Q', 1)==1)
        protein_chainID(i)=17;
    elseif (strncmp(PDBData1.Model.Atom(1,i).chainID, 'R', 1)==1)
        protein_chainID(i)=18;
    elseif (strncmp(PDBData1.Model.Atom(1,i).chainID, 'S', 1)==1)
        protein_chainID(i)=19;
    elseif (strncmp(PDBData1.Model.Atom(1,i).chainID, 'T', 1)==1)
        protein_chainID(i)=20;
    elseif (strncmp(PDBData1.Model.Atom(1,i).chainID, 'U', 1)==1)
        protein_chainID(i)=21;
    elseif (strncmp(PDBData1.Model.Atom(1,i).chainID, 'V', 1)==1)
        protein_chainID(i)=22;
    elseif (strncmp(PDBData1.Model.Atom(1,i).chainID, 'W', 1)==1)
        protein_chainID(i)=23;
    elseif (strncmp(PDBData1.Model.Atom(1,i).chainID, 'X', 1)==1)
        protein_chainID(i)=24;
    elseif (strncmp(PDBData1.Model.Atom(1,i).chainID, 'Y', 1)==1)
        protein_chainID(i)=25;
    elseif (strncmp(PDBData1.Model.Atom(1,i).chainID, 'Z', 1)==1)
        protein_chainID(i)=26;
    elseif (strncmp(PDBData1.Model.Atom(1,i).chainID, '1', 1)==1)
        protein_chainID(i)=1;
    elseif (strncmp(PDBData1.Model.Atom(1,i).chainID, '2', 1)==1)
        protein_chainID(i)=2;
    end
    
    protein_resseq(i)=PDBData1.Model.Atom(1,i).resSeq;
    
    
end

protential_protein=[protein_atom,protein_name,transpose(protein_atom_hb),transpose(protein_atom_hb),transpose(protein_backbone),transpose(protein_resseq),transpose(protein_atom_hb)];

size_protential_protein=size(protential_protein);



protein_atom_num=[1:1:size_protential_protein(1,1)];

protential_protein(:,7)=transpose(protein_atom_num);
protein_H=protential_protein(protential_protein(:,4)==23,:);

size_protein_H=size(protein_H);

protential_protein(protential_protein(:,4)==23,:)=[];

size_protential_protein=size(protential_protein);


protein_frame=protential_protein;
size_protein_frame=size(protein_frame);
    

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    %cd('E:\Mona\96_p1.0_ligands\zinc_ligand\');


%ligand initialization
clear fidin;
clear tline;
clear atom_conj_num atom name name_str ligand_atom_num atom_contact contact_type contact_name atom_contact_num mass atom_hb atom_mass atom_radius;
fidin=fopen(ligand_dir(1,:));
tline=fgetl(fidin);
k=1;
l=1;
m=1;
while ~feof(fidin)
    if length(tline)>=70
        atom(k,1)=str2num(tline(18:26));
        atom(k,2)=str2num(tline(28:36));
        atom(k,3)=str2num(tline(38:46));
        name_str(k,1:5)=tline(48:52);
        name_list(k,1:4)=tline(9:12);
        
        ligand_atom_num(k,1)=str2num(tline(1:7));
        k=k+1;
        tline=fgetl(fidin);
    elseif (length(tline)==13)&(strcmp(tline(1:13), '@<TRIPOS>BOND')==1)
       tline=fgetl(fidin);
        while (length(tline)<21)|(strcmp(tline(1:21),'@<TRIPOS>SUBSTRUCTURE')~=1)
            %tline=fgetl(fidin);
            atom_contact(l,1)=str2num(tline(9:12));
            l=l+1;
            atom_contact(l,1)=str2num(tline(14:17));
            l=l+1;
            contact_type(m,1)=str2num(tline(9:12));
            contact_type(m,2)=str2num(tline(14:17));
            contact_name(m,1:2)=tline(18:19);
            m=m+1;
            tline=fgetl(fidin);

            if feof(fidin)==1
                break
            end
        end
    elseif (length(tline)==21)&(strcmp(tline(1:21), '@<TRIPOS>SUBSTRUCTURE')==1)
        break
    elseif feof(fidin)==1
        break
    else
        tline=fgetl(fidin);
    end
end

size_atom=size(atom);

size_name_str=size(name_str);

fclose(fidin);

atom_contact=sortrows(atom_contact);


s=1;
t=1;
atom_contact_num(size(atom_contact),2)=0;
for i=1:1:size(atom_contact)
    if atom_contact(i,1)==s
        atom_contact_num(t,1)=s;
        atom_contact_num(t,2)=atom_contact_num(t,2)+1;
    elseif atom_contact(i,1)~=s
        s=atom_contact(i,1);
        t=t+1;
        atom_contact_num(t,1)=s;
        atom_contact_num(t,2)=1;
    end
end
    
atom_contact_num(atom_contact_num(:,1)==0,:)=[];
    
size_atom=size(atom);

size_name_str=size(name_str);


%ligand initialization
        
        
for i=1:length(atom)
    atom_conj_num(i)=-1;
    for j=1:length(atom)
        if (atom(i,1)-atom(j,1))^2+(atom(i,2)-atom(j,2))^2+(atom(i,3)-atom(j,3))^2<=3.321
            atom_conj_num(i)=atom_conj_num(i)+1;
        end
    end
end



C_3=1;
C_2=2;
C_1=3;
C_ar=4;
O_3=5;
O_2=6;
O_w=7;
N_3=8;
N_2=9;
N_1=10;
N_ar=11;
N_am=12;
N_pl3=13;
S_3=14;
S_2=15;
S_o=16;
S_o2=17;
F=18;
Cl=19;
Br=20;
I=21;
P=22;
H=23;
C_cat=24;
O_co2=25;
N_4=26;

LI   = 27; 
NA   = 28;	
K    = 29 ;    
MG   = 30  ;  
CAA   = 31	;
AL   = 32	;
MN   = 33  	;
FE   = 34    ;
CO   = 35    ;
NI   = 36    ;
CU   = 37    ;
ZN   = 38    ;
        
        
N_A=0;
D=1;
D2=2;
A=3;
DA=4;
        
ALA=101;
ARG=102;
ASN=103;
ASP=104;
CYS=105;
GLN=106;
GLU=107;
GLY=108;
HIS=109;
ILE=110;
LEU=111;
LYS=112;
MET=113;
PHE=114;
PRO=115;
SER=116;
THR=117;
TRP=118;
TYR=119;
VAL=120;



carbon='C';
oxygen='O';
nitrogen='N';
sulfer='S';
fluorin='F';
chlorine='Cl';
bromine='Br';
iodine='I';
phosphor='P';
hydrogen='H';


size_name_str=size(name_str);

clear name
for i=1:1:size_name_str(1,1)
    if strcmp(name_str(i,1:5), 'H    ')==1
        name(i,1)=H;
    elseif strcmp(name_str(i,1:5), 'C.3  ')==1
        name(i,1)=C_3;
    elseif strcmp(name_str(i,1:5), 'C.2  ')==1
        name(i,1)=C_2;
    elseif strcmp(name_str(i,1:5), 'C.1  ')==1
        name(i,1)=C_1;
    elseif strcmp(name_str(i,1:5), 'C.ar ')==1
        name(i,1)=C_ar;
    elseif strcmp(name_str(i,1:5), 'O.3  ')==1
        name(i,1)=O_3;
    elseif strcmp(name_str(i,1:5), 'O.2  ')==1
        name(i,1)=O_2;
    elseif strcmp(name_str(i,1:5), 'O.w  ')==1
        name(i,1)=O_w;
    elseif strcmp(name_str(i,1:5), 'N.3  ')==1
        name(i,1)=N_3;
    elseif strcmp(name_str(i,1:5), 'N.2  ')==1
        name(i,1)=N_2;
    elseif strcmp(name_str(i,1:5), 'N.1  ')==1
        name(i,1)=N_1;
    elseif strcmp(name_str(i,1:5), 'N.ar ')==1
        name(i,1)=N_ar;
    elseif strcmp(name_str(i,1:5), 'N.am ')==1
        name(i,1)=N_am;
    elseif strcmp(name_str(i,1:5), 'N.pl3')==1
        name(i,1)=N_pl3;
    elseif strcmp(name_str(i,1:5), 'S.3  ')==1
        name(i,1)=S_3;
    elseif strcmp(name_str(i,1:5), 'S.2  ')==1
        name(i,1)=S_2;
    elseif strcmp(name_str(i,1:5), 'S.o  ')==1
        name(i,1)=S_o;
    elseif strcmp(name_str(i,1:5), 'S.o2 ')==1
        name(i,1)=S_o2;
    elseif strcmp(name_str(i,1:5), 'F    ')==1
        name(i,1)=F;
    elseif strcmp(name_str(i,1:5), 'Cl   ')==1
        name(i,1)=Cl;
    elseif strcmp(name_str(i,1:5), 'Br   ')==1
        name(i,1)=Br;
    elseif strcmp(name_str(i,1:5), 'I    ')==1
        name(i,1)=I;
    elseif strcmp(name_str(i,1:5), 'P.3  ')==1
        name(i,1)=P;
    elseif strcmp(name_str(i,1:5), 'C.cat')==1
        name(i,1)=C_cat;
    elseif strcmp(name_str(i,1:5), 'O.co2')==1
        name(i,1)=O_2;
    elseif strcmp(name_str(i,1:5), 'N.4  ')==1
        name(i,1)=N_4;
    end
end
     

  

N_A=0;
D=1;
D2=2;
A=3;
DA=4;

size_atom=size(atom);
for i=1:1:size_atom(1,1)
    if ((name(i)==C_3)|(name(i)==C_2)|(name(i)==C_1)|(name(i)==C_ar))
        atom_radius(i)=1.9;
        atom_hb(i)=N_A;
        acceptor_hb_angle(i)=0;
        donor_hb_angle(i)=0;
        atom_mass(i)=12;
        atom_charge(i)=0;
    elseif ((name(i)==N_3)|(name(i)==N_2))
        atom_radius(i)=1.86;
        atom_hb(i)=N_A;
        acceptor_hb_angle(i)=0;
        donor_hb_angle(i)=pi;
        atom_mass(i)=14;
        atom_charge(i)=0;
    elseif (name(i)==N_4)
        atom_radius(i)=1.86;
        atom_hb(i)=N_A;
        acceptor_hb_angle(i)=0;
        donor_hb_angle(i)=pi;
        atom_mass(i)=14;
        atom_charge(i)=1;
    elseif ((name(i)==N_am)|(name(i)==N_pl3))
        atom_radius(i)=1.86;
        atom_hb(i)=N_A;
        atom_mass(i)=14;
        atom_charge(i)=0;
    elseif ((name(i)==O_3)|(name(i)==O_2))
        atom_radius(i)=1.86;
        atom_hb(i)=A;
        acceptor_hb_angle(i)=0;
        donor_hb_angle(i)=pi;
        atom_mass(i)=16;
        atom_charge(i)=0;
        
    elseif name(i)==S_3
        atom_radius(i)=2.09;
        atom_hb(i)=A;
        acceptor_hb_angle(i)=0.75*pi;
        donor_hb_angle(i)=0;
        atom_mass(i)=32;
        atom_charge(i)=0;
        
        

    elseif name(i)==S_2
        atom_radius(i)=2.09;
        atom_hb(i)=A;
        acceptor_hb_angle(i)=0.75*pi;
        donor_hb_angle(i)=0;
        atom_mass(i)=32;
        atom_charge(i)=0;
    elseif name(i)==S_o
        atom_radius(i)=2.09;
        atom_hb(i)=A;
        acceptor_hb_angle(i)=0.75*pi;
        donor_hb_angle(i)=0;
        atom_mass(i)=32;
        atom_charge(i)=0;
    elseif name(i)==S_o2
        atom_radius(i)=2.09;
        atom_hb(i)=N_A;
        acceptor_hb_angle(i)=0;
        donor_hb_angle(i)=0;
        atom_mass(i)=32;
        atom_charge(i)=0;
    elseif name(i)==P
        atom_radius(i)=2.03;
        atom_hb(i)=N_A;
        acceptor_hb_angle(i)=0;
        donor_hb_angle(i)=0;
        atom_mass(i)=31;
        atom_charge(i)=0;
    elseif name(i)==F
        atom_radius(i)=1.77;
        atom_hb(i)=A;
        acceptor_hb_angle(i)=0;
        donor_hb_angle(i)=0;
        atom_mass(i)=19;
        atom_charge(i)=0;
    elseif name(i)==Cl
        atom_radius(i)=2.00;
        atom_hb(i)=A;
        acceptor_hb_angle(i)=0;
        donor_hb_angle(i)=0;
        atom_mass(i)=35.5;
        atom_charge(i)=0;
    elseif name(i)==Br
        atom_radius(i)=2.20;
        atom_hb(i)=A;
        acceptor_hb_angle(i)=0;
        donor_hb_angle(i)=0;
        atom_mass(i)=80;
        atom_charge(i)=0;
    elseif name(i)==I
        atom_radius(i)=2.40;
        atom_hb(i)=A;
        acceptor_hb_angle(i)=0;
        donor_hb_angle(i)=0;
        atom_mass(i)=127;
        atom_charge(i)=0;
    elseif name(i)==H
        atom_radius(i)=1.0;
        atom_hb(i)=N_A;
        acceptor_hb_angle(i)=0;
        donor_hb_angle(i)=0;
        atom_mass(i)=1;
        atom_charge(i)=0;
    end
end

total_atom_mass=sum(atom_mass);



for i=1:1:size_atom(1,1)
    neib_H(i)=0;
    for j=1:1:size_atom(1,1)
    if ((name(i)==N_3)|(name(i)==N_2)|(name(i)==N_am)|(name(i)==N_pl3)|(name(i)==N_4))&(name(j)==H)&(sqrt((atom(i,1)-atom(j,1))^2+(atom(i,2)-atom(j,2))^2+(atom(i,3)-atom(j,3))^2)<1.6)
        neib_H(i)=neib_H(i)+1;
    elseif ((name(i)==S_3)|(name(i)==S_2))&(name(j)==H)&(sqrt((atom(i,1)-atom(j,1))^2+(atom(i,2)-atom(j,2))^2+(atom(i,3)-atom(j,3))^2)<1.6)
        neib_H(i)=neib_H(i)+1;
    end
    end
end



for i=1:1:size_atom(1,1)
    if (neib_H(i)==1)&((name(i)==N_3)|(name(i)==N_2)|(name(i)==N_am)|(name(i)==N_pl3)|(name(i)==N_4))
        atom_hb(i)=D;
        donor_hb_angle(i)=pi;
    elseif (neib_H(i)==2)&((name(i)==N_3)|(name(i)==N_2)|(name(i)==N_am)|(name(i)==N_pl3)|(name(i)==N_4))
        atom_hb(i)=D2;
        donor_hb_angle(i)=pi;
    elseif (neib_H(i)==1)&((name(i)==S_3)|(name(i)==S_2))
        atom_hb(i)=DA;
        donor_hb_angle(i)=pi;
        acceptor_hb_angle(i)=0.6083*pi;
    end
end

       

for i=1:1:size_atom(1,1)
    for j=1:1:size_atom(1,1)
    if (name(i)==O_3)&(name(j)==H)&(sqrt((atom(i,1)-atom(j,1))^2+(atom(i,2)-atom(j,2))^2+(atom(i,3)-atom(j,3))^2)<1.6)
       atom_hb(i)=DA;
       donor_hb_angle(i)=pi;
       acceptor_hb_angle(i)=0.6083*pi;

    end
    end
end
        
        
        
        

%for j=1:1:size_atom(1,1)
%    if (name(j)==1)|(name(j)==2)|(name(j)==3)|(name(j)==4)|(name(j)==24)
%       plot3(atom(j,1),atom(j,2),atom(j,3),'d');
%    elseif (name(j)==5)|(name(j)==6)|(name(j)==7)
%           plot3(atom(j,1),atom(j,2),atom(j,3),'o');
%    elseif (name(j)==8)|(name(j)==9)|(name(j)==10)|(name(j)==11)|(name(j)==12)|(name(j)==13)
%           plot3(atom(j,1),atom(j,2),atom(j,3),'^');
%    elseif (name(j)==14)|(name(j)==15)|(name(j)==16)|(name(j)==17)
%           plot3(atom(j,1),atom(j,2),atom(j,3),'s');
%    else
%        plot3(atom(j,1),atom(j,2),atom(j,3),'x');
%    end
%    hold on
%end


ligand=[atom,name,transpose(atom_mass),transpose(atom_radius),transpose(atom_hb),transpose(atom_hb),transpose(atom_hb),transpose(atom_hb)];
size_ligand=size(ligand);

ligand_atom_num=[1:1:size_ligand(1,1)];
ligand(:,5)=0;
ligand(:,6)=0;
ligand(:,10)=0;
ligand(:,11)=0;
ligand(:,7)=transpose(ligand_atom_num);
ligand(:,9)=transpose(ligand_atom_num);
ligand_H=ligand(ligand(:,4)==23,:);

size_ligand_H=size(ligand_H);

lig_with_H=ligand;
size_lig_with_H=size(lig_with_H);

ligand(ligand(:,4)==23,:)=[];
size_ligand=size(ligand);


clear centroidb;

qj=1;
for i=1:1:size_protential_protein(1,1)
    for j=1:1:size_lig_with_H(1,1)
        if norm(protential_protein(i,1:3)-lig_with_H(j,1:3))<=5
            centroidb(qj,:)=protential_protein(i,:);
            qj=qj+1;
            break
        end
    end
end

%size_centroidb=size(centroidb);
%protential_protein=centroidb;
    

%size_protential_protein=size(protential_protein);


Heatmap=Heatmap_function5_5f(ligand,protential_protein,protential_protein);

grid_br_final=Heatmap(:,1:5);
grid_br_final(grid_br_final(:,4)==0,:)=[];
size_grid_br_final=size(grid_br_final);

grid_cl_final=Heatmap(:,6:10);
grid_cl_final(grid_cl_final(:,4)==0,:)=[];
size_grid_cl_final=size(grid_cl_final);

grid_car_final=Heatmap(:,11:15);
grid_car_final(grid_car_final(:,4)==0,:)=[];
size_grid_car_final=size(grid_car_final);

grid_n4_final=Heatmap(:,16:20);
grid_n4_final(grid_n4_final(:,4)==0,:)=[];
size_grid_n4_final=size(grid_n4_final);

grid_nam_final=Heatmap(:,21:25);
grid_nam_final(grid_nam_final(:,4)==0,:)=[];
size_grid_nam_final=size(grid_nam_final);

grid_namHB_final=Heatmap(:,26:30);
grid_namHB_final(grid_namHB_final(:,4)==0,:)=[];
size_grid_namHB_final=size(grid_namHB_final);

grid_npl3_final=Heatmap(:,31:35);
grid_npl3_final(grid_npl3_final(:,4)==0,:)=[];
size_grid_npl3_final=size(grid_npl3_final);

grid_npl3HB_final=Heatmap(:,36:40);
grid_npl3HB_final(grid_npl3HB_final(:,4)==0,:)=[];
size_grid_npl3HB_final=size(grid_npl3HB_final);

grid_o2_final=Heatmap(:,41:45);
grid_o2_final(grid_o2_final(:,4)==0,:)=[];
size_grid_o2_final=size(grid_o2_final);

grid_o3_final=Heatmap(:,46:50);
grid_o3_final(grid_o3_final(:,4)==0,:)=[];
size_grid_o3_final=size(grid_o3_final);

grid_o3HB_final=Heatmap(:,51:55);
grid_o3HB_final(grid_o3HB_final(:,4)==0,:)=[];
size_grid_o3HB_final=size(grid_o3HB_final);

grid_s_final=Heatmap(:,56:60);
grid_s_final(grid_s_final(:,4)==0,:)=[];
size_grid_s_final=size(grid_s_final);

grid_sHB_final=Heatmap(:,61:65);
grid_sHB_final(grid_sHB_final(:,4)==0,:)=[];
size_grid_sHB_final=size(grid_sHB_final);



grid_nam_final=[grid_nam_final
    grid_namHB_final];
%grid_nam_final=grid_namHB_final;
size_grid_nam_final=size(grid_nam_final);

grid_npl3_final=[grid_npl3_final
    grid_npl3HB_final];

%grid_npl3_final=grid_npl3HB_final;
size_grid_npl3_final=size(grid_npl3_final);


grid_o3_final=[grid_o3_final
    grid_o3HB_final];

%grid_o3_final=grid_o3HB_final;
size_grid_o3_final=size(grid_o3_final);






















size_contact_type=size(contact_type);

bond_count(1:size_ligand(1,1),1)=1:size_ligand(1,1);
bond_count(1:size_ligand(1,1),2)=0;
size_bond_count=size(bond_count);


contact_type_A=contact_type;


for i=1:1:size_ligand_H(1,1)
    contact_type_A(contact_type_A(:,1)==ligand_H(i,7)|contact_type_A(:,2)==ligand_H(i,7),:)=[];
end




size_contact_type_A=size(contact_type_A);

ring_num=size_contact_type_A(1,1)-size_bond_count(1,1)+1;

% 4-membered ring

for a=1:1:size_ligand(1,1)
    for b=1:1:size_ligand(1,1)
        for z=1:1:size_contact_type_A(1,1)
            if (contact_type_A(z,1)==ligand(a,7)&contact_type_A(z,2)==ligand(b,7))|(contact_type_A(z,2)==ligand(a,7)&contact_type_A(z,1)==ligand(b,7))
                for c=1:1:size_ligand(1,1)
                    for y=1:1:size_contact_type_A(1,1)
                        if c~=a&c~=b&y~=z&((contact_type_A(y,1)==ligand(b,7)&contact_type_A(y,2)==ligand(c,7))|(contact_type_A(y,2)==ligand(b,7)&contact_type_A(y,1)==ligand(c,7)))
                            for d=1:1:size_ligand(1,1)
                                for x=1:1:size_contact_type_A(1,1)
                                    if d~=a&d~=b&d~=c&x~=z&x~=y&((contact_type_A(x,1)==ligand(c,7)&contact_type_A(x,2)==ligand(d,7))|(contact_type_A(x,2)==ligand(c,7)&contact_type_A(x,1)==ligand(d,7)))
                                        for w=1:1:size_contact_type_A(1,1)
                                            if w~=z&w~=y&w~=z&((contact_type_A(w,1)==ligand(d,7)&contact_type_A(w,2)==ligand(a,7))|(contact_type_A(w,2)==ligand(d,7)&contact_type_A(w,1)==ligand(a,7)))
                                                ligand(a,5)=4;
                                                ligand(b,5)=4;
                                                ligand(c,5)=4;
                                                ligand(d,5)=4;
                                                break
                                            end
                                        end
                                    end
                                end
                            end
                        end
                    end
                end
            end
        end
    end
end
                                                
               


% 5-membered ring

for a=1:1:size_ligand(1,1)
    for b=1:1:size_ligand(1,1)
        for z=1:1:size_contact_type_A(1,1)
            if (contact_type_A(z,1)==ligand(a,7)&contact_type_A(z,2)==ligand(b,7))|(contact_type_A(z,2)==ligand(a,7)&contact_type_A(z,1)==ligand(b,7))
                for c=1:1:size_ligand(1,1)
                    for y=1:1:size_contact_type_A(1,1)
                        if c~=a&c~=b&y~=z&((contact_type_A(y,1)==ligand(b,7)&contact_type_A(y,2)==ligand(c,7))|(contact_type_A(y,2)==ligand(b,7)&contact_type_A(y,1)==ligand(c,7)))
                            for d=1:1:size_ligand(1,1)
                                for x=1:1:size_contact_type_A(1,1)
                                    if d~=a&d~=b&d~=c&x~=z&x~=y&((contact_type_A(x,1)==ligand(c,7)&contact_type_A(x,2)==ligand(d,7))|(contact_type_A(x,2)==ligand(c,7)&contact_type_A(x,1)==ligand(d,7)))
                                        for e=1:1:size_ligand(1,1)
                                            for w=1:1:size_contact_type_A(1,1)
                                                if e~=a&e~=b&e~=c&e~=d&w~=z&w~=y&w~=x&((contact_type_A(w,1)==ligand(d,7)&contact_type_A(w,2)==ligand(e,7))|(contact_type_A(w,2)==ligand(d,7)&contact_type_A(w,1)==ligand(e,7)))
                                                    for v=1:1:size_contact_type_A(1,1)
                                                        if v~=z&v~=y&v~=x&v~=w&((contact_type_A(v,1)==ligand(e,7)&contact_type_A(v,2)==ligand(a,7))|(contact_type_A(v,2)==ligand(e,7)&contact_type_A(v,1)==ligand(a,7)))
                                                            ligand(a,5)=5;
                                                            ligand(b,5)=5;
                                                            ligand(c,5)=5;
                                                            ligand(d,5)=5;
                                                            ligand(e,5)=5;
                                                            break
                                                        end
                                                    end
                                                end
                                            end
                                        end
                                    end
                                end
                            end
                        end
                    end
                end
            end
        end
    end
end
                                                    
                            
% 6-membered ring

for a=1:1:size_ligand(1,1)
    for b=1:1:size_ligand(1,1)
        for z=1:1:size_contact_type_A(1,1)
            if (contact_type_A(z,1)==ligand(a,7)&contact_type_A(z,2)==ligand(b,7))|(contact_type_A(z,2)==ligand(a,7)&contact_type_A(z,1)==ligand(b,7))
                for c=1:1:size_ligand(1,1)
                    for y=1:1:size_contact_type_A(1,1)
                        if c~=a&c~=b&y~=z&((contact_type_A(y,1)==ligand(b,7)&contact_type_A(y,2)==ligand(c,7))|(contact_type_A(y,2)==ligand(b,7)&contact_type_A(y,1)==ligand(c,7)))
                            for d=1:1:size_ligand(1,1)
                                for x=1:1:size_contact_type_A(1,1)
                                    if d~=a&d~=b&d~=c&x~=z&x~=y&((contact_type_A(x,1)==ligand(c,7)&contact_type_A(x,2)==ligand(d,7))|(contact_type_A(x,2)==ligand(c,7)&contact_type_A(x,1)==ligand(d,7)))
                                        for e=1:1:size_ligand(1,1)
                                            for w=1:1:size_contact_type_A(1,1)
                                                if e~=a&e~=b&e~=c&e~=d&w~=z&w~=y&w~=x&((contact_type_A(w,1)==ligand(d,7)&contact_type_A(w,2)==ligand(e,7))|(contact_type_A(w,2)==ligand(d,7)&contact_type_A(w,1)==ligand(e,7)))
                                                    for f=1:1:size_ligand(1,1)
                                                        for v=1:1:size_contact_type_A(1,1)
                                                            if f~=a&f~=b&f~=c&f~=d&f~=e&v~=z&v~=y&v~=x&v~=w&((contact_type_A(v,1)==ligand(e,7)&contact_type_A(v,2)==ligand(f,7))|(contact_type_A(v,2)==ligand(e,7)&contact_type_A(v,1)==ligand(f,7)))
                                                                for u=1:1:size_contact_type_A(1,1)
                                                                    if u~=z&u~=y&u~=x&u~=w&u~=v&((contact_type_A(u,1)==ligand(f,7)&contact_type_A(u,2)==ligand(a,7))|(contact_type_A(u,2)==ligand(f,7)&contact_type_A(u,1)==ligand(a,7)))
                                                                        ligand(a,5)=6;
                                                                        ligand(b,5)=6;
                                                                        ligand(c,5)=6;
                                                                        ligand(d,5)=6;
                                                                        ligand(e,5)=6;
                                                                        ligand(f,5)=6;
                                                                        break
                                                                    end
                                                                end
                                                            end
                                                        end
                                                    end
                                                end
                                            end
                                        end
                                    end
                                end
                            end
                        end
                    end
                end
            end
        end
    end
end
       

for i=1:1:size_ligand(1,1)
    for j=1:1:size_contact_type_A(1,1)
        if contact_type_A(j,1)==ligand(i,7)
            ligand(i,10)=ligand(i,10)+1;
        end
        if contact_type_A(j,2)==ligand(i,7)
            ligand(i,10)=ligand(i,10)+1;
        end
    end
end

       
ligand(ligand(:,4)==1&ligand(:,5)==0&ligand(:,10)==2,12)=1;



%ligand(:,10)==>atom_bond_number   ligand(:,11)==>atom_chosen_marker
%ligand(:,12)==>c3_keyatom  ligand(:,13)==>frag_number

ligand(:,13)=0;

C3_rot=ligand(ligand(:,4)==1&ligand(:,5)==0&ligand(:,10)==2,:);
size_C3_rot=size(C3_rot);

C3_rot(:,11)=3;
ligand(C3_rot(:,7),11)=3;
frag_num=1;
A_frag=[];
size_A_frag=size(A_frag);
frag_num=1;




for zh=1:1:size_C3_rot(1,1)
    compare_num=0;
    C3_rot(zh,11)=3;
    ligand(C3_rot(zh,7),11)=3;
    start_atom=C3_rot(zh,:);
    inter_frag=[];
    inter_frag(1,:)=C3_rot(zh,:);
    
    sum_ligand_chosen=sum(ligand(:,11));
    
    %%%%% fragments for each C3_rot
    while compare_num~=sum_ligand_chosen
        compare_num=sum(ligand(:,11));
        for zz=1:1:start_atom(1,10)
            inter_frag=[];
            inter_frag(1,:)=start_atom(1,:);
            q=2;
            %%%%% find all branches starting key point of each start_atom
            for hl=1:1:size_contact_type_A(1,1)
                if contact_type_A(hl,1)==start_atom(1,7)&ligand(contact_type_A(hl,2),11)==0 % atom not chosen
                    ligand(contact_type_A(hl,2),11)=2; % turn it to key atom
                    
                elseif contact_type_A(hl,2)==start_atom(1,7)&ligand(contact_type_A(hl,1),11)==0 % atom not chosen
                    ligand(contact_type_A(hl,1),11)=2; % turn it to key atom
                    
                elseif contact_type_A(hl,1)==start_atom(1,7)&ligand(contact_type_A(hl,2),11)==3 % C3 rot
                    ligand(contact_type_A(hl,2),11)=1;
                    inter_frag(q,:)=ligand(contact_type_A(hl,2),:);
                    q=q+1;
                elseif contact_type_A(hl,2)==start_atom(1,7)&ligand(contact_type_A(hl,1),11)==3 % C3 rot
                    ligand(contact_type_A(hl,1),11)=1;
                    inter_frag(q,:)=ligand(contact_type_A(hl,1),:);
                    q=q+1;    
                elseif contact_type_A(hl,1)==start_atom(1,7)&ligand(contact_type_A(hl,2),12)==1 % C3 rot
                    ligand(contact_type_A(hl,2),11)=1;
                    inter_frag(q,:)=ligand(contact_type_A(hl,2),:);
                    q=q+1;
                elseif contact_type_A(hl,2)==start_atom(1,7)&ligand(contact_type_A(hl,1),12)==1 % C3 rot
                    ligand(contact_type_A(hl,1),11)=1;
                    inter_frag(q,:)=ligand(contact_type_A(hl,1),:);
                    q=q+1;     
                    
                end
            end
            ligand(start_atom(1,7),11)=1;
            key_atom=ligand(ligand(:,11)==2,:);
            size_key_atom=size(key_atom);
            compare_sub_num=0;
            sum_ligand_chosen=sum(ligand(:,11));
            
            inter_frag_beiyong=inter_frag;
            q_beiyong=q;

            %%%%% find each branch (from each key atom) of each start_atom
            for za=1:1:size_key_atom(1,1)
                clear inter_frag
                inter_frag=inter_frag_beiyong;
                q=q_beiyong;
                
                inter_frag(q,:)=ligand(key_atom(za,7),:);
                q=q+1;
                
                
            for hl=1:1:size_contact_type_A(1,1)
                if contact_type_A(hl,1)==key_atom(za,7)&ligand(contact_type_A(hl,2),11)==0
                    ligand(contact_type_A(hl,2),11)=5;
                    inter_frag(q,:)=ligand(contact_type_A(hl,2),:);
                    q=q+1;
                elseif contact_type_A(hl,2)==key_atom(za,7)&ligand(contact_type_A(hl,1),11)==0
                    ligand(contact_type_A(hl,1),11)=5;
                    inter_frag(q,:)=ligand(contact_type_A(hl,1),:);
                    q=q+1;
                elseif contact_type_A(hl,1)==key_atom(za,7)&ligand(contact_type_A(hl,2),11)==3
                    ligand(contact_type_A(hl,2),11)=1;
                    inter_frag(q,:)=ligand(contact_type_A(hl,2),:);
                    q=q+1;
                elseif contact_type_A(hl,2)==key_atom(za,7)&ligand(contact_type_A(hl,1),11)==3
                    ligand(contact_type_A(hl,1),11)=1;
                    inter_frag(q,:)=ligand(contact_type_A(hl,1),:);
                    q=q+1;    
                elseif contact_type_A(hl,1)==key_atom(za,7)&ligand(contact_type_A(hl,2),11)==2
                    ligand(contact_type_A(hl,2),11)=1;
                    inter_frag(q,:)=ligand(contact_type_A(hl,2),:);
                    q=q+1;
                elseif contact_type_A(hl,2)==key_atom(za,7)&ligand(contact_type_A(hl,1),11)==2
                    ligand(contact_type_A(hl,1),11)=1;
                    inter_frag(q,:)=ligand(contact_type_A(hl,1),:);
                    q=q+1;    
                elseif contact_type_A(hl,1)==key_atom(za,7)&ligand(contact_type_A(hl,2),12)==1
                    ligand(contact_type_A(hl,2),11)=1;
                    inter_frag(q,:)=ligand(contact_type_A(hl,2),:);
                    q=q+1;
                elseif contact_type_A(hl,2)==key_atom(za,7)&ligand(contact_type_A(hl,1),12)==1
                    ligand(contact_type_A(hl,1),11)=1;
                    inter_frag(q,:)=ligand(contact_type_A(hl,1),:);
                    q=q+1;    
                    
                    
                end
            end
            ligand(start_atom(1,7),11)=1;
            ligand(key_atom(za,7),11)=1;
            kkey_atom=ligand(ligand(:,11)==5,:);
            size_kkey_atom=size(kkey_atom);
            compare_sub_num=0;
            sum_ligand_chosen=sum(ligand(:,11));
            
           

                
            while compare_sub_num~=sum_ligand_chosen
                compare_sub_num=sum(ligand(:,11));
                for zhe=1:1:size_kkey_atom(1,1)
                    %inter_frag(q,:)=ligand(kkey_atom(zhe,7),:);
                    %q=q+1;
                    for ha=1:1:size_contact_type_A(1,1)
                        if contact_type_A(ha,1)==kkey_atom(zhe,7)&ligand(contact_type_A(ha,2),11)==0
                            ligand(contact_type_A(ha,2),11)=5;
                            inter_frag(q,:)=ligand(contact_type_A(ha,2),:);
                            q=q+1;
                        elseif contact_type_A(ha,2)==kkey_atom(zhe,7)&ligand(contact_type_A(ha,1),11)==0
                            ligand(contact_type_A(ha,1),11)=5;
                            inter_frag(q,:)=ligand(contact_type_A(ha,1),:);
                            q=q+1;
                        elseif contact_type_A(ha,1)==kkey_atom(zhe,7)&ligand(contact_type_A(ha,2),11)==2
                            ligand(contact_type_A(ha,2),11)=1;
                            inter_frag(q,:)=ligand(contact_type_A(ha,2),:);
                            q=q+1;    
                        elseif contact_type_A(ha,2)==kkey_atom(zhe,7)&ligand(contact_type_A(ha,1),11)==2
                            ligand(contact_type_A(ha,1),11)=1;
                            inter_frag(q,:)=ligand(contact_type_A(ha,1),:);
                            q=q+1;
                        elseif contact_type_A(ha,1)==kkey_atom(zhe,7)&ligand(contact_type_A(ha,2),11)==3
                            ligand(contact_type_A(ha,2),11)=1;
                            inter_frag(q,:)=ligand(contact_type_A(ha,2),:);
                            q=q+1;    
                        elseif contact_type_A(ha,2)==kkey_atom(zhe,7)&ligand(contact_type_A(ha,1),11)==3
                            ligand(contact_type_A(ha,1),11)=1;
                            inter_frag(q,:)=ligand(contact_type_A(ha,1),:);
                            q=q+1;    
                        elseif contact_type_A(ha,1)==kkey_atom(zhe,7)&ligand(contact_type_A(ha,2),12)==1
                            ligand(contact_type_A(ha,2),11)=1;
                            inter_frag(q,:)=ligand(contact_type_A(ha,2),:);
                            q=q+1;    
                        elseif contact_type_A(ha,2)==kkey_atom(zhe,7)&ligand(contact_type_A(ha,1),12)==1
                            ligand(contact_type_A(ha,1),11)=1;
                            inter_frag(q,:)=ligand(contact_type_A(ha,1),:);
                            q=q+1;    
                            
                            
                        end
                    end
                    ligand(kkey_atom(zhe,7),11)=1;
                    
                end
                clear kkey_atom
                kkey_atom=ligand(ligand(:,11)==5,:);
                size_kkey_atom=size(kkey_atom);
                sum_ligand_chosen=sum(ligand(:,11));
                

                
                

                
                
                
            end%while finish
            inter_frag(:,13)=frag_num;
            size_inter_frag=size(inter_frag);
            A_frag(size_A_frag+1:size_A_frag+size_inter_frag(1,1),:)=inter_frag;
            size_A_frag=size(A_frag);
            frag_num=frag_num+1;
            sum_ligand_chosen=sum(ligand(:,11));

            
            
            end %za key_atom finish
        end
    end
end
A_frag(:,11)=0;

A_frag=unique(A_frag,'rows');
size_A_frag=size(A_frag);
if size_A_frag(1,1)==0|(size_A_frag(1,1)==1&A_frag(1,4)==0)
    A_frag=ligand;
    A_frag(:,11)=0;
end





A_frag=sortrows(A_frag,[13 4]);
size_A_frag=size(A_frag);

frag_num=frag_num-1;
frag_info(1,1:3)=0;


for i=1:1:frag_num
    frag_info(i,1)=i;
    frag_part=A_frag(A_frag(:,13)==i,:);
    size_frag_part=size(frag_part);
    for j=1:1:size_frag_part(1,1)
        if frag_part(j,4)==4
            frag_info(i,3)=frag_info(i,3)+1;
        elseif frag_part(j,4)==5
            frag_info(i,2)=frag_info(i,2)+1;
        elseif frag_part(j,4)==6
            frag_info(i,2)=frag_info(i,2)+1;
        elseif frag_part(j,4)==25
            frag_info(i,2)=frag_info(i,2)+1;
        elseif frag_part(j,4)==8&(frag_part(j,8)==1|frag_part(j,8)==2|frag_part(j,8)==4)
            frag_info(i,2)=frag_info(i,2)+1;
        elseif frag_part(j,4)==12&(frag_part(j,8)==1|frag_part(j,8)==2|frag_part(j,8)==4)
            frag_info(i,2)=frag_info(i,2)+1;
        elseif frag_part(j,4)==13&(frag_part(j,8)==1|frag_part(j,8)==2|frag_part(j,8)==4)
            frag_info(i,2)=frag_info(i,2)+1;
        elseif frag_part(j,4)==19
            frag_info(i,2)=frag_info(i,2)+1;
        elseif frag_part(j,4)==20
            frag_info(i,2)=frag_info(i,2)+1;
        elseif frag_part(j,4)==26&(frag_part(j,8)==1|frag_part(j,8)==2|frag_part(j,8)==4)
            frag_info(i,2)=frag_info(i,2)+1;
        end
    end
    
end

for i=1:1:frag_num
    frag_info(i,3)=ceil(frag_info(i,3)/6);
end

frag_info(:,4)=frag_info(:,3)+frag_info(:,2);





frag_info=sortrows(frag_info,[4 3]);
frag_info=flipud(frag_info);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%docking the first motif

    A_frag(A_frag(:,13)==frag_info(1,1),11)=1;
    frag_chos=A_frag(A_frag(:,13)==frag_info(1,1),:);
    
    size_frag_chos=size(frag_chos);
    frag_chos(:,7)=1:size_frag_chos(1,1);
    
c3_num=0;
c2_num=0;
c1_num=0;
car_num=0;
o3_num=0;
o2_num=0;
n2_num=0;
nar_num=0;
nam_num=0;
npl3_num=0;
s_num=0;
f_num=0;
cl_num=0;
br_num=0;
n4_num=0;


for laoz=1:1:size_frag_chos(1,1)
    
    if frag_chos(laoz,4)==4
        car_num=car_num+1;  
    elseif frag_chos(laoz,4)==5
        o3_num=o3_num+1;      
    elseif (frag_chos(laoz,4)==6)|(frag_chos(laoz,4)==25)
        o2_num=o2_num+1;                     
    elseif (frag_chos(laoz,4)==12|frag_chos(laoz,4)==N_4)&frag_chos(laoz,8)>0
        nam_num=nam_num+1;              
    elseif frag_chos(laoz,4)==13&frag_chos(laoz,8)>0
        npl3_num=npl3_num+1;                
    elseif (frag_chos(laoz,4)>=14)&(frag_chos(laoz,4)<=17)
        s_num=s_num+1;
    elseif frag_chos(laoz,4)==19
        cl_num=cl_num+1;
    elseif frag_chos(laoz,4)==20
        br_num=br_num+1;
    elseif frag_chos(laoz,4)==26
        n4_num=n4_num+1;    
    end
end




o2_atom=[];
nam_atom=[];
npl3_atom=[];
o3_atom=[];
nar_atom=[];
n2_atom=[];
c3_atom=[];
c2_atom=[];
car_atom=[];
n4_atom=[];


lia=1;
lib=1;
lic=1;
lid=1;
lie=1;
lif=1;
liff=1;
lifff=1;
liffff=1;
lih=1;

for i=1:1:size_frag_chos(1,1)
    if frag_chos(i,4)==O_2
        o2_atom(lia,:)=frag_chos(i,:);
        lia=lia+1;
    elseif (frag_chos(i,4)==N_am|frag_chos(i,4)==N_4)&frag_chos(i,8)>0
        nam_atom(lib,:)=frag_chos(i,:);
        lib=lib+1;
    elseif frag_chos(i,4)==N_pl3&frag_chos(i,8)>0
        npl3_atom(lic,:)=frag_chos(i,:);
        lic=lic+1;
    elseif frag_chos(i,4)==O_3
        o3_atom(lid,:)=frag_chos(i,:);
        lid=lid+1;    
    elseif frag_chos(i,4)==Cl
        cl_atom(lif,:)=frag_chos(i,:);
        lif=lif+1; 
    elseif frag_chos(i,4)==Br
        br_atom(liff,:)=frag_chos(i,:);
        liff=liff+1; 

    elseif frag_chos(i,4)==C_ar
        car_atom(liffff,:)=frag_chos(i,:);
        liffff=liffff+1; 
    elseif frag_chos(i,4)==N_4
        n4_atom(lih,:)=frag_chos(i,:);
        lih=lih+1; 

    end
end



















%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%clear best_struc;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
cxt=0;

   o2_o2_o2_combo=[];     
if o2_num>=3
    o2_o2_o2_combo=[1 1 1 1 1 1];
hug1=1;
for i=1:1:o2_num-2
    for x=1:1:size_grid_o2_final(1,1)
            for j=i+1:1:o2_num-1
                for y=1:1:size_grid_o2_final(1,1)
                    if (abs(norm(o2_atom(i,1:3)-o2_atom(j,1:3))-norm(grid_o2_final(x,1:3)-grid_o2_final(y,1:3)))<=0.5)&grid_o2_final(x,4)~=grid_o2_final(y,4)
                        for k=j+1:1:o2_num
                            for z=1:1:size_grid_o2_final(1,1)
                                if (abs(norm(o2_atom(i,1:3)-o2_atom(k,1:3))-norm(grid_o2_final(x,1:3)-grid_o2_final(z,1:3)))<=0.5)&(abs(norm(o2_atom(j,1:3)-o2_atom(k,1:3))-norm(grid_o2_final(y,1:3)-grid_o2_final(z,1:3)))<=0.5)&grid_o2_final(x,4)~=grid_o2_final(z,4)&grid_o2_final(y,4)~=grid_o2_final(z,4)

                                    o2_o2_o2_combo(hug1,1)=x;
                                    o2_o2_o2_combo(hug1,2)=y;
                                    o2_o2_o2_combo(hug1,3)=z;
                                    
                                    o2_o2_o2_combo(hug1,4)=i;
                                    o2_o2_o2_combo(hug1,5)=j;
                                    o2_o2_o2_combo(hug1,6)=k;
                                    hug1=hug1+1;
                                end
                            end
                        end
                    end
                end
            end
    end
end


o2_o2_o2_combo(:,7)=0;

size_o2_o2_o2_combo=size(o2_o2_o2_combo);


end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



o2_o2_nam_combo=[];     
if (o2_num>=2)&(nam_num>=1)
    o2_o2_nam_combo=[1 1 1 1 1 1];  
hug1=1;
for i=1:1:o2_num-1
    for x=1:1:size_grid_o2_final(1,1)
            for j=i+1:1:o2_num
                for y=1:1:size_grid_o2_final(1,1)
                    if (abs(norm(o2_atom(i,1:3)-o2_atom(j,1:3))-norm(grid_o2_final(x,1:3)-grid_o2_final(y,1:3)))<=0.5)&grid_o2_final(x,4)~=grid_o2_final(y,4)
                        for k=1:1:nam_num
                            for z=1:1:size_grid_nam_final(1,1)
                                if (abs(norm(o2_atom(i,1:3)-nam_atom(k,1:3))-norm(grid_o2_final(x,1:3)-grid_nam_final(z,1:3)))<=0.5)&(abs(norm(o2_atom(j,1:3)-nam_atom(k,1:3))-norm(grid_o2_final(y,1:3)-grid_nam_final(z,1:3)))<=0.5)&grid_o2_final(x,4)~=grid_nam_final(z,4)&grid_o2_final(y,4)~=grid_nam_final(z,4)

                                    o2_o2_nam_combo(hug1,1)=x;
                                    o2_o2_nam_combo(hug1,2)=y;
                                    o2_o2_nam_combo(hug1,3)=z;
                                    o2_o2_nam_combo(hug1,4)=i;
                                    o2_o2_nam_combo(hug1,5)=j;
                                    o2_o2_nam_combo(hug1,6)=k;
                                    
                                    
                                    hug1=hug1+1;
                                end
                            end
                        end
                    end
                end
            end
    end
end


o2_o2_nam_combo(:,7)=0;

size_o2_o2_nam_combo=size(o2_o2_nam_combo);

end





o2_o2_car_combo=[];     
if (o2_num>=2)&(car_num>=1)
    o2_o2_car_combo=[1 1 1 1 1 1];  
hug1=1;
for i=1:1:o2_num-1
    for x=1:1:size_grid_o2_final(1,1)
            for j=i+1:1:o2_num
                for y=1:1:size_grid_o2_final(1,1)
                    if (abs(norm(o2_atom(i,1:3)-o2_atom(j,1:3))-norm(grid_o2_final(x,1:3)-grid_o2_final(y,1:3)))<=0.5)&grid_o2_final(x,4)~=grid_o2_final(y,4)
                        for k=1:1:car_num
                            for z=1:1:size_grid_car_final(1,1)
                                if (abs(norm(o2_atom(i,1:3)-car_atom(k,1:3))-norm(grid_o2_final(x,1:3)-grid_car_final(z,1:3)))<=0.5)&(abs(norm(o2_atom(j,1:3)-car_atom(k,1:3))-norm(grid_o2_final(y,1:3)-grid_car_final(z,1:3)))<=0.5)

                                    o2_o2_car_combo(hug1,1)=x;
                                    o2_o2_car_combo(hug1,2)=y;
                                    o2_o2_car_combo(hug1,3)=z;
                                    o2_o2_car_combo(hug1,4)=i;
                                    o2_o2_car_combo(hug1,5)=j;
                                    o2_o2_car_combo(hug1,6)=k;
                                    
                                    
                                    hug1=hug1+1;
                                end
                            end
                        end
                    end
                end
            end
    end
end


o2_o2_car_combo(:,7)=0;

size_o2_o2_car_combo=size(o2_o2_car_combo);

end








o2_nam_nam_combo=[];     
if (o2_num>=1)&(nam_num>=2)
    o2_nam_nam_combo=[1 1 1 1 1 1];
hug1=1;
for i=1:1:o2_num
    for x=1:1:size_grid_o2_final(1,1)
            for j=1:1:nam_num-1
                for y=1:1:size_grid_nam_final(1,1)
                    if (abs(norm(o2_atom(i,1:3)-nam_atom(j,1:3))-norm(grid_o2_final(x,1:3)-grid_nam_final(y,1:3)))<=0.5)&grid_o2_final(x,4)~=grid_nam_final(y,4)
                        for k=j+1:1:nam_num
                            for z=1:1:size_grid_nam_final(1,1)
                                if (abs(norm(o2_atom(i,1:3)-nam_atom(k,1:3))-norm(grid_o2_final(x,1:3)-grid_nam_final(z,1:3)))<=0.5)&(abs(norm(nam_atom(j,1:3)-nam_atom(k,1:3))-norm(grid_nam_final(y,1:3)-grid_nam_final(z,1:3)))<=0.5)&grid_o2_final(x,4)~=grid_nam_final(z,4)&grid_nam_final(y,4)~=grid_nam_final(z,4)


                                    o2_nam_nam_combo(hug1,1)=x;
                                    o2_nam_nam_combo(hug1,2)=y;
                                    o2_nam_nam_combo(hug1,3)=z;
                                    o2_nam_nam_combo(hug1,4)=i;
                                    o2_nam_nam_combo(hug1,5)=j;
                                    o2_nam_nam_combo(hug1,6)=k;
                                    
                                    
                                    
                                    hug1=hug1+1;
                                end
                            end
                        end
                    end
                end
            end
    end
end




o2_nam_nam_combo(:,7)=0;

size_o2_nam_nam_combo=size(o2_nam_nam_combo);

end





o2_o2_npl3_combo=[];     
if (o2_num>=2)&(npl3_num>=1)
    o2_o2_npl3_combo=[1 1 1 1 1 1];
hug1=1;
for i=1:1:o2_num-1
    for x=1:1:size_grid_o2_final(1,1)
            for j=i+1:1:o2_num
                for y=1:1:size_grid_o2_final(1,1)
                    if (abs(norm(o2_atom(i,1:3)-o2_atom(j,1:3))-norm(grid_o2_final(x,1:3)-grid_o2_final(y,1:3)))<=0.5)&grid_o2_final(x,4)~=grid_o2_final(y,4)
                        for k=1:1:npl3_num
                            for z=1:1:size_grid_npl3_final(1,1)
                                if (abs(norm(o2_atom(i,1:3)-npl3_atom(k,1:3))-norm(grid_o2_final(x,1:3)-grid_npl3_final(z,1:3)))<=0.5)&(abs(norm(o2_atom(j,1:3)-npl3_atom(k,1:3))-norm(grid_o2_final(y,1:3)-grid_npl3_final(z,1:3)))<=0.5)&(norm(grid_o2_final(x,1:3)-grid_o2_final(y,1:3))>1)&(norm(grid_o2_final(x,1:3)-grid_npl3_final(z,1:3))>1)&(norm(grid_o2_final(y,1:3)-grid_npl3_final(z,1:3))>1)&grid_o2_final(x,4)~=grid_npl3_final(z,4)&grid_o2_final(y,4)~=grid_npl3_final(z,4)

                                    o2_o2_npl3_combo(hug1,1)=x;
                                    o2_o2_npl3_combo(hug1,2)=y;
                                    o2_o2_npl3_combo(hug1,3)=z;
                                    
                                    o2_o2_npl3_combo(hug1,4)=i;
                                    o2_o2_npl3_combo(hug1,5)=j;
                                    o2_o2_npl3_combo(hug1,6)=k;
                                    hug1=hug1+1;
                                end
                            end
                        end
                    end
                end
            end
    end
end




o2_o2_npl3_combo(:,7)=0;

size_o2_o2_npl3_combo=size(o2_o2_npl3_combo);

end



o2_npl3_npl3_combo=[];     
if (o2_num>=1)&(npl3_num>=2)
    o2_npl3_npl3_combo=[1 1 1 1 1 1];
hug1=1;
for i=1:1:o2_num
    for x=1:1:size_grid_o2_final(1,1)
            for j=1:1:npl3_num-1
                for y=1:1:size_grid_npl3_final(1,1)
                    if (abs(norm(o2_atom(i,1:3)-npl3_atom(j,1:3))-norm(grid_o2_final(x,1:3)-grid_npl3_final(y,1:3)))<=0.5)&grid_o2_final(x,4)~=grid_npl3_final(y,4)
                        for k=j+1:1:npl3_num
                            for z=1:1:size_grid_npl3_final(1,1)
                                if (abs(norm(o2_atom(i,1:3)-npl3_atom(k,1:3))-norm(grid_o2_final(x,1:3)-grid_npl3_final(z,1:3)))<=0.5)&(abs(norm(npl3_atom(j,1:3)-npl3_atom(k,1:3))-norm(grid_npl3_final(y,1:3)-grid_npl3_final(z,1:3)))<=0.5)&grid_o2_final(x,4)~=grid_npl3_final(z,4)&grid_npl3_final(y,4)~=grid_npl3_final(z,4)

                                    o2_npl3_npl3_combo(hug1,1)=x;
                                    o2_npl3_npl3_combo(hug1,2)=y;
                                    o2_npl3_npl3_combo(hug1,3)=z;
                                    
                                    o2_npl3_npl3_combo(hug1,4)=i;
                                    o2_npl3_npl3_combo(hug1,5)=j;
                                    o2_npl3_npl3_combo(hug1,6)=k;
                                    hug1=hug1+1;
                                end
                            end
                        end
                    end
                end
            end
    end
end


o2_npl3_npl3_combo(:,7)=0;
size_o2_npl3_npl3_combo=size(o2_npl3_npl3_combo);

end


o2_o2_n4_combo=[];     
if (o2_num>=2)&(n4_num>=1)
    o2_o2_n4_combo=[1 1 1 1 1 1];
hug1=1;
for i=1:1:o2_num-1
    for x=1:1:size_grid_o2_final(1,1)
            for j=i+1:1:o2_num
                for y=1:1:size_grid_o2_final(1,1)
                    if (abs(norm(o2_atom(i,1:3)-o2_atom(j,1:3))-norm(grid_o2_final(x,1:3)-grid_o2_final(y,1:3)))<=0.5)&grid_o2_final(x,4)~=grid_o2_final(y,4)
                        for k=1:1:n4_num
                            for z=1:1:size_grid_n4_final(1,1)
                                if (abs(norm(o2_atom(i,1:3)-n4_atom(k,1:3))-norm(grid_o2_final(x,1:3)-grid_n4_final(z,1:3)))<=0.5)&(abs(norm(o2_atom(j,1:3)-n4_atom(k,1:3))-norm(grid_o2_final(y,1:3)-grid_n4_final(z,1:3)))<=0.5)&(norm(grid_o2_final(x,1:3)-grid_o2_final(y,1:3))>1)&(norm(grid_o2_final(x,1:3)-grid_n4_final(z,1:3))>1)&(norm(grid_o2_final(y,1:3)-grid_n4_final(z,1:3))>1)&grid_o2_final(x,4)~=grid_n4_final(z,4)&grid_o2_final(y,4)~=grid_n4_final(z,4)

                                    o2_o2_n4_combo(hug1,1)=x;
                                    o2_o2_n4_combo(hug1,2)=y;
                                    o2_o2_n4_combo(hug1,3)=z;
                                    
                                    o2_o2_n4_combo(hug1,4)=i;
                                    o2_o2_n4_combo(hug1,5)=j;
                                    o2_o2_n4_combo(hug1,6)=k;
                                    hug1=hug1+1;
                                end
                            end
                        end
                    end
                end
            end
    end
end



o2_o2_n4_combo(:,7)=0;
size_o2_o2_n4_combo=size(o2_o2_n4_combo);

end


o2_n4_n4_combo=[];     
if (o2_num>=1)&(n4_num>=2)
    o2_n4_n4_combo=[1 1 1 1 1 1];
hug1=1;
for i=1:1:o2_num
    for x=1:1:size_grid_o2_final(1,1)
            for j=1:1:n4_num-1
                for y=1:1:size_grid_n4_final(1,1)
                    if (abs(norm(o2_atom(i,1:3)-n4_atom(j,1:3))-norm(grid_o2_final(x,1:3)-grid_n4_final(y,1:3)))<=0.5)&grid_o2_final(x,4)~=grid_n4_final(y,4)
                        for k=j+1:1:n4_num
                            for z=1:1:size_grid_n4_final(1,1)
                                if (abs(norm(o2_atom(i,1:3)-n4_atom(k,1:3))-norm(grid_o2_final(x,1:3)-grid_n4_final(z,1:3)))<=0.5)&(abs(norm(n4_atom(j,1:3)-n4_atom(k,1:3))-norm(grid_n4_final(y,1:3)-grid_n4_final(z,1:3)))<=0.5)&grid_o2_final(x,4)~=grid_n4_final(z,4)&grid_n4_final(y,4)~=grid_n4_final(z,4)

                                    o2_n4_n4_combo(hug1,1)=x;
                                    o2_n4_n4_combo(hug1,2)=y;
                                    o2_n4_n4_combo(hug1,3)=z;
                                    
                                    o2_n4_n4_combo(hug1,4)=i;
                                    o2_n4_n4_combo(hug1,5)=j;
                                    o2_n4_n4_combo(hug1,6)=k;
                                    hug1=hug1+1;
                                end
                            end
                        end
                    end
                end
            end
    end
end



o2_n4_n4_combo(:,7)=0;
size_o2_n4_n4_combo=size(o2_n4_n4_combo);

end



o2_o2_o3_combo=[];     
if (o2_num>=2)&(o3_num>=1)
    o2_o2_o3_combo=[1 1 1 1 1 1];
hug1=1;
for i=1:1:o2_num-1
    for x=1:1:size_grid_o2_final(1,1)
            for j=i+1:1:o2_num
                for y=1:1:size_grid_o2_final(1,1)
                    if (abs(norm(o2_atom(i,1:3)-o2_atom(j,1:3))-norm(grid_o2_final(x,1:3)-grid_o2_final(y,1:3)))<=0.5)&grid_o2_final(x,4)~=grid_o2_final(y,4)
                        for k=1:1:o3_num
                            for z=1:1:size_grid_o3_final(1,1)
                                if (abs(norm(o2_atom(i,1:3)-o3_atom(k,1:3))-norm(grid_o2_final(x,1:3)-grid_o3_final(z,1:3)))<=0.5)&(abs(norm(o2_atom(j,1:3)-o3_atom(k,1:3))-norm(grid_o2_final(y,1:3)-grid_o3_final(z,1:3)))<=0.5)&(norm(grid_o2_final(x,1:3)-grid_o2_final(y,1:3))>1)&(norm(grid_o2_final(x,1:3)-grid_o3_final(z,1:3))>1)&(norm(grid_o2_final(y,1:3)-grid_o3_final(z,1:3))>1)&grid_o2_final(x,4)~=grid_o3_final(z,4)&grid_o2_final(y,4)~=grid_o3_final(z,4)
                                    
                                    o2_o2_o3_combo(hug1,1)=x;
                                    o2_o2_o3_combo(hug1,2)=y;
                                    o2_o2_o3_combo(hug1,3)=z;
                                    o2_o2_o3_combo(hug1,4)=i;
                                    o2_o2_o3_combo(hug1,5)=j;
                                    o2_o2_o3_combo(hug1,6)=k;
                                    
                                    hug1=hug1+1;
                                end
                            end
                        end
                    end
                end
            end
    end
end



o2_o2_o3_combo(:,7)=0;
size_o2_o2_o3_combo=size(o2_o2_o3_combo);

end


o2_o3_o3_combo=[];     
if (o2_num>=1)&(o3_num>=2)
    o2_o3_o3_combo=[1 1 1 1 1 1];
hug1=1;
for i=1:1:o2_num
    for x=1:1:size_grid_o2_final(1,1)
            for j=1:1:o3_num-1
                for y=1:1:size_grid_o3_final(1,1)
                    if (abs(norm(o2_atom(i,1:3)-o3_atom(j,1:3))-norm(grid_o2_final(x,1:3)-grid_o3_final(y,1:3)))<=0.5)&grid_o2_final(x,4)~=grid_o3_final(y,4)
                        for k=j+1:1:o3_num
                            for z=1:1:size_grid_o3_final(1,1)
                                if (abs(norm(o2_atom(i,1:3)-o3_atom(k,1:3))-norm(grid_o2_final(x,1:3)-grid_o3_final(z,1:3)))<=0.5)&(abs(norm(o3_atom(j,1:3)-o3_atom(k,1:3))-norm(grid_o3_final(y,1:3)-grid_o3_final(z,1:3)))<=0.5)&(norm(grid_o2_final(x,1:3)-grid_o3_final(y,1:3))>1)&(norm(grid_o2_final(x,1:3)-grid_o3_final(z,1:3))>1)&(norm(grid_o3_final(y,1:3)-grid_o3_final(z,1:3))>1)&grid_o2_final(x,4)~=grid_o3_final(z,4)&grid_o3_final(y,4)~=grid_o3_final(z,4)

                                    o2_o3_o3_combo(hug1,1)=x;
                                    o2_o3_o3_combo(hug1,2)=y;
                                    o2_o3_o3_combo(hug1,3)=z;
                                    o2_o3_o3_combo(hug1,4)=i;
                                    o2_o3_o3_combo(hug1,5)=j;
                                    o2_o3_o3_combo(hug1,6)=k;
                                    hug1=hug1+1;
                                end
                            end
                        end
                    end
                end
            end
    end
end



o2_o3_o3_combo(:,7)=0;
size_o2_o3_o3_combo=size(o2_o3_o3_combo);

end

                

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


            

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



nam_nam_npl3_combo=[];     
if (nam_num>=2)&(npl3_num>=1)
    nam_nam_npl3_combo=[1 1 1 1 1 1];
hug1=1;
for i=1:1:nam_num-1
    for x=1:1:size_grid_nam_final(1,1)
            for j=i+1:1:nam_num
                for y=1:1:size_grid_nam_final(1,1)
                    if (abs(norm(nam_atom(i,1:3)-nam_atom(j,1:3))-norm(grid_nam_final(x,1:3)-grid_nam_final(y,1:3)))<=0.5)&grid_nam_final(x,4)~=grid_nam_final(y,4)
                        for k=1:1:npl3_num
                            for z=1:1:size_grid_npl3_final(1,1)
                                if (abs(norm(nam_atom(i,1:3)-npl3_atom(k,1:3))-norm(grid_nam_final(x,1:3)-grid_npl3_final(z,1:3)))<=0.5)&(abs(norm(nam_atom(j,1:3)-npl3_atom(k,1:3))-norm(grid_nam_final(y,1:3)-grid_npl3_final(z,1:3)))<=0.5)&(norm(grid_nam_final(x,1:3)-grid_nam_final(y,1:3))>1)&(norm(grid_nam_final(x,1:3)-grid_npl3_final(z,1:3))>1)&(norm(grid_nam_final(y,1:3)-grid_npl3_final(z,1:3))>1)&grid_nam_final(x,4)~=grid_npl3_final(z,4)&grid_nam_final(y,4)~=grid_npl3_final(z,4)
                                    nam_nam_npl3_combo(hug1,1)=x;
                                    nam_nam_npl3_combo(hug1,2)=y;
                                    nam_nam_npl3_combo(hug1,3)=z;
                                    nam_nam_npl3_combo(hug1,4)=i;
                                    nam_nam_npl3_combo(hug1,5)=j;
                                    nam_nam_npl3_combo(hug1,6)=k;
                                    hug1=hug1+1;
                                end
                            end
                        end
                    end
                end
            end
    end
end



nam_nam_npl3_combo(:,7)=0;
size_nam_nam_npl3_combo=size(nam_nam_npl3_combo);

end




nam_nam_car_combo=[];     
if (nam_num>=2)&(car_num>=1)
    nam_nam_car_combo=[1 1 1 1 1 1];
hug1=1;
for i=1:1:nam_num-1
    for x=1:1:size_grid_nam_final(1,1)
            for j=i+1:1:nam_num
                for y=1:1:size_grid_nam_final(1,1)
                    if (abs(norm(nam_atom(i,1:3)-nam_atom(j,1:3))-norm(grid_nam_final(x,1:3)-grid_nam_final(y,1:3)))<=0.5)&grid_nam_final(x,4)~=grid_nam_final(y,4)
                        for k=1:1:car_num
                            for z=1:1:size_grid_car_final(1,1)
                                if (abs(norm(nam_atom(i,1:3)-car_atom(k,1:3))-norm(grid_nam_final(x,1:3)-grid_car_final(z,1:3)))<=0.5)&(abs(norm(nam_atom(j,1:3)-car_atom(k,1:3))-norm(grid_nam_final(y,1:3)-grid_car_final(z,1:3)))<=0.5)&(norm(grid_nam_final(x,1:3)-grid_nam_final(y,1:3))>1)&(norm(grid_nam_final(x,1:3)-grid_car_final(z,1:3))>1)&(norm(grid_nam_final(y,1:3)-grid_car_final(z,1:3))>1)
                                    nam_nam_car_combo(hug1,1)=x;
                                    nam_nam_car_combo(hug1,2)=y;
                                    nam_nam_car_combo(hug1,3)=z;
                                    nam_nam_car_combo(hug1,4)=i;
                                    nam_nam_car_combo(hug1,5)=j;
                                    nam_nam_car_combo(hug1,6)=k;
                                    hug1=hug1+1;
                                end
                            end
                        end
                    end
                end
            end
    end
end



nam_nam_car_combo(:,7)=0;
size_nam_nam_car_combo=size(nam_nam_car_combo);

end





nam_npl3_npl3_combo=[];     
if (nam_num>=1)&(npl3_num>=2)
    nam_npl3_npl3_combo=[1 1 1 1 1 1];
hug1=1;
for i=1:1:nam_num
    for x=1:1:size_grid_nam_final(1,1)
            for j=1:1:npl3_num-1
                for y=1:1:size_grid_npl3_final(1,1)
                    if (abs(norm(nam_atom(i,1:3)-npl3_atom(j,1:3))-norm(grid_nam_final(x,1:3)-grid_npl3_final(y,1:3)))<=0.5)&grid_nam_final(x,4)~=grid_npl3_final(y,4)
                        for k=j+1:1:npl3_num
                            for z=1:1:size_grid_npl3_final(1,1)
                                if (abs(norm(nam_atom(i,1:3)-npl3_atom(k,1:3))-norm(grid_nam_final(x,1:3)-grid_npl3_final(z,1:3)))<=0.5)&(abs(norm(npl3_atom(j,1:3)-npl3_atom(k,1:3))-norm(grid_npl3_final(y,1:3)-grid_npl3_final(z,1:3)))<=0.5)&(norm(grid_nam_final(x,1:3)-grid_npl3_final(y,1:3))>1)&(norm(grid_nam_final(x,1:3)-grid_npl3_final(z,1:3))>1)&(norm(grid_npl3_final(y,1:3)-grid_npl3_final(z,1:3))>1)&grid_nam_final(x,4)~=grid_npl3_final(z,4)&grid_npl3_final(y,4)~=grid_npl3_final(z,4)
                                    %nam_npl3_npl3_combo(hug1,i)=nam_mat(i,x);
                                    %nam_npl3_npl3_combo(hug1,j)=npl3_mat(j,y);
                                    %nam_npl3_npl3_combo(hug1,k)=npl3_mat(k,z);
                                    nam_npl3_npl3_combo(hug1,1)=x;
                                    nam_npl3_npl3_combo(hug1,2)=y;
                                    nam_npl3_npl3_combo(hug1,3)=z;
                                    nam_npl3_npl3_combo(hug1,4)=i;
                                    nam_npl3_npl3_combo(hug1,5)=j;
                                    nam_npl3_npl3_combo(hug1,6)=k;
                                    hug1=hug1+1;
                                end
                            end
                        end
                    end
                end
            end
    end
end



nam_npl3_npl3_combo(:,7)=0;
size_nam_npl3_npl3_combo=size(nam_npl3_npl3_combo);

end

nam_nam_n4_combo=[];     
if (nam_num>=2)&(n4_num>=1)
    nam_nam_n4_combo=[1 1 1 1 1 1];
hug1=1;
for i=1:1:nam_num-1
    for x=1:1:size_grid_nam_final(1,1)
            for j=i+1:1:nam_num
                for y=1:1:size_grid_nam_final(1,1)
                    if (abs(norm(nam_atom(i,1:3)-nam_atom(j,1:3))-norm(grid_nam_final(x,1:3)-grid_nam_final(y,1:3)))<=0.5)&grid_nam_final(x,4)~=grid_nam_final(y,4)
                        for k=1:1:n4_num
                            for z=1:1:size_grid_n4_final(1,1)
                                if (abs(norm(nam_atom(i,1:3)-n4_atom(k,1:3))-norm(grid_nam_final(x,1:3)-grid_n4_final(z,1:3)))<=0.5)&(abs(norm(nam_atom(j,1:3)-n4_atom(k,1:3))-norm(grid_nam_final(y,1:3)-grid_n4_final(z,1:3)))<=0.5)&(norm(grid_nam_final(x,1:3)-grid_nam_final(y,1:3))>1)&(norm(grid_nam_final(x,1:3)-grid_n4_final(z,1:3))>1)&(norm(grid_nam_final(y,1:3)-grid_n4_final(z,1:3))>1)&grid_nam_final(x,4)~=grid_n4_final(z,4)&grid_nam_final(y,4)~=grid_n4_final(z,4)

                                    nam_nam_n4_combo(hug1,1)=x;
                                    nam_nam_n4_combo(hug1,2)=y;
                                    nam_nam_n4_combo(hug1,3)=z;
                                    
                                    nam_nam_n4_combo(hug1,4)=i;
                                    nam_nam_n4_combo(hug1,5)=j;
                                    nam_nam_n4_combo(hug1,6)=k;
                                    hug1=hug1+1;
                                end
                            end
                        end
                    end
                end
            end
    end
end



nam_nam_n4_combo(:,7)=0;
size_nam_nam_n4_combo=size(nam_nam_n4_combo);


end

nam_n4_n4_combo=[];     
if (nam_num>=1)&(n4_num>=2)
    nam_n4_n4_combo=[1 1 1 1 1 1];
hug1=1;
for i=1:1:nam_num
    for x=1:1:size_grid_nam_final(1,1)
            for j=1:1:n4_num-1
                for y=1:1:size_grid_n4_final(1,1)
                    if (abs(norm(nam_atom(i,1:3)-n4_atom(j,1:3))-norm(grid_nam_final(x,1:3)-grid_n4_final(y,1:3)))<=0.5)&grid_nam_final(x,4)~=grid_n4_final(y,4)
                        for k=j+1:1:n4_num
                            for z=1:1:size_grid_n4_final(1,1)
                                if (abs(norm(nam_atom(i,1:3)-n4_atom(k,1:3))-norm(grid_nam_final(x,1:3)-grid_n4_final(z,1:3)))<=0.5)&(abs(norm(n4_atom(j,1:3)-n4_atom(k,1:3))-norm(grid_n4_final(y,1:3)-grid_n4_final(z,1:3)))<=0.5)&grid_nam_final(x,4)~=grid_n4_final(z,4)&grid_n4_final(y,4)~=grid_n4_final(z,4)

                                    nam_n4_n4_combo(hug1,1)=x;
                                    nam_n4_n4_combo(hug1,2)=y;
                                    nam_n4_n4_combo(hug1,3)=z;
                                    
                                    nam_n4_n4_combo(hug1,4)=i;
                                    nam_n4_n4_combo(hug1,5)=j;
                                    nam_n4_n4_combo(hug1,6)=k;
                                    hug1=hug1+1;
                                end
                            end
                        end
                    end
                end
            end
    end
end




nam_n4_n4_combo(:,7)=0;
size_nam_n4_n4_combo=size(nam_n4_n4_combo);

end


nam_nam_o3_combo=[];     
if (nam_num>=2)&(o3_num>=1)
    nam_nam_o3_combo=[1 1 1 1 1 1];
hug1=1;
for i=1:1:nam_num-1
    for x=1:1:size_grid_nam_final(1,1)
            for j=i+1:1:nam_num
                for y=1:1:size_grid_nam_final(1,1)
                    if (abs(norm(nam_atom(i,1:3)-nam_atom(j,1:3))-norm(grid_nam_final(x,1:3)-grid_nam_final(y,1:3)))<=0.5)&grid_nam_final(x,4)~=grid_nam_final(y,4)
                        for k=1:1:o3_num
                            for z=1:1:size_grid_o3_final(1,1)
                                if (abs(norm(nam_atom(i,1:3)-o3_atom(k,1:3))-norm(grid_nam_final(x,1:3)-grid_o3_final(z,1:3)))<=0.5)&(abs(norm(nam_atom(j,1:3)-o3_atom(k,1:3))-norm(grid_nam_final(y,1:3)-grid_o3_final(z,1:3)))<=0.5)&(norm(grid_nam_final(x,1:3)-grid_nam_final(y,1:3))>1)&(norm(grid_nam_final(x,1:3)-grid_o3_final(z,1:3))>1)&(norm(grid_nam_final(y,1:3)-grid_o3_final(z,1:3))>1)&grid_nam_final(x,4)~=grid_o3_final(z,4)&grid_nam_final(y,4)~=grid_o3_final(z,4)
                                    nam_nam_o3_combo(hug1,1)=x;
                                    nam_nam_o3_combo(hug1,2)=y;
                                    nam_nam_o3_combo(hug1,3)=z;
                                    nam_nam_o3_combo(hug1,4)=i;
                                    nam_nam_o3_combo(hug1,5)=j;
                                    nam_nam_o3_combo(hug1,6)=k;
                                    hug1=hug1+1;
                                end
                            end
                        end
                    end
                end
            end
    end
end



nam_nam_o3_combo(:,7)=0;
size_nam_nam_o3_combo=size(nam_nam_o3_combo);

end

nam_o3_o3_combo=[];     
if (nam_num>=1)&(o3_num>=2)
    nam_o3_o3_combo=[1 1 1 1 1 1];
hug1=1;
for i=1:1:nam_num
    for x=1:1:size_grid_nam_final(1,1)
            for j=1:1:o3_num-1
                for y=1:1:size_grid_o3_final(1,1)
                    if (abs(norm(nam_atom(i,1:3)-o3_atom(j,1:3))-norm(grid_nam_final(x,1:3)-grid_o3_final(y,1:3)))<=0.5)&grid_nam_final(x,4)~=grid_o3_final(y,4)
                        for k=j+1:1:o3_num
                            for z=1:1:size_grid_o3_final(1,1)
                                if (abs(norm(nam_atom(i,1:3)-o3_atom(k,1:3))-norm(grid_nam_final(x,1:3)-grid_o3_final(z,1:3)))<=0.5)&(abs(norm(o3_atom(j,1:3)-o3_atom(k,1:3))-norm(grid_o3_final(y,1:3)-grid_o3_final(z,1:3)))<=0.5)&(norm(grid_nam_final(x,1:3)-grid_o3_final(y,1:3))>1)&(norm(grid_nam_final(x,1:3)-grid_o3_final(z,1:3))>1)&(norm(grid_o3_final(y,1:3)-grid_o3_final(z,1:3))>1)&grid_nam_final(x,4)~=grid_o3_final(z,4)&grid_o3_final(y,4)~=grid_o3_final(z,4)
                                    %nam_o3_o3_combo(hug1,i)=nam_mat(i,x);
                                    %nam_o3_o3_combo(hug1,j)=o3_mat(j,y);
                                    %nam_o3_o3_combo(hug1,k)=o3_mat(k,z);
                                    nam_o3_o3_combo(hug1,1)=x;
                                    nam_o3_o3_combo(hug1,2)=y;
                                    nam_o3_o3_combo(hug1,3)=z;
                                    nam_o3_o3_combo(hug1,4)=i;
                                    nam_o3_o3_combo(hug1,5)=j;
                                    nam_o3_o3_combo(hug1,6)=k;
                                    hug1=hug1+1;
                                end
                            end
                        end
                    end
                end
            end
    end
end



nam_o3_o3_combo(:,7)=0;
size_nam_o3_o3_combo=size(nam_o3_o3_combo);

end
                                    



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%





npl3_npl3_o3_combo=[];     
if (npl3_num>=2)&(o3_num>=1)
    npl3_npl3_o3_combo=[1 1 1 1 1 1];
hug1=1;
for i=1:1:npl3_num-1
    for x=1:1:size_grid_npl3_final(1,1)
            for j=i+1:1:npl3_num
                for y=1:1:size_grid_npl3_final(1,1)
                    if (abs(norm(npl3_atom(i,1:3)-npl3_atom(j,1:3))-norm(grid_npl3_final(x,1:3)-grid_npl3_final(y,1:3)))<=0.5)&grid_npl3_final(x,4)~=grid_npl3_final(y,4)
                        for k=1:1:o3_num
                            for z=1:1:size_grid_o3_final(1,1)
                                if (abs(norm(npl3_atom(i,1:3)-o3_atom(k,1:3))-norm(grid_npl3_final(x,1:3)-grid_o3_final(z,1:3)))<=0.5)&(abs(norm(npl3_atom(j,1:3)-o3_atom(k,1:3))-norm(grid_npl3_final(y,1:3)-grid_o3_final(z,1:3)))<=0.5)&(norm(grid_npl3_final(x,1:3)-grid_npl3_final(y,1:3))>1)&(norm(grid_npl3_final(x,1:3)-grid_o3_final(z,1:3))>1)&(norm(grid_npl3_final(y,1:3)-grid_o3_final(z,1:3))>1)&grid_npl3_final(x,4)~=grid_o3_final(z,4)&grid_npl3_final(y,4)~=grid_o3_final(z,4)
                                    npl3_npl3_o3_combo(hug1,1)=x;
                                    npl3_npl3_o3_combo(hug1,2)=y;
                                    npl3_npl3_o3_combo(hug1,3)=z;
                                    npl3_npl3_o3_combo(hug1,4)=i;
                                    npl3_npl3_o3_combo(hug1,5)=j;
                                    npl3_npl3_o3_combo(hug1,6)=k;
                                    hug1=hug1+1;
                                end
                            end
                        end
                    end
                end
            end
    end
end



npl3_npl3_o3_combo(:,7)=0;
size_npl3_npl3_o3_combo=size(npl3_npl3_o3_combo);

end



npl3_npl3_car_combo=[];     
if (npl3_num>=2)&(car_num>=1)
    npl3_npl3_car_combo=[1 1 1 1 1 1];
hug1=1;
for i=1:1:npl3_num-1
    for x=1:1:size_grid_npl3_final(1,1)
            for j=i+1:1:npl3_num
                for y=1:1:size_grid_npl3_final(1,1)
                    if (abs(norm(npl3_atom(i,1:3)-npl3_atom(j,1:3))-norm(grid_npl3_final(x,1:3)-grid_npl3_final(y,1:3)))<=0.5)&grid_npl3_final(x,4)~=grid_npl3_final(y,4)
                        for k=1:1:car_num
                            for z=1:1:size_grid_car_final(1,1)
                                if (abs(norm(npl3_atom(i,1:3)-car_atom(k,1:3))-norm(grid_npl3_final(x,1:3)-grid_car_final(z,1:3)))<=0.5)&(abs(norm(npl3_atom(j,1:3)-car_atom(k,1:3))-norm(grid_npl3_final(y,1:3)-grid_car_final(z,1:3)))<=0.5)&(norm(grid_npl3_final(x,1:3)-grid_npl3_final(y,1:3))>1)&(norm(grid_npl3_final(x,1:3)-grid_car_final(z,1:3))>1)&(norm(grid_npl3_final(y,1:3)-grid_car_final(z,1:3))>1)
                                    npl3_npl3_car_combo(hug1,1)=x;
                                    npl3_npl3_car_combo(hug1,2)=y;
                                    npl3_npl3_car_combo(hug1,3)=z;
                                    npl3_npl3_car_combo(hug1,4)=i;
                                    npl3_npl3_car_combo(hug1,5)=j;
                                    npl3_npl3_car_combo(hug1,6)=k;
                                    hug1=hug1+1;
                                end
                            end
                        end
                    end
                end
            end
    end
end



npl3_npl3_car_combo(:,7)=0;
size_npl3_npl3_car_combo=size(npl3_npl3_car_combo);

end




npl3_o3_o3_combo=[];     
if (npl3_num>=1)&(o3_num>=2)
    npl3_o3_o3_combo=[1 1 1 1 1 1];
hug1=1;
for i=1:1:npl3_num
    for x=1:1:size_grid_npl3_final(1,1)
            for j=1:1:o3_num-1
                for y=1:1:size_grid_o3_final(1,1)
                    if (abs(norm(npl3_atom(i,1:3)-o3_atom(j,1:3))-norm(grid_npl3_final(x,1:3)-grid_o3_final(y,1:3)))<=0.5)&grid_npl3_final(x,4)~=grid_o3_final(y,4)
                        for k=j+1:1:o3_num
                            for z=1:1:size_grid_o3_final(1,1)
                                if (abs(norm(npl3_atom(i,1:3)-o3_atom(k,1:3))-norm(grid_npl3_final(x,1:3)-grid_o3_final(z,1:3)))<=0.5)&(abs(norm(o3_atom(j,1:3)-o3_atom(k,1:3))-norm(grid_o3_final(y,1:3)-grid_o3_final(z,1:3)))<=0.5)&(norm(grid_npl3_final(x,1:3)-grid_o3_final(y,1:3))>1)&(norm(grid_npl3_final(x,1:3)-grid_o3_final(z,1:3))>1)&(norm(grid_o3_final(y,1:3)-grid_o3_final(z,1:3))>1)&grid_npl3_final(x,4)~=grid_o3_final(z,4)&grid_o3_final(y,4)~=grid_o3_final(z,4)
                                    %npl3_o3_o3_combo(hug1,i)=npl3_mat(i,x);
                                    %npl3_o3_o3_combo(hug1,j)=o3_mat(j,y);
                                    %npl3_o3_o3_combo(hug1,k)=o3_mat(k,z);
                                    npl3_o3_o3_combo(hug1,1)=x;
                                    npl3_o3_o3_combo(hug1,2)=y;
                                    npl3_o3_o3_combo(hug1,3)=z;
                                    npl3_o3_o3_combo(hug1,4)=i;
                                    npl3_o3_o3_combo(hug1,5)=j;
                                    npl3_o3_o3_combo(hug1,6)=k;
                                    hug1=hug1+1;
                                end
                            end
                        end
                    end
                end
            end
    end
end



npl3_o3_o3_combo(:,7)=0;
size_npl3_o3_o3_combo=size(npl3_o3_o3_combo);

end


npl3_npl3_n4_combo=[];     
if (npl3_num>=2)&(n4_num>=1)
    npl3_npl3_n4_combo=[1 1 1 1 1 1];
hug1=1;
for i=1:1:npl3_num-1
    for x=1:1:size_grid_npl3_final(1,1)
            for j=i+1:1:npl3_num
                for y=1:1:size_grid_npl3_final(1,1)
                    if (abs(norm(npl3_atom(i,1:3)-npl3_atom(j,1:3))-norm(grid_npl3_final(x,1:3)-grid_npl3_final(y,1:3)))<=0.5)&grid_npl3_final(x,4)~=grid_npl3_final(y,4)
                        for k=1:1:n4_num
                            for z=1:1:size_grid_n4_final(1,1)
                                if (abs(norm(npl3_atom(i,1:3)-n4_atom(k,1:3))-norm(grid_npl3_final(x,1:3)-grid_n4_final(z,1:3)))<=0.5)&(abs(norm(npl3_atom(j,1:3)-n4_atom(k,1:3))-norm(grid_npl3_final(y,1:3)-grid_n4_final(z,1:3)))<=0.5)&(norm(grid_npl3_final(x,1:3)-grid_npl3_final(y,1:3))>1)&(norm(grid_npl3_final(x,1:3)-grid_n4_final(z,1:3))>1)&(norm(grid_npl3_final(y,1:3)-grid_n4_final(z,1:3))>1)&grid_npl3_final(x,4)~=grid_n4_final(z,4)&grid_npl3_final(y,4)~=grid_n4_final(z,4)

                                    npl3_npl3_n4_combo(hug1,1)=x;
                                    npl3_npl3_n4_combo(hug1,2)=y;
                                    npl3_npl3_n4_combo(hug1,3)=z;
                                    
                                    npl3_npl3_n4_combo(hug1,4)=i;
                                    npl3_npl3_n4_combo(hug1,5)=j;
                                    npl3_npl3_n4_combo(hug1,6)=k;
                                    hug1=hug1+1;
                                end
                            end
                        end
                    end
                end
            end
    end
end



npl3_npl3_n4_combo(:,7)=0;
size_npl3_npl3_n4_combo=size(npl3_npl3_n4_combo);

end


npl3_n4_n4_combo=[];     
if (npl3_num>=1)&(n4_num>=2)
    npl3_n4_n4_combo=[1 1 1 1 1 1];
hug1=1;
for i=1:1:npl3_num
    for x=1:1:size_grid_npl3_final(1,1)
            for j=1:1:n4_num-1
                for y=1:1:size_grid_n4_final(1,1)
                    if (abs(norm(npl3_atom(i,1:3)-n4_atom(j,1:3))-norm(grid_npl3_final(x,1:3)-grid_n4_final(y,1:3)))<=0.5)&grid_npl3_final(x,4)~=grid_n4_final(y,4)
                        for k=j+1:1:n4_num
                            for z=1:1:size_grid_n4_final(1,1)
                                if (abs(norm(npl3_atom(i,1:3)-n4_atom(k,1:3))-norm(grid_npl3_final(x,1:3)-grid_n4_final(z,1:3)))<=0.5)&(abs(norm(n4_atom(j,1:3)-n4_atom(k,1:3))-norm(grid_n4_final(y,1:3)-grid_n4_final(z,1:3)))<=0.5)&grid_npl3_final(x,4)~=grid_n4_final(z,4)&grid_n4_final(y,4)~=grid_n4_final(z,4)

                                    npl3_n4_n4_combo(hug1,1)=x;
                                    npl3_n4_n4_combo(hug1,2)=y;
                                    npl3_n4_n4_combo(hug1,3)=z;
                                    
                                    npl3_n4_n4_combo(hug1,4)=i;
                                    npl3_n4_n4_combo(hug1,5)=j;
                                    npl3_n4_n4_combo(hug1,6)=k;
                                    hug1=hug1+1;
                                end
                            end
                        end
                    end
                end
            end
    end
end




npl3_n4_n4_combo(:,7)=0;
size_npl3_n4_n4_combo=size(npl3_n4_n4_combo);

end
                                    





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



o3_o3_car_combo=[];     
if (o3_num>=2)&(car_num>=1)
    o3_o3_car_combo=[1 1 1 1 1 1];
hug1=1;
for i=1:1:o3_num-1
    for x=1:1:size_grid_o3_final(1,1)
            for j=i+1:1:o3_num
                for y=1:1:size_grid_o3_final(1,1)
                    if (abs(norm(o3_atom(i,1:3)-o3_atom(j,1:3))-norm(grid_o3_final(x,1:3)-grid_o3_final(y,1:3)))<=0.5)&grid_o3_final(x,4)~=grid_o3_final(y,4)
                        for k=1:1:car_num
                            for z=1:1:size_grid_car_final(1,1)
                                if (abs(norm(o3_atom(i,1:3)-car_atom(k,1:3))-norm(grid_o3_final(x,1:3)-grid_car_final(z,1:3)))<=0.5)&(abs(norm(o3_atom(j,1:3)-car_atom(k,1:3))-norm(grid_o3_final(y,1:3)-grid_car_final(z,1:3)))<=0.5)&(norm(grid_o3_final(x,1:3)-grid_o3_final(y,1:3))>1)&(norm(grid_o3_final(x,1:3)-grid_car_final(z,1:3))>1)&(norm(grid_o3_final(y,1:3)-grid_car_final(z,1:3))>1)
                                    o3_o3_car_combo(hug1,1)=x;
                                    o3_o3_car_combo(hug1,2)=y;
                                    o3_o3_car_combo(hug1,3)=z;
                                    o3_o3_car_combo(hug1,4)=i;
                                    o3_o3_car_combo(hug1,5)=j;
                                    o3_o3_car_combo(hug1,6)=k;
                                    hug1=hug1+1;
                                end
                            end
                        end
                    end
                end
            end
    end
end



o3_o3_car_combo(:,7)=0;
size_o3_o3_car_combo=size(o3_o3_car_combo);

end


                           

o3_o3_n4_combo=[];     
if (o3_num>=2)&(n4_num>=1)
    o3_o3_n4_combo=[1 1 1 1 1 1];
hug1=1;
for i=1:1:o3_num-1
    for x=1:1:size_grid_o3_final(1,1)
            for j=i+1:1:o3_num
                for y=1:1:size_grid_o3_final(1,1)
                    if (abs(norm(o3_atom(i,1:3)-o3_atom(j,1:3))-norm(grid_o3_final(x,1:3)-grid_o3_final(y,1:3)))<=0.5)&grid_o3_final(x,4)~=grid_o3_final(y,4)
                        for k=1:1:n4_num
                            for z=1:1:size_grid_n4_final(1,1)
                                if (abs(norm(o3_atom(i,1:3)-n4_atom(k,1:3))-norm(grid_o3_final(x,1:3)-grid_n4_final(z,1:3)))<=0.5)&(abs(norm(o3_atom(j,1:3)-n4_atom(k,1:3))-norm(grid_o3_final(y,1:3)-grid_n4_final(z,1:3)))<=0.5)&(norm(grid_o3_final(x,1:3)-grid_o3_final(y,1:3))>1)&(norm(grid_o3_final(x,1:3)-grid_n4_final(z,1:3))>1)&(norm(grid_o3_final(y,1:3)-grid_n4_final(z,1:3))>1)&grid_o3_final(x,4)~=grid_n4_final(z,4)&grid_o3_final(y,4)~=grid_n4_final(z,4)

                                    o3_o3_n4_combo(hug1,1)=x;
                                    o3_o3_n4_combo(hug1,2)=y;
                                    o3_o3_n4_combo(hug1,3)=z;
                                    
                                    o3_o3_n4_combo(hug1,4)=i;
                                    o3_o3_n4_combo(hug1,5)=j;
                                    o3_o3_n4_combo(hug1,6)=k;
                                    hug1=hug1+1;
                                end
                            end
                        end
                    end
                end
            end
    end
end



o3_o3_n4_combo(:,7)=0;
size_o3_o3_n4_combo=size(o3_o3_n4_combo);


end

o3_n4_n4_combo=[];     
if (o3_num>=1)&(n4_num>=2)
    o3_n4_n4_combo=[1 1 1 1 1 1];
hug1=1;
for i=1:1:o3_num
    for x=1:1:size_grid_o3_final(1,1)
            for j=1:1:n4_num-1
                for y=1:1:size_grid_n4_final(1,1)
                    if (abs(norm(o3_atom(i,1:3)-n4_atom(j,1:3))-norm(grid_o3_final(x,1:3)-grid_n4_final(y,1:3)))<=0.5)&grid_o3_final(x,4)~=grid_n4_final(y,4)
                        for k=j+1:1:n4_num
                            for z=1:1:size_grid_n4_final(1,1)
                                if (abs(norm(o3_atom(i,1:3)-n4_atom(k,1:3))-norm(grid_o3_final(x,1:3)-grid_n4_final(z,1:3)))<=0.5)&(abs(norm(n4_atom(j,1:3)-n4_atom(k,1:3))-norm(grid_n4_final(y,1:3)-grid_n4_final(z,1:3)))<=0.5)&grid_o3_final(x,4)~=grid_n4_final(z,4)&grid_n4_final(y,4)~=grid_n4_final(z,4)

                                    o3_n4_n4_combo(hug1,1)=x;
                                    o3_n4_n4_combo(hug1,2)=y;
                                    o3_n4_n4_combo(hug1,3)=z;
                                    
                                    o3_n4_n4_combo(hug1,4)=i;
                                    o3_n4_n4_combo(hug1,5)=j;
                                    o3_n4_n4_combo(hug1,6)=k;
                                    hug1=hug1+1;
                                end
                            end
                        end
                    end
                end
            end
    end
end



o3_n4_n4_combo(:,7)=0;
size_o3_n4_n4_combo=size(o3_n4_n4_combo);

end



o3_car_car_combo=[];     
if (o3_num>=1)&(car_num>=2)
    o3_car_car_combo=[1 1 1 1 1 1];
hug1=1;
for i=1:1:o3_num
    for x=1:1:size_grid_o3_final(1,1)
            for j=1:1:car_num-1
                for y=1:1:size_grid_car_final(1,1)
                    if (abs(norm(o3_atom(i,1:3)-car_atom(j,1:3))-norm(grid_o3_final(x,1:3)-grid_car_final(y,1:3)))<=0.5)&grid_o3_final(x,4)~=grid_car_final(y,4)
                        for k=j+1:1:car_num
                            for z=1:1:size_grid_car_final(1,1)
                                if (abs(norm(o3_atom(i,1:3)-car_atom(k,1:3))-norm(grid_o3_final(x,1:3)-grid_car_final(z,1:3)))<=0.5)&(abs(norm(car_atom(j,1:3)-car_atom(k,1:3))-norm(grid_car_final(y,1:3)-grid_car_final(z,1:3)))<=0.5)&grid_o3_final(x,4)~=grid_car_final(z,4)&grid_car_final(y,4)~=grid_car_final(z,4)

                                    o3_car_car_combo(hug1,1)=x;
                                    o3_car_car_combo(hug1,2)=y;
                                    o3_car_car_combo(hug1,3)=z;
                                    
                                    o3_car_car_combo(hug1,4)=i;
                                    o3_car_car_combo(hug1,5)=j;
                                    o3_car_car_combo(hug1,6)=k;
                                    hug1=hug1+1;
                                end
                            end
                        end
                    end
                end
            end
    end
end



o3_car_car_combo(:,7)=0;
size_o3_car_car_combo=size(o3_car_car_combo);

end

o2_car_car_combo=[];     
if (o2_num>=1)&(car_num>=2)
    o2_car_car_combo=[1 1 1 1 1 1];
hug1=1;
for i=1:1:o2_num
    for x=1:1:size_grid_o2_final(1,1)
            for j=1:1:car_num-1
                for y=1:1:size_grid_car_final(1,1)
                    if (abs(norm(o2_atom(i,1:3)-car_atom(j,1:3))-norm(grid_o2_final(x,1:3)-grid_car_final(y,1:3)))<=0.5)&grid_o2_final(x,4)~=grid_car_final(y,4)
                        for k=j+1:1:car_num
                            for z=1:1:size_grid_car_final(1,1)
                                if (abs(norm(o2_atom(i,1:3)-car_atom(k,1:3))-norm(grid_o2_final(x,1:3)-grid_car_final(z,1:3)))<=0.5)&(abs(norm(car_atom(j,1:3)-car_atom(k,1:3))-norm(grid_car_final(y,1:3)-grid_car_final(z,1:3)))<=0.5)&grid_o2_final(x,4)~=grid_car_final(z,4)&grid_car_final(y,4)~=grid_car_final(z,4)

                                    o2_car_car_combo(hug1,1)=x;
                                    o2_car_car_combo(hug1,2)=y;
                                    o2_car_car_combo(hug1,3)=z;
                                    
                                    o2_car_car_combo(hug1,4)=i;
                                    o2_car_car_combo(hug1,5)=j;
                                    o2_car_car_combo(hug1,6)=k;
                                    hug1=hug1+1;
                                end
                            end
                        end
                    end
                end
            end
    end
end



o2_car_car_combo(:,7)=0;
size_o2_car_car_combo=size(o2_car_car_combo);

end

nam_car_car_combo=[];     
if (nam_num>=1)&(car_num>=2)
    nam_car_car_combo=[1 1 1 1 1 1];
hug1=1;
for i=1:1:nam_num
    for x=1:1:size_grid_nam_final(1,1)
            for j=1:1:car_num-1
                for y=1:1:size_grid_car_final(1,1)
                    if (abs(norm(nam_atom(i,1:3)-car_atom(j,1:3))-norm(grid_nam_final(x,1:3)-grid_car_final(y,1:3)))<=0.5)&grid_nam_final(x,4)~=grid_car_final(y,4)
                        for k=j+1:1:car_num
                            for z=1:1:size_grid_car_final(1,1)
                                if (abs(norm(nam_atom(i,1:3)-car_atom(k,1:3))-norm(grid_nam_final(x,1:3)-grid_car_final(z,1:3)))<=0.5)&(abs(norm(car_atom(j,1:3)-car_atom(k,1:3))-norm(grid_car_final(y,1:3)-grid_car_final(z,1:3)))<=0.5)&grid_nam_final(x,4)~=grid_car_final(z,4)&grid_car_final(y,4)~=grid_car_final(z,4)

                                    nam_car_car_combo(hug1,1)=x;
                                    nam_car_car_combo(hug1,2)=y;
                                    nam_car_car_combo(hug1,3)=z;
                                    
                                    nam_car_car_combo(hug1,4)=i;
                                    nam_car_car_combo(hug1,5)=j;
                                    nam_car_car_combo(hug1,6)=k;
                                    hug1=hug1+1;
                                end
                            end
                        end
                    end
                end
            end
    end
end



nam_car_car_combo(:,7)=0;
size_nam_car_car_combo=size(nam_car_car_combo);

end

npl3_car_car_combo=[];     
if (npl3_num>=1)&(car_num>=2)
    npl3_car_car_combo=[1 1 1 1 1 1];
hug1=1;
for i=1:1:npl3_num
    for x=1:1:size_grid_npl3_final(1,1)
            for j=1:1:car_num-1
                for y=1:1:size_grid_car_final(1,1)
                    if (abs(norm(npl3_atom(i,1:3)-car_atom(j,1:3))-norm(grid_npl3_final(x,1:3)-grid_car_final(y,1:3)))<=0.5)&grid_npl3_final(x,4)~=grid_car_final(y,4)
                        for k=j+1:1:car_num
                            for z=1:1:size_grid_car_final(1,1)
                                if (abs(norm(npl3_atom(i,1:3)-car_atom(k,1:3))-norm(grid_npl3_final(x,1:3)-grid_car_final(z,1:3)))<=0.5)&(abs(norm(car_atom(j,1:3)-car_atom(k,1:3))-norm(grid_car_final(y,1:3)-grid_car_final(z,1:3)))<=0.5)&grid_npl3_final(x,4)~=grid_car_final(z,4)&grid_car_final(y,4)~=grid_car_final(z,4)

                                    npl3_car_car_combo(hug1,1)=x;
                                    npl3_car_car_combo(hug1,2)=y;
                                    npl3_car_car_combo(hug1,3)=z;
                                    
                                    npl3_car_car_combo(hug1,4)=i;
                                    npl3_car_car_combo(hug1,5)=j;
                                    npl3_car_car_combo(hug1,6)=k;
                                    hug1=hug1+1;
                                end
                            end
                        end
                    end
                end
            end
    end
end



npl3_car_car_combo(:,7)=0;
size_npl3_car_car_combo=size(npl3_car_car_combo);

end

n4_car_car_combo=[];     
if (n4_num>=1)&(car_num>=2)
    n4_car_car_combo=[1 1 1 1 1 1];
hug1=1;
for i=1:1:n4_num
    for x=1:1:size_grid_n4_final(1,1)
            for j=1:1:car_num-1
                for y=1:1:size_grid_car_final(1,1)
                    if (abs(norm(n4_atom(i,1:3)-car_atom(j,1:3))-norm(grid_n4_final(x,1:3)-grid_car_final(y,1:3)))<=0.5)&grid_n4_final(x,4)~=grid_car_final(y,4)
                        for k=j+1:1:car_num
                            for z=1:1:size_grid_car_final(1,1)
                                if (abs(norm(n4_atom(i,1:3)-car_atom(k,1:3))-norm(grid_n4_final(x,1:3)-grid_car_final(z,1:3)))<=0.5)&(abs(norm(car_atom(j,1:3)-car_atom(k,1:3))-norm(grid_car_final(y,1:3)-grid_car_final(z,1:3)))<=0.5)&grid_n4_final(x,4)~=grid_car_final(z,4)&grid_car_final(y,4)~=grid_car_final(z,4)

                                    n4_car_car_combo(hug1,1)=x;
                                    n4_car_car_combo(hug1,2)=y;
                                    n4_car_car_combo(hug1,3)=z;
                                    
                                    n4_car_car_combo(hug1,4)=i;
                                    n4_car_car_combo(hug1,5)=j;
                                    n4_car_car_combo(hug1,6)=k;
                                    hug1=hug1+1;
                                end
                            end
                        end
                    end
                end
            end
    end
end



n4_car_car_combo(:,7)=0;
size_n4_car_car_combo=size(n4_car_car_combo);

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



                       
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

         








%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        
   nam_nam_nam_combo=[];     
if nam_num>=3
    nam_nam_nam_combo=[1 1 1 1 1 1];
hug1=1;
for i=1:1:nam_num-2
    for x=1:1:size_grid_nam_final(1,1)
            for j=i+1:1:nam_num-1
                for y=1:1:size_grid_nam_final(1,1)
                    if (abs(norm(nam_atom(i,1:3)-nam_atom(j,1:3))-norm(grid_nam_final(x,1:3)-grid_nam_final(y,1:3)))<=0.5)&grid_nam_final(x,4)~=grid_nam_final(y,4)
                        for k=j+1:1:nam_num
                            for z=1:1:size_grid_nam_final(1,1)
                                if (abs(norm(nam_atom(i,1:3)-nam_atom(k,1:3))-norm(grid_nam_final(x,1:3)-grid_nam_final(z,1:3)))<=0.5)&(abs(norm(nam_atom(j,1:3)-nam_atom(k,1:3))-norm(grid_nam_final(y,1:3)-grid_nam_final(z,1:3)))<=0.5)&grid_nam_final(x,4)~=grid_nam_final(z,4)&grid_nam_final(y,4)~=grid_nam_final(z,4)
                                    nam_nam_nam_combo(hug1,1)=x;
                                    nam_nam_nam_combo(hug1,2)=y;
                                    nam_nam_nam_combo(hug1,3)=z;
                                    nam_nam_nam_combo(hug1,4)=i;
                                    nam_nam_nam_combo(hug1,5)=j;
                                    nam_nam_nam_combo(hug1,6)=k;
                                    hug1=hug1+1;
                                end
                            end
                        end
                    end
                end
            end
    end
end



nam_nam_nam_combo(:,7)=0;
size_nam_nam_nam_combo=size(nam_nam_nam_combo);

end
              
        
   npl3_npl3_npl3_combo=[];     
if npl3_num>=3
    npl3_npl3_npl3_combo=[1 1 1 1 1 1];
hug1=1;
for i=1:1:npl3_num-2
    for x=1:1:size_grid_npl3_final(1,1)
            for j=i+1:1:npl3_num-1
                for y=1:1:size_grid_npl3_final(1,1)
                    if (abs(norm(npl3_atom(i,1:3)-npl3_atom(j,1:3))-norm(grid_npl3_final(x,1:3)-grid_npl3_final(y,1:3)))<=0.5)&grid_npl3_final(x,4)~=grid_npl3_final(y,4)
                        for k=j+1:1:npl3_num
                            for z=1:1:size_grid_npl3_final(1,1)
                                if (abs(norm(npl3_atom(i,1:3)-npl3_atom(k,1:3))-norm(grid_npl3_final(x,1:3)-grid_npl3_final(z,1:3)))<=0.5)&(abs(norm(npl3_atom(j,1:3)-npl3_atom(k,1:3))-norm(grid_npl3_final(y,1:3)-grid_npl3_final(z,1:3)))<=0.5)&grid_npl3_final(x,4)~=grid_npl3_final(z,4)&grid_npl3_final(y,4)~=grid_npl3_final(z,4)
                                    npl3_npl3_npl3_combo(hug1,1)=x;
                                    npl3_npl3_npl3_combo(hug1,2)=y;
                                    npl3_npl3_npl3_combo(hug1,3)=z;
                                    npl3_npl3_npl3_combo(hug1,4)=i;
                                    npl3_npl3_npl3_combo(hug1,5)=j;
                                    npl3_npl3_npl3_combo(hug1,6)=k;
                                    hug1=hug1+1;
                                end
                            end
                        end
                    end
                end
            end
    end
end



npl3_npl3_npl3_combo(:,7)=0;
size_npl3_npl3_npl3_combo=size(npl3_npl3_npl3_combo);

end

   o3_o3_o3_combo=[];     
if o3_num>=3
    o3_o3_o3_combo=[1 1 1 1 1 1];
hug1=1;
for i=1:1:o3_num-2
    for x=1:1:size_grid_o3_final(1,1)
            for j=i+1:1:o3_num-1
                for y=1:1:size_grid_o3_final(1,1)
                    if (abs(norm(o3_atom(i,1:3)-o3_atom(j,1:3))-norm(grid_o3_final(x,1:3)-grid_o3_final(y,1:3)))<=0.5)&grid_o3_final(x,4)~=grid_o3_final(y,4)
                        for k=j+1:1:o3_num
                            for z=1:1:size_grid_o3_final(1,1)
                                if (abs(norm(o3_atom(i,1:3)-o3_atom(k,1:3))-norm(grid_o3_final(x,1:3)-grid_o3_final(z,1:3)))<=0.5)&(abs(norm(o3_atom(j,1:3)-o3_atom(k,1:3))-norm(grid_o3_final(y,1:3)-grid_o3_final(z,1:3)))<=0.5)&grid_o3_final(x,4)~=grid_o3_final(z,4)&grid_o3_final(y,4)~=grid_o3_final(z,4)
                                    o3_o3_o3_combo(hug1,1)=x;
                                    o3_o3_o3_combo(hug1,2)=y;
                                    o3_o3_o3_combo(hug1,3)=z;
                                    o3_o3_o3_combo(hug1,4)=i;
                                    o3_o3_o3_combo(hug1,5)=j;
                                    o3_o3_o3_combo(hug1,6)=k;
                                    hug1=hug1+1;
                                end
                            end
                        end
                    end
                end
            end
    end
end


o3_o3_o3_combo(:,7)=0;
size_o3_o3_o3_combo=size(o3_o3_o3_combo);

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



o2_nam_npl3_combo=[];     
if (o2_num>=1)&(nam_num>=1)&(npl3_num>=1)
    o2_nam_npl3_combo=[1 1 1 1 1 1];
hug1=1;
for i=1:1:o2_num
    for x=1:1:size_grid_o2_final(1,1)
            for j=1:1:nam_num
                for y=1:1:size_grid_nam_final(1,1)
                    if (abs(norm(o2_atom(i,1:3)-nam_atom(j,1:3))-norm(grid_o2_final(x,1:3)-grid_nam_final(y,1:3)))<=0.5)&grid_o2_final(x,4)~=grid_nam_final(y,4)
                        for k=1:1:npl3_num
                            for z=1:1:size_grid_npl3_final(1,1)
                                if (abs(norm(o2_atom(i,1:3)-npl3_atom(k,1:3))-norm(grid_o2_final(x,1:3)-grid_npl3_final(z,1:3)))<=0.5)&(abs(norm(nam_atom(j,1:3)-npl3_atom(k,1:3))-norm(grid_nam_final(y,1:3)-grid_npl3_final(z,1:3)))<=0.5)&grid_o2_final(x,4)~=grid_npl3_final(z,4)&grid_nam_final(y,4)~=grid_npl3_final(z,4)

                                    o2_nam_npl3_combo(hug1,1)=x;
                                    o2_nam_npl3_combo(hug1,2)=y;
                                    o2_nam_npl3_combo(hug1,3)=z;
                                    o2_nam_npl3_combo(hug1,4)=i;
                                    o2_nam_npl3_combo(hug1,5)=j;
                                    o2_nam_npl3_combo(hug1,6)=k;
                                    hug1=hug1+1;
                                end
                            end
                        end
                    end
                end
            end
    end
end


o2_nam_npl3_combo(:,7)=0;
size_o2_nam_npl3_combo=size(o2_nam_npl3_combo);

end


o2_nam_car_combo=[];     
if (o2_num>=1)&(nam_num>=1)&(car_num>=1)
    o2_nam_car_combo=[1 1 1 1 1 1];
hug1=1;
for i=1:1:o2_num
    for x=1:1:size_grid_o2_final(1,1)
            for j=1:1:nam_num
                for y=1:1:size_grid_nam_final(1,1)
                    if (abs(norm(o2_atom(i,1:3)-nam_atom(j,1:3))-norm(grid_o2_final(x,1:3)-grid_nam_final(y,1:3)))<=0.5)&grid_o2_final(x,4)~=grid_nam_final(y,4)
                        for k=1:1:car_num
                            for z=1:1:size_grid_car_final(1,1)
                                if (abs(norm(o2_atom(i,1:3)-car_atom(k,1:3))-norm(grid_o2_final(x,1:3)-grid_car_final(z,1:3)))<=0.5)&(abs(norm(nam_atom(j,1:3)-car_atom(k,1:3))-norm(grid_nam_final(y,1:3)-grid_car_final(z,1:3)))<=0.5)&grid_o2_final(x,4)~=grid_car_final(z,4)&grid_nam_final(y,4)~=grid_car_final(z,4)

                                    o2_nam_car_combo(hug1,1)=x;
                                    o2_nam_car_combo(hug1,2)=y;
                                    o2_nam_car_combo(hug1,3)=z;
                                    o2_nam_car_combo(hug1,4)=i;
                                    o2_nam_car_combo(hug1,5)=j;
                                    o2_nam_car_combo(hug1,6)=k;
                                    hug1=hug1+1;
                                end
                            end
                        end
                    end
                end
            end
    end
end


o2_nam_car_combo(:,7)=0;
size_o2_nam_car_combo=size(o2_nam_car_combo);

end




o2_car_npl3_combo=[];     
if (o2_num>=1)&(car_num>=1)&(npl3_num>=1)
    o2_car_npl3_combo=[1 1 1 1 1 1];
hug1=1;
for i=1:1:o2_num
    for x=1:1:size_grid_o2_final(1,1)
            for j=1:1:car_num
                for y=1:1:size_grid_car_final(1,1)
                    if (abs(norm(o2_atom(i,1:3)-car_atom(j,1:3))-norm(grid_o2_final(x,1:3)-grid_car_final(y,1:3)))<=0.5)&grid_o2_final(x,4)~=grid_car_final(y,4)
                        for k=1:1:npl3_num
                            for z=1:1:size_grid_npl3_final(1,1)
                                if (abs(norm(o2_atom(i,1:3)-npl3_atom(k,1:3))-norm(grid_o2_final(x,1:3)-grid_npl3_final(z,1:3)))<=0.5)&(abs(norm(car_atom(j,1:3)-npl3_atom(k,1:3))-norm(grid_car_final(y,1:3)-grid_npl3_final(z,1:3)))<=0.5)&grid_o2_final(x,4)~=grid_npl3_final(z,4)&grid_car_final(y,4)~=grid_npl3_final(z,4)

                                    o2_car_npl3_combo(hug1,1)=x;
                                    o2_car_npl3_combo(hug1,2)=y;
                                    o2_car_npl3_combo(hug1,3)=z;
                                    o2_car_npl3_combo(hug1,4)=i;
                                    o2_car_npl3_combo(hug1,5)=j;
                                    o2_car_npl3_combo(hug1,6)=k;
                                    hug1=hug1+1;
                                end
                            end
                        end
                    end
                end
            end
    end
end


o2_car_npl3_combo(:,7)=0;
size_o2_car_npl3_combo=size(o2_car_npl3_combo);

end



o2_nam_n4_combo=[];     
if (o2_num>=1)&(nam_num>=1)&(n4_num>=1)
    o2_nam_n4_combo=[1 1 1 1 1 1];
hug1=1;
for i=1:1:o2_num
    for x=1:1:size_grid_o2_final(1,1)
            for j=1:1:nam_num
                for y=1:1:size_grid_nam_final(1,1)
                    if (abs(norm(o2_atom(i,1:3)-nam_atom(j,1:3))-norm(grid_o2_final(x,1:3)-grid_nam_final(y,1:3)))<=0.5)&grid_o2_final(x,4)~=grid_nam_final(y,4)
                        for k=1:1:n4_num
                            for z=1:1:size_grid_n4_final(1,1)
                                if (abs(norm(o2_atom(i,1:3)-n4_atom(k,1:3))-norm(grid_o2_final(x,1:3)-grid_n4_final(z,1:3)))<=0.5)&(abs(norm(nam_atom(j,1:3)-n4_atom(k,1:3))-norm(grid_nam_final(y,1:3)-grid_n4_final(z,1:3)))<=0.5)&grid_o2_final(x,4)~=grid_n4_final(z,4)&grid_nam_final(y,4)~=grid_n4_final(z,4)

                                    o2_nam_n4_combo(hug1,1)=x;
                                    o2_nam_n4_combo(hug1,2)=y;
                                    o2_nam_n4_combo(hug1,3)=z;
                                    o2_nam_n4_combo(hug1,4)=i;
                                    o2_nam_n4_combo(hug1,5)=j;
                                    o2_nam_n4_combo(hug1,6)=k;
                                    hug1=hug1+1;
                                end
                            end
                        end
                    end
                end
            end
    end
end



o2_nam_n4_combo(:,7)=0;
size_o2_nam_n4_combo=size(o2_nam_n4_combo);

end


o2_car_n4_combo=[];     
if (o2_num>=1)&(car_num>=1)&(n4_num>=1)
    o2_car_n4_combo=[1 1 1 1 1 1];
hug1=1;
for i=1:1:o2_num
    for x=1:1:size_grid_o2_final(1,1)
            for j=1:1:car_num
                for y=1:1:size_grid_car_final(1,1)
                    if (abs(norm(o2_atom(i,1:3)-car_atom(j,1:3))-norm(grid_o2_final(x,1:3)-grid_car_final(y,1:3)))<=0.5)&grid_o2_final(x,4)~=grid_car_final(y,4)
                        for k=1:1:n4_num
                            for z=1:1:size_grid_n4_final(1,1)
                                if (abs(norm(o2_atom(i,1:3)-n4_atom(k,1:3))-norm(grid_o2_final(x,1:3)-grid_n4_final(z,1:3)))<=0.5)&(abs(norm(car_atom(j,1:3)-n4_atom(k,1:3))-norm(grid_car_final(y,1:3)-grid_n4_final(z,1:3)))<=0.5)&grid_o2_final(x,4)~=grid_n4_final(z,4)&grid_car_final(y,4)~=grid_n4_final(z,4)

                                    o2_car_n4_combo(hug1,1)=x;
                                    o2_car_n4_combo(hug1,2)=y;
                                    o2_car_n4_combo(hug1,3)=z;
                                    o2_car_n4_combo(hug1,4)=i;
                                    o2_car_n4_combo(hug1,5)=j;
                                    o2_car_n4_combo(hug1,6)=k;
                                    hug1=hug1+1;
                                end
                            end
                        end
                    end
                end
            end
    end
end



o2_car_n4_combo(:,7)=0;
size_o2_car_n4_combo=size(o2_car_n4_combo);


end



o3_car_n4_combo=[];     
if (o3_num>=1)&(car_num>=1)&(n4_num>=1)
    o3_car_n4_combo=[1 1 1 1 1 1];
hug1=1;
for i=1:1:o3_num
    for x=1:1:size_grid_o3_final(1,1)
            for j=1:1:car_num
                for y=1:1:size_grid_car_final(1,1)
                    if (abs(norm(o3_atom(i,1:3)-car_atom(j,1:3))-norm(grid_o3_final(x,1:3)-grid_car_final(y,1:3)))<=0.5)&grid_o3_final(x,4)~=grid_car_final(y,4)
                        for k=1:1:n4_num
                            for z=1:1:size_grid_n4_final(1,1)
                                if (abs(norm(o3_atom(i,1:3)-n4_atom(k,1:3))-norm(grid_o3_final(x,1:3)-grid_n4_final(z,1:3)))<=0.5)&(abs(norm(car_atom(j,1:3)-n4_atom(k,1:3))-norm(grid_car_final(y,1:3)-grid_n4_final(z,1:3)))<=0.5)&grid_o3_final(x,4)~=grid_n4_final(z,4)&grid_car_final(y,4)~=grid_n4_final(z,4)

                                    o3_car_n4_combo(hug1,1)=x;
                                    o3_car_n4_combo(hug1,2)=y;
                                    o3_car_n4_combo(hug1,3)=z;
                                    o3_car_n4_combo(hug1,4)=i;
                                    o3_car_n4_combo(hug1,5)=j;
                                    o3_car_n4_combo(hug1,6)=k;
                                    hug1=hug1+1;
                                end
                            end
                        end
                    end
                end
            end
    end
end



o3_car_n4_combo(:,7)=0;
size_o3_car_n4_combo=size(o3_car_n4_combo);

end



o3_nam_n4_combo=[];     
if (o3_num>=1)&(nam_num>=1)&(n4_num>=1)
    o3_nam_n4_combo=[1 1 1 1 1 1];
hug1=1;
for i=1:1:o3_num
    for x=1:1:size_grid_o3_final(1,1)
            for j=1:1:nam_num
                for y=1:1:size_grid_nam_final(1,1)
                    if (abs(norm(o3_atom(i,1:3)-nam_atom(j,1:3))-norm(grid_o3_final(x,1:3)-grid_nam_final(y,1:3)))<=0.5)&grid_o3_final(x,4)~=grid_nam_final(y,4)
                        for k=1:1:n4_num
                            for z=1:1:size_grid_n4_final(1,1)
                                if (abs(norm(o3_atom(i,1:3)-n4_atom(k,1:3))-norm(grid_o3_final(x,1:3)-grid_n4_final(z,1:3)))<=0.5)&(abs(norm(nam_atom(j,1:3)-n4_atom(k,1:3))-norm(grid_nam_final(y,1:3)-grid_n4_final(z,1:3)))<=0.5)&grid_o3_final(x,4)~=grid_n4_final(z,4)&grid_nam_final(y,4)~=grid_n4_final(z,4)

                                    o3_nam_n4_combo(hug1,1)=x;
                                    o3_nam_n4_combo(hug1,2)=y;
                                    o3_nam_n4_combo(hug1,3)=z;
                                    o3_nam_n4_combo(hug1,4)=i;
                                    o3_nam_n4_combo(hug1,5)=j;
                                    o3_nam_n4_combo(hug1,6)=k;
                                    hug1=hug1+1;
                                end
                            end
                        end
                    end
                end
            end
    end
end



o3_nam_n4_combo(:,7)=0;
size_o3_nam_n4_combo=size(o3_nam_n4_combo);


end


o3_nam_car_combo=[];     
if (o3_num>=1)&(nam_num>=1)&(car_num>=1)
    o3_nam_car_combo=[1 1 1 1 1 1];
hug1=1;
for i=1:1:o3_num
    for x=1:1:size_grid_o3_final(1,1)
            for j=1:1:nam_num
                for y=1:1:size_grid_nam_final(1,1)
                    if (abs(norm(o3_atom(i,1:3)-nam_atom(j,1:3))-norm(grid_o3_final(x,1:3)-grid_nam_final(y,1:3)))<=0.5)&grid_o3_final(x,4)~=grid_nam_final(y,4)
                        for k=1:1:car_num
                            for z=1:1:size_grid_car_final(1,1)
                                if (abs(norm(o3_atom(i,1:3)-car_atom(k,1:3))-norm(grid_o3_final(x,1:3)-grid_car_final(z,1:3)))<=0.5)&(abs(norm(nam_atom(j,1:3)-car_atom(k,1:3))-norm(grid_nam_final(y,1:3)-grid_car_final(z,1:3)))<=0.5)&grid_o3_final(x,4)~=grid_car_final(z,4)&grid_nam_final(y,4)~=grid_car_final(z,4)

                                    o3_nam_car_combo(hug1,1)=x;
                                    o3_nam_car_combo(hug1,2)=y;
                                    o3_nam_car_combo(hug1,3)=z;
                                    o3_nam_car_combo(hug1,4)=i;
                                    o3_nam_car_combo(hug1,5)=j;
                                    o3_nam_car_combo(hug1,6)=k;
                                    hug1=hug1+1;
                                end
                            end
                        end
                    end
                end
            end
    end
end



o3_nam_car_combo(:,7)=0;
size_o3_nam_car_combo=size(o3_nam_car_combo);

end



o3_npl3_n4_combo=[];     
if (o3_num>=1)&(npl3_num>=1)&(n4_num>=1)
    o3_npl3_n4_combo=[1 1 1 1 1 1];
hug1=1;
for i=1:1:o3_num
    for x=1:1:size_grid_o3_final(1,1)
            for j=1:1:npl3_num
                for y=1:1:size_grid_npl3_final(1,1)
                    if (abs(norm(o3_atom(i,1:3)-npl3_atom(j,1:3))-norm(grid_o3_final(x,1:3)-grid_npl3_final(y,1:3)))<=0.5)&grid_o3_final(x,4)~=grid_npl3_final(y,4)
                        for k=1:1:n4_num
                            for z=1:1:size_grid_n4_final(1,1)
                                if (abs(norm(o3_atom(i,1:3)-n4_atom(k,1:3))-norm(grid_o3_final(x,1:3)-grid_n4_final(z,1:3)))<=0.5)&(abs(norm(npl3_atom(j,1:3)-n4_atom(k,1:3))-norm(grid_npl3_final(y,1:3)-grid_n4_final(z,1:3)))<=0.5)&grid_o3_final(x,4)~=grid_n4_final(z,4)&grid_npl3_final(y,4)~=grid_n4_final(z,4)

                                    o3_npl3_n4_combo(hug1,1)=x;
                                    o3_npl3_n4_combo(hug1,2)=y;
                                    o3_npl3_n4_combo(hug1,3)=z;
                                    o3_npl3_n4_combo(hug1,4)=i;
                                    o3_npl3_n4_combo(hug1,5)=j;
                                    o3_npl3_n4_combo(hug1,6)=k;
                                    hug1=hug1+1;
                                end
                            end
                        end
                    end
                end
            end
    end
end



o3_npl3_n4_combo(:,7)=0;
size_o3_npl3_n4_combo=size(o3_npl3_n4_combo);


end





o3_npl3_car_combo=[];     
if (o3_num>=1)&(npl3_num>=1)&(car_num>=1)
    o3_npl3_car_combo=[1 1 1 1 1 1];
hug1=1;
for i=1:1:o3_num
    for x=1:1:size_grid_o3_final(1,1)
            for j=1:1:npl3_num
                for y=1:1:size_grid_npl3_final(1,1)
                    if (abs(norm(o3_atom(i,1:3)-npl3_atom(j,1:3))-norm(grid_o3_final(x,1:3)-grid_npl3_final(y,1:3)))<=0.5)&grid_o3_final(x,4)~=grid_npl3_final(y,4)
                        for k=1:1:car_num
                            for z=1:1:size_grid_car_final(1,1)
                                if (abs(norm(o3_atom(i,1:3)-car_atom(k,1:3))-norm(grid_o3_final(x,1:3)-grid_car_final(z,1:3)))<=0.5)&(abs(norm(npl3_atom(j,1:3)-car_atom(k,1:3))-norm(grid_npl3_final(y,1:3)-grid_car_final(z,1:3)))<=0.5)&grid_o3_final(x,4)~=grid_car_final(z,4)&grid_npl3_final(y,4)~=grid_car_final(z,4)

                                    o3_npl3_car_combo(hug1,1)=x;
                                    o3_npl3_car_combo(hug1,2)=y;
                                    o3_npl3_car_combo(hug1,3)=z;
                                    o3_npl3_car_combo(hug1,4)=i;
                                    o3_npl3_car_combo(hug1,5)=j;
                                    o3_npl3_car_combo(hug1,6)=k;
                                    hug1=hug1+1;
                                end
                            end
                        end
                    end
                end
            end
    end
end



o3_npl3_car_combo(:,7)=0;
size_o3_npl3_car_combo=size(o3_npl3_car_combo);

end






nam_npl3_n4_combo=[];     
if (nam_num>=1)&(npl3_num>=1)&(n4_num>=1)
    nam_npl3_n4_combo=[1 1 1 1 1 1];
hug1=1;
for i=1:1:nam_num
    for x=1:1:size_grid_nam_final(1,1)
            for j=1:1:npl3_num
                for y=1:1:size_grid_npl3_final(1,1)
                    if (abs(norm(nam_atom(i,1:3)-npl3_atom(j,1:3))-norm(grid_nam_final(x,1:3)-grid_npl3_final(y,1:3)))<=0.5)&grid_nam_final(x,4)~=grid_npl3_final(y,4)
                        for k=1:1:n4_num
                            for z=1:1:size_grid_n4_final(1,1)
                                if (abs(norm(nam_atom(i,1:3)-n4_atom(k,1:3))-norm(grid_nam_final(x,1:3)-grid_n4_final(z,1:3)))<=0.5)&(abs(norm(npl3_atom(j,1:3)-n4_atom(k,1:3))-norm(grid_npl3_final(y,1:3)-grid_n4_final(z,1:3)))<=0.5)&grid_nam_final(x,4)~=grid_n4_final(z,4)&grid_npl3_final(y,4)~=grid_n4_final(z,4)

                                    nam_npl3_n4_combo(hug1,1)=x;
                                    nam_npl3_n4_combo(hug1,2)=y;
                                    nam_npl3_n4_combo(hug1,3)=z;
                                    nam_npl3_n4_combo(hug1,4)=i;
                                    nam_npl3_n4_combo(hug1,5)=j;
                                    nam_npl3_n4_combo(hug1,6)=k;
                                    hug1=hug1+1;
                                end
                            end
                        end
                    end
                end
            end
    end
end



nam_npl3_n4_combo(:,7)=0;
size_nam_npl3_n4_combo=size(nam_npl3_n4_combo);

end




nam_npl3_car_combo=[];     
if (nam_num>=1)&(npl3_num>=1)&(car_num>=1)
    nam_npl3_car_combo=[1 1 1 1 1 1];
hug1=1;
for i=1:1:nam_num
    for x=1:1:size_grid_nam_final(1,1)
            for j=1:1:npl3_num
                for y=1:1:size_grid_npl3_final(1,1)
                    if (abs(norm(nam_atom(i,1:3)-npl3_atom(j,1:3))-norm(grid_nam_final(x,1:3)-grid_npl3_final(y,1:3)))<=0.5)&grid_nam_final(x,4)~=grid_npl3_final(y,4)
                        for k=1:1:car_num
                            for z=1:1:size_grid_car_final(1,1)
                                if (abs(norm(nam_atom(i,1:3)-car_atom(k,1:3))-norm(grid_nam_final(x,1:3)-grid_car_final(z,1:3)))<=0.5)&(abs(norm(npl3_atom(j,1:3)-car_atom(k,1:3))-norm(grid_npl3_final(y,1:3)-grid_car_final(z,1:3)))<=0.5)&grid_nam_final(x,4)~=grid_car_final(z,4)&grid_npl3_final(y,4)~=grid_car_final(z,4)

                                    nam_npl3_car_combo(hug1,1)=x;
                                    nam_npl3_car_combo(hug1,2)=y;
                                    nam_npl3_car_combo(hug1,3)=z;
                                    nam_npl3_car_combo(hug1,4)=i;
                                    nam_npl3_car_combo(hug1,5)=j;
                                    nam_npl3_car_combo(hug1,6)=k;
                                    hug1=hug1+1;
                                end
                            end
                        end
                    end
                end
            end
    end
end



nam_npl3_car_combo(:,7)=0;
size_nam_npl3_car_combo=size(nam_npl3_car_combo);

end




nam_car_n4_combo=[];     
if (nam_num>=1)&(car_num>=1)&(n4_num>=1)
    nam_car_n4_combo=[1 1 1 1 1 1];
hug1=1;
for i=1:1:nam_num
    for x=1:1:size_grid_nam_final(1,1)
            for j=1:1:car_num
                for y=1:1:size_grid_car_final(1,1)
                    if (abs(norm(nam_atom(i,1:3)-car_atom(j,1:3))-norm(grid_nam_final(x,1:3)-grid_car_final(y,1:3)))<=0.5)&grid_nam_final(x,4)~=grid_car_final(y,4)
                        for k=1:1:n4_num
                            for z=1:1:size_grid_n4_final(1,1)
                                if (abs(norm(nam_atom(i,1:3)-n4_atom(k,1:3))-norm(grid_nam_final(x,1:3)-grid_n4_final(z,1:3)))<=0.5)&(abs(norm(car_atom(j,1:3)-n4_atom(k,1:3))-norm(grid_car_final(y,1:3)-grid_n4_final(z,1:3)))<=0.5)&grid_nam_final(x,4)~=grid_n4_final(z,4)&grid_car_final(y,4)~=grid_n4_final(z,4)

                                    nam_car_n4_combo(hug1,1)=x;
                                    nam_car_n4_combo(hug1,2)=y;
                                    nam_car_n4_combo(hug1,3)=z;
                                    nam_car_n4_combo(hug1,4)=i;
                                    nam_car_n4_combo(hug1,5)=j;
                                    nam_car_n4_combo(hug1,6)=k;
                                    hug1=hug1+1;
                                end
                            end
                        end
                    end
                end
            end
    end
end



nam_car_n4_combo(:,7)=0;
size_nam_car_n4_combo=size(nam_car_n4_combo);

end





car_npl3_n4_combo=[];     
if (car_num>=1)&(npl3_num>=1)&(n4_num>=1)
    car_npl3_n4_combo=[1 1 1 1 1 1];
hug1=1;
for i=1:1:car_num
    for x=1:1:size_grid_car_final(1,1)
            for j=1:1:npl3_num
                for y=1:1:size_grid_npl3_final(1,1)
                    if (abs(norm(car_atom(i,1:3)-npl3_atom(j,1:3))-norm(grid_car_final(x,1:3)-grid_npl3_final(y,1:3)))<=0.5)&grid_car_final(x,4)~=grid_npl3_final(y,4)
                        for k=1:1:n4_num
                            for z=1:1:size_grid_n4_final(1,1)
                                if (abs(norm(car_atom(i,1:3)-n4_atom(k,1:3))-norm(grid_car_final(x,1:3)-grid_n4_final(z,1:3)))<=0.5)&(abs(norm(npl3_atom(j,1:3)-n4_atom(k,1:3))-norm(grid_npl3_final(y,1:3)-grid_n4_final(z,1:3)))<=0.5)&grid_car_final(x,4)~=grid_n4_final(z,4)&grid_npl3_final(y,4)~=grid_n4_final(z,4)

                                    car_npl3_n4_combo(hug1,1)=x;
                                    car_npl3_n4_combo(hug1,2)=y;
                                    car_npl3_n4_combo(hug1,3)=z;
                                    car_npl3_n4_combo(hug1,4)=i;
                                    car_npl3_n4_combo(hug1,5)=j;
                                    car_npl3_n4_combo(hug1,6)=k;
                                    hug1=hug1+1;
                                end
                            end
                        end
                    end
                end
            end
    end
end



car_npl3_n4_combo(:,7)=0;
size_car_npl3_n4_combo=size(car_npl3_n4_combo);

end






o2_nam_o3_combo=[];     
if (o2_num>=1)&(nam_num>=1)&(o3_num>=1)
    o2_nam_o3_combo=[1 1 1 1 1 1];
hug1=1;
for i=1:1:o2_num
    for x=1:1:size_grid_o2_final(1,1)
            for j=1:1:nam_num
                for y=1:1:size_grid_nam_final(1,1)
                    if (abs(norm(o2_atom(i,1:3)-nam_atom(j,1:3))-norm(grid_o2_final(x,1:3)-grid_nam_final(y,1:3)))<=0.5)&grid_o2_final(x,4)~=grid_nam_final(y,4)
                        for k=1:1:o3_num
                            for z=1:1:size_grid_o3_final(1,1)
                                if (abs(norm(o2_atom(i,1:3)-o3_atom(k,1:3))-norm(grid_o2_final(x,1:3)-grid_o3_final(z,1:3)))<=0.5)&(abs(norm(nam_atom(j,1:3)-o3_atom(k,1:3))-norm(grid_nam_final(y,1:3)-grid_o3_final(z,1:3)))<=0.5)&grid_o2_final(x,4)~=grid_o3_final(z,4)&grid_nam_final(y,4)~=grid_o3_final(z,4)

                                    o2_nam_o3_combo(hug1,1)=x;
                                    o2_nam_o3_combo(hug1,2)=y;
                                    o2_nam_o3_combo(hug1,3)=z;
                                    o2_nam_o3_combo(hug1,4)=i;
                                    o2_nam_o3_combo(hug1,5)=j;
                                    o2_nam_o3_combo(hug1,6)=k;
                                    hug1=hug1+1;
                                end
                            end
                        end
                    end
                end
            end
    end
end
      

o2_nam_o3_combo(:,7)=0;
size_o2_nam_o3_combo=size(o2_nam_o3_combo);

end   




o2_car_o3_combo=[];     
if (o2_num>=1)&(car_num>=1)&(o3_num>=1)
    o2_car_o3_combo=[1 1 1 1 1 1];
hug1=1;
for i=1:1:o2_num
    for x=1:1:size_grid_o2_final(1,1)
            for j=1:1:car_num
                for y=1:1:size_grid_car_final(1,1)
                    if (abs(norm(o2_atom(i,1:3)-car_atom(j,1:3))-norm(grid_o2_final(x,1:3)-grid_car_final(y,1:3)))<=0.5)&grid_o2_final(x,4)~=grid_car_final(y,4)
                        for k=1:1:o3_num
                            for z=1:1:size_grid_o3_final(1,1)
                                if (abs(norm(o2_atom(i,1:3)-o3_atom(k,1:3))-norm(grid_o2_final(x,1:3)-grid_o3_final(z,1:3)))<=0.5)&(abs(norm(car_atom(j,1:3)-o3_atom(k,1:3))-norm(grid_car_final(y,1:3)-grid_o3_final(z,1:3)))<=0.5)&grid_o2_final(x,4)~=grid_o3_final(z,4)&grid_car_final(y,4)~=grid_o3_final(z,4)

                                    o2_car_o3_combo(hug1,1)=x;
                                    o2_car_o3_combo(hug1,2)=y;
                                    o2_car_o3_combo(hug1,3)=z;
                                    o2_car_o3_combo(hug1,4)=i;
                                    o2_car_o3_combo(hug1,5)=j;
                                    o2_car_o3_combo(hug1,6)=k;
                                    hug1=hug1+1;
                                end
                            end
                        end
                    end
                end
            end
    end
end
      

o2_car_o3_combo(:,7)=0;
size_o2_car_o3_combo=size(o2_car_o3_combo);

end   





o2_npl3_o3_combo=[];     
if (o2_num>=1)&(npl3_num>=1)&(o3_num>=1)
    o2_npl3_o3_combo=[1 1 1 1 1 1];
hug1=1;
for i=1:1:o2_num
    for x=1:1:size_grid_o2_final(1,1)
            for j=1:1:npl3_num
                for y=1:1:size_grid_npl3_final(1,1)
                    if (abs(norm(o2_atom(i,1:3)-npl3_atom(j,1:3))-norm(grid_o2_final(x,1:3)-grid_npl3_final(y,1:3)))<=0.5)&grid_o2_final(x,4)~=grid_npl3_final(y,4)
                        for k=1:1:o3_num
                            for z=1:1:size_grid_o3_final(1,1)
                                if (abs(norm(o2_atom(i,1:3)-o3_atom(k,1:3))-norm(grid_o2_final(x,1:3)-grid_o3_final(z,1:3)))<=0.5)&(abs(norm(npl3_atom(j,1:3)-o3_atom(k,1:3))-norm(grid_npl3_final(y,1:3)-grid_o3_final(z,1:3)))<=0.5)&grid_o2_final(x,4)~=grid_o3_final(z,4)&grid_npl3_final(y,4)~=grid_o3_final(z,4)

                                    o2_npl3_o3_combo(hug1,1)=x;
                                    o2_npl3_o3_combo(hug1,2)=y;
                                    o2_npl3_o3_combo(hug1,3)=z;
                                    o2_npl3_o3_combo(hug1,4)=i;
                                    o2_npl3_o3_combo(hug1,5)=j;
                                    o2_npl3_o3_combo(hug1,6)=k;
                                    hug1=hug1+1;
                                end
                            end
                        end
                    end
                end
            end
    end
end

   

o2_npl3_o3_combo(:,7)=0;
size_o2_npl3_o3_combo=size(o2_npl3_o3_combo);


end       

o2_npl3_n4_combo=[];     
if (o2_num>=1)&(npl3_num>=1)&(n4_num>=1)
    o2_npl3_n4_combo=[1 1 1 1 1 1];
hug1=1;
for i=1:1:o2_num
    for x=1:1:size_grid_o2_final(1,1)
            for j=1:1:npl3_num
                for y=1:1:size_grid_npl3_final(1,1)
                    if (abs(norm(o2_atom(i,1:3)-npl3_atom(j,1:3))-norm(grid_o2_final(x,1:3)-grid_npl3_final(y,1:3)))<=0.5)&grid_o2_final(x,4)~=grid_npl3_final(y,4)
                        for k=1:1:n4_num
                            for z=1:1:size_grid_n4_final(1,1)
                                if (abs(norm(o2_atom(i,1:3)-n4_atom(k,1:3))-norm(grid_o2_final(x,1:3)-grid_n4_final(z,1:3)))<=0.5)&(abs(norm(npl3_atom(j,1:3)-n4_atom(k,1:3))-norm(grid_npl3_final(y,1:3)-grid_n4_final(z,1:3)))<=0.5)&grid_o2_final(x,4)~=grid_n4_final(z,4)&grid_npl3_final(y,4)~=grid_n4_final(z,4)

                                    o2_npl3_n4_combo(hug1,1)=x;
                                    o2_npl3_n4_combo(hug1,2)=y;
                                    o2_npl3_n4_combo(hug1,3)=z;
                                    o2_npl3_n4_combo(hug1,4)=i;
                                    o2_npl3_n4_combo(hug1,5)=j;
                                    o2_npl3_n4_combo(hug1,6)=k;
                                    hug1=hug1+1;
                                end
                            end
                        end
                    end
                end
            end
    end
end

   

o2_npl3_n4_combo(:,7)=0;
size_o2_npl3_n4_combo=size(o2_npl3_n4_combo);

end       

   


o2_o3_n4_combo=[];     
if (o2_num>=1)&(o3_num>=1)&(n4_num>=1)
    o2_o3_n4_combo=[1 1 1 1 1 1];
hug1=1;
for i=1:1:o2_num
    for x=1:1:size_grid_o2_final(1,1)
            for j=1:1:o3_num
                for y=1:1:size_grid_o3_final(1,1)
                    if (abs(norm(o2_atom(i,1:3)-o3_atom(j,1:3))-norm(grid_o2_final(x,1:3)-grid_o3_final(y,1:3)))<=0.5)&grid_o2_final(x,4)~=grid_o3_final(y,4)
                        for k=1:1:n4_num
                            for z=1:1:size_grid_n4_final(1,1)
                                if (abs(norm(o2_atom(i,1:3)-n4_atom(k,1:3))-norm(grid_o2_final(x,1:3)-grid_n4_final(z,1:3)))<=0.5)&(abs(norm(o3_atom(j,1:3)-n4_atom(k,1:3))-norm(grid_o3_final(y,1:3)-grid_n4_final(z,1:3)))<=0.5)&grid_o2_final(x,4)~=grid_n4_final(z,4)&grid_o3_final(y,4)~=grid_n4_final(z,4)

                                    o2_o3_n4_combo(hug1,1)=x;
                                    o2_o3_n4_combo(hug1,2)=y;
                                    o2_o3_n4_combo(hug1,3)=z;
                                    o2_o3_n4_combo(hug1,4)=i;
                                    o2_o3_n4_combo(hug1,5)=j;
                                    o2_o3_n4_combo(hug1,6)=k;
                                    hug1=hug1+1;
                                end
                            end
                        end
                    end
                end
            end
    end
end


o2_o3_n4_combo(:,7)=0;
size_o2_o3_n4_combo=size(o2_o3_n4_combo);

end       


o3_nam_npl3_combo=[];     
if (o3_num>=1)&(nam_num>=1)&(npl3_num>=1)
    o3_nam_npl3_combo=[1 1 1 1 1 1];
hug1=1;
for i=1:1:o3_num
    for x=1:1:size_grid_o3_final(1,1)
            for j=1:1:nam_num
                for y=1:1:size_grid_nam_final(1,1)
                    if (abs(norm(o3_atom(i,1:3)-nam_atom(j,1:3))-norm(grid_o3_final(x,1:3)-grid_nam_final(y,1:3)))<=0.5)&grid_o3_final(x,4)~=grid_nam_final(y,4)
                        for k=1:1:npl3_num
                            for z=1:1:size_grid_npl3_final(1,1)
                                if (abs(norm(o3_atom(i,1:3)-npl3_atom(k,1:3))-norm(grid_o3_final(x,1:3)-grid_npl3_final(z,1:3)))<=0.5)&(abs(norm(nam_atom(j,1:3)-npl3_atom(k,1:3))-norm(grid_nam_final(y,1:3)-grid_npl3_final(z,1:3)))<=0.5)&grid_o3_final(x,4)~=grid_npl3_final(z,4)&grid_nam_final(y,4)~=grid_npl3_final(z,4)

                                    o3_nam_npl3_combo(hug1,1)=x;
                                    o3_nam_npl3_combo(hug1,2)=y;
                                    o3_nam_npl3_combo(hug1,3)=z;
                                    o3_nam_npl3_combo(hug1,4)=i;
                                    o3_nam_npl3_combo(hug1,5)=j;
                                    o3_nam_npl3_combo(hug1,6)=k;
                                    hug1=hug1+1;
                                end
                            end
                        end
                    end
                end
            end
    end
end



o3_nam_npl3_combo(:,7)=0;
size_o3_nam_npl3_combo=size(o3_nam_npl3_combo);

end    



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%cd('E:\MATLAB701\work\KECSA\Dock Posing\whole set');
asdf=1;
%clear best_struc;

if length(o2_o2_o2_combo)~=0
    size_o2_o2_o2_combo=size(o2_o2_o2_combo);
    
    ffff=1
    for daz=1:1:size_o2_o2_o2_combo(1,1)
        modia=o2_o2_o2_combo(daz,1);
        modi1=o2_atom(o2_o2_o2_combo(daz,4),7);
        modib=o2_o2_o2_combo(daz,2);
        modi2=o2_atom(o2_o2_o2_combo(daz,5),7);
        modic=o2_o2_o2_combo(daz,3);
        modi3=o2_atom(o2_o2_o2_combo(daz,6),7);
        
        
        stan_frag_chos=grid_o2_final;
        x_move=stan_frag_chos(modia,1)-frag_chos(modi1,1);
        y_move=stan_frag_chos(modia,2)-frag_chos(modi1,2);
        z_move=stan_frag_chos(modia,3)-frag_chos(modi1,3);
        clear frag_chos_stage1;
        for i=1:1:size_frag_chos(1,1)
            frag_chos_stage1(i,1)=frag_chos(i,1)+x_move;
            frag_chos_stage1(i,2)=frag_chos(i,2)+y_move;
            frag_chos_stage1(i,3)=frag_chos(i,3)+z_move;
            frag_chos_stage1(i,4:13)=frag_chos(i,4:13);
        end
        
        size_frag_chos_stage1=size(frag_chos_stage1);
        
        %frag_chos_pointer(1,:)=frag_chos_stage1(3,:);
        frag_chos_pointer(1,:)=frag_chos_stage1(modi2,:);
        frag_chos_pointer(2,:)=frag_chos_stage1(modi3,:);
        N_pointer=frag_chos_pointer(1,:);
        C_pointer=frag_chos_pointer(2,:);
        
        
        
        
        %stan_frag_chos_pointer(1,:)=stan_frag_chos(modi,:);
        stan_frag_chos_pointer(1,:)=grid_o2_final(modib,:);
        stan_frag_chos_pointer(2,:)=grid_o2_final(modic,:);
        stan_N_pointer=stan_frag_chos_pointer(1,:);
        stan_C_pointer=stan_frag_chos_pointer(2,:);
        
        size_frag_chos_pointer=size(frag_chos_pointer);
        
        size_stan_frag_chos_pointer=size(stan_frag_chos_pointer);
        
        
        
        frag_chos_stage3=rota51(N_pointer,C_pointer,stan_frag_chos,stan_N_pointer,stan_C_pointer,frag_chos_stage1,modia,modib,modic,modi1,modi2,modi3,protein_frame);
        size_frag_chos_stage3=size(frag_chos_stage3);
        if size_frag_chos_stage3(1,1)<size_frag_chos(1,1)
            continue
        elseif size_frag_chos_stage3(1,2)==size_frag_chos(1,2)
            test_stage3=frag_chos_stage3;
            distj=judge_dist(test_stage3,frag_chos_stage1);
            if distj==1
                eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3;')  );
                asdf=asdf+1;
            end
        elseif size_frag_chos_stage3(1,2)==2*size_frag_chos(1,2)
            test_stage3_1=frag_chos_stage3(:,1:13);
            test_stage3_2=frag_chos_stage3(:,14:26);
            distj1=judge_dist(test_stage3_1,frag_chos_stage1);
            distj2=judge_dist(test_stage3_2,frag_chos_stage1);
            if distj1==1&distj2~=1
                eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
                asdf=asdf+1;
            elseif distj1~=1&distj2==1
                eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
                asdf=asdf+1;
            elseif  distj1==1&distj2==1
                eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
                asdf=asdf+1;
                eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
                asdf=asdf+1;
            end
            
        end
        %eval( strcat(  'o2_o2_o2_struc_',num2str(daz),'=[];')  );
        
        
        
        
        %out(asdf,1)=kecsamt1(protein_dir,frag_chos_dir,frag_chos_stage3(:,1:3));
        
        %eval( strcat(  'best_struc',num2str(asdf),'=o2_o2_o2_struc_',num2str(daz),';')  );
        %clear input;
        %eval( strcat(  'input_struc=best_struc',num2str(asdf),';')  );
        %pp=lig_output(input_struc,frag_chos_dir,asdf);
        
        
        clear functions;
        pack
    end
    
    
    
    
end

                


if length(o2_o2_nam_combo)~=0
    size_o2_o2_nam_combo=size(o2_o2_nam_combo);
    for daz=1:1:size_o2_o2_nam_combo(1,1)
        
        modia=o2_o2_nam_combo(daz,1);
        modi1=o2_atom(o2_o2_nam_combo(daz,4),7);
        modib=o2_o2_nam_combo(daz,2);
        modi2=o2_atom(o2_o2_nam_combo(daz,5),7);
        modic=o2_o2_nam_combo(daz,3);
        modi3=nam_atom(o2_o2_nam_combo(daz,6),7);
        
        
        stan_frag_chos=grid_o2_final;
%stan_frag_chos(modib,:)=grid_o2_final(modib,:);
%stan_frag_chos(modic+20,:)=grid_nam_final(modic,:);


x_move=stan_frag_chos(modia,1)-frag_chos(modi1,1);
y_move=stan_frag_chos(modia,2)-frag_chos(modi1,2);
z_move=stan_frag_chos(modia,3)-frag_chos(modi1,3);
clear frag_chos_stage1;
for i=1:1:size_frag_chos(1,1)
    frag_chos_stage1(i,1)=frag_chos(i,1)+x_move;
    frag_chos_stage1(i,2)=frag_chos(i,2)+y_move;
    frag_chos_stage1(i,3)=frag_chos(i,3)+z_move;
    frag_chos_stage1(i,4:13)=frag_chos(i,4:13);
end

size_frag_chos_stage1=size(frag_chos_stage1);

%frag_chos_pointer(1,:)=frag_chos_stage1(3,:);
frag_chos_pointer(1,:)=frag_chos_stage1(modi2,:);
frag_chos_pointer(2,:)=frag_chos_stage1(modi3,:);
N_pointer=frag_chos_pointer(1,:);
C_pointer=frag_chos_pointer(2,:);

    
%stan_frag_chos_pointer(1,:)=stan_frag_chos(modi,:);
stan_frag_chos_pointer(1,:)=grid_o2_final(modib,:);
stan_frag_chos_pointer(2,:)=grid_nam_final(modic,:);
stan_N_pointer=stan_frag_chos_pointer(1,:);
stan_C_pointer=stan_frag_chos_pointer(2,:);

size_frag_chos_pointer=size(frag_chos_pointer);

size_stan_frag_chos_pointer=size(stan_frag_chos_pointer);

frag_chos_stage3=rota51(N_pointer,C_pointer,stan_frag_chos,stan_N_pointer,stan_C_pointer,frag_chos_stage1,modia,modib,modic,modi1,modi2,modi3,protein_frame);
size_frag_chos_stage3=size(frag_chos_stage3);
if size_frag_chos_stage3(1,1)<size_frag_chos(1,1)
    continue
elseif size_frag_chos_stage3(1,2)==size_frag_chos(1,2)
    test_stage3=frag_chos_stage3;
    distj=judge_dist(test_stage3,frag_chos_stage1);
    if distj==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3;')  );
        asdf=asdf+1;
    end
elseif size_frag_chos_stage3(1,2)==2*size_frag_chos(1,2)
    test_stage3_1=frag_chos_stage3(:,1:13);
    test_stage3_2=frag_chos_stage3(:,14:26);
    distj1=judge_dist(test_stage3_1,frag_chos_stage1);
    distj2=judge_dist(test_stage3_2,frag_chos_stage1);
    if distj1==1&distj2~=1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
    elseif distj1~=1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    elseif  distj1==1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    end
    
end
    



        clear functions;
        pack
    end
    
end
      
            
            
            
            

if length(o2_nam_nam_combo)~=0
    size_o2_nam_nam_combo=size(o2_nam_nam_combo);
    for daz=1:1:size_o2_nam_nam_combo(1,1)
        modia=o2_nam_nam_combo(daz,1);
        modi1=o2_atom(o2_nam_nam_combo(daz,4),7);
        modib=o2_nam_nam_combo(daz,2);
        modi2=nam_atom(o2_nam_nam_combo(daz,5),7);
        modic=o2_nam_nam_combo(daz,3);
        modi3=nam_atom(o2_nam_nam_combo(daz,6),7);

stan_frag_chos=grid_o2_final;
%stan_frag_chos(modib+20,:)=grid_nam_final(modib,:);
%stan_frag_chos(modic+20,:)=grid_nam_final(modic,:);


x_move=stan_frag_chos(modia,1)-frag_chos(modi1,1);
y_move=stan_frag_chos(modia,2)-frag_chos(modi1,2);
z_move=stan_frag_chos(modia,3)-frag_chos(modi1,3);
clear frag_chos_stage1;
for i=1:1:size_frag_chos(1,1)
    frag_chos_stage1(i,1)=frag_chos(i,1)+x_move;
    frag_chos_stage1(i,2)=frag_chos(i,2)+y_move;
    frag_chos_stage1(i,3)=frag_chos(i,3)+z_move;
    frag_chos_stage1(i,4:13)=frag_chos(i,4:13);
end

size_frag_chos_stage1=size(frag_chos_stage1);

%frag_chos_pointer(1,:)=frag_chos_stage1(3,:);
frag_chos_pointer(1,:)=frag_chos_stage1(modi2,:);
frag_chos_pointer(2,:)=frag_chos_stage1(modi3,:);
N_pointer=frag_chos_pointer(1,:);
C_pointer=frag_chos_pointer(2,:);

    
%stan_frag_chos_pointer(1,:)=stan_frag_chos(modi,:);
stan_frag_chos_pointer(1,:)=grid_nam_final(modib,:);
stan_frag_chos_pointer(2,:)=grid_nam_final(modic,:);
stan_N_pointer=stan_frag_chos_pointer(1,:);
stan_C_pointer=stan_frag_chos_pointer(2,:);

size_frag_chos_pointer=size(frag_chos_pointer);

size_stan_frag_chos_pointer=size(stan_frag_chos_pointer);

frag_chos_stage3=rota51(N_pointer,C_pointer,stan_frag_chos,stan_N_pointer,stan_C_pointer,frag_chos_stage1,modia,modib,modic,modi1,modi2,modi3,protein_frame);
size_frag_chos_stage3=size(frag_chos_stage3);
if size_frag_chos_stage3(1,1)<size_frag_chos(1,1)
    continue
elseif size_frag_chos_stage3(1,2)==size_frag_chos(1,2)
    test_stage3=frag_chos_stage3;
    distj=judge_dist(test_stage3,frag_chos_stage1);
    if distj==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3;')  );
        asdf=asdf+1;
    end
elseif size_frag_chos_stage3(1,2)==2*size_frag_chos(1,2)
    test_stage3_1=frag_chos_stage3(:,1:13);
    test_stage3_2=frag_chos_stage3(:,14:26);
    distj1=judge_dist(test_stage3_1,frag_chos_stage1);
    distj2=judge_dist(test_stage3_2,frag_chos_stage1);
    if distj1==1&distj2~=1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
    elseif distj1~=1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    elseif  distj1==1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    end
    
end

        

clear functions;

pack


    end
    
    
end
      
         
            

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            

if length(o2_o2_npl3_combo)~=0
    size_o2_o2_npl3_combo=size(o2_o2_npl3_combo);
    for daz=1:1:size_o2_o2_npl3_combo(1,1)
        modia=o2_o2_npl3_combo(daz,1);
        modi1=o2_atom(o2_o2_npl3_combo(daz,4),7);
        modib=o2_o2_npl3_combo(daz,2);
        modi2=o2_atom(o2_o2_npl3_combo(daz,5),7);
        modic=o2_o2_npl3_combo(daz,3);
        modi3=npl3_atom(o2_o2_npl3_combo(daz,6),7);

stan_frag_chos=grid_o2_final;
%stan_frag_chos(modib,:)=grid_o2_final(modib,:);
%stan_frag_chos(modic+20,:)=grid_npl3_final(modic,:);


x_move=stan_frag_chos(modia,1)-frag_chos(modi1,1);
y_move=stan_frag_chos(modia,2)-frag_chos(modi1,2);
z_move=stan_frag_chos(modia,3)-frag_chos(modi1,3);
clear frag_chos_stage1;
for i=1:1:size_frag_chos(1,1)
    frag_chos_stage1(i,1)=frag_chos(i,1)+x_move;
    frag_chos_stage1(i,2)=frag_chos(i,2)+y_move;
    frag_chos_stage1(i,3)=frag_chos(i,3)+z_move;
    frag_chos_stage1(i,4:13)=frag_chos(i,4:13);
end

size_frag_chos_stage1=size(frag_chos_stage1);

%frag_chos_pointer(1,:)=frag_chos_stage1(3,:);
frag_chos_pointer(1,:)=frag_chos_stage1(modi2,:);
frag_chos_pointer(2,:)=frag_chos_stage1(modi3,:);
N_pointer=frag_chos_pointer(1,:);
C_pointer=frag_chos_pointer(2,:);

    
%stan_frag_chos_pointer(1,:)=stan_frag_chos(modi,:);
stan_frag_chos_pointer(1,:)=grid_o2_final(modib,:);
stan_frag_chos_pointer(2,:)=grid_npl3_final(modic,:);
stan_N_pointer=stan_frag_chos_pointer(1,:);
stan_C_pointer=stan_frag_chos_pointer(2,:);

size_frag_chos_pointer=size(frag_chos_pointer);

size_stan_frag_chos_pointer=size(stan_frag_chos_pointer);

frag_chos_stage3=rota51(N_pointer,C_pointer,stan_frag_chos,stan_N_pointer,stan_C_pointer,frag_chos_stage1,modia,modib,modic,modi1,modi2,modi3,protein_frame);
size_frag_chos_stage3=size(frag_chos_stage3);
if size_frag_chos_stage3(1,1)<size_frag_chos(1,1)
    continue
elseif size_frag_chos_stage3(1,2)==size_frag_chos(1,2)
    test_stage3=frag_chos_stage3;
    distj=judge_dist(test_stage3,frag_chos_stage1);
    if distj==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3;')  );
        asdf=asdf+1;
    end
elseif size_frag_chos_stage3(1,2)==2*size_frag_chos(1,2)
    test_stage3_1=frag_chos_stage3(:,1:13);
    test_stage3_2=frag_chos_stage3(:,14:26);
    distj1=judge_dist(test_stage3_1,frag_chos_stage1);
    distj2=judge_dist(test_stage3_2,frag_chos_stage1);
    if distj1==1&distj2~=1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
    elseif distj1~=1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    elseif  distj1==1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    end
    
end



        



clear functions;

pack


    end
    
    
    
    
    
    
    
    
    
end
      
            
            
            
            

if length(o2_npl3_npl3_combo)~=0
    size_o2_npl3_npl3_combo=size(o2_npl3_npl3_combo);
    for daz=1:1:size_o2_npl3_npl3_combo(1,1)
        modia=o2_npl3_npl3_combo(daz,1);
        modi1=o2_atom(o2_npl3_npl3_combo(daz,4),7);
        modib=o2_npl3_npl3_combo(daz,2);
        modi2=npl3_atom(o2_npl3_npl3_combo(daz,5),7);
        modic=o2_npl3_npl3_combo(daz,3);
        modi3=npl3_atom(o2_npl3_npl3_combo(daz,6),7);
stan_frag_chos=grid_o2_final;
%stan_frag_chos(modib+20,:)=grid_npl3_final(modib,:);
%stan_frag_chos(modic+20,:)=grid_npl3_final(modic,:);


x_move=stan_frag_chos(modia,1)-frag_chos(modi1,1);
y_move=stan_frag_chos(modia,2)-frag_chos(modi1,2);
z_move=stan_frag_chos(modia,3)-frag_chos(modi1,3);
clear frag_chos_stage1;
for i=1:1:size_frag_chos(1,1)
    frag_chos_stage1(i,1)=frag_chos(i,1)+x_move;
    frag_chos_stage1(i,2)=frag_chos(i,2)+y_move;
    frag_chos_stage1(i,3)=frag_chos(i,3)+z_move;
    frag_chos_stage1(i,4:13)=frag_chos(i,4:13);
end

size_frag_chos_stage1=size(frag_chos_stage1);

%frag_chos_pointer(1,:)=frag_chos_stage1(3,:);
frag_chos_pointer(1,:)=frag_chos_stage1(modi2,:);
frag_chos_pointer(2,:)=frag_chos_stage1(modi3,:);
N_pointer=frag_chos_pointer(1,:);
C_pointer=frag_chos_pointer(2,:);

    
%stan_frag_chos_pointer(1,:)=stan_frag_chos(modi,:);
stan_frag_chos_pointer(1,:)=grid_npl3_final(modib,:);
stan_frag_chos_pointer(2,:)=grid_npl3_final(modic,:);
stan_N_pointer=stan_frag_chos_pointer(1,:);
stan_C_pointer=stan_frag_chos_pointer(2,:);

size_frag_chos_pointer=size(frag_chos_pointer);

size_stan_frag_chos_pointer=size(stan_frag_chos_pointer);

frag_chos_stage3=rota51(N_pointer,C_pointer,stan_frag_chos,stan_N_pointer,stan_C_pointer,frag_chos_stage1,modia,modib,modic,modi1,modi2,modi3,protein_frame);
size_frag_chos_stage3=size(frag_chos_stage3);
if size_frag_chos_stage3(1,1)<size_frag_chos(1,1)
    continue
elseif size_frag_chos_stage3(1,2)==size_frag_chos(1,2)
    test_stage3=frag_chos_stage3;
    distj=judge_dist(test_stage3,frag_chos_stage1);
    if distj==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3;')  );
        asdf=asdf+1;
    end
elseif size_frag_chos_stage3(1,2)==2*size_frag_chos(1,2)
    test_stage3_1=frag_chos_stage3(:,1:13);
    test_stage3_2=frag_chos_stage3(:,14:26);
    distj1=judge_dist(test_stage3_1,frag_chos_stage1);
    distj2=judge_dist(test_stage3_2,frag_chos_stage1);
    if distj1==1&distj2~=1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
    elseif distj1~=1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    elseif  distj1==1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    end
    
end



        


clear functions;

pack
    end
    
    
    
    
    
    
    
    
    
end
      
            
            

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            

if length(o2_o2_o3_combo)~=0
    size_o2_o2_o3_combo=size(o2_o2_o3_combo);
    for daz=1:1:size_o2_o2_o3_combo(1,1)
        modia=o2_o2_o3_combo(daz,1);
        modi1=o2_atom(o2_o2_o3_combo(daz,4),7);
        modib=o2_o2_o3_combo(daz,2);
        modi2=o2_atom(o2_o2_o3_combo(daz,5),7);
        modic=o2_o2_o3_combo(daz,3);
        modi3=o3_atom(o2_o2_o3_combo(daz,6),7);
        
        
        stan_frag_chos=grid_o2_final;
%stan_frag_chos(modib,:)=grid_o2_final(modib,:);
%stan_frag_chos(modic+20,:)=grid_o3_final(modic,:);


x_move=stan_frag_chos(modia,1)-frag_chos(modi1,1);
y_move=stan_frag_chos(modia,2)-frag_chos(modi1,2);
z_move=stan_frag_chos(modia,3)-frag_chos(modi1,3);
clear frag_chos_stage1;
for i=1:1:size_frag_chos(1,1)
    frag_chos_stage1(i,1)=frag_chos(i,1)+x_move;
    frag_chos_stage1(i,2)=frag_chos(i,2)+y_move;
    frag_chos_stage1(i,3)=frag_chos(i,3)+z_move;
    frag_chos_stage1(i,4:13)=frag_chos(i,4:13);
end

size_frag_chos_stage1=size(frag_chos_stage1);

%frag_chos_pointer(1,:)=frag_chos_stage1(3,:);
frag_chos_pointer(1,:)=frag_chos_stage1(modi2,:);
frag_chos_pointer(2,:)=frag_chos_stage1(modi3,:);
N_pointer=frag_chos_pointer(1,:);
C_pointer=frag_chos_pointer(2,:);

    
%stan_frag_chos_pointer(1,:)=stan_frag_chos(modi,:);
stan_frag_chos_pointer(1,:)=grid_o2_final(modib,:);
stan_frag_chos_pointer(2,:)=grid_o3_final(modic,:);
stan_N_pointer=stan_frag_chos_pointer(1,:);
stan_C_pointer=stan_frag_chos_pointer(2,:);

size_frag_chos_pointer=size(frag_chos_pointer);

size_stan_frag_chos_pointer=size(stan_frag_chos_pointer);

frag_chos_stage3=rota51(N_pointer,C_pointer,stan_frag_chos,stan_N_pointer,stan_C_pointer,frag_chos_stage1,modia,modib,modic,modi1,modi2,modi3,protein_frame);
size_frag_chos_stage3=size(frag_chos_stage3);
if size_frag_chos_stage3(1,1)<size_frag_chos(1,1)
    continue
elseif size_frag_chos_stage3(1,2)==size_frag_chos(1,2)
    test_stage3=frag_chos_stage3;
    distj=judge_dist(test_stage3,frag_chos_stage1);
    if distj==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3;')  );
        asdf=asdf+1;
    end
elseif size_frag_chos_stage3(1,2)==2*size_frag_chos(1,2)
    test_stage3_1=frag_chos_stage3(:,1:13);
    test_stage3_2=frag_chos_stage3(:,14:26);
    distj1=judge_dist(test_stage3_1,frag_chos_stage1);
    distj2=judge_dist(test_stage3_2,frag_chos_stage1);
    if distj1==1&distj2~=1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
    elseif distj1~=1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    elseif  distj1==1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    end
    
end



        


clear functions;

pack





    end
    
    
    
    
    
    
    
    
    
end
      
            
            
            
            

if length(o2_o3_o3_combo)~=0
    size_o2_o3_o3_combo=size(o2_o3_o3_combo);
    for daz=1:1:size_o2_o3_o3_combo(1,1)
        modia=o2_o3_o3_combo(daz,1);
        modi1=o2_atom(o2_o3_o3_combo(daz,4),7);
        modib=o2_o3_o3_combo(daz,2);
        modi2=o3_atom(o2_o3_o3_combo(daz,5),7);
        modic=o2_o3_o3_combo(daz,3);
        modi3=o3_atom(o2_o3_o3_combo(daz,6),7);

stan_frag_chos=grid_o2_final;
%stan_frag_chos(modib+20,:)=grid_o3_final(modib,:);
%stan_frag_chos(modic+20,:)=grid_o3_final(modic,:);


x_move=stan_frag_chos(modia,1)-frag_chos(modi1,1);
y_move=stan_frag_chos(modia,2)-frag_chos(modi1,2);
z_move=stan_frag_chos(modia,3)-frag_chos(modi1,3);
clear frag_chos_stage1;
for i=1:1:size_frag_chos(1,1)
    frag_chos_stage1(i,1)=frag_chos(i,1)+x_move;
    frag_chos_stage1(i,2)=frag_chos(i,2)+y_move;
    frag_chos_stage1(i,3)=frag_chos(i,3)+z_move;
    frag_chos_stage1(i,4:13)=frag_chos(i,4:13);
end

size_frag_chos_stage1=size(frag_chos_stage1);

%frag_chos_pointer(1,:)=frag_chos_stage1(3,:);
frag_chos_pointer(1,:)=frag_chos_stage1(modi2,:);
frag_chos_pointer(2,:)=frag_chos_stage1(modi3,:);
N_pointer=frag_chos_pointer(1,:);
C_pointer=frag_chos_pointer(2,:);

    
%stan_frag_chos_pointer(1,:)=stan_frag_chos(modi,:);
stan_frag_chos_pointer(1,:)=grid_o3_final(modib,:);
stan_frag_chos_pointer(2,:)=grid_o3_final(modic,:);
stan_N_pointer=stan_frag_chos_pointer(1,:);
stan_C_pointer=stan_frag_chos_pointer(2,:);

size_frag_chos_pointer=size(frag_chos_pointer);

size_stan_frag_chos_pointer=size(stan_frag_chos_pointer);

frag_chos_stage3=rota51(N_pointer,C_pointer,stan_frag_chos,stan_N_pointer,stan_C_pointer,frag_chos_stage1,modia,modib,modic,modi1,modi2,modi3,protein_frame);
size_frag_chos_stage3=size(frag_chos_stage3);
if size_frag_chos_stage3(1,1)<size_frag_chos(1,1)
    continue
elseif size_frag_chos_stage3(1,2)==size_frag_chos(1,2)
    test_stage3=frag_chos_stage3;
    distj=judge_dist(test_stage3,frag_chos_stage1);
    if distj==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3;')  );
        asdf=asdf+1;
    end
elseif size_frag_chos_stage3(1,2)==2*size_frag_chos(1,2)
    test_stage3_1=frag_chos_stage3(:,1:13);
    test_stage3_2=frag_chos_stage3(:,14:26);
    distj1=judge_dist(test_stage3_1,frag_chos_stage1);
    distj2=judge_dist(test_stage3_2,frag_chos_stage1);
    if distj1==1&distj2~=1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
    elseif distj1~=1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    elseif  distj1==1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    end
    
end
    


        


clear functions;
pack
    end
    
    
    
    
    
    
    
    
    
end
      
            
            
            


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            


          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


if length(nam_nam_nam_combo)~=0
    size_nam_nam_nam_combo=size(nam_nam_nam_combo);
    for daz=1:1:size_nam_nam_nam_combo(1,1)
        modia=nam_nam_nam_combo(daz,1);
        modi1=nam_atom(nam_nam_nam_combo(daz,4),7);
        modib=nam_nam_nam_combo(daz,2);
        modi2=nam_atom(nam_nam_nam_combo(daz,5),7);
        modic=nam_nam_nam_combo(daz,3);
        modi3=nam_atom(nam_nam_nam_combo(daz,6),7);
    
    
stan_frag_chos=grid_nam_final;
x_move=stan_frag_chos(modia,1)-frag_chos(modi1,1);
y_move=stan_frag_chos(modia,2)-frag_chos(modi1,2);
z_move=stan_frag_chos(modia,3)-frag_chos(modi1,3);
clear frag_chos_stage1;
for i=1:1:size_frag_chos(1,1)
    frag_chos_stage1(i,1)=frag_chos(i,1)+x_move;
    frag_chos_stage1(i,2)=frag_chos(i,2)+y_move;
    frag_chos_stage1(i,3)=frag_chos(i,3)+z_move;
    frag_chos_stage1(i,4:13)=frag_chos(i,4:13);
end

size_frag_chos_stage1=size(frag_chos_stage1);

%frag_chos_pointer(1,:)=frag_chos_stage1(3,:);
frag_chos_pointer(1,:)=frag_chos_stage1(modi2,:);
frag_chos_pointer(2,:)=frag_chos_stage1(modi3,:);
N_pointer=frag_chos_pointer(1,:);
C_pointer=frag_chos_pointer(2,:);

    
%stan_frag_chos_pointer(1,:)=stan_frag_chos(modi,:);
stan_frag_chos_pointer(1,:)=grid_nam_final(modib,:);
stan_frag_chos_pointer(2,:)=grid_nam_final(modic,:);
stan_N_pointer=stan_frag_chos_pointer(1,:);
stan_C_pointer=stan_frag_chos_pointer(2,:);

size_frag_chos_pointer=size(frag_chos_pointer);

size_stan_frag_chos_pointer=size(stan_frag_chos_pointer);

frag_chos_stage3=rota51(N_pointer,C_pointer,stan_frag_chos,stan_N_pointer,stan_C_pointer,frag_chos_stage1,modia,modib,modic,modi1,modi2,modi3,protein_frame);
size_frag_chos_stage3=size(frag_chos_stage3);
if size_frag_chos_stage3(1,1)<size_frag_chos(1,1)
    continue
elseif size_frag_chos_stage3(1,2)==size_frag_chos(1,2)
    test_stage3=frag_chos_stage3;
    distj=judge_dist(test_stage3,frag_chos_stage1);
    if distj==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3;')  );
        asdf=asdf+1;
    end
elseif size_frag_chos_stage3(1,2)==2*size_frag_chos(1,2)
    test_stage3_1=frag_chos_stage3(:,1:13);
    test_stage3_2=frag_chos_stage3(:,14:26);
    distj1=judge_dist(test_stage3_1,frag_chos_stage1);
    distj2=judge_dist(test_stage3_2,frag_chos_stage1);
    if distj1==1&distj2~=1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
    elseif distj1~=1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    elseif  distj1==1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    end
    
end
    



        

clear functions;
pack
    end



    
end

            

            

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            

if length(nam_nam_npl3_combo)~=0
    size_nam_nam_npl3_combo=size(nam_nam_npl3_combo);
    for daz=1:1:size_nam_nam_npl3_combo(1,1)
        modia=nam_nam_npl3_combo(daz,1);
        modi1=nam_atom(nam_nam_npl3_combo(daz,4),7);
        modib=nam_nam_npl3_combo(daz,2);
        modi2=nam_atom(nam_nam_npl3_combo(daz,5),7);
        modic=nam_nam_npl3_combo(daz,3);
        modi3=npl3_atom(nam_nam_npl3_combo(daz,6),7);

stan_frag_chos=grid_nam_final;
%stan_frag_chos(modib,:)=grid_nam_final(modib,:);
%stan_frag_chos(modic+20,:)=grid_npl3_final(modic,:);


x_move=stan_frag_chos(modia,1)-frag_chos(modi1,1);
y_move=stan_frag_chos(modia,2)-frag_chos(modi1,2);
z_move=stan_frag_chos(modia,3)-frag_chos(modi1,3);
clear frag_chos_stage1;
for i=1:1:size_frag_chos(1,1)
    frag_chos_stage1(i,1)=frag_chos(i,1)+x_move;
    frag_chos_stage1(i,2)=frag_chos(i,2)+y_move;
    frag_chos_stage1(i,3)=frag_chos(i,3)+z_move;
    frag_chos_stage1(i,4:13)=frag_chos(i,4:13);
end

size_frag_chos_stage1=size(frag_chos_stage1);

%frag_chos_pointer(1,:)=frag_chos_stage1(3,:);
frag_chos_pointer(1,:)=frag_chos_stage1(modi2,:);
frag_chos_pointer(2,:)=frag_chos_stage1(modi3,:);
N_pointer=frag_chos_pointer(1,:);
C_pointer=frag_chos_pointer(2,:);

    
%stan_frag_chos_pointer(1,:)=stan_frag_chos(modi,:);
stan_frag_chos_pointer(1,:)=grid_nam_final(modib,:);
stan_frag_chos_pointer(2,:)=grid_npl3_final(modic,:);
stan_N_pointer=stan_frag_chos_pointer(1,:);
stan_C_pointer=stan_frag_chos_pointer(2,:);

size_frag_chos_pointer=size(frag_chos_pointer);

size_stan_frag_chos_pointer=size(stan_frag_chos_pointer);

frag_chos_stage3=rota51(N_pointer,C_pointer,stan_frag_chos,stan_N_pointer,stan_C_pointer,frag_chos_stage1,modia,modib,modic,modi1,modi2,modi3,protein_frame);
size_frag_chos_stage3=size(frag_chos_stage3);
if size_frag_chos_stage3(1,1)<size_frag_chos(1,1)
    continue
elseif size_frag_chos_stage3(1,2)==size_frag_chos(1,2)
    test_stage3=frag_chos_stage3;
    distj=judge_dist(test_stage3,frag_chos_stage1);
    if distj==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3;')  );
        asdf=asdf+1;
    end
elseif size_frag_chos_stage3(1,2)==2*size_frag_chos(1,2)
    test_stage3_1=frag_chos_stage3(:,1:13);
    test_stage3_2=frag_chos_stage3(:,14:26);
    distj1=judge_dist(test_stage3_1,frag_chos_stage1);
    distj2=judge_dist(test_stage3_2,frag_chos_stage1);
    if distj1==1&distj2~=1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
    elseif distj1~=1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    elseif  distj1==1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    end
    
end
    






clear functions;


pack


    end
    
    
    
    
    
    
    
    
    
end
      
            
            
            
            

if length(nam_npl3_npl3_combo)~=0
    size_nam_npl3_npl3_combo=size(nam_npl3_npl3_combo);
    for daz=1:1:size_nam_npl3_npl3_combo(1,1)
        modia=nam_npl3_npl3_combo(daz,1);
        modi1=nam_atom(nam_npl3_npl3_combo(daz,4),7);
        modib=nam_npl3_npl3_combo(daz,2);
        modi2=npl3_atom(nam_npl3_npl3_combo(daz,5),7);
        modic=nam_npl3_npl3_combo(daz,3);
        modi3=npl3_atom(nam_npl3_npl3_combo(daz,6),7);
stan_frag_chos=grid_nam_final;
%stan_frag_chos(modib+20,:)=grid_npl3_final(modib,:);
%stan_frag_chos(modic+20,:)=grid_npl3_final(modic,:);


x_move=stan_frag_chos(modia,1)-frag_chos(modi1,1);
y_move=stan_frag_chos(modia,2)-frag_chos(modi1,2);
z_move=stan_frag_chos(modia,3)-frag_chos(modi1,3);
clear frag_chos_stage1;
for i=1:1:size_frag_chos(1,1)
    frag_chos_stage1(i,1)=frag_chos(i,1)+x_move;
    frag_chos_stage1(i,2)=frag_chos(i,2)+y_move;
    frag_chos_stage1(i,3)=frag_chos(i,3)+z_move;
    frag_chos_stage1(i,4:13)=frag_chos(i,4:13);
end

size_frag_chos_stage1=size(frag_chos_stage1);

%frag_chos_pointer(1,:)=frag_chos_stage1(3,:);
frag_chos_pointer(1,:)=frag_chos_stage1(modi2,:);
frag_chos_pointer(2,:)=frag_chos_stage1(modi3,:);
N_pointer=frag_chos_pointer(1,:);
C_pointer=frag_chos_pointer(2,:);

    
%stan_frag_chos_pointer(1,:)=stan_frag_chos(modi,:);
stan_frag_chos_pointer(1,:)=grid_npl3_final(modib,:);
stan_frag_chos_pointer(2,:)=grid_npl3_final(modic,:);
stan_N_pointer=stan_frag_chos_pointer(1,:);
stan_C_pointer=stan_frag_chos_pointer(2,:);

size_frag_chos_pointer=size(frag_chos_pointer);

size_stan_frag_chos_pointer=size(stan_frag_chos_pointer);

frag_chos_stage3=rota51(N_pointer,C_pointer,stan_frag_chos,stan_N_pointer,stan_C_pointer,frag_chos_stage1,modia,modib,modic,modi1,modi2,modi3,protein_frame);
size_frag_chos_stage3=size(frag_chos_stage3);
if size_frag_chos_stage3(1,1)<size_frag_chos(1,1)
    continue
elseif size_frag_chos_stage3(1,2)==size_frag_chos(1,2)
    test_stage3=frag_chos_stage3;
    distj=judge_dist(test_stage3,frag_chos_stage1);
    if distj==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3;')  );
        asdf=asdf+1;
    end
elseif size_frag_chos_stage3(1,2)==2*size_frag_chos(1,2)
    test_stage3_1=frag_chos_stage3(:,1:13);
    test_stage3_2=frag_chos_stage3(:,14:26);
    distj1=judge_dist(test_stage3_1,frag_chos_stage1);
    distj2=judge_dist(test_stage3_2,frag_chos_stage1);
    if distj1==1&distj2~=1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
    elseif distj1~=1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    elseif  distj1==1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    end
    
end








clear functions;


pack


    end
    
    
    
    
    
    
    
    
    
end
      
            
            

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if length(o2_car_car_combo)~=0
    size_o2_car_car_combo=size(o2_car_car_combo);
    for daz=1:1:size_o2_car_car_combo(1,1)
        modia=o2_car_car_combo(daz,1);
        modi1=o2_atom(o2_car_car_combo(daz,4),7);
        modib=o2_car_car_combo(daz,2);
        modi2=car_atom(o2_car_car_combo(daz,5),7);
        modic=o2_car_car_combo(daz,3);
        modi3=car_atom(o2_car_car_combo(daz,6),7);
stan_frag_chos=grid_o2_final;
%stan_frag_chos(modib+20,:)=grid_car_final(modib,:);
%stan_frag_chos(modic+20,:)=grid_car_final(modic,:);


x_move=stan_frag_chos(modia,1)-frag_chos(modi1,1);
y_move=stan_frag_chos(modia,2)-frag_chos(modi1,2);
z_move=stan_frag_chos(modia,3)-frag_chos(modi1,3);
clear frag_chos_stage1;
for i=1:1:size_frag_chos(1,1)
    frag_chos_stage1(i,1)=frag_chos(i,1)+x_move;
    frag_chos_stage1(i,2)=frag_chos(i,2)+y_move;
    frag_chos_stage1(i,3)=frag_chos(i,3)+z_move;
    frag_chos_stage1(i,4:13)=frag_chos(i,4:13);
end

size_frag_chos_stage1=size(frag_chos_stage1);

%frag_chos_pointer(1,:)=frag_chos_stage1(3,:);
frag_chos_pointer(1,:)=frag_chos_stage1(modi2,:);
frag_chos_pointer(2,:)=frag_chos_stage1(modi3,:);
N_pointer=frag_chos_pointer(1,:);
C_pointer=frag_chos_pointer(2,:);

    
%stan_frag_chos_pointer(1,:)=stan_frag_chos(modi,:);
stan_frag_chos_pointer(1,:)=grid_car_final(modib,:);
stan_frag_chos_pointer(2,:)=grid_car_final(modic,:);
stan_N_pointer=stan_frag_chos_pointer(1,:);
stan_C_pointer=stan_frag_chos_pointer(2,:);

size_frag_chos_pointer=size(frag_chos_pointer);

size_stan_frag_chos_pointer=size(stan_frag_chos_pointer);

frag_chos_stage3=rota51(N_pointer,C_pointer,stan_frag_chos,stan_N_pointer,stan_C_pointer,frag_chos_stage1,modia,modib,modic,modi1,modi2,modi3,protein_frame);
size_frag_chos_stage3=size(frag_chos_stage3);
if size_frag_chos_stage3(1,1)<size_frag_chos(1,1)
    continue
elseif size_frag_chos_stage3(1,2)==size_frag_chos(1,2)
    test_stage3=frag_chos_stage3;
    distj=judge_dist(test_stage3,frag_chos_stage1);
    if distj==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3;')  );
        asdf=asdf+1;
    end
elseif size_frag_chos_stage3(1,2)==2*size_frag_chos(1,2)
    test_stage3_1=frag_chos_stage3(:,1:13);
    test_stage3_2=frag_chos_stage3(:,14:26);
    distj1=judge_dist(test_stage3_1,frag_chos_stage1);
    distj2=judge_dist(test_stage3_2,frag_chos_stage1);
    if distj1==1&distj2~=1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
    elseif distj1~=1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    elseif  distj1==1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    end
    
end








clear functions;


pack


    end
    
    
    
    
    
    
    
    
    
end
      
            

if length(o3_car_car_combo)~=0
    size_o3_car_car_combo=size(o3_car_car_combo);
    for daz=1:1:size_o3_car_car_combo(1,1)
        modia=o3_car_car_combo(daz,1);
        modi1=o3_atom(o3_car_car_combo(daz,4),7);
        modib=o3_car_car_combo(daz,2);
        modi2=car_atom(o3_car_car_combo(daz,5),7);
        modic=o3_car_car_combo(daz,3);
        modi3=car_atom(o3_car_car_combo(daz,6),7);
stan_frag_chos=grid_o3_final;
%stan_frag_chos(modib+20,:)=grid_car_final(modib,:);
%stan_frag_chos(modic+20,:)=grid_car_final(modic,:);


x_move=stan_frag_chos(modia,1)-frag_chos(modi1,1);
y_move=stan_frag_chos(modia,2)-frag_chos(modi1,2);
z_move=stan_frag_chos(modia,3)-frag_chos(modi1,3);
clear frag_chos_stage1;
for i=1:1:size_frag_chos(1,1)
    frag_chos_stage1(i,1)=frag_chos(i,1)+x_move;
    frag_chos_stage1(i,2)=frag_chos(i,2)+y_move;
    frag_chos_stage1(i,3)=frag_chos(i,3)+z_move;
    frag_chos_stage1(i,4:13)=frag_chos(i,4:13);
end

size_frag_chos_stage1=size(frag_chos_stage1);

%frag_chos_pointer(1,:)=frag_chos_stage1(3,:);
frag_chos_pointer(1,:)=frag_chos_stage1(modi2,:);
frag_chos_pointer(2,:)=frag_chos_stage1(modi3,:);
N_pointer=frag_chos_pointer(1,:);
C_pointer=frag_chos_pointer(2,:);

    
%stan_frag_chos_pointer(1,:)=stan_frag_chos(modi,:);
stan_frag_chos_pointer(1,:)=grid_car_final(modib,:);
stan_frag_chos_pointer(2,:)=grid_car_final(modic,:);
stan_N_pointer=stan_frag_chos_pointer(1,:);
stan_C_pointer=stan_frag_chos_pointer(2,:);

size_frag_chos_pointer=size(frag_chos_pointer);

size_stan_frag_chos_pointer=size(stan_frag_chos_pointer);

frag_chos_stage3=rota51(N_pointer,C_pointer,stan_frag_chos,stan_N_pointer,stan_C_pointer,frag_chos_stage1,modia,modib,modic,modi1,modi2,modi3,protein_frame);
size_frag_chos_stage3=size(frag_chos_stage3);
if size_frag_chos_stage3(1,1)<size_frag_chos(1,1)
    continue
elseif size_frag_chos_stage3(1,2)==size_frag_chos(1,2)
    test_stage3=frag_chos_stage3;
    distj=judge_dist(test_stage3,frag_chos_stage1);
    if distj==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3;')  );
        asdf=asdf+1;
    end
elseif size_frag_chos_stage3(1,2)==2*size_frag_chos(1,2)
    test_stage3_1=frag_chos_stage3(:,1:13);
    test_stage3_2=frag_chos_stage3(:,14:26);
    distj1=judge_dist(test_stage3_1,frag_chos_stage1);
    distj2=judge_dist(test_stage3_2,frag_chos_stage1);
    if distj1==1&distj2~=1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
    elseif distj1~=1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    elseif  distj1==1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    end
    
end








clear functions;


pack


    end
    
    
    
    
    
    
    
    
    
end
      


if length(npl3_car_car_combo)~=0
    size_npl3_car_car_combo=size(npl3_car_car_combo);
    for daz=1:1:size_npl3_car_car_combo(1,1)
        modia=npl3_car_car_combo(daz,1);
        modi1=npl3_atom(npl3_car_car_combo(daz,4),7);
        modib=npl3_car_car_combo(daz,2);
        modi2=car_atom(npl3_car_car_combo(daz,5),7);
        modic=npl3_car_car_combo(daz,3);
        modi3=car_atom(npl3_car_car_combo(daz,6),7);
stan_frag_chos=grid_npl3_final;
%stan_frag_chos(modib+20,:)=grid_car_final(modib,:);
%stan_frag_chos(modic+20,:)=grid_car_final(modic,:);


x_move=stan_frag_chos(modia,1)-frag_chos(modi1,1);
y_move=stan_frag_chos(modia,2)-frag_chos(modi1,2);
z_move=stan_frag_chos(modia,3)-frag_chos(modi1,3);
clear frag_chos_stage1;
for i=1:1:size_frag_chos(1,1)
    frag_chos_stage1(i,1)=frag_chos(i,1)+x_move;
    frag_chos_stage1(i,2)=frag_chos(i,2)+y_move;
    frag_chos_stage1(i,3)=frag_chos(i,3)+z_move;
    frag_chos_stage1(i,4:13)=frag_chos(i,4:13);
end

size_frag_chos_stage1=size(frag_chos_stage1);

%frag_chos_pointer(1,:)=frag_chos_stage1(3,:);
frag_chos_pointer(1,:)=frag_chos_stage1(modi2,:);
frag_chos_pointer(2,:)=frag_chos_stage1(modi3,:);
N_pointer=frag_chos_pointer(1,:);
C_pointer=frag_chos_pointer(2,:);

    
%stan_frag_chos_pointer(1,:)=stan_frag_chos(modi,:);
stan_frag_chos_pointer(1,:)=grid_car_final(modib,:);
stan_frag_chos_pointer(2,:)=grid_car_final(modic,:);
stan_N_pointer=stan_frag_chos_pointer(1,:);
stan_C_pointer=stan_frag_chos_pointer(2,:);

size_frag_chos_pointer=size(frag_chos_pointer);

size_stan_frag_chos_pointer=size(stan_frag_chos_pointer);

frag_chos_stage3=rota51(N_pointer,C_pointer,stan_frag_chos,stan_N_pointer,stan_C_pointer,frag_chos_stage1,modia,modib,modic,modi1,modi2,modi3,protein_frame);
size_frag_chos_stage3=size(frag_chos_stage3);
if size_frag_chos_stage3(1,1)<size_frag_chos(1,1)
    continue
elseif size_frag_chos_stage3(1,2)==size_frag_chos(1,2)
    test_stage3=frag_chos_stage3;
    distj=judge_dist(test_stage3,frag_chos_stage1);
    if distj==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3;')  );
        asdf=asdf+1;
    end
elseif size_frag_chos_stage3(1,2)==2*size_frag_chos(1,2)
    test_stage3_1=frag_chos_stage3(:,1:13);
    test_stage3_2=frag_chos_stage3(:,14:26);
    distj1=judge_dist(test_stage3_1,frag_chos_stage1);
    distj2=judge_dist(test_stage3_2,frag_chos_stage1);
    if distj1==1&distj2~=1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
    elseif distj1~=1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    elseif  distj1==1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    end
    
end








clear functions;


pack


    end
    
    
    
    
    
    
    
    
    
end
      


if length(nam_car_car_combo)~=0
    size_nam_car_car_combo=size(nam_car_car_combo);
    for daz=1:1:size_nam_car_car_combo(1,1)
        modia=nam_car_car_combo(daz,1);
        modi1=nam_atom(nam_car_car_combo(daz,4),7);
        modib=nam_car_car_combo(daz,2);
        modi2=car_atom(nam_car_car_combo(daz,5),7);
        modic=nam_car_car_combo(daz,3);
        modi3=car_atom(nam_car_car_combo(daz,6),7);
stan_frag_chos=grid_nam_final;
%stan_frag_chos(modib+20,:)=grid_car_final(modib,:);
%stan_frag_chos(modic+20,:)=grid_car_final(modic,:);


x_move=stan_frag_chos(modia,1)-frag_chos(modi1,1);
y_move=stan_frag_chos(modia,2)-frag_chos(modi1,2);
z_move=stan_frag_chos(modia,3)-frag_chos(modi1,3);
clear frag_chos_stage1;
for i=1:1:size_frag_chos(1,1)
    frag_chos_stage1(i,1)=frag_chos(i,1)+x_move;
    frag_chos_stage1(i,2)=frag_chos(i,2)+y_move;
    frag_chos_stage1(i,3)=frag_chos(i,3)+z_move;
    frag_chos_stage1(i,4:13)=frag_chos(i,4:13);
end

size_frag_chos_stage1=size(frag_chos_stage1);

%frag_chos_pointer(1,:)=frag_chos_stage1(3,:);
frag_chos_pointer(1,:)=frag_chos_stage1(modi2,:);
frag_chos_pointer(2,:)=frag_chos_stage1(modi3,:);
N_pointer=frag_chos_pointer(1,:);
C_pointer=frag_chos_pointer(2,:);

    
%stan_frag_chos_pointer(1,:)=stan_frag_chos(modi,:);
stan_frag_chos_pointer(1,:)=grid_car_final(modib,:);
stan_frag_chos_pointer(2,:)=grid_car_final(modic,:);
stan_N_pointer=stan_frag_chos_pointer(1,:);
stan_C_pointer=stan_frag_chos_pointer(2,:);

size_frag_chos_pointer=size(frag_chos_pointer);

size_stan_frag_chos_pointer=size(stan_frag_chos_pointer);

frag_chos_stage3=rota51(N_pointer,C_pointer,stan_frag_chos,stan_N_pointer,stan_C_pointer,frag_chos_stage1,modia,modib,modic,modi1,modi2,modi3,protein_frame);
size_frag_chos_stage3=size(frag_chos_stage3);
if size_frag_chos_stage3(1,1)<size_frag_chos(1,1)
    continue
elseif size_frag_chos_stage3(1,2)==size_frag_chos(1,2)
    test_stage3=frag_chos_stage3;
    distj=judge_dist(test_stage3,frag_chos_stage1);
    if distj==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3;')  );
        asdf=asdf+1;
    end
elseif size_frag_chos_stage3(1,2)==2*size_frag_chos(1,2)
    test_stage3_1=frag_chos_stage3(:,1:13);
    test_stage3_2=frag_chos_stage3(:,14:26);
    distj1=judge_dist(test_stage3_1,frag_chos_stage1);
    distj2=judge_dist(test_stage3_2,frag_chos_stage1);
    if distj1==1&distj2~=1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
    elseif distj1~=1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    elseif  distj1==1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    end
    
end








clear functions;


pack


    end
    
    
    
    
    
    
    
    
    
end
      
            

if length(n4_car_car_combo)~=0
    size_n4_car_car_combo=size(n4_car_car_combo);
    for daz=1:1:size_n4_car_car_combo(1,1)
        modia=n4_car_car_combo(daz,1);
        modi1=n4_atom(n4_car_car_combo(daz,4),7);
        modib=n4_car_car_combo(daz,2);
        modi2=car_atom(n4_car_car_combo(daz,5),7);
        modic=n4_car_car_combo(daz,3);
        modi3=car_atom(n4_car_car_combo(daz,6),7);
stan_frag_chos=grid_n4_final;
%stan_frag_chos(modib+20,:)=grid_car_final(modib,:);
%stan_frag_chos(modic+20,:)=grid_car_final(modic,:);


x_move=stan_frag_chos(modia,1)-frag_chos(modi1,1);
y_move=stan_frag_chos(modia,2)-frag_chos(modi1,2);
z_move=stan_frag_chos(modia,3)-frag_chos(modi1,3);
clear frag_chos_stage1;
for i=1:1:size_frag_chos(1,1)
    frag_chos_stage1(i,1)=frag_chos(i,1)+x_move;
    frag_chos_stage1(i,2)=frag_chos(i,2)+y_move;
    frag_chos_stage1(i,3)=frag_chos(i,3)+z_move;
    frag_chos_stage1(i,4:13)=frag_chos(i,4:13);
end

size_frag_chos_stage1=size(frag_chos_stage1);

%frag_chos_pointer(1,:)=frag_chos_stage1(3,:);
frag_chos_pointer(1,:)=frag_chos_stage1(modi2,:);
frag_chos_pointer(2,:)=frag_chos_stage1(modi3,:);
N_pointer=frag_chos_pointer(1,:);
C_pointer=frag_chos_pointer(2,:);

    
%stan_frag_chos_pointer(1,:)=stan_frag_chos(modi,:);
stan_frag_chos_pointer(1,:)=grid_car_final(modib,:);
stan_frag_chos_pointer(2,:)=grid_car_final(modic,:);
stan_N_pointer=stan_frag_chos_pointer(1,:);
stan_C_pointer=stan_frag_chos_pointer(2,:);

size_frag_chos_pointer=size(frag_chos_pointer);

size_stan_frag_chos_pointer=size(stan_frag_chos_pointer);

frag_chos_stage3=rota51(N_pointer,C_pointer,stan_frag_chos,stan_N_pointer,stan_C_pointer,frag_chos_stage1,modia,modib,modic,modi1,modi2,modi3,protein_frame);
size_frag_chos_stage3=size(frag_chos_stage3);
if size_frag_chos_stage3(1,1)<size_frag_chos(1,1)
    continue
elseif size_frag_chos_stage3(1,2)==size_frag_chos(1,2)
    test_stage3=frag_chos_stage3;
    distj=judge_dist(test_stage3,frag_chos_stage1);
    if distj==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3;')  );
        asdf=asdf+1;
    end
elseif size_frag_chos_stage3(1,2)==2*size_frag_chos(1,2)
    test_stage3_1=frag_chos_stage3(:,1:13);
    test_stage3_2=frag_chos_stage3(:,14:26);
    distj1=judge_dist(test_stage3_1,frag_chos_stage1);
    distj2=judge_dist(test_stage3_2,frag_chos_stage1);
    if distj1==1&distj2~=1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
    elseif distj1~=1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    elseif  distj1==1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    end
    
end








clear functions;


pack


    end
    
    
    
    
    
    
    
    
    
end
      
            
           











            

if length(nam_nam_o3_combo)~=0
    size_nam_nam_o3_combo=size(nam_nam_o3_combo);
    for daz=1:1:size_nam_nam_o3_combo(1,1)
        modia=nam_nam_o3_combo(daz,1);
        modi1=nam_atom(nam_nam_o3_combo(daz,4),7);
        modib=nam_nam_o3_combo(daz,2);
        modi2=nam_atom(nam_nam_o3_combo(daz,5),7);
        modic=nam_nam_o3_combo(daz,3);
        modi3=o3_atom(nam_nam_o3_combo(daz,6),7);
        
        
        stan_frag_chos=grid_nam_final;
%stan_frag_chos(modib,:)=grid_nam_final(modib,:);
%stan_frag_chos(modic+20,:)=grid_o3_final(modic,:);


x_move=stan_frag_chos(modia,1)-frag_chos(modi1,1);
y_move=stan_frag_chos(modia,2)-frag_chos(modi1,2);
z_move=stan_frag_chos(modia,3)-frag_chos(modi1,3);
clear frag_chos_stage1;
for i=1:1:size_frag_chos(1,1)
    frag_chos_stage1(i,1)=frag_chos(i,1)+x_move;
    frag_chos_stage1(i,2)=frag_chos(i,2)+y_move;
    frag_chos_stage1(i,3)=frag_chos(i,3)+z_move;
    frag_chos_stage1(i,4:13)=frag_chos(i,4:13);
end

size_frag_chos_stage1=size(frag_chos_stage1);

%frag_chos_pointer(1,:)=frag_chos_stage1(3,:);
frag_chos_pointer(1,:)=frag_chos_stage1(modi2,:);
frag_chos_pointer(2,:)=frag_chos_stage1(modi3,:);
N_pointer=frag_chos_pointer(1,:);
C_pointer=frag_chos_pointer(2,:);

    
%stan_frag_chos_pointer(1,:)=stan_frag_chos(modi,:);
stan_frag_chos_pointer(1,:)=grid_nam_final(modib,:);
stan_frag_chos_pointer(2,:)=grid_o3_final(modic,:);
stan_N_pointer=stan_frag_chos_pointer(1,:);
stan_C_pointer=stan_frag_chos_pointer(2,:);

size_frag_chos_pointer=size(frag_chos_pointer);

size_stan_frag_chos_pointer=size(stan_frag_chos_pointer);

frag_chos_stage3=rota51(N_pointer,C_pointer,stan_frag_chos,stan_N_pointer,stan_C_pointer,frag_chos_stage1,modia,modib,modic,modi1,modi2,modi3,protein_frame);
size_frag_chos_stage3=size(frag_chos_stage3);
if size_frag_chos_stage3(1,1)<size_frag_chos(1,1)
    continue
elseif size_frag_chos_stage3(1,2)==size_frag_chos(1,2)
    test_stage3=frag_chos_stage3;
    distj=judge_dist(test_stage3,frag_chos_stage1);
    if distj==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3;')  );
        asdf=asdf+1;
    end
elseif size_frag_chos_stage3(1,2)==2*size_frag_chos(1,2)
    test_stage3_1=frag_chos_stage3(:,1:13);
    test_stage3_2=frag_chos_stage3(:,14:26);
    distj1=judge_dist(test_stage3_1,frag_chos_stage1);
    distj2=judge_dist(test_stage3_2,frag_chos_stage1);
    if distj1==1&distj2~=1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
    elseif distj1~=1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    elseif  distj1==1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    end
    
end






clear functions;


pack

    end
    
    
    
    
    
    
    
    
    
end
      
            
            
            
            

if length(nam_o3_o3_combo)~=0
    size_nam_o3_o3_combo=size(nam_o3_o3_combo);
    for daz=1:1:size_nam_o3_o3_combo(1,1)
        modia=nam_o3_o3_combo(daz,1);
        modi1=nam_atom(nam_o3_o3_combo(daz,4),7);
        modib=nam_o3_o3_combo(daz,2);
        modi2=o3_atom(nam_o3_o3_combo(daz,5),7);
        modic=nam_o3_o3_combo(daz,3);
        modi3=o3_atom(nam_o3_o3_combo(daz,6),7);

stan_frag_chos=grid_nam_final;
%stan_frag_chos(modib+20,:)=grid_o3_final(modib,:);
%stan_frag_chos(modic+20,:)=grid_o3_final(modic,:);


x_move=stan_frag_chos(modia,1)-frag_chos(modi1,1);
y_move=stan_frag_chos(modia,2)-frag_chos(modi1,2);
z_move=stan_frag_chos(modia,3)-frag_chos(modi1,3);
clear frag_chos_stage1;
for i=1:1:size_frag_chos(1,1)
    frag_chos_stage1(i,1)=frag_chos(i,1)+x_move;
    frag_chos_stage1(i,2)=frag_chos(i,2)+y_move;
    frag_chos_stage1(i,3)=frag_chos(i,3)+z_move;
    frag_chos_stage1(i,4:13)=frag_chos(i,4:13);
end

size_frag_chos_stage1=size(frag_chos_stage1);

%frag_chos_pointer(1,:)=frag_chos_stage1(3,:);
frag_chos_pointer(1,:)=frag_chos_stage1(modi2,:);
frag_chos_pointer(2,:)=frag_chos_stage1(modi3,:);
N_pointer=frag_chos_pointer(1,:);
C_pointer=frag_chos_pointer(2,:);

    
%stan_frag_chos_pointer(1,:)=stan_frag_chos(modi,:);
stan_frag_chos_pointer(1,:)=grid_o3_final(modib,:);
stan_frag_chos_pointer(2,:)=grid_o3_final(modic,:);
stan_N_pointer=stan_frag_chos_pointer(1,:);
stan_C_pointer=stan_frag_chos_pointer(2,:);

size_frag_chos_pointer=size(frag_chos_pointer);

size_stan_frag_chos_pointer=size(stan_frag_chos_pointer);

frag_chos_stage3=rota51(N_pointer,C_pointer,stan_frag_chos,stan_N_pointer,stan_C_pointer,frag_chos_stage1,modia,modib,modic,modi1,modi2,modi3,protein_frame);
size_frag_chos_stage3=size(frag_chos_stage3);
if size_frag_chos_stage3(1,1)<size_frag_chos(1,1)
    continue
elseif size_frag_chos_stage3(1,2)==size_frag_chos(1,2)
    test_stage3=frag_chos_stage3;
    distj=judge_dist(test_stage3,frag_chos_stage1);
    if distj==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3;')  );
        asdf=asdf+1;
    end
elseif size_frag_chos_stage3(1,2)==2*size_frag_chos(1,2)
    test_stage3_1=frag_chos_stage3(:,1:13);
    test_stage3_2=frag_chos_stage3(:,14:26);
    distj1=judge_dist(test_stage3_1,frag_chos_stage1);
    distj2=judge_dist(test_stage3_2,frag_chos_stage1);
    if distj1==1&distj2~=1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
    elseif distj1~=1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    elseif  distj1==1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    end
    
end





clear functions;
pack
    end
    
    
    
    
    
    
    
    
    
end
      
            
            
            


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            
       
            

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            
            


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


if length(npl3_npl3_npl3_combo)~=0
    size_npl3_npl3_npl3_combo=size(npl3_npl3_npl3_combo);
    for daz=1:1:size_npl3_npl3_npl3_combo(1,1)
        modia=npl3_npl3_npl3_combo(daz,1);
        modi1=npl3_atom(npl3_npl3_npl3_combo(daz,4),7);
        modib=npl3_npl3_npl3_combo(daz,2);
        modi2=npl3_atom(npl3_npl3_npl3_combo(daz,5),7);
        modic=npl3_npl3_npl3_combo(daz,3);
        modi3=npl3_atom(npl3_npl3_npl3_combo(daz,6),7);
    
    
stan_frag_chos=grid_npl3_final;
x_move=stan_frag_chos(modia,1)-frag_chos(modi1,1);
y_move=stan_frag_chos(modia,2)-frag_chos(modi1,2);
z_move=stan_frag_chos(modia,3)-frag_chos(modi1,3);
clear frag_chos_stage1;
for i=1:1:size_frag_chos(1,1)
    frag_chos_stage1(i,1)=frag_chos(i,1)+x_move;
    frag_chos_stage1(i,2)=frag_chos(i,2)+y_move;
    frag_chos_stage1(i,3)=frag_chos(i,3)+z_move;
    frag_chos_stage1(i,4:13)=frag_chos(i,4:13);
end

size_frag_chos_stage1=size(frag_chos_stage1);

%frag_chos_pointer(1,:)=frag_chos_stage1(3,:);
frag_chos_pointer(1,:)=frag_chos_stage1(modi2,:);
frag_chos_pointer(2,:)=frag_chos_stage1(modi3,:);
N_pointer=frag_chos_pointer(1,:);
C_pointer=frag_chos_pointer(2,:);

    
%stan_frag_chos_pointer(1,:)=stan_frag_chos(modi,:);
stan_frag_chos_pointer(1,:)=stan_frag_chos(modib,:);
stan_frag_chos_pointer(2,:)=stan_frag_chos(modic,:);
stan_N_pointer=stan_frag_chos_pointer(1,:);
stan_C_pointer=stan_frag_chos_pointer(2,:);

size_frag_chos_pointer=size(frag_chos_pointer);

size_stan_frag_chos_pointer=size(stan_frag_chos_pointer);

frag_chos_stage3=rota51(N_pointer,C_pointer,stan_frag_chos,stan_N_pointer,stan_C_pointer,frag_chos_stage1,modia,modib,modic,modi1,modi2,modi3,protein_frame);
size_frag_chos_stage3=size(frag_chos_stage3);
if size_frag_chos_stage3(1,1)<size_frag_chos(1,1)
    continue
elseif size_frag_chos_stage3(1,2)==size_frag_chos(1,2)
    test_stage3=frag_chos_stage3;
    distj=judge_dist(test_stage3,frag_chos_stage1);
    if distj==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3;')  );
        asdf=asdf+1;
    end
elseif size_frag_chos_stage3(1,2)==2*size_frag_chos(1,2)
    test_stage3_1=frag_chos_stage3(:,1:13);
    test_stage3_2=frag_chos_stage3(:,14:26);
    distj1=judge_dist(test_stage3_1,frag_chos_stage1);
    distj2=judge_dist(test_stage3_2,frag_chos_stage1);
    if distj1==1&distj2~=1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
    elseif distj1~=1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    elseif  distj1==1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    end
    
end



clear functions;
pack
    end



    
end

            

            
            
   

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            

if length(npl3_npl3_o3_combo)~=0
    size_npl3_npl3_o3_combo=size(npl3_npl3_o3_combo);
    for daz=1:1:size_npl3_npl3_o3_combo(1,1)
        modia=npl3_npl3_o3_combo(daz,1);
        modi1=npl3_atom(npl3_npl3_o3_combo(daz,4),7);
        modib=npl3_npl3_o3_combo(daz,2);
        modi2=npl3_atom(npl3_npl3_o3_combo(daz,5),7);
        modic=npl3_npl3_o3_combo(daz,3);
        modi3=o3_atom(npl3_npl3_o3_combo(daz,6),7);
        
        
        stan_frag_chos=grid_npl3_final;
%stan_frag_chos(modib,:)=grid_npl3_final(modib,:);
%stan_frag_chos(modic+20,:)=grid_o3_final(modic,:);


x_move=stan_frag_chos(modia,1)-frag_chos(modi1,1);
y_move=stan_frag_chos(modia,2)-frag_chos(modi1,2);
z_move=stan_frag_chos(modia,3)-frag_chos(modi1,3);
clear frag_chos_stage1;
for i=1:1:size_frag_chos(1,1)
    frag_chos_stage1(i,1)=frag_chos(i,1)+x_move;
    frag_chos_stage1(i,2)=frag_chos(i,2)+y_move;
    frag_chos_stage1(i,3)=frag_chos(i,3)+z_move;
    frag_chos_stage1(i,4:13)=frag_chos(i,4:13);
end

size_frag_chos_stage1=size(frag_chos_stage1);

%frag_chos_pointer(1,:)=frag_chos_stage1(3,:);
frag_chos_pointer(1,:)=frag_chos_stage1(modi2,:);
frag_chos_pointer(2,:)=frag_chos_stage1(modi3,:);
N_pointer=frag_chos_pointer(1,:);
C_pointer=frag_chos_pointer(2,:);

    
%stan_frag_chos_pointer(1,:)=stan_frag_chos(modi,:);
stan_frag_chos_pointer(1,:)=grid_npl3_final(modib,:);
stan_frag_chos_pointer(2,:)=grid_o3_final(modic,:);
stan_N_pointer=stan_frag_chos_pointer(1,:);
stan_C_pointer=stan_frag_chos_pointer(2,:);

size_frag_chos_pointer=size(frag_chos_pointer);

size_stan_frag_chos_pointer=size(stan_frag_chos_pointer);

frag_chos_stage3=rota51(N_pointer,C_pointer,stan_frag_chos,stan_N_pointer,stan_C_pointer,frag_chos_stage1,modia,modib,modic,modi1,modi2,modi3,protein_frame);
size_frag_chos_stage3=size(frag_chos_stage3);
if size_frag_chos_stage3(1,1)<size_frag_chos(1,1)
    continue
elseif size_frag_chos_stage3(1,2)==size_frag_chos(1,2)
    test_stage3=frag_chos_stage3;
    distj=judge_dist(test_stage3,frag_chos_stage1);
    if distj==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3;')  );
        asdf=asdf+1;
    end
elseif size_frag_chos_stage3(1,2)==2*size_frag_chos(1,2)
    test_stage3_1=frag_chos_stage3(:,1:13);
    test_stage3_2=frag_chos_stage3(:,14:26);
    distj1=judge_dist(test_stage3_1,frag_chos_stage1);
    distj2=judge_dist(test_stage3_2,frag_chos_stage1);
    if distj1==1&distj2~=1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
    elseif distj1~=1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    elseif  distj1==1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    end
    
end



clear functions;


pack




    end
    
    
    
    
    
    
    
    
    
end
      
            
            
            
            

if length(npl3_o3_o3_combo)~=0
    size_npl3_o3_o3_combo=size(npl3_o3_o3_combo);
    for daz=1:1:size_npl3_o3_o3_combo(1,1)
        modia=npl3_o3_o3_combo(daz,1);
        modi1=npl3_atom(npl3_o3_o3_combo(daz,4),7);
        modib=npl3_o3_o3_combo(daz,2);
        modi2=o3_atom(npl3_o3_o3_combo(daz,5),7);
        modic=npl3_o3_o3_combo(daz,3);
        modi3=o3_atom(npl3_o3_o3_combo(daz,6),7);

stan_frag_chos=grid_npl3_final;
%stan_frag_chos(modib+20,:)=grid_o3_final(modib,:);
%stan_frag_chos(modic+20,:)=grid_o3_final(modic,:);


x_move=stan_frag_chos(modia,1)-frag_chos(modi1,1);
y_move=stan_frag_chos(modia,2)-frag_chos(modi1,2);
z_move=stan_frag_chos(modia,3)-frag_chos(modi1,3);
clear frag_chos_stage1;
for i=1:1:size_frag_chos(1,1)
    frag_chos_stage1(i,1)=frag_chos(i,1)+x_move;
    frag_chos_stage1(i,2)=frag_chos(i,2)+y_move;
    frag_chos_stage1(i,3)=frag_chos(i,3)+z_move;
    frag_chos_stage1(i,4:13)=frag_chos(i,4:13);
end

size_frag_chos_stage1=size(frag_chos_stage1);

%frag_chos_pointer(1,:)=frag_chos_stage1(3,:);
frag_chos_pointer(1,:)=frag_chos_stage1(modi2,:);
frag_chos_pointer(2,:)=frag_chos_stage1(modi3,:);
N_pointer=frag_chos_pointer(1,:);
C_pointer=frag_chos_pointer(2,:);

    
%stan_frag_chos_pointer(1,:)=stan_frag_chos(modi,:);
stan_frag_chos_pointer(1,:)=grid_o3_final(modib,:);
stan_frag_chos_pointer(2,:)=grid_o3_final(modic,:);
stan_N_pointer=stan_frag_chos_pointer(1,:);
stan_C_pointer=stan_frag_chos_pointer(2,:);

size_frag_chos_pointer=size(frag_chos_pointer);

size_stan_frag_chos_pointer=size(stan_frag_chos_pointer);

frag_chos_stage3=rota51(N_pointer,C_pointer,stan_frag_chos,stan_N_pointer,stan_C_pointer,frag_chos_stage1,modia,modib,modic,modi1,modi2,modi3,protein_frame);
size_frag_chos_stage3=size(frag_chos_stage3);
if size_frag_chos_stage3(1,1)<size_frag_chos(1,1)
    continue
elseif size_frag_chos_stage3(1,2)==size_frag_chos(1,2)
    test_stage3=frag_chos_stage3;
    distj=judge_dist(test_stage3,frag_chos_stage1);
    if distj==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3;')  );
        asdf=asdf+1;
    end
elseif size_frag_chos_stage3(1,2)==2*size_frag_chos(1,2)
    test_stage3_1=frag_chos_stage3(:,1:13);
    test_stage3_2=frag_chos_stage3(:,14:26);
    distj1=judge_dist(test_stage3_1,frag_chos_stage1);
    distj2=judge_dist(test_stage3_2,frag_chos_stage1);
    if distj1==1&distj2~=1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
    elseif distj1~=1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    elseif  distj1==1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    end
    
end



clear functions;
pack
    end
    
    
    
    
    
    
    
    
    
end
      
            
            
            


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            
   

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            
            

if (length(o3_o3_o3_combo)~=0)
    size_o3_o3_o3_combo=size(o3_o3_o3_combo);
    for daz=1:1:size_o3_o3_o3_combo(1,1)
        modia=o3_o3_o3_combo(daz,1);
        modi1=o3_atom(o3_o3_o3_combo(daz,4),7);
        modib=o3_o3_o3_combo(daz,2);
        modi2=o3_atom(o3_o3_o3_combo(daz,5),7);
        modic=o3_o3_o3_combo(daz,3);
        modi3=o3_atom(o3_o3_o3_combo(daz,6),7);
    
    
stan_frag_chos=grid_o3_final;
x_move=stan_frag_chos(modia,1)-frag_chos(modi1,1);
y_move=stan_frag_chos(modia,2)-frag_chos(modi1,2);
z_move=stan_frag_chos(modia,3)-frag_chos(modi1,3);
clear frag_chos_stage1;
for i=1:1:size_frag_chos(1,1)
    frag_chos_stage1(i,1)=frag_chos(i,1)+x_move;
    frag_chos_stage1(i,2)=frag_chos(i,2)+y_move;
    frag_chos_stage1(i,3)=frag_chos(i,3)+z_move;
    frag_chos_stage1(i,4:13)=frag_chos(i,4:13);
end

size_frag_chos_stage1=size(frag_chos_stage1);

%frag_chos_pointer(1,:)=frag_chos_stage1(3,:);
frag_chos_pointer(1,:)=frag_chos_stage1(modi2,:);
frag_chos_pointer(2,:)=frag_chos_stage1(modi3,:);
N_pointer=frag_chos_pointer(1,:);
C_pointer=frag_chos_pointer(2,:);

    
%stan_frag_chos_pointer(1,:)=stan_frag_chos(modi,:);
stan_frag_chos_pointer(1,:)=stan_frag_chos(modib,:);
stan_frag_chos_pointer(2,:)=stan_frag_chos(modic,:);
stan_N_pointer=stan_frag_chos_pointer(1,:);
stan_C_pointer=stan_frag_chos_pointer(2,:);

size_frag_chos_pointer=size(frag_chos_pointer);

size_stan_frag_chos_pointer=size(stan_frag_chos_pointer);

frag_chos_stage3=rota51(N_pointer,C_pointer,stan_frag_chos,stan_N_pointer,stan_C_pointer,frag_chos_stage1,modia,modib,modic,modi1,modi2,modi3,protein_frame);
size_frag_chos_stage3=size(frag_chos_stage3);
if size_frag_chos_stage3(1,1)<size_frag_chos(1,1)
    continue
elseif size_frag_chos_stage3(1,2)==size_frag_chos(1,2)
    test_stage3=frag_chos_stage3;
    distj=judge_dist(test_stage3,frag_chos_stage1);
    if distj==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3;')  );
        asdf=asdf+1;
    end
elseif size_frag_chos_stage3(1,2)==2*size_frag_chos(1,2)
    test_stage3_1=frag_chos_stage3(:,1:13);
    test_stage3_2=frag_chos_stage3(:,14:26);
    distj1=judge_dist(test_stage3_1,frag_chos_stage1);
    distj2=judge_dist(test_stage3_2,frag_chos_stage1);
    if distj1==1&distj2~=1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
    elseif distj1~=1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    elseif  distj1==1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    end
    
end



clear functions;
pack
    end



    
end

            

            
            
   

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            
            
 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

          

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            
  
   
          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            
            

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


if length(o2_nam_npl3_combo)~=0
    size_o2_nam_npl3_combo=size(o2_nam_npl3_combo);
    for daz=1:1:size_o2_nam_npl3_combo(1,1)
        modia=o2_nam_npl3_combo(daz,1);
        modi1=o2_atom(o2_nam_npl3_combo(daz,4),7);
        modib=o2_nam_npl3_combo(daz,2);
        modi2=nam_atom(o2_nam_npl3_combo(daz,5),7);
        modic=o2_nam_npl3_combo(daz,3);
        modi3=npl3_atom(o2_nam_npl3_combo(daz,6),7);
        
        
stan_frag_chos=grid_o2_final;
%stan_frag_chos(modib+20,:)=grid_nam_final(modib,:);
%stan_frag_chos(modic+40,:)=grid_npl3_final(modic,:);


x_move=stan_frag_chos(modia,1)-frag_chos(modi1,1);
y_move=stan_frag_chos(modia,2)-frag_chos(modi1,2);
z_move=stan_frag_chos(modia,3)-frag_chos(modi1,3);
clear frag_chos_stage1;
for i=1:1:size_frag_chos(1,1)
    frag_chos_stage1(i,1)=frag_chos(i,1)+x_move;
    frag_chos_stage1(i,2)=frag_chos(i,2)+y_move;
    frag_chos_stage1(i,3)=frag_chos(i,3)+z_move;
    frag_chos_stage1(i,4:13)=frag_chos(i,4:13);
end

size_frag_chos_stage1=size(frag_chos_stage1);

%frag_chos_pointer(1,:)=frag_chos_stage1(3,:);
frag_chos_pointer(1,:)=frag_chos_stage1(modi2,:);
frag_chos_pointer(2,:)=frag_chos_stage1(modi3,:);
N_pointer=frag_chos_pointer(1,:);
C_pointer=frag_chos_pointer(2,:);

    
%stan_frag_chos_pointer(1,:)=stan_frag_chos(modi,:);
stan_frag_chos_pointer(1,:)=grid_nam_final(modib,:);
stan_frag_chos_pointer(2,:)=grid_npl3_final(modic,:);
stan_N_pointer=stan_frag_chos_pointer(1,:);
stan_C_pointer=stan_frag_chos_pointer(2,:);

size_frag_chos_pointer=size(frag_chos_pointer);

size_stan_frag_chos_pointer=size(stan_frag_chos_pointer);

frag_chos_stage3=rota51(N_pointer,C_pointer,stan_frag_chos,stan_N_pointer,stan_C_pointer,frag_chos_stage1,modia,modib,modic,modi1,modi2,modi3,protein_frame);
size_frag_chos_stage3=size(frag_chos_stage3);
if size_frag_chos_stage3(1,1)<size_frag_chos(1,1)
    continue
elseif size_frag_chos_stage3(1,2)==size_frag_chos(1,2)
    test_stage3=frag_chos_stage3;
    distj=judge_dist(test_stage3,frag_chos_stage1);
    if distj==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3;')  );
        asdf=asdf+1;
    end
elseif size_frag_chos_stage3(1,2)==2*size_frag_chos(1,2)
    test_stage3_1=frag_chos_stage3(:,1:13);
    test_stage3_2=frag_chos_stage3(:,14:26);
    distj1=judge_dist(test_stage3_1,frag_chos_stage1);
    distj2=judge_dist(test_stage3_2,frag_chos_stage1);
    if distj1==1&distj2~=1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
    elseif distj1~=1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    elseif  distj1==1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    end
    
end



        clear functions;
        pack
    end
    
end
      



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%





if length(o2_nam_o3_combo)~=0
    size_o2_nam_o3_combo=size(o2_nam_o3_combo);
    for daz=1:1:size_o2_nam_o3_combo(1,1)
        modia=o2_nam_o3_combo(daz,1);
        modi1=o2_atom(o2_nam_o3_combo(daz,4),7);
        modib=o2_nam_o3_combo(daz,2);
        modi2=nam_atom(o2_nam_o3_combo(daz,5),7);
        modic=o2_nam_o3_combo(daz,3);
        modi3=o3_atom(o2_nam_o3_combo(daz,6),7);
        
        
stan_frag_chos=grid_o2_final;
%stan_frag_chos(modib+20,:)=grid_nam_final(modib,:);
%stan_frag_chos(modic+40,:)=grid_o3_final(modic,:);


x_move=stan_frag_chos(modia,1)-frag_chos(modi1,1);
y_move=stan_frag_chos(modia,2)-frag_chos(modi1,2);
z_move=stan_frag_chos(modia,3)-frag_chos(modi1,3);
clear frag_chos_stage1;
for i=1:1:size_frag_chos(1,1)
    frag_chos_stage1(i,1)=frag_chos(i,1)+x_move;
    frag_chos_stage1(i,2)=frag_chos(i,2)+y_move;
    frag_chos_stage1(i,3)=frag_chos(i,3)+z_move;
    frag_chos_stage1(i,4:13)=frag_chos(i,4:13);
end

size_frag_chos_stage1=size(frag_chos_stage1);

%frag_chos_pointer(1,:)=frag_chos_stage1(3,:);
frag_chos_pointer(1,:)=frag_chos_stage1(modi2,:);
frag_chos_pointer(2,:)=frag_chos_stage1(modi3,:);
N_pointer=frag_chos_pointer(1,:);
C_pointer=frag_chos_pointer(2,:);

    
%stan_frag_chos_pointer(1,:)=stan_frag_chos(modi,:);
stan_frag_chos_pointer(1,:)=grid_nam_final(modib,:);
stan_frag_chos_pointer(2,:)=grid_o3_final(modic,:);
stan_N_pointer=stan_frag_chos_pointer(1,:);
stan_C_pointer=stan_frag_chos_pointer(2,:);

size_frag_chos_pointer=size(frag_chos_pointer);

size_stan_frag_chos_pointer=size(stan_frag_chos_pointer);

frag_chos_stage3=rota51(N_pointer,C_pointer,stan_frag_chos,stan_N_pointer,stan_C_pointer,frag_chos_stage1,modia,modib,modic,modi1,modi2,modi3,protein_frame);
size_frag_chos_stage3=size(frag_chos_stage3);
if size_frag_chos_stage3(1,1)<size_frag_chos(1,1)
    continue
elseif size_frag_chos_stage3(1,2)==size_frag_chos(1,2)
    test_stage3=frag_chos_stage3;
    distj=judge_dist(test_stage3,frag_chos_stage1);
    if distj==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3;')  );
        asdf=asdf+1;
    end
elseif size_frag_chos_stage3(1,2)==2*size_frag_chos(1,2)
    test_stage3_1=frag_chos_stage3(:,1:13);
    test_stage3_2=frag_chos_stage3(:,14:26);
    distj1=judge_dist(test_stage3_1,frag_chos_stage1);
    distj2=judge_dist(test_stage3_2,frag_chos_stage1);
    if distj1==1&distj2~=1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
    elseif distj1~=1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    elseif  distj1==1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    end
    
end



        clear functions;
        pack
    end
    
end
      



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%





if length(o2_npl3_o3_combo)~=0
    size_o2_npl3_o3_combo=size(o2_npl3_o3_combo);
    for daz=1:1:size_o2_npl3_o3_combo(1,1)
        modia=o2_npl3_o3_combo(daz,1);
        modi1=o2_atom(o2_npl3_o3_combo(daz,4),7);
        modib=o2_npl3_o3_combo(daz,2);
        modi2=npl3_atom(o2_npl3_o3_combo(daz,5),7);
        modic=o2_npl3_o3_combo(daz,3);
        modi3=o3_atom(o2_npl3_o3_combo(daz,6),7);
        
        
stan_frag_chos=grid_o2_final;
%stan_frag_chos(modib+20,:)=grid_npl3_final(modib,:);
%stan_frag_chos(modic+40,:)=grid_o3_final(modic,:);


x_move=stan_frag_chos(modia,1)-frag_chos(modi1,1);
y_move=stan_frag_chos(modia,2)-frag_chos(modi1,2);
z_move=stan_frag_chos(modia,3)-frag_chos(modi1,3);
clear frag_chos_stage1;
for i=1:1:size_frag_chos(1,1)
    frag_chos_stage1(i,1)=frag_chos(i,1)+x_move;
    frag_chos_stage1(i,2)=frag_chos(i,2)+y_move;
    frag_chos_stage1(i,3)=frag_chos(i,3)+z_move;
    frag_chos_stage1(i,4:13)=frag_chos(i,4:13);
end

size_frag_chos_stage1=size(frag_chos_stage1);

%frag_chos_pointer(1,:)=frag_chos_stage1(3,:);
frag_chos_pointer(1,:)=frag_chos_stage1(modi2,:);
frag_chos_pointer(2,:)=frag_chos_stage1(modi3,:);
N_pointer=frag_chos_pointer(1,:);
C_pointer=frag_chos_pointer(2,:);

    
%stan_frag_chos_pointer(1,:)=stan_frag_chos(modi,:);
stan_frag_chos_pointer(1,:)=grid_npl3_final(modib,:);
stan_frag_chos_pointer(2,:)=grid_o3_final(modic,:);
stan_N_pointer=stan_frag_chos_pointer(1,:);
stan_C_pointer=stan_frag_chos_pointer(2,:);

size_frag_chos_pointer=size(frag_chos_pointer);

size_stan_frag_chos_pointer=size(stan_frag_chos_pointer);

frag_chos_stage3=rota51(N_pointer,C_pointer,stan_frag_chos,stan_N_pointer,stan_C_pointer,frag_chos_stage1,modia,modib,modic,modi1,modi2,modi3,protein_frame);
size_frag_chos_stage3=size(frag_chos_stage3);
if size_frag_chos_stage3(1,1)<size_frag_chos(1,1)
    continue
elseif size_frag_chos_stage3(1,2)==size_frag_chos(1,2)
    test_stage3=frag_chos_stage3;
    distj=judge_dist(test_stage3,frag_chos_stage1);
    if distj==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3;')  );
        asdf=asdf+1;
    end
elseif size_frag_chos_stage3(1,2)==2*size_frag_chos(1,2)
    test_stage3_1=frag_chos_stage3(:,1:13);
    test_stage3_2=frag_chos_stage3(:,14:26);
    distj1=judge_dist(test_stage3_1,frag_chos_stage1);
    distj2=judge_dist(test_stage3_2,frag_chos_stage1);
    if distj1==1&distj2~=1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
    elseif distj1~=1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    elseif  distj1==1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    end
    
end



        clear functions;
        pack
    end
    
end
      

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%





if length(o3_nam_npl3_combo)~=0
    size_o3_nam_npl3_combo=size(o3_nam_npl3_combo);
    for daz=1:1:size_o3_nam_npl3_combo(1,1)
        modia=o3_nam_npl3_combo(daz,1);
        modi1=o3_atom(o3_nam_npl3_combo(daz,4),7);
        modib=o3_nam_npl3_combo(daz,2);
        modi2=nam_atom(o3_nam_npl3_combo(daz,5),7);
        modic=o3_nam_npl3_combo(daz,3);
        modi3=npl3_atom(o3_nam_npl3_combo(daz,6),7);
        
        
stan_frag_chos=grid_o3_final;
%stan_frag_chos(modib+20,:)=grid_nam_final(modib,:);
%stan_frag_chos(modic+40,:)=grid_npl3_final(modic,:);


x_move=stan_frag_chos(modia,1)-frag_chos(modi1,1);
y_move=stan_frag_chos(modia,2)-frag_chos(modi1,2);
z_move=stan_frag_chos(modia,3)-frag_chos(modi1,3);
clear frag_chos_stage1;
for i=1:1:size_frag_chos(1,1)
    frag_chos_stage1(i,1)=frag_chos(i,1)+x_move;
    frag_chos_stage1(i,2)=frag_chos(i,2)+y_move;
    frag_chos_stage1(i,3)=frag_chos(i,3)+z_move;
    frag_chos_stage1(i,4:13)=frag_chos(i,4:13);
end

size_frag_chos_stage1=size(frag_chos_stage1);

%frag_chos_pointer(1,:)=frag_chos_stage1(3,:);
frag_chos_pointer(1,:)=frag_chos_stage1(modi2,:);
frag_chos_pointer(2,:)=frag_chos_stage1(modi3,:);
N_pointer=frag_chos_pointer(1,:);
C_pointer=frag_chos_pointer(2,:);

    
%stan_frag_chos_pointer(1,:)=stan_frag_chos(modi,:);
stan_frag_chos_pointer(1,:)=grid_nam_final(modib,:);
stan_frag_chos_pointer(2,:)=grid_npl3_final(modic,:);
stan_N_pointer=stan_frag_chos_pointer(1,:);
stan_C_pointer=stan_frag_chos_pointer(2,:);

size_frag_chos_pointer=size(frag_chos_pointer);

size_stan_frag_chos_pointer=size(stan_frag_chos_pointer);

frag_chos_stage3=rota51(N_pointer,C_pointer,stan_frag_chos,stan_N_pointer,stan_C_pointer,frag_chos_stage1,modia,modib,modic,modi1,modi2,modi3,protein_frame);
size_frag_chos_stage3=size(frag_chos_stage3);
if size_frag_chos_stage3(1,1)<size_frag_chos(1,1)
    continue
elseif size_frag_chos_stage3(1,2)==size_frag_chos(1,2)
    test_stage3=frag_chos_stage3;
    distj=judge_dist(test_stage3,frag_chos_stage1);
    if distj==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3;')  );
        asdf=asdf+1;
    end
elseif size_frag_chos_stage3(1,2)==2*size_frag_chos(1,2)
    test_stage3_1=frag_chos_stage3(:,1:13);
    test_stage3_2=frag_chos_stage3(:,14:26);
    distj1=judge_dist(test_stage3_1,frag_chos_stage1);
    distj2=judge_dist(test_stage3_2,frag_chos_stage1);
    if distj1==1&distj2~=1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
    elseif distj1~=1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    elseif  distj1==1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    end
    
end
    



        clear functions;
        pack
    end
    
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%









if length(o2_o3_n4_combo)~=0
    size_o2_o3_n4_combo=size(o2_o3_n4_combo);
    for daz=1:1:size_o2_o3_n4_combo(1,1)
        modia=o2_o3_n4_combo(daz,1);
        modi1=o2_atom(o2_o3_n4_combo(daz,4),7);
        modib=o2_o3_n4_combo(daz,2);
        modi2=o3_atom(o2_o3_n4_combo(daz,5),7);
        modic=o2_o3_n4_combo(daz,3);
        modi3=n4_atom(o2_o3_n4_combo(daz,6),7);
        
        
stan_frag_chos=grid_o2_final;
%stan_frag_chos(modib+20,:)=grid_o3_final(modib,:);
%stan_frag_chos(modic+40,:)=grid_n4_final(modic,:);


x_move=stan_frag_chos(modia,1)-frag_chos(modi1,1);
y_move=stan_frag_chos(modia,2)-frag_chos(modi1,2);
z_move=stan_frag_chos(modia,3)-frag_chos(modi1,3);
clear frag_chos_stage1;
for i=1:1:size_frag_chos(1,1)
    frag_chos_stage1(i,1)=frag_chos(i,1)+x_move;
    frag_chos_stage1(i,2)=frag_chos(i,2)+y_move;
    frag_chos_stage1(i,3)=frag_chos(i,3)+z_move;
    frag_chos_stage1(i,4:13)=frag_chos(i,4:13);
end

size_frag_chos_stage1=size(frag_chos_stage1);

%frag_chos_pointer(1,:)=frag_chos_stage1(3,:);
frag_chos_pointer(1,:)=frag_chos_stage1(modi2,:);
frag_chos_pointer(2,:)=frag_chos_stage1(modi3,:);
N_pointer=frag_chos_pointer(1,:);
C_pointer=frag_chos_pointer(2,:);

    
%stan_frag_chos_pointer(1,:)=stan_frag_chos(modi,:);
stan_frag_chos_pointer(1,:)=grid_o3_final(modib,:);
stan_frag_chos_pointer(2,:)=grid_n4_final(modic,:);
stan_N_pointer=stan_frag_chos_pointer(1,:);
stan_C_pointer=stan_frag_chos_pointer(2,:);

size_frag_chos_pointer=size(frag_chos_pointer);

size_stan_frag_chos_pointer=size(stan_frag_chos_pointer);

frag_chos_stage3=rota51(N_pointer,C_pointer,stan_frag_chos,stan_N_pointer,stan_C_pointer,frag_chos_stage1,modia,modib,modic,modi1,modi2,modi3,protein_frame);
size_frag_chos_stage3=size(frag_chos_stage3);
if size_frag_chos_stage3(1,1)<size_frag_chos(1,1)
    continue
elseif size_frag_chos_stage3(1,2)==size_frag_chos(1,2)
    test_stage3=frag_chos_stage3;
    distj=judge_dist(test_stage3,frag_chos_stage1);
    if distj==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3;')  );
        asdf=asdf+1;
    end
elseif size_frag_chos_stage3(1,2)==2*size_frag_chos(1,2)
    test_stage3_1=frag_chos_stage3(:,1:13);
    test_stage3_2=frag_chos_stage3(:,14:26);
    distj1=judge_dist(test_stage3_1,frag_chos_stage1);
    distj2=judge_dist(test_stage3_2,frag_chos_stage1);
    if distj1==1&distj2~=1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
    elseif distj1~=1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    elseif  distj1==1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    end
    
end

        clear functions;
        pack
    end
    
end





if length(o2_o3_n4_combo)~=0
    size_o2_o3_n4_combo=size(o2_o3_n4_combo);
    for daz=1:1:size_o2_o3_n4_combo(1,1)
        modia=o2_o3_n4_combo(daz,1);
        modi1=o2_atom(o2_o3_n4_combo(daz,4),7);
        modib=o2_o3_n4_combo(daz,2);
        modi2=o3_atom(o2_o3_n4_combo(daz,5),7);
        modic=o2_o3_n4_combo(daz,3);
        modi3=n4_atom(o2_o3_n4_combo(daz,6),7);
        
        
stan_frag_chos=grid_o2_final;
%stan_frag_chos(modib+20,:)=grid_o3_final(modib,:);
%stan_frag_chos(modic+40,:)=grid_n4_final(modic,:);


x_move=stan_frag_chos(modia,1)-frag_chos(modi1,1);
y_move=stan_frag_chos(modia,2)-frag_chos(modi1,2);
z_move=stan_frag_chos(modia,3)-frag_chos(modi1,3);
clear frag_chos_stage1;
for i=1:1:size_frag_chos(1,1)
    frag_chos_stage1(i,1)=frag_chos(i,1)+x_move;
    frag_chos_stage1(i,2)=frag_chos(i,2)+y_move;
    frag_chos_stage1(i,3)=frag_chos(i,3)+z_move;
    frag_chos_stage1(i,4:13)=frag_chos(i,4:13);
end

size_frag_chos_stage1=size(frag_chos_stage1);

%frag_chos_pointer(1,:)=frag_chos_stage1(3,:);
frag_chos_pointer(1,:)=frag_chos_stage1(modi2,:);
frag_chos_pointer(2,:)=frag_chos_stage1(modi3,:);
N_pointer=frag_chos_pointer(1,:);
C_pointer=frag_chos_pointer(2,:);

    
%stan_frag_chos_pointer(1,:)=stan_frag_chos(modi,:);
stan_frag_chos_pointer(1,:)=grid_o3_final(modib,:);
stan_frag_chos_pointer(2,:)=grid_n4_final(modic,:);
stan_N_pointer=stan_frag_chos_pointer(1,:);
stan_C_pointer=stan_frag_chos_pointer(2,:);

size_frag_chos_pointer=size(frag_chos_pointer);

size_stan_frag_chos_pointer=size(stan_frag_chos_pointer);

frag_chos_stage3=rota51(N_pointer,C_pointer,stan_frag_chos,stan_N_pointer,stan_C_pointer,frag_chos_stage1,modia,modib,modic,modi1,modi2,modi3,protein_frame);
size_frag_chos_stage3=size(frag_chos_stage3);
if size_frag_chos_stage3(1,1)<size_frag_chos(1,1)
    continue
elseif size_frag_chos_stage3(1,2)==size_frag_chos(1,2)
    test_stage3=frag_chos_stage3;
    distj=judge_dist(test_stage3,frag_chos_stage1);
    if distj==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3;')  );
        asdf=asdf+1;
    end
elseif size_frag_chos_stage3(1,2)==2*size_frag_chos(1,2)
    test_stage3_1=frag_chos_stage3(:,1:13);
    test_stage3_2=frag_chos_stage3(:,14:26);
    distj1=judge_dist(test_stage3_1,frag_chos_stage1);
    distj2=judge_dist(test_stage3_2,frag_chos_stage1);
    if distj1==1&distj2~=1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
    elseif distj1~=1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    elseif  distj1==1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    end
    
end

        clear functions;
        pack
    end
    
end






if length(o2_nam_n4_combo)~=0
    size_o2_nam_n4_combo=size(o2_nam_n4_combo);
    for daz=1:1:size_o2_nam_n4_combo(1,1)
        modia=o2_nam_n4_combo(daz,1);
        modi1=o2_atom(o2_nam_n4_combo(daz,4),7);
        modib=o2_nam_n4_combo(daz,2);
        modi2=nam_atom(o2_nam_n4_combo(daz,5),7);
        modic=o2_nam_n4_combo(daz,3);
        modi3=n4_atom(o2_nam_n4_combo(daz,6),7);
        
        
stan_frag_chos=grid_o2_final;
%stan_frag_chos(modib+20,:)=grid_nam_final(modib,:);
%stan_frag_chos(modic+40,:)=grid_n4_final(modic,:);


x_move=stan_frag_chos(modia,1)-frag_chos(modi1,1);
y_move=stan_frag_chos(modia,2)-frag_chos(modi1,2);
z_move=stan_frag_chos(modia,3)-frag_chos(modi1,3);
clear frag_chos_stage1;
for i=1:1:size_frag_chos(1,1)
    frag_chos_stage1(i,1)=frag_chos(i,1)+x_move;
    frag_chos_stage1(i,2)=frag_chos(i,2)+y_move;
    frag_chos_stage1(i,3)=frag_chos(i,3)+z_move;
    frag_chos_stage1(i,4:13)=frag_chos(i,4:13);
end

size_frag_chos_stage1=size(frag_chos_stage1);

%frag_chos_pointer(1,:)=frag_chos_stage1(3,:);
frag_chos_pointer(1,:)=frag_chos_stage1(modi2,:);
frag_chos_pointer(2,:)=frag_chos_stage1(modi3,:);
N_pointer=frag_chos_pointer(1,:);
C_pointer=frag_chos_pointer(2,:);

    
%stan_frag_chos_pointer(1,:)=stan_frag_chos(modi,:);
stan_frag_chos_pointer(1,:)=grid_nam_final(modib,:);
stan_frag_chos_pointer(2,:)=grid_n4_final(modic,:);
stan_N_pointer=stan_frag_chos_pointer(1,:);
stan_C_pointer=stan_frag_chos_pointer(2,:);

size_frag_chos_pointer=size(frag_chos_pointer);

size_stan_frag_chos_pointer=size(stan_frag_chos_pointer);

frag_chos_stage3=rota51(N_pointer,C_pointer,stan_frag_chos,stan_N_pointer,stan_C_pointer,frag_chos_stage1,modia,modib,modic,modi1,modi2,modi3,protein_frame);
size_frag_chos_stage3=size(frag_chos_stage3);
if size_frag_chos_stage3(1,1)<size_frag_chos(1,1)
    continue
elseif size_frag_chos_stage3(1,2)==size_frag_chos(1,2)
    test_stage3=frag_chos_stage3;
    distj=judge_dist(test_stage3,frag_chos_stage1);
    if distj==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3;')  );
        asdf=asdf+1;
    end
elseif size_frag_chos_stage3(1,2)==2*size_frag_chos(1,2)
    test_stage3_1=frag_chos_stage3(:,1:13);
    test_stage3_2=frag_chos_stage3(:,14:26);
    distj1=judge_dist(test_stage3_1,frag_chos_stage1);
    distj2=judge_dist(test_stage3_2,frag_chos_stage1);
    if distj1==1&distj2~=1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
    elseif distj1~=1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    elseif  distj1==1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    end
    
end

        clear functions;
        pack
    end
    
end




if length(o2_nam_n4_combo)~=0
    size_o2_nam_n4_combo=size(o2_nam_n4_combo);
    for daz=1:1:size_o2_nam_n4_combo(1,1)
        modia=o2_nam_n4_combo(daz,1);
        modi1=o2_atom(o2_nam_n4_combo(daz,4),7);
        modib=o2_nam_n4_combo(daz,2);
        modi2=nam_atom(o2_nam_n4_combo(daz,5),7);
        modic=o2_nam_n4_combo(daz,3);
        modi3=n4_atom(o2_nam_n4_combo(daz,6),7);
        
        
stan_frag_chos=grid_o2_final;
%stan_frag_chos(modib+20,:)=grid_nam_final(modib,:);
%stan_frag_chos(modic+40,:)=grid_n4_final(modic,:);


x_move=stan_frag_chos(modia,1)-frag_chos(modi1,1);
y_move=stan_frag_chos(modia,2)-frag_chos(modi1,2);
z_move=stan_frag_chos(modia,3)-frag_chos(modi1,3);
clear frag_chos_stage1;
for i=1:1:size_frag_chos(1,1)
    frag_chos_stage1(i,1)=frag_chos(i,1)+x_move;
    frag_chos_stage1(i,2)=frag_chos(i,2)+y_move;
    frag_chos_stage1(i,3)=frag_chos(i,3)+z_move;
    frag_chos_stage1(i,4:13)=frag_chos(i,4:13);
end

size_frag_chos_stage1=size(frag_chos_stage1);

%frag_chos_pointer(1,:)=frag_chos_stage1(3,:);
frag_chos_pointer(1,:)=frag_chos_stage1(modi2,:);
frag_chos_pointer(2,:)=frag_chos_stage1(modi3,:);
N_pointer=frag_chos_pointer(1,:);
C_pointer=frag_chos_pointer(2,:);

    
%stan_frag_chos_pointer(1,:)=stan_frag_chos(modi,:);
stan_frag_chos_pointer(1,:)=grid_nam_final(modib,:);
stan_frag_chos_pointer(2,:)=grid_n4_final(modic,:);
stan_N_pointer=stan_frag_chos_pointer(1,:);
stan_C_pointer=stan_frag_chos_pointer(2,:);

size_frag_chos_pointer=size(frag_chos_pointer);

size_stan_frag_chos_pointer=size(stan_frag_chos_pointer);

frag_chos_stage3=rota51(N_pointer,C_pointer,stan_frag_chos,stan_N_pointer,stan_C_pointer,frag_chos_stage1,modia,modib,modic,modi1,modi2,modi3,protein_frame);
size_frag_chos_stage3=size(frag_chos_stage3);
if size_frag_chos_stage3(1,1)<size_frag_chos(1,1)
    continue
elseif size_frag_chos_stage3(1,2)==size_frag_chos(1,2)
    test_stage3=frag_chos_stage3;
    distj=judge_dist(test_stage3,frag_chos_stage1);
    if distj==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3;')  );
        asdf=asdf+1;
    end
elseif size_frag_chos_stage3(1,2)==2*size_frag_chos(1,2)
    test_stage3_1=frag_chos_stage3(:,1:13);
    test_stage3_2=frag_chos_stage3(:,14:26);
    distj1=judge_dist(test_stage3_1,frag_chos_stage1);
    distj2=judge_dist(test_stage3_2,frag_chos_stage1);
    if distj1==1&distj2~=1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
    elseif distj1~=1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    elseif  distj1==1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    end
    
end

        clear functions;
        pack
    end
    
end




if length(o2_npl3_n4_combo)~=0
    size_o2_npl3_n4_combo=size(o2_npl3_n4_combo);
    for daz=1:1:size_o2_npl3_n4_combo(1,1)
        modia=o2_npl3_n4_combo(daz,1);
        modi1=o2_atom(o2_npl3_n4_combo(daz,4),7);
        modib=o2_npl3_n4_combo(daz,2);
        modi2=npl3_atom(o2_npl3_n4_combo(daz,5),7);
        modic=o2_npl3_n4_combo(daz,3);
        modi3=n4_atom(o2_npl3_n4_combo(daz,6),7);
        
        
stan_frag_chos=grid_o2_final;
%stan_frag_chos(modib+20,:)=grid_npl3_final(modib,:);
%stan_frag_chos(modic+40,:)=grid_n4_final(modic,:);


x_move=stan_frag_chos(modia,1)-frag_chos(modi1,1);
y_move=stan_frag_chos(modia,2)-frag_chos(modi1,2);
z_move=stan_frag_chos(modia,3)-frag_chos(modi1,3);
clear frag_chos_stage1;
for i=1:1:size_frag_chos(1,1)
    frag_chos_stage1(i,1)=frag_chos(i,1)+x_move;
    frag_chos_stage1(i,2)=frag_chos(i,2)+y_move;
    frag_chos_stage1(i,3)=frag_chos(i,3)+z_move;
    frag_chos_stage1(i,4:13)=frag_chos(i,4:13);
end

size_frag_chos_stage1=size(frag_chos_stage1);

%frag_chos_pointer(1,:)=frag_chos_stage1(3,:);
frag_chos_pointer(1,:)=frag_chos_stage1(modi2,:);
frag_chos_pointer(2,:)=frag_chos_stage1(modi3,:);
N_pointer=frag_chos_pointer(1,:);
C_pointer=frag_chos_pointer(2,:);

    
%stan_frag_chos_pointer(1,:)=stan_frag_chos(modi,:);
stan_frag_chos_pointer(1,:)=grid_npl3_final(modib,:);
stan_frag_chos_pointer(2,:)=grid_n4_final(modic,:);
stan_N_pointer=stan_frag_chos_pointer(1,:);
stan_C_pointer=stan_frag_chos_pointer(2,:);

size_frag_chos_pointer=size(frag_chos_pointer);

size_stan_frag_chos_pointer=size(stan_frag_chos_pointer);

frag_chos_stage3=rota51(N_pointer,C_pointer,stan_frag_chos,stan_N_pointer,stan_C_pointer,frag_chos_stage1,modia,modib,modic,modi1,modi2,modi3,protein_frame);
size_frag_chos_stage3=size(frag_chos_stage3);
if size_frag_chos_stage3(1,1)<size_frag_chos(1,1)
    continue
elseif size_frag_chos_stage3(1,2)==size_frag_chos(1,2)
    test_stage3=frag_chos_stage3;
    distj=judge_dist(test_stage3,frag_chos_stage1);
    if distj==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3;')  );
        asdf=asdf+1;
    end
elseif size_frag_chos_stage3(1,2)==2*size_frag_chos(1,2)
    test_stage3_1=frag_chos_stage3(:,1:13);
    test_stage3_2=frag_chos_stage3(:,14:26);
    distj1=judge_dist(test_stage3_1,frag_chos_stage1);
    distj2=judge_dist(test_stage3_2,frag_chos_stage1);
    if distj1==1&distj2~=1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
    elseif distj1~=1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    elseif  distj1==1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    end
    
end

        clear functions;
        pack
    end
    
end




if length(o3_nam_n4_combo)~=0
    size_o3_nam_n4_combo=size(o3_nam_n4_combo);
    for daz=1:1:size_o3_nam_n4_combo(1,1)
        modia=o3_nam_n4_combo(daz,1);
        modi1=o3_atom(o3_nam_n4_combo(daz,4),7);
        modib=o3_nam_n4_combo(daz,2);
        modi2=nam_atom(o3_nam_n4_combo(daz,5),7);
        modic=o3_nam_n4_combo(daz,3);
        modi3=n4_atom(o3_nam_n4_combo(daz,6),7);
        
        
stan_frag_chos=grid_o3_final;
%stan_frag_chos(modib+20,:)=grid_nam_final(modib,:);
%stan_frag_chos(modic+40,:)=grid_n4_final(modic,:);


x_move=stan_frag_chos(modia,1)-frag_chos(modi1,1);
y_move=stan_frag_chos(modia,2)-frag_chos(modi1,2);
z_move=stan_frag_chos(modia,3)-frag_chos(modi1,3);
clear frag_chos_stage1;
for i=1:1:size_frag_chos(1,1)
    frag_chos_stage1(i,1)=frag_chos(i,1)+x_move;
    frag_chos_stage1(i,2)=frag_chos(i,2)+y_move;
    frag_chos_stage1(i,3)=frag_chos(i,3)+z_move;
    frag_chos_stage1(i,4:13)=frag_chos(i,4:13);
end

size_frag_chos_stage1=size(frag_chos_stage1);

%frag_chos_pointer(1,:)=frag_chos_stage1(3,:);
frag_chos_pointer(1,:)=frag_chos_stage1(modi2,:);
frag_chos_pointer(2,:)=frag_chos_stage1(modi3,:);
N_pointer=frag_chos_pointer(1,:);
C_pointer=frag_chos_pointer(2,:);

    
%stan_frag_chos_pointer(1,:)=stan_frag_chos(modi,:);
stan_frag_chos_pointer(1,:)=grid_nam_final(modib,:);
stan_frag_chos_pointer(2,:)=grid_n4_final(modic,:);
stan_N_pointer=stan_frag_chos_pointer(1,:);
stan_C_pointer=stan_frag_chos_pointer(2,:);

size_frag_chos_pointer=size(frag_chos_pointer);

size_stan_frag_chos_pointer=size(stan_frag_chos_pointer);

frag_chos_stage3=rota51(N_pointer,C_pointer,stan_frag_chos,stan_N_pointer,stan_C_pointer,frag_chos_stage1,modia,modib,modic,modi1,modi2,modi3,protein_frame);
size_frag_chos_stage3=size(frag_chos_stage3);
if size_frag_chos_stage3(1,1)<size_frag_chos(1,1)
    continue
elseif size_frag_chos_stage3(1,2)==size_frag_chos(1,2)
    test_stage3=frag_chos_stage3;
    distj=judge_dist(test_stage3,frag_chos_stage1);
    if distj==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3;')  );
        asdf=asdf+1;
    end
elseif size_frag_chos_stage3(1,2)==2*size_frag_chos(1,2)
    test_stage3_1=frag_chos_stage3(:,1:13);
    test_stage3_2=frag_chos_stage3(:,14:26);
    distj1=judge_dist(test_stage3_1,frag_chos_stage1);
    distj2=judge_dist(test_stage3_2,frag_chos_stage1);
    if distj1==1&distj2~=1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
    elseif distj1~=1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    elseif  distj1==1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    end
    
end

        clear functions;
        pack
    end
    
end



if length(o3_npl3_n4_combo)~=0
    size_o3_npl3_n4_combo=size(o3_npl3_n4_combo);
    for daz=1:1:size_o3_npl3_n4_combo(1,1)
        modia=o3_npl3_n4_combo(daz,1);
        modi1=o3_atom(o3_npl3_n4_combo(daz,4),7);
        modib=o3_npl3_n4_combo(daz,2);
        modi2=npl3_atom(o3_npl3_n4_combo(daz,5),7);
        modic=o3_npl3_n4_combo(daz,3);
        modi3=n4_atom(o3_npl3_n4_combo(daz,6),7);
        
        
stan_frag_chos=grid_o3_final;
%stan_frag_chos(modib+20,:)=grid_npl3_final(modib,:);
%stan_frag_chos(modic+40,:)=grid_n4_final(modic,:);


x_move=stan_frag_chos(modia,1)-frag_chos(modi1,1);
y_move=stan_frag_chos(modia,2)-frag_chos(modi1,2);
z_move=stan_frag_chos(modia,3)-frag_chos(modi1,3);
clear frag_chos_stage1;
for i=1:1:size_frag_chos(1,1)
    frag_chos_stage1(i,1)=frag_chos(i,1)+x_move;
    frag_chos_stage1(i,2)=frag_chos(i,2)+y_move;
    frag_chos_stage1(i,3)=frag_chos(i,3)+z_move;
    frag_chos_stage1(i,4:13)=frag_chos(i,4:13);
end

size_frag_chos_stage1=size(frag_chos_stage1);

%frag_chos_pointer(1,:)=frag_chos_stage1(3,:);
frag_chos_pointer(1,:)=frag_chos_stage1(modi2,:);
frag_chos_pointer(2,:)=frag_chos_stage1(modi3,:);
N_pointer=frag_chos_pointer(1,:);
C_pointer=frag_chos_pointer(2,:);

    
%stan_frag_chos_pointer(1,:)=stan_frag_chos(modi,:);
stan_frag_chos_pointer(1,:)=grid_npl3_final(modib,:);
stan_frag_chos_pointer(2,:)=grid_n4_final(modic,:);
stan_N_pointer=stan_frag_chos_pointer(1,:);
stan_C_pointer=stan_frag_chos_pointer(2,:);

size_frag_chos_pointer=size(frag_chos_pointer);

size_stan_frag_chos_pointer=size(stan_frag_chos_pointer);

frag_chos_stage3=rota51(N_pointer,C_pointer,stan_frag_chos,stan_N_pointer,stan_C_pointer,frag_chos_stage1,modia,modib,modic,modi1,modi2,modi3,protein_frame);
size_frag_chos_stage3=size(frag_chos_stage3);
if size_frag_chos_stage3(1,1)<size_frag_chos(1,1)
    continue
elseif size_frag_chos_stage3(1,2)==size_frag_chos(1,2)
    test_stage3=frag_chos_stage3;
    distj=judge_dist(test_stage3,frag_chos_stage1);
    if distj==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3;')  );
        asdf=asdf+1;
    end
elseif size_frag_chos_stage3(1,2)==2*size_frag_chos(1,2)
    test_stage3_1=frag_chos_stage3(:,1:13);
    test_stage3_2=frag_chos_stage3(:,14:26);
    distj1=judge_dist(test_stage3_1,frag_chos_stage1);
    distj2=judge_dist(test_stage3_2,frag_chos_stage1);
    if distj1==1&distj2~=1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
    elseif distj1~=1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    elseif  distj1==1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    end
    
end

        clear functions;
        pack
    end
    
end



if length(nam_npl3_n4_combo)~=0
    size_nam_npl3_n4_combo=size(nam_npl3_n4_combo);
    for daz=1:1:size_nam_npl3_n4_combo(1,1)
        modia=nam_npl3_n4_combo(daz,1);
        modi1=nam_atom(nam_npl3_n4_combo(daz,4),7);
        modib=nam_npl3_n4_combo(daz,2);
        modi2=npl3_atom(nam_npl3_n4_combo(daz,5),7);
        modic=nam_npl3_n4_combo(daz,3);
        modi3=n4_atom(nam_npl3_n4_combo(daz,6),7);
        
        
stan_frag_chos=grid_nam_final;
%stan_frag_chos(modib+20,:)=grid_npl3_final(modib,:);
%stan_frag_chos(modic+40,:)=grid_n4_final(modic,:);


x_move=stan_frag_chos(modia,1)-frag_chos(modi1,1);
y_move=stan_frag_chos(modia,2)-frag_chos(modi1,2);
z_move=stan_frag_chos(modia,3)-frag_chos(modi1,3);
clear frag_chos_stage1;
for i=1:1:size_frag_chos(1,1)
    frag_chos_stage1(i,1)=frag_chos(i,1)+x_move;
    frag_chos_stage1(i,2)=frag_chos(i,2)+y_move;
    frag_chos_stage1(i,3)=frag_chos(i,3)+z_move;
    frag_chos_stage1(i,4:13)=frag_chos(i,4:13);
end

size_frag_chos_stage1=size(frag_chos_stage1);

%frag_chos_pointer(1,:)=frag_chos_stage1(3,:);
frag_chos_pointer(1,:)=frag_chos_stage1(modi2,:);
frag_chos_pointer(2,:)=frag_chos_stage1(modi3,:);
N_pointer=frag_chos_pointer(1,:);
C_pointer=frag_chos_pointer(2,:);

    
%stan_frag_chos_pointer(1,:)=stan_frag_chos(modi,:);
stan_frag_chos_pointer(1,:)=grid_npl3_final(modib,:);
stan_frag_chos_pointer(2,:)=grid_n4_final(modic,:);
stan_N_pointer=stan_frag_chos_pointer(1,:);
stan_C_pointer=stan_frag_chos_pointer(2,:);

size_frag_chos_pointer=size(frag_chos_pointer);

size_stan_frag_chos_pointer=size(stan_frag_chos_pointer);

frag_chos_stage3=rota51(N_pointer,C_pointer,stan_frag_chos,stan_N_pointer,stan_C_pointer,frag_chos_stage1,modia,modib,modic,modi1,modi2,modi3,protein_frame);
size_frag_chos_stage3=size(frag_chos_stage3);
if size_frag_chos_stage3(1,1)<size_frag_chos(1,1)
    continue
elseif size_frag_chos_stage3(1,2)==size_frag_chos(1,2)
    test_stage3=frag_chos_stage3;
    distj=judge_dist(test_stage3,frag_chos_stage1);
    if distj==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3;')  );
        asdf=asdf+1;
    end
elseif size_frag_chos_stage3(1,2)==2*size_frag_chos(1,2)
    test_stage3_1=frag_chos_stage3(:,1:13);
    test_stage3_2=frag_chos_stage3(:,14:26);
    distj1=judge_dist(test_stage3_1,frag_chos_stage1);
    distj2=judge_dist(test_stage3_2,frag_chos_stage1);
    if distj1==1&distj2~=1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
    elseif distj1~=1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    elseif  distj1==1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    end
    
end

        clear functions;
        pack
    end
    
end



if length(o2_nam_car_combo)~=0
    size_o2_nam_car_combo=size(o2_nam_car_combo);
    for daz=1:1:size_o2_nam_car_combo(1,1)
        modia=o2_nam_car_combo(daz,1);
        modi1=o2_atom(o2_nam_car_combo(daz,4),7);
        modib=o2_nam_car_combo(daz,2);
        modi2=nam_atom(o2_nam_car_combo(daz,5),7);
        modic=o2_nam_car_combo(daz,3);
        modi3=car_atom(o2_nam_car_combo(daz,6),7);
        
        
stan_frag_chos=grid_o2_final;
%stan_frag_chos(modib+20,:)=grid_nam_final(modib,:);
%stan_frag_chos(modic+40,:)=grid_car_final(modic,:);


x_move=stan_frag_chos(modia,1)-frag_chos(modi1,1);
y_move=stan_frag_chos(modia,2)-frag_chos(modi1,2);
z_move=stan_frag_chos(modia,3)-frag_chos(modi1,3);
clear frag_chos_stage1;
for i=1:1:size_frag_chos(1,1)
    frag_chos_stage1(i,1)=frag_chos(i,1)+x_move;
    frag_chos_stage1(i,2)=frag_chos(i,2)+y_move;
    frag_chos_stage1(i,3)=frag_chos(i,3)+z_move;
    frag_chos_stage1(i,4:13)=frag_chos(i,4:13);
end

size_frag_chos_stage1=size(frag_chos_stage1);

%frag_chos_pointer(1,:)=frag_chos_stage1(3,:);
frag_chos_pointer(1,:)=frag_chos_stage1(modi2,:);
frag_chos_pointer(2,:)=frag_chos_stage1(modi3,:);
N_pointer=frag_chos_pointer(1,:);
C_pointer=frag_chos_pointer(2,:);

    
%stan_frag_chos_pointer(1,:)=stan_frag_chos(modi,:);
stan_frag_chos_pointer(1,:)=grid_nam_final(modib,:);
stan_frag_chos_pointer(2,:)=grid_car_final(modic,:);
stan_N_pointer=stan_frag_chos_pointer(1,:);
stan_C_pointer=stan_frag_chos_pointer(2,:);

size_frag_chos_pointer=size(frag_chos_pointer);

size_stan_frag_chos_pointer=size(stan_frag_chos_pointer);

frag_chos_stage3=rota51(N_pointer,C_pointer,stan_frag_chos,stan_N_pointer,stan_C_pointer,frag_chos_stage1,modia,modib,modic,modi1,modi2,modi3,protein_frame);
size_frag_chos_stage3=size(frag_chos_stage3);
if size_frag_chos_stage3(1,1)<size_frag_chos(1,1)
    continue
elseif size_frag_chos_stage3(1,2)==size_frag_chos(1,2)
    test_stage3=frag_chos_stage3;
    distj=judge_dist(test_stage3,frag_chos_stage1);
    if distj==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3;')  );
        asdf=asdf+1;
    end
elseif size_frag_chos_stage3(1,2)==2*size_frag_chos(1,2)
    test_stage3_1=frag_chos_stage3(:,1:13);
    test_stage3_2=frag_chos_stage3(:,14:26);
    distj1=judge_dist(test_stage3_1,frag_chos_stage1);
    distj2=judge_dist(test_stage3_2,frag_chos_stage1);
    if distj1==1&distj2~=1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
    elseif distj1~=1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    elseif  distj1==1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    end
    
end

        clear functions;
        pack
    end
    
end



if length(o2_car_npl3_combo)~=0
    size_o2_car_npl3_combo=size(o2_car_npl3_combo);
    for daz=1:1:size_o2_car_npl3_combo(1,1)
        modia=o2_car_npl3_combo(daz,1);
        modi1=o2_atom(o2_car_npl3_combo(daz,4),7);
        modib=o2_car_npl3_combo(daz,2);
        modi2=car_atom(o2_car_npl3_combo(daz,5),7);
        modic=o2_car_npl3_combo(daz,3);
        modi3=npl3_atom(o2_car_npl3_combo(daz,6),7);
        
        
stan_frag_chos=grid_o2_final;
%stan_frag_chos(modib+20,:)=grid_car_final(modib,:);
%stan_frag_chos(modic+40,:)=grid_npl3_final(modic,:);


x_move=stan_frag_chos(modia,1)-frag_chos(modi1,1);
y_move=stan_frag_chos(modia,2)-frag_chos(modi1,2);
z_move=stan_frag_chos(modia,3)-frag_chos(modi1,3);
clear frag_chos_stage1;
for i=1:1:size_frag_chos(1,1)
    frag_chos_stage1(i,1)=frag_chos(i,1)+x_move;
    frag_chos_stage1(i,2)=frag_chos(i,2)+y_move;
    frag_chos_stage1(i,3)=frag_chos(i,3)+z_move;
    frag_chos_stage1(i,4:13)=frag_chos(i,4:13);
end

size_frag_chos_stage1=size(frag_chos_stage1);

%frag_chos_pointer(1,:)=frag_chos_stage1(3,:);
frag_chos_pointer(1,:)=frag_chos_stage1(modi2,:);
frag_chos_pointer(2,:)=frag_chos_stage1(modi3,:);
N_pointer=frag_chos_pointer(1,:);
C_pointer=frag_chos_pointer(2,:);

    
%stan_frag_chos_pointer(1,:)=stan_frag_chos(modi,:);
stan_frag_chos_pointer(1,:)=grid_car_final(modib,:);
stan_frag_chos_pointer(2,:)=grid_npl3_final(modic,:);
stan_N_pointer=stan_frag_chos_pointer(1,:);
stan_C_pointer=stan_frag_chos_pointer(2,:);

size_frag_chos_pointer=size(frag_chos_pointer);

size_stan_frag_chos_pointer=size(stan_frag_chos_pointer);

frag_chos_stage3=rota51(N_pointer,C_pointer,stan_frag_chos,stan_N_pointer,stan_C_pointer,frag_chos_stage1,modia,modib,modic,modi1,modi2,modi3,protein_frame);
size_frag_chos_stage3=size(frag_chos_stage3);
if size_frag_chos_stage3(1,1)<size_frag_chos(1,1)
    continue
elseif size_frag_chos_stage3(1,2)==size_frag_chos(1,2)
    test_stage3=frag_chos_stage3;
    distj=judge_dist(test_stage3,frag_chos_stage1);
    if distj==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3;')  );
        asdf=asdf+1;
    end
elseif size_frag_chos_stage3(1,2)==2*size_frag_chos(1,2)
    test_stage3_1=frag_chos_stage3(:,1:13);
    test_stage3_2=frag_chos_stage3(:,14:26);
    distj1=judge_dist(test_stage3_1,frag_chos_stage1);
    distj2=judge_dist(test_stage3_2,frag_chos_stage1);
    if distj1==1&distj2~=1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
    elseif distj1~=1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    elseif  distj1==1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    end
    
end

        clear functions;
        pack
    end
    
end



if length(o2_car_n4_combo)~=0
    size_o2_car_n4_combo=size(o2_car_n4_combo);
    for daz=1:1:size_o2_car_n4_combo(1,1)
        modia=o2_car_n4_combo(daz,1);
        modi1=o2_atom(o2_car_n4_combo(daz,4),7);
        modib=o2_car_n4_combo(daz,2);
        modi2=car_atom(o2_car_n4_combo(daz,5),7);
        modic=o2_car_n4_combo(daz,3);
        modi3=n4_atom(o2_car_n4_combo(daz,6),7);
        
        
stan_frag_chos=grid_o2_final;
%stan_frag_chos(modib+20,:)=grid_car_final(modib,:);
%stan_frag_chos(modic+40,:)=grid_n4_final(modic,:);


x_move=stan_frag_chos(modia,1)-frag_chos(modi1,1);
y_move=stan_frag_chos(modia,2)-frag_chos(modi1,2);
z_move=stan_frag_chos(modia,3)-frag_chos(modi1,3);
clear frag_chos_stage1;
for i=1:1:size_frag_chos(1,1)
    frag_chos_stage1(i,1)=frag_chos(i,1)+x_move;
    frag_chos_stage1(i,2)=frag_chos(i,2)+y_move;
    frag_chos_stage1(i,3)=frag_chos(i,3)+z_move;
    frag_chos_stage1(i,4:13)=frag_chos(i,4:13);
end

size_frag_chos_stage1=size(frag_chos_stage1);

%frag_chos_pointer(1,:)=frag_chos_stage1(3,:);
frag_chos_pointer(1,:)=frag_chos_stage1(modi2,:);
frag_chos_pointer(2,:)=frag_chos_stage1(modi3,:);
N_pointer=frag_chos_pointer(1,:);
C_pointer=frag_chos_pointer(2,:);

    
%stan_frag_chos_pointer(1,:)=stan_frag_chos(modi,:);
stan_frag_chos_pointer(1,:)=grid_car_final(modib,:);
stan_frag_chos_pointer(2,:)=grid_n4_final(modic,:);
stan_N_pointer=stan_frag_chos_pointer(1,:);
stan_C_pointer=stan_frag_chos_pointer(2,:);

size_frag_chos_pointer=size(frag_chos_pointer);

size_stan_frag_chos_pointer=size(stan_frag_chos_pointer);

frag_chos_stage3=rota51(N_pointer,C_pointer,stan_frag_chos,stan_N_pointer,stan_C_pointer,frag_chos_stage1,modia,modib,modic,modi1,modi2,modi3,protein_frame);
size_frag_chos_stage3=size(frag_chos_stage3);
if size_frag_chos_stage3(1,1)<size_frag_chos(1,1)
    continue
elseif size_frag_chos_stage3(1,2)==size_frag_chos(1,2)
    test_stage3=frag_chos_stage3;
    distj=judge_dist(test_stage3,frag_chos_stage1);
    if distj==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3;')  );
        asdf=asdf+1;
    end
elseif size_frag_chos_stage3(1,2)==2*size_frag_chos(1,2)
    test_stage3_1=frag_chos_stage3(:,1:13);
    test_stage3_2=frag_chos_stage3(:,14:26);
    distj1=judge_dist(test_stage3_1,frag_chos_stage1);
    distj2=judge_dist(test_stage3_2,frag_chos_stage1);
    if distj1==1&distj2~=1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
    elseif distj1~=1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    elseif  distj1==1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    end
    
end

        clear functions;
        pack
    end
    
end








if length(o3_car_n4_combo)~=0
    size_o3_car_n4_combo=size(o3_car_n4_combo);
    for daz=1:1:size_o3_car_n4_combo(1,1)
        modia=o3_car_n4_combo(daz,1);
        modi1=o3_atom(o3_car_n4_combo(daz,4),7);
        modib=o3_car_n4_combo(daz,2);
        modi2=car_atom(o3_car_n4_combo(daz,5),7);
        modic=o3_car_n4_combo(daz,3);
        modi3=n4_atom(o3_car_n4_combo(daz,6),7);
        
        
stan_frag_chos=grid_o3_final;
%stan_frag_chos(modib+20,:)=grid_car_final(modib,:);
%stan_frag_chos(modic+40,:)=grid_n4_final(modic,:);


x_move=stan_frag_chos(modia,1)-frag_chos(modi1,1);
y_move=stan_frag_chos(modia,2)-frag_chos(modi1,2);
z_move=stan_frag_chos(modia,3)-frag_chos(modi1,3);
clear frag_chos_stage1;
for i=1:1:size_frag_chos(1,1)
    frag_chos_stage1(i,1)=frag_chos(i,1)+x_move;
    frag_chos_stage1(i,2)=frag_chos(i,2)+y_move;
    frag_chos_stage1(i,3)=frag_chos(i,3)+z_move;
    frag_chos_stage1(i,4:13)=frag_chos(i,4:13);
end

size_frag_chos_stage1=size(frag_chos_stage1);

%frag_chos_pointer(1,:)=frag_chos_stage1(3,:);
frag_chos_pointer(1,:)=frag_chos_stage1(modi2,:);
frag_chos_pointer(2,:)=frag_chos_stage1(modi3,:);
N_pointer=frag_chos_pointer(1,:);
C_pointer=frag_chos_pointer(2,:);

    
%stan_frag_chos_pointer(1,:)=stan_frag_chos(modi,:);
stan_frag_chos_pointer(1,:)=grid_car_final(modib,:);
stan_frag_chos_pointer(2,:)=grid_n4_final(modic,:);
stan_N_pointer=stan_frag_chos_pointer(1,:);
stan_C_pointer=stan_frag_chos_pointer(2,:);

size_frag_chos_pointer=size(frag_chos_pointer);

size_stan_frag_chos_pointer=size(stan_frag_chos_pointer);

frag_chos_stage3=rota51(N_pointer,C_pointer,stan_frag_chos,stan_N_pointer,stan_C_pointer,frag_chos_stage1,modia,modib,modic,modi1,modi2,modi3,protein_frame);
size_frag_chos_stage3=size(frag_chos_stage3);
if size_frag_chos_stage3(1,1)<size_frag_chos(1,1)
    continue
elseif size_frag_chos_stage3(1,2)==size_frag_chos(1,2)
    test_stage3=frag_chos_stage3;
    distj=judge_dist(test_stage3,frag_chos_stage1);
    if distj==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3;')  );
        asdf=asdf+1;
    end
elseif size_frag_chos_stage3(1,2)==2*size_frag_chos(1,2)
    test_stage3_1=frag_chos_stage3(:,1:13);
    test_stage3_2=frag_chos_stage3(:,14:26);
    distj1=judge_dist(test_stage3_1,frag_chos_stage1);
    distj2=judge_dist(test_stage3_2,frag_chos_stage1);
    if distj1==1&distj2~=1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
    elseif distj1~=1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    elseif  distj1==1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    end
    
end

        clear functions;
        pack
    end
    
end



if length(o3_nam_car_combo)~=0
    size_o3_nam_car_combo=size(o3_nam_car_combo);
    for daz=1:1:size_o3_nam_car_combo(1,1)
        modia=o3_nam_car_combo(daz,1);
        modi1=o3_atom(o3_nam_car_combo(daz,4),7);
        modib=o3_nam_car_combo(daz,2);
        modi2=nam_atom(o3_nam_car_combo(daz,5),7);
        modic=o3_nam_car_combo(daz,3);
        modi3=car_atom(o3_nam_car_combo(daz,6),7);
        
        
stan_frag_chos=grid_o3_final;
%stan_frag_chos(modib+20,:)=grid_nam_final(modib,:);
%stan_frag_chos(modic+40,:)=grid_car_final(modic,:);


x_move=stan_frag_chos(modia,1)-frag_chos(modi1,1);
y_move=stan_frag_chos(modia,2)-frag_chos(modi1,2);
z_move=stan_frag_chos(modia,3)-frag_chos(modi1,3);
clear frag_chos_stage1;
for i=1:1:size_frag_chos(1,1)
    frag_chos_stage1(i,1)=frag_chos(i,1)+x_move;
    frag_chos_stage1(i,2)=frag_chos(i,2)+y_move;
    frag_chos_stage1(i,3)=frag_chos(i,3)+z_move;
    frag_chos_stage1(i,4:13)=frag_chos(i,4:13);
end

size_frag_chos_stage1=size(frag_chos_stage1);

%frag_chos_pointer(1,:)=frag_chos_stage1(3,:);
frag_chos_pointer(1,:)=frag_chos_stage1(modi2,:);
frag_chos_pointer(2,:)=frag_chos_stage1(modi3,:);
N_pointer=frag_chos_pointer(1,:);
C_pointer=frag_chos_pointer(2,:);

    
%stan_frag_chos_pointer(1,:)=stan_frag_chos(modi,:);
stan_frag_chos_pointer(1,:)=grid_nam_final(modib,:);
stan_frag_chos_pointer(2,:)=grid_car_final(modic,:);
stan_N_pointer=stan_frag_chos_pointer(1,:);
stan_C_pointer=stan_frag_chos_pointer(2,:);

size_frag_chos_pointer=size(frag_chos_pointer);

size_stan_frag_chos_pointer=size(stan_frag_chos_pointer);

frag_chos_stage3=rota51(N_pointer,C_pointer,stan_frag_chos,stan_N_pointer,stan_C_pointer,frag_chos_stage1,modia,modib,modic,modi1,modi2,modi3,protein_frame);
size_frag_chos_stage3=size(frag_chos_stage3);
if size_frag_chos_stage3(1,1)<size_frag_chos(1,1)
    continue
elseif size_frag_chos_stage3(1,2)==size_frag_chos(1,2)
    test_stage3=frag_chos_stage3;
    distj=judge_dist(test_stage3,frag_chos_stage1);
    if distj==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3;')  );
        asdf=asdf+1;
    end
elseif size_frag_chos_stage3(1,2)==2*size_frag_chos(1,2)
    test_stage3_1=frag_chos_stage3(:,1:13);
    test_stage3_2=frag_chos_stage3(:,14:26);
    distj1=judge_dist(test_stage3_1,frag_chos_stage1);
    distj2=judge_dist(test_stage3_2,frag_chos_stage1);
    if distj1==1&distj2~=1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
    elseif distj1~=1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    elseif  distj1==1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    end
    
end

        clear functions;
        pack
    end
    
end






if length(o3_npl3_car_combo)~=0
    size_o3_npl3_car_combo=size(o3_npl3_car_combo);
    for daz=1:1:size_o3_npl3_car_combo(1,1)
        modia=o3_npl3_car_combo(daz,1);
        modi1=o3_atom(o3_npl3_car_combo(daz,4),7);
        modib=o3_npl3_car_combo(daz,2);
        modi2=npl3_atom(o3_npl3_car_combo(daz,5),7);
        modic=o3_npl3_car_combo(daz,3);
        modi3=car_atom(o3_npl3_car_combo(daz,6),7);
        
        
stan_frag_chos=grid_o3_final;
%stan_frag_chos(modib+20,:)=grid_npl3_final(modib,:);
%stan_frag_chos(modic+40,:)=grid_car_final(modic,:);


x_move=stan_frag_chos(modia,1)-frag_chos(modi1,1);
y_move=stan_frag_chos(modia,2)-frag_chos(modi1,2);
z_move=stan_frag_chos(modia,3)-frag_chos(modi1,3);
clear frag_chos_stage1;
for i=1:1:size_frag_chos(1,1)
    frag_chos_stage1(i,1)=frag_chos(i,1)+x_move;
    frag_chos_stage1(i,2)=frag_chos(i,2)+y_move;
    frag_chos_stage1(i,3)=frag_chos(i,3)+z_move;
    frag_chos_stage1(i,4:13)=frag_chos(i,4:13);
end

size_frag_chos_stage1=size(frag_chos_stage1);

%frag_chos_pointer(1,:)=frag_chos_stage1(3,:);
frag_chos_pointer(1,:)=frag_chos_stage1(modi2,:);
frag_chos_pointer(2,:)=frag_chos_stage1(modi3,:);
N_pointer=frag_chos_pointer(1,:);
C_pointer=frag_chos_pointer(2,:);

    
%stan_frag_chos_pointer(1,:)=stan_frag_chos(modi,:);
stan_frag_chos_pointer(1,:)=grid_npl3_final(modib,:);
stan_frag_chos_pointer(2,:)=grid_car_final(modic,:);
stan_N_pointer=stan_frag_chos_pointer(1,:);
stan_C_pointer=stan_frag_chos_pointer(2,:);

size_frag_chos_pointer=size(frag_chos_pointer);

size_stan_frag_chos_pointer=size(stan_frag_chos_pointer);

frag_chos_stage3=rota51(N_pointer,C_pointer,stan_frag_chos,stan_N_pointer,stan_C_pointer,frag_chos_stage1,modia,modib,modic,modi1,modi2,modi3,protein_frame);
size_frag_chos_stage3=size(frag_chos_stage3);
if size_frag_chos_stage3(1,1)<size_frag_chos(1,1)
    continue
elseif size_frag_chos_stage3(1,2)==size_frag_chos(1,2)
    test_stage3=frag_chos_stage3;
    distj=judge_dist(test_stage3,frag_chos_stage1);
    if distj==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3;')  );
        asdf=asdf+1;
    end
elseif size_frag_chos_stage3(1,2)==2*size_frag_chos(1,2)
    test_stage3_1=frag_chos_stage3(:,1:13);
    test_stage3_2=frag_chos_stage3(:,14:26);
    distj1=judge_dist(test_stage3_1,frag_chos_stage1);
    distj2=judge_dist(test_stage3_2,frag_chos_stage1);
    if distj1==1&distj2~=1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
    elseif distj1~=1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    elseif  distj1==1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    end
    
end

        clear functions;
        pack
    end
    
end



if length(nam_car_n4_combo)~=0
    size_nam_car_n4_combo=size(nam_car_n4_combo);
    for daz=1:1:size_nam_car_n4_combo(1,1)
        modia=nam_car_n4_combo(daz,1);
        modi1=nam_atom(nam_car_n4_combo(daz,4),7);
        modib=nam_car_n4_combo(daz,2);
        modi2=car_atom(nam_car_n4_combo(daz,5),7);
        modic=nam_car_n4_combo(daz,3);
        modi3=n4_atom(nam_car_n4_combo(daz,6),7);
        
        
stan_frag_chos=grid_nam_final;
%stan_frag_chos(modib+20,:)=grid_car_final(modib,:);
%stan_frag_chos(modic+40,:)=grid_n4_final(modic,:);


x_move=stan_frag_chos(modia,1)-frag_chos(modi1,1);
y_move=stan_frag_chos(modia,2)-frag_chos(modi1,2);
z_move=stan_frag_chos(modia,3)-frag_chos(modi1,3);
clear frag_chos_stage1;
for i=1:1:size_frag_chos(1,1)
    frag_chos_stage1(i,1)=frag_chos(i,1)+x_move;
    frag_chos_stage1(i,2)=frag_chos(i,2)+y_move;
    frag_chos_stage1(i,3)=frag_chos(i,3)+z_move;
    frag_chos_stage1(i,4:13)=frag_chos(i,4:13);
end

size_frag_chos_stage1=size(frag_chos_stage1);

%frag_chos_pointer(1,:)=frag_chos_stage1(3,:);
frag_chos_pointer(1,:)=frag_chos_stage1(modi2,:);
frag_chos_pointer(2,:)=frag_chos_stage1(modi3,:);
N_pointer=frag_chos_pointer(1,:);
C_pointer=frag_chos_pointer(2,:);

    
%stan_frag_chos_pointer(1,:)=stan_frag_chos(modi,:);
stan_frag_chos_pointer(1,:)=grid_car_final(modib,:);
stan_frag_chos_pointer(2,:)=grid_n4_final(modic,:);
stan_N_pointer=stan_frag_chos_pointer(1,:);
stan_C_pointer=stan_frag_chos_pointer(2,:);

size_frag_chos_pointer=size(frag_chos_pointer);

size_stan_frag_chos_pointer=size(stan_frag_chos_pointer);

frag_chos_stage3=rota51(N_pointer,C_pointer,stan_frag_chos,stan_N_pointer,stan_C_pointer,frag_chos_stage1,modia,modib,modic,modi1,modi2,modi3,protein_frame);
size_frag_chos_stage3=size(frag_chos_stage3);
if size_frag_chos_stage3(1,1)<size_frag_chos(1,1)
    continue
elseif size_frag_chos_stage3(1,2)==size_frag_chos(1,2)
    test_stage3=frag_chos_stage3;
    distj=judge_dist(test_stage3,frag_chos_stage1);
    if distj==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3;')  );
        asdf=asdf+1;
    end
elseif size_frag_chos_stage3(1,2)==2*size_frag_chos(1,2)
    test_stage3_1=frag_chos_stage3(:,1:13);
    test_stage3_2=frag_chos_stage3(:,14:26);
    distj1=judge_dist(test_stage3_1,frag_chos_stage1);
    distj2=judge_dist(test_stage3_2,frag_chos_stage1);
    if distj1==1&distj2~=1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
    elseif distj1~=1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    elseif  distj1==1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    end
    
end

        clear functions;
        pack
    end
    
end



if length(nam_npl3_car_combo)~=0
    size_nam_npl3_car_combo=size(nam_npl3_car_combo);
    for daz=1:1:size_nam_npl3_car_combo(1,1)
        modia=nam_npl3_car_combo(daz,1);
        modi1=nam_atom(nam_npl3_car_combo(daz,4),7);
        modib=nam_npl3_car_combo(daz,2);
        modi2=npl3_atom(nam_npl3_car_combo(daz,5),7);
        modic=nam_npl3_car_combo(daz,3);
        modi3=car_atom(nam_npl3_car_combo(daz,6),7);
        
        
stan_frag_chos=grid_nam_final;
%stan_frag_chos(modib+20,:)=grid_npl3_final(modib,:);
%stan_frag_chos(modic+40,:)=grid_car_final(modic,:);


x_move=stan_frag_chos(modia,1)-frag_chos(modi1,1);
y_move=stan_frag_chos(modia,2)-frag_chos(modi1,2);
z_move=stan_frag_chos(modia,3)-frag_chos(modi1,3);
clear frag_chos_stage1;
for i=1:1:size_frag_chos(1,1)
    frag_chos_stage1(i,1)=frag_chos(i,1)+x_move;
    frag_chos_stage1(i,2)=frag_chos(i,2)+y_move;
    frag_chos_stage1(i,3)=frag_chos(i,3)+z_move;
    frag_chos_stage1(i,4:13)=frag_chos(i,4:13);
end

size_frag_chos_stage1=size(frag_chos_stage1);

%frag_chos_pointer(1,:)=frag_chos_stage1(3,:);
frag_chos_pointer(1,:)=frag_chos_stage1(modi2,:);
frag_chos_pointer(2,:)=frag_chos_stage1(modi3,:);
N_pointer=frag_chos_pointer(1,:);
C_pointer=frag_chos_pointer(2,:);

    
%stan_frag_chos_pointer(1,:)=stan_frag_chos(modi,:);
stan_frag_chos_pointer(1,:)=grid_npl3_final(modib,:);
stan_frag_chos_pointer(2,:)=grid_car_final(modic,:);
stan_N_pointer=stan_frag_chos_pointer(1,:);
stan_C_pointer=stan_frag_chos_pointer(2,:);

size_frag_chos_pointer=size(frag_chos_pointer);

size_stan_frag_chos_pointer=size(stan_frag_chos_pointer);

frag_chos_stage3=rota51(N_pointer,C_pointer,stan_frag_chos,stan_N_pointer,stan_C_pointer,frag_chos_stage1,modia,modib,modic,modi1,modi2,modi3,protein_frame);
size_frag_chos_stage3=size(frag_chos_stage3);
if size_frag_chos_stage3(1,1)<size_frag_chos(1,1)
    continue
elseif size_frag_chos_stage3(1,2)==size_frag_chos(1,2)
    test_stage3=frag_chos_stage3;
    distj=judge_dist(test_stage3,frag_chos_stage1);
    if distj==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3;')  );
        asdf=asdf+1;
    end
elseif size_frag_chos_stage3(1,2)==2*size_frag_chos(1,2)
    test_stage3_1=frag_chos_stage3(:,1:13);
    test_stage3_2=frag_chos_stage3(:,14:26);
    distj1=judge_dist(test_stage3_1,frag_chos_stage1);
    distj2=judge_dist(test_stage3_2,frag_chos_stage1);
    if distj1==1&distj2~=1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
    elseif distj1~=1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    elseif  distj1==1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    end
    
end

        clear functions;
        pack
    end
    
end



if length(car_npl3_n4_combo)~=0
    size_car_npl3_n4_combo=size(car_npl3_n4_combo);
    for daz=1:1:size_car_npl3_n4_combo(1,1)
        modia=car_npl3_n4_combo(daz,1);
        modi1=car_atom(car_npl3_n4_combo(daz,4),7);
        modib=car_npl3_n4_combo(daz,2);
        modi2=npl3_atom(car_npl3_n4_combo(daz,5),7);
        modic=car_npl3_n4_combo(daz,3);
        modi3=n4_atom(car_npl3_n4_combo(daz,6),7);
        
        
stan_frag_chos=grid_car_final;
%stan_frag_chos(modib+20,:)=grid_npl3_final(modib,:);
%stan_frag_chos(modic+40,:)=grid_n4_final(modic,:);


x_move=stan_frag_chos(modia,1)-frag_chos(modi1,1);
y_move=stan_frag_chos(modia,2)-frag_chos(modi1,2);
z_move=stan_frag_chos(modia,3)-frag_chos(modi1,3);
clear frag_chos_stage1;
for i=1:1:size_frag_chos(1,1)
    frag_chos_stage1(i,1)=frag_chos(i,1)+x_move;
    frag_chos_stage1(i,2)=frag_chos(i,2)+y_move;
    frag_chos_stage1(i,3)=frag_chos(i,3)+z_move;
    frag_chos_stage1(i,4:13)=frag_chos(i,4:13);
end

size_frag_chos_stage1=size(frag_chos_stage1);

%frag_chos_pointer(1,:)=frag_chos_stage1(3,:);
frag_chos_pointer(1,:)=frag_chos_stage1(modi2,:);
frag_chos_pointer(2,:)=frag_chos_stage1(modi3,:);
N_pointer=frag_chos_pointer(1,:);
C_pointer=frag_chos_pointer(2,:);

    
%stan_frag_chos_pointer(1,:)=stan_frag_chos(modi,:);
stan_frag_chos_pointer(1,:)=grid_npl3_final(modib,:);
stan_frag_chos_pointer(2,:)=grid_n4_final(modic,:);
stan_N_pointer=stan_frag_chos_pointer(1,:);
stan_C_pointer=stan_frag_chos_pointer(2,:);

size_frag_chos_pointer=size(frag_chos_pointer);

size_stan_frag_chos_pointer=size(stan_frag_chos_pointer);

frag_chos_stage3=rota51(N_pointer,C_pointer,stan_frag_chos,stan_N_pointer,stan_C_pointer,frag_chos_stage1,modia,modib,modic,modi1,modi2,modi3,protein_frame);
size_frag_chos_stage3=size(frag_chos_stage3);
if size_frag_chos_stage3(1,1)<size_frag_chos(1,1)
    continue
elseif size_frag_chos_stage3(1,2)==size_frag_chos(1,2)
    test_stage3=frag_chos_stage3;
    distj=judge_dist(test_stage3,frag_chos_stage1);
    if distj==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3;')  );
        asdf=asdf+1;
    end
elseif size_frag_chos_stage3(1,2)==2*size_frag_chos(1,2)
    test_stage3_1=frag_chos_stage3(:,1:13);
    test_stage3_2=frag_chos_stage3(:,14:26);
    distj1=judge_dist(test_stage3_1,frag_chos_stage1);
    distj2=judge_dist(test_stage3_2,frag_chos_stage1);
    if distj1==1&distj2~=1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
    elseif distj1~=1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    elseif  distj1==1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    end
    
end

        clear functions;
        pack
    end
    
end



if length(o2_car_o3_combo)~=0
    size_o2_car_o3_combo=size(o2_car_o3_combo);
    for daz=1:1:size_o2_car_o3_combo(1,1)
        modia=o2_car_o3_combo(daz,1);
        modi1=o2_atom(o2_car_o3_combo(daz,4),7);
        modib=o2_car_o3_combo(daz,2);
        modi2=car_atom(o2_car_o3_combo(daz,5),7);
        modic=o2_car_o3_combo(daz,3);
        modi3=o3_atom(o2_car_o3_combo(daz,6),7);
        
        
stan_frag_chos=grid_o2_final;
%stan_frag_chos(modib+20,:)=grid_car_final(modib,:);
%stan_frag_chos(modic+40,:)=grid_o3_final(modic,:);


x_move=stan_frag_chos(modia,1)-frag_chos(modi1,1);
y_move=stan_frag_chos(modia,2)-frag_chos(modi1,2);
z_move=stan_frag_chos(modia,3)-frag_chos(modi1,3);
clear frag_chos_stage1;
for i=1:1:size_frag_chos(1,1)
    frag_chos_stage1(i,1)=frag_chos(i,1)+x_move;
    frag_chos_stage1(i,2)=frag_chos(i,2)+y_move;
    frag_chos_stage1(i,3)=frag_chos(i,3)+z_move;
    frag_chos_stage1(i,4:13)=frag_chos(i,4:13);
end

size_frag_chos_stage1=size(frag_chos_stage1);

%frag_chos_pointer(1,:)=frag_chos_stage1(3,:);
frag_chos_pointer(1,:)=frag_chos_stage1(modi2,:);
frag_chos_pointer(2,:)=frag_chos_stage1(modi3,:);
N_pointer=frag_chos_pointer(1,:);
C_pointer=frag_chos_pointer(2,:);

    
%stan_frag_chos_pointer(1,:)=stan_frag_chos(modi,:);
stan_frag_chos_pointer(1,:)=grid_car_final(modib,:);
stan_frag_chos_pointer(2,:)=grid_o3_final(modic,:);
stan_N_pointer=stan_frag_chos_pointer(1,:);
stan_C_pointer=stan_frag_chos_pointer(2,:);

size_frag_chos_pointer=size(frag_chos_pointer);

size_stan_frag_chos_pointer=size(stan_frag_chos_pointer);

frag_chos_stage3=rota51(N_pointer,C_pointer,stan_frag_chos,stan_N_pointer,stan_C_pointer,frag_chos_stage1,modia,modib,modic,modi1,modi2,modi3,protein_frame);
size_frag_chos_stage3=size(frag_chos_stage3);
if size_frag_chos_stage3(1,1)<size_frag_chos(1,1)
    continue
elseif size_frag_chos_stage3(1,2)==size_frag_chos(1,2)
    test_stage3=frag_chos_stage3;
    distj=judge_dist(test_stage3,frag_chos_stage1);
    if distj==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3;')  );
        asdf=asdf+1;
    end
elseif size_frag_chos_stage3(1,2)==2*size_frag_chos(1,2)
    test_stage3_1=frag_chos_stage3(:,1:13);
    test_stage3_2=frag_chos_stage3(:,14:26);
    distj1=judge_dist(test_stage3_1,frag_chos_stage1);
    distj2=judge_dist(test_stage3_2,frag_chos_stage1);
    if distj1==1&distj2~=1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
    elseif distj1~=1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    elseif  distj1==1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    end
    
end

        clear functions;
        pack
    end
    
end



if length(o2_o2_car_combo)~=0
    size_o2_o2_car_combo=size(o2_o2_car_combo);
    for daz=1:1:size_o2_o2_car_combo(1,1)
        modia=o2_o2_car_combo(daz,1);
        modi1=o2_atom(o2_o2_car_combo(daz,4),7);
        modib=o2_o2_car_combo(daz,2);
        modi2=o2_atom(o2_o2_car_combo(daz,5),7);
        modic=o2_o2_car_combo(daz,3);
        modi3=car_atom(o2_o2_car_combo(daz,6),7);
        
        
        stan_frag_chos=grid_o2_final;
%stan_frag_chos(modib,:)=grid_o2_final(modib,:);
%stan_frag_chos(modic+20,:)=grid_car_final(modic,:);


x_move=stan_frag_chos(modia,1)-frag_chos(modi1,1);
y_move=stan_frag_chos(modia,2)-frag_chos(modi1,2);
z_move=stan_frag_chos(modia,3)-frag_chos(modi1,3);
clear frag_chos_stage1;
for i=1:1:size_frag_chos(1,1)
    frag_chos_stage1(i,1)=frag_chos(i,1)+x_move;
    frag_chos_stage1(i,2)=frag_chos(i,2)+y_move;
    frag_chos_stage1(i,3)=frag_chos(i,3)+z_move;
    frag_chos_stage1(i,4:13)=frag_chos(i,4:13);
end

size_frag_chos_stage1=size(frag_chos_stage1);

%frag_chos_pointer(1,:)=frag_chos_stage1(3,:);
frag_chos_pointer(1,:)=frag_chos_stage1(modi2,:);
frag_chos_pointer(2,:)=frag_chos_stage1(modi3,:);
N_pointer=frag_chos_pointer(1,:);
C_pointer=frag_chos_pointer(2,:);

    
%stan_frag_chos_pointer(1,:)=stan_frag_chos(modi,:);
stan_frag_chos_pointer(1,:)=grid_o2_final(modib,:);
stan_frag_chos_pointer(2,:)=grid_car_final(modic,:);
stan_N_pointer=stan_frag_chos_pointer(1,:);
stan_C_pointer=stan_frag_chos_pointer(2,:);

size_frag_chos_pointer=size(frag_chos_pointer);

size_stan_frag_chos_pointer=size(stan_frag_chos_pointer);

frag_chos_stage3=rota51(N_pointer,C_pointer,stan_frag_chos,stan_N_pointer,stan_C_pointer,frag_chos_stage1,modia,modib,modic,modi1,modi2,modi3,protein_frame);
size_frag_chos_stage3=size(frag_chos_stage3);
if size_frag_chos_stage3(1,1)<size_frag_chos(1,1)
    continue
elseif size_frag_chos_stage3(1,2)==size_frag_chos(1,2)
    test_stage3=frag_chos_stage3;
    distj=judge_dist(test_stage3,frag_chos_stage1);
    if distj==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3;')  );
        asdf=asdf+1;
    end
elseif size_frag_chos_stage3(1,2)==2*size_frag_chos(1,2)
    test_stage3_1=frag_chos_stage3(:,1:13);
    test_stage3_2=frag_chos_stage3(:,14:26);
    distj1=judge_dist(test_stage3_1,frag_chos_stage1);
    distj2=judge_dist(test_stage3_2,frag_chos_stage1);
    if distj1==1&distj2~=1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
    elseif distj1~=1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    elseif  distj1==1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    end
    
end


clear functions;

pack





    end
    
    
    
    
    
    
    
    
    
end
      
      

if length(o3_o3_car_combo)~=0
    size_o3_o3_car_combo=size(o3_o3_car_combo);
    for daz=1:1:size_o3_o3_car_combo(1,1)
        modia=o3_o3_car_combo(daz,1);
        modi1=o3_atom(o3_o3_car_combo(daz,4),7);
        modib=o3_o3_car_combo(daz,2);
        modi2=o3_atom(o3_o3_car_combo(daz,5),7);
        modic=o3_o3_car_combo(daz,3);
        modi3=car_atom(o3_o3_car_combo(daz,6),7);
        
        
        stan_frag_chos=grid_o3_final;
%stan_frag_chos(modib,:)=grid_o3_final(modib,:);
%stan_frag_chos(modic+20,:)=grid_car_final(modic,:);


x_move=stan_frag_chos(modia,1)-frag_chos(modi1,1);
y_move=stan_frag_chos(modia,2)-frag_chos(modi1,2);
z_move=stan_frag_chos(modia,3)-frag_chos(modi1,3);
clear frag_chos_stage1;
for i=1:1:size_frag_chos(1,1)
    frag_chos_stage1(i,1)=frag_chos(i,1)+x_move;
    frag_chos_stage1(i,2)=frag_chos(i,2)+y_move;
    frag_chos_stage1(i,3)=frag_chos(i,3)+z_move;
    frag_chos_stage1(i,4:13)=frag_chos(i,4:13);
end

size_frag_chos_stage1=size(frag_chos_stage1);

%frag_chos_pointer(1,:)=frag_chos_stage1(3,:);
frag_chos_pointer(1,:)=frag_chos_stage1(modi2,:);
frag_chos_pointer(2,:)=frag_chos_stage1(modi3,:);
N_pointer=frag_chos_pointer(1,:);
C_pointer=frag_chos_pointer(2,:);

    
%stan_frag_chos_pointer(1,:)=stan_frag_chos(modi,:);
stan_frag_chos_pointer(1,:)=grid_o3_final(modib,:);
stan_frag_chos_pointer(2,:)=grid_car_final(modic,:);
stan_N_pointer=stan_frag_chos_pointer(1,:);
stan_C_pointer=stan_frag_chos_pointer(2,:);

size_frag_chos_pointer=size(frag_chos_pointer);

size_stan_frag_chos_pointer=size(stan_frag_chos_pointer);

frag_chos_stage3=rota51(N_pointer,C_pointer,stan_frag_chos,stan_N_pointer,stan_C_pointer,frag_chos_stage1,modia,modib,modic,modi1,modi2,modi3,protein_frame);
size_frag_chos_stage3=size(frag_chos_stage3);
if size_frag_chos_stage3(1,1)<size_frag_chos(1,1)
    continue
elseif size_frag_chos_stage3(1,2)==size_frag_chos(1,2)
    test_stage3=frag_chos_stage3;
    distj=judge_dist(test_stage3,frag_chos_stage1);
    if distj==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3;')  );
        asdf=asdf+1;
    end
elseif size_frag_chos_stage3(1,2)==2*size_frag_chos(1,2)
    test_stage3_1=frag_chos_stage3(:,1:13);
    test_stage3_2=frag_chos_stage3(:,14:26);
    distj1=judge_dist(test_stage3_1,frag_chos_stage1);
    distj2=judge_dist(test_stage3_2,frag_chos_stage1);
    if distj1==1&distj2~=1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
    elseif distj1~=1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    elseif  distj1==1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    end
    
end


clear functions;

pack





    end
    
    
    
    
    
    
    
    
    
end
      

if length(nam_nam_car_combo)~=0
    size_nam_nam_car_combo=size(nam_nam_car_combo);
    for daz=1:1:size_nam_nam_car_combo(1,1)
        modia=nam_nam_car_combo(daz,1);
        modi1=nam_atom(nam_nam_car_combo(daz,4),7);
        modib=nam_nam_car_combo(daz,2);
        modi2=nam_atom(nam_nam_car_combo(daz,5),7);
        modic=nam_nam_car_combo(daz,3);
        modi3=car_atom(nam_nam_car_combo(daz,6),7);
        
        
        stan_frag_chos=grid_nam_final;
%stan_frag_chos(modib,:)=grid_nam_final(modib,:);
%stan_frag_chos(modic+20,:)=grid_car_final(modic,:);


x_move=stan_frag_chos(modia,1)-frag_chos(modi1,1);
y_move=stan_frag_chos(modia,2)-frag_chos(modi1,2);
z_move=stan_frag_chos(modia,3)-frag_chos(modi1,3);
clear frag_chos_stage1;
for i=1:1:size_frag_chos(1,1)
    frag_chos_stage1(i,1)=frag_chos(i,1)+x_move;
    frag_chos_stage1(i,2)=frag_chos(i,2)+y_move;
    frag_chos_stage1(i,3)=frag_chos(i,3)+z_move;
    frag_chos_stage1(i,4:13)=frag_chos(i,4:13);
end

size_frag_chos_stage1=size(frag_chos_stage1);

%frag_chos_pointer(1,:)=frag_chos_stage1(3,:);
frag_chos_pointer(1,:)=frag_chos_stage1(modi2,:);
frag_chos_pointer(2,:)=frag_chos_stage1(modi3,:);
N_pointer=frag_chos_pointer(1,:);
C_pointer=frag_chos_pointer(2,:);

    
%stan_frag_chos_pointer(1,:)=stan_frag_chos(modi,:);
stan_frag_chos_pointer(1,:)=grid_nam_final(modib,:);
stan_frag_chos_pointer(2,:)=grid_car_final(modic,:);
stan_N_pointer=stan_frag_chos_pointer(1,:);
stan_C_pointer=stan_frag_chos_pointer(2,:);

size_frag_chos_pointer=size(frag_chos_pointer);

size_stan_frag_chos_pointer=size(stan_frag_chos_pointer);

frag_chos_stage3=rota51(N_pointer,C_pointer,stan_frag_chos,stan_N_pointer,stan_C_pointer,frag_chos_stage1,modia,modib,modic,modi1,modi2,modi3,protein_frame);
size_frag_chos_stage3=size(frag_chos_stage3);
if size_frag_chos_stage3(1,1)<size_frag_chos(1,1)
    continue
elseif size_frag_chos_stage3(1,2)==size_frag_chos(1,2)
    test_stage3=frag_chos_stage3;
    distj=judge_dist(test_stage3,frag_chos_stage1);
    if distj==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3;')  );
        asdf=asdf+1;
    end
elseif size_frag_chos_stage3(1,2)==2*size_frag_chos(1,2)
    test_stage3_1=frag_chos_stage3(:,1:13);
    test_stage3_2=frag_chos_stage3(:,14:26);
    distj1=judge_dist(test_stage3_1,frag_chos_stage1);
    distj2=judge_dist(test_stage3_2,frag_chos_stage1);
    if distj1==1&distj2~=1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
    elseif distj1~=1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    elseif  distj1==1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    end
    
end
        


clear functions;

pack





    end
    
    
    
    
    
    
    
    
    
end
      
      

if length(npl3_npl3_car_combo)~=0
    size_npl3_npl3_car_combo=size(npl3_npl3_car_combo);
    for daz=1:1:size_npl3_npl3_car_combo(1,1)
        modia=npl3_npl3_car_combo(daz,1);
        modi1=npl3_atom(npl3_npl3_car_combo(daz,4),7);
        modib=npl3_npl3_car_combo(daz,2);
        modi2=npl3_atom(npl3_npl3_car_combo(daz,5),7);
        modic=npl3_npl3_car_combo(daz,3);
        modi3=car_atom(npl3_npl3_car_combo(daz,6),7);
        
        
        stan_frag_chos=grid_npl3_final;
%stan_frag_chos(modib,:)=grid_npl3_final(modib,:);
%stan_frag_chos(modic+20,:)=grid_car_final(modic,:);


x_move=stan_frag_chos(modia,1)-frag_chos(modi1,1);
y_move=stan_frag_chos(modia,2)-frag_chos(modi1,2);
z_move=stan_frag_chos(modia,3)-frag_chos(modi1,3);
clear frag_chos_stage1;
for i=1:1:size_frag_chos(1,1)
    frag_chos_stage1(i,1)=frag_chos(i,1)+x_move;
    frag_chos_stage1(i,2)=frag_chos(i,2)+y_move;
    frag_chos_stage1(i,3)=frag_chos(i,3)+z_move;
    frag_chos_stage1(i,4:13)=frag_chos(i,4:13);
end

size_frag_chos_stage1=size(frag_chos_stage1);

%frag_chos_pointer(1,:)=frag_chos_stage1(3,:);
frag_chos_pointer(1,:)=frag_chos_stage1(modi2,:);
frag_chos_pointer(2,:)=frag_chos_stage1(modi3,:);
N_pointer=frag_chos_pointer(1,:);
C_pointer=frag_chos_pointer(2,:);

    
%stan_frag_chos_pointer(1,:)=stan_frag_chos(modi,:);
stan_frag_chos_pointer(1,:)=grid_npl3_final(modib,:);
stan_frag_chos_pointer(2,:)=grid_car_final(modic,:);
stan_N_pointer=stan_frag_chos_pointer(1,:);
stan_C_pointer=stan_frag_chos_pointer(2,:);

size_frag_chos_pointer=size(frag_chos_pointer);

size_stan_frag_chos_pointer=size(stan_frag_chos_pointer);

frag_chos_stage3=rota51(N_pointer,C_pointer,stan_frag_chos,stan_N_pointer,stan_C_pointer,frag_chos_stage1,modia,modib,modic,modi1,modi2,modi3,protein_frame);
size_frag_chos_stage3=size(frag_chos_stage3);
if size_frag_chos_stage3(1,1)<size_frag_chos(1,1)
    continue
elseif size_frag_chos_stage3(1,2)==size_frag_chos(1,2)
    test_stage3=frag_chos_stage3;
    distj=judge_dist(test_stage3,frag_chos_stage1);
    if distj==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3;')  );
        asdf=asdf+1;
    end
elseif size_frag_chos_stage3(1,2)==2*size_frag_chos(1,2)
    test_stage3_1=frag_chos_stage3(:,1:13);
    test_stage3_2=frag_chos_stage3(:,14:26);
    distj1=judge_dist(test_stage3_1,frag_chos_stage1);
    distj2=judge_dist(test_stage3_2,frag_chos_stage1);
    if distj1==1&distj2~=1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
    elseif distj1~=1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    elseif  distj1==1&distj2==1
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,1:13);')  );
        asdf=asdf+1;
        eval( strcat(  'best_struc_',num2str(asdf),'_1=frag_chos_stage3(:,14:26);')  );
        asdf=asdf+1;
    end
    
end

        


clear functions;

pack





    end
    
    
    
    
    
    
    
    
    
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


ap=1;
for z1=1:1:asdf-2
    for z2=z1+1:1:asdf-1
    eval( strcat(  'comp1=best_struc_',num2str(z1),'_1;')  );
    eval( strcat(  'comp2=best_struc_',num2str(z2),'_1;')  );
    if sum(sum(comp1))~=0&sum(sum(comp2))~=0
        rmsd=rmsd_comp(comp1,comp2);
        if rmsd<2
            eval( strcat(  'best_struc_',num2str(z2),'_1=0;')  );
        end
    end
    
    end
end

%out_list=['1li2'];

%for z1=1:1:asdf-1
%    eval( strcat(  'input_struc=best_struc_',num2str(z1),'_1;')  );
%    size_input_struc=size(input_struc);
%    if size_input_struc(1,1)~=1
%        out=lig_output(input_struc,out_list,z1);
%    end
%end
        










%for z1=1:1:asdf-1
%    eval( strcat(  'comp=best_struc_',num2str(z1),';')  );
%    if sum(sum(comp))==0
%        eval( strcat(  'clear best_struc_',num2str(z1),';')  );
%    end
%end


%fdsa=1;
jis=0
frn=frag_num;
frag2=1;
frag22=frag2;

Ind_table(1,1:frag_num)=0

while frn<=frag_num

    if frn<=3
        frag21=1;
    elseif frn>3
        frag21=frag22+1;
    end

    if frn<=2
        frag22=1;
    elseif frn>2
        frag22=frag2-1;
    end
    
    
%frag1(1:frag_num,1)=1;
%while frag1<=asdf-1
for frag1=1:1:asdf-1
    
    
    
    for ifr2=frag21:1:frag22
jis=jis+1;
    mid_best_struc_1_1=[];

if frn==2
eval( strcat(  'input_struc=best_struc_',num2str(frag1),'_',num2str(ifr2),';')  );
elseif frn>2
eval( strcat(  'input_struc=mid_best_struc_',num2str(frag1),'_',num2str(ifr2),';')  );
end
size_input_struc=size(input_struc);

clear input_struc_1 input_struc_2;
if size_input_struc(1,2)==13
    input_struc_1=input_struc(:,1:13);
    input_struc_1(:,7)=input_struc_1(:,9);
    input_struc_1(:,9)=1:size_input_struc(1,1);
    
    
elseif size_input_struc(1,2)==26
    input_struc_1=input_struc(:,1:13);
    input_struc_1(:,7)=input_struc_1(:,9);
    input_struc_1(:,9)=1:size_input_struc(1,1);
    input_struc_2=input_struc(:,14:26);
    input_struc_2(:,7)=input_struc_2(:,9);
    input_struc_2(:,9)=1:size_input_struc(1,1);
else
    continue
end







size_input_struc_1=size(input_struc_1);

size_input_struc_2=size_input_struc_1;



connect_struc=input_struc_1(input_struc_1(:,12)==1,:);

size_connect_struc=size(connect_struc);



double_bridge_c3=[];
ab=1;
for i=size_contact_type_A(1,1)
    for j=1:1:size_connect_struc(1,1)-1
        for k=j+1:1:size_connect_struc(1,1)
            if (connect_struc(j,7)==contact_type_A(i,1)&connect_struc(k,7)==contact_type_A(i,2))|(connect_struc(k,7)==contact_type_A(i,1)&connect_struc(j,7)==contact_type_A(i,2))
                double_bridge_c3(ab,:)=connect_struc(j,:);
                double_bridge_c3(ab+1,:)=connect_struc(k,:);
                double_bridge_c3(ab,6)=ab;
                double_bridge_c3(ab+1,6)=ab;
                ab=ab+2;
            end
        end
    end
end
    
size_double_bridge_c3=size(double_bridge_c3);
    
single_bridge_c3=[];
single_bridge_c3=connect_struc;
for i=1:1:size_connect_struc(1,1)
    for j=1:1:size_double_bridge_c3(1,1)
        if double_bridge_c3(j,7)==single_bridge_c3(i,7)
            single_bridge_c3(i,:)=0;
        end
    end
end
single_bridge_c3(single_bridge_c3(:,1)==0,:)=[];
size_single_bridge_c3=size(single_bridge_c3);


at=1;
att=1;
double_midfrag_point=[];
double_midfrag_point_atom=[];
for i=1:2:size_double_bridge_c3(1,1)-1
    for j=1:1:size_A_frag(1,1)-1
        for k=j+1:1:size_A_frag(1,1)
            if ((A_frag(j,7)==double_bridge_c3(i,7)&A_frag(k,7)==double_bridge_c3(i+1,7))|(A_frag(k,7)==double_bridge_c3(i,7)&A_frag(j,7)==double_bridge_c3(i+1,7)))&A_frag(j,13)==A_frag(k,13)&A_frag(j,13)~=double_bridge_c3(i,13)
                double_midfrag_point(at,1)=A_frag(j,13);
                double_midfrag_point_atom(att,:)=double_bridge_c3(i,:);
                double_midfrag_point_atom(att+1,:)=double_bridge_c3(i+1,:);
                at=at+1;
                att=att+2;
            end
        end
    end
end

size_double_midfrag_point=size(double_midfrag_point);

sp=1;
single_midfrag_point_atom=[];
for i=1:1:size_single_bridge_c3(1,1)
    for j=1:1:size_A_frag(1,1)
        if A_frag(j,7)==single_bridge_c3(i,7)&A_frag(j,13)~=single_bridge_c3(i,13)
            single_midfrag_point_atom(sp,:)=A_frag(j,:);
            single_midfrag_point_atom(sp,6)=i;
            sp=sp+1;
        end
    end
end

size_single_midfrag_point_atom=size(single_midfrag_point_atom);




%ligand(:,10)==>atom_bond_number   %ligand(:,11)==>atom_chosen_marker
%ligand(:,12)==>c3_keyatom  ligand(:,13)==>frag_number
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%single_bridge_c3

%find alpha atom of input_struc_1


input_alpha=[];
for i=1:1:size_single_bridge_c3(1,1)
    for j=1:1:size_contact_type_A(1,1)
        for k=1:1:size_input_struc_1(1,1)
            if (single_bridge_c3(i,7)==contact_type_A(j,1)&input_struc_1(k,7)==contact_type_A(j,2))|(input_struc_1(k,7)==contact_type_A(j,1)&single_bridge_c3(i,7)==contact_type_A(j,2))
                input_alpha(i,:)=input_struc_1(k,:);
                break
            end
        end
    end
end
size_input_alpha=size(input_alpha);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%single_bridge_c3_modeling%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for laoz=1:1:size_single_bridge_c3(1,1)
%laoz=1;
old_input_alpha=[];
midfrag_alpha=[];
old_input_alpha(laoz,:)=A_frag(A_frag(:,7)==input_alpha(laoz,7)&A_frag(:,13)==input_alpha(laoz,13),:);
single_midfrag_point_atom_c=single_midfrag_point_atom(single_midfrag_point_atom(:,6)==laoz,:);
size_single_midfrag_point_atom_c=size(single_midfrag_point_atom_c);

    for zlao=1:1:size_single_midfrag_point_atom_c(1,1)
    %if laoz==1|single_bridge_c3(laoz,10)<3
        
        
        %midfrag_alpha(zlao,:)  =A_frag(A_frag(:,7)==input_alpha(laoz,7)&A_frag(:,13)~=input_alpha(laoz,13),:);
        
        
        
        midfrag_struc=A_frag(A_frag(:,13)==single_midfrag_point_atom_c(zlao,13),:);
        size_midfrag_struc=size(midfrag_struc);
        midfrag_struc(:,9)=1:size_midfrag_struc(1,1);
        clear double_midfrag_point_atom single_midfrag_point_atom

        
        
at=1;
att=1;
double_midfrag_point=[];
double_midfrag_point_atom=[];
for i=1:2:size_double_bridge_c3(1,1)-1
    for j=1:1:size_midfrag_struc(1,1)-1
        for k=j+1:1:size_midfrag_struc(1,1)
            if ((midfrag_struc(j,7)==double_bridge_c3(i,7)&midfrag_struc(k,7)==double_bridge_c3(i+1,7))|(midfrag_struc(k,7)==double_bridge_c3(i,7)&midfrag_struc(j,7)==double_bridge_c3(i+1,7)))&midfrag_struc(j,13)==midfrag_struc(k,13)&midfrag_struc(j,13)~=double_bridge_c3(i,13)
                double_midfrag_point(at,1)=midfrag_struc(j,13);
                double_midfrag_point_atom(att,:)=bridge_c3(i,:);
                double_midfrag_point_atom(att+1,:)=bridge_c3(i+1,:);
                at=at+1;
                att=att+2;
            end
        end
    end
end

size_double_midfrag_point=size(double_midfrag_point);


%single_midfrag_point_atom=[];
%for i=1:1:size_single_midfrag_point_atom_c(1,1)
    for j=1:1:size_midfrag_struc(1,1)
        if midfrag_struc(j,7)==single_midfrag_point_atom_c(zlao,7)&midfrag_struc(j,13)==single_midfrag_point_atom_c(zlao,13)
            single_midfrag_point_atom_c(zlao,9)=midfrag_struc(j,9);
        end
    end
%end

%size_single_midfrag_point_atom=size(single_midfrag_point_atom);


    midfrag_alpha=[];
    for j=1:1:size_contact_type_A(1,1)
        for k=1:1:size_midfrag_struc(1,1)
            if (single_midfrag_point_atom_c(zlao,7)==contact_type_A(j,1)&midfrag_struc(k,7)==contact_type_A(j,2))|(midfrag_struc(k,7)==contact_type_A(j,1)&single_midfrag_point_atom_c(zlao,7)==contact_type_A(j,2))
                midfrag_alpha(zlao,:)=midfrag_struc(k,:);
                %break
            end
        end
    end
grid_d=0.02;  
max_x=max(single_bridge_c3(laoz,1))+1.8;
min_x=min(single_bridge_c3(laoz,1))-1.8;
max_y=max(single_bridge_c3(laoz,2))+1.8;
min_y=min(single_bridge_c3(laoz,2))-1.8;
max_z=max(single_bridge_c3(laoz,3))+1.8;
min_z=min(single_bridge_c3(laoz,3))-1.8;
sig_x=transpose(min_x:grid_d:max_x);
sig_y=transpose(min_y:grid_d:max_y);
sig_z=transpose(min_z:grid_d:max_z);

size_sig_x=size(sig_x);
size_sig_y=size(sig_y);
size_sig_z=size(sig_z);

clear grid_coor grid_coor1r

for i=1:1:size_sig_x(1,1)
    grid_coor((i-1)*size_sig_y(1,1)*size_sig_z(1,1)+1:i*size_sig_y(1,1)*size_sig_z(1,1),1)=sig_x(i,1);
end

for i=1:1:size_sig_y(1,1)
    grid_coor1r((i-1)*size_sig_z(1,1)+1:i*size_sig_z(1,1),1)=sig_y(i,1);
end

grid_coor(:,2)=repmat(grid_coor1r,size_sig_x(1,1),1);
grid_coor(:,3)=repmat(sig_z,size_sig_x(1,1)*size_sig_y(1,1),1);
grid_coor(:,4)=laoz;
size_grid_coor=size(grid_coor);
P_sh=grid_coor(abs(sqrt( (grid_coor(:,1)-single_bridge_c3(laoz,1)).^2+(grid_coor(:,2)-single_bridge_c3(laoz,2)).^2+(grid_coor(:,3)-single_bridge_c3(laoz,3)).^2 )- norm(midfrag_alpha(laoz,1:3)-single_midfrag_point_atom_c(zlao,1:3))   )<=0.01&   abs( sqrt( (grid_coor(:,1)-input_alpha(laoz,1)).^2+(grid_coor(:,2)-input_alpha(laoz,2)).^2+(grid_coor(:,3)-input_alpha(laoz,3)).^2 )- norm(midfrag_alpha(laoz,1:3)-old_input_alpha(laoz,1:3))) < 0.01   ,:);
clear grid_coor grid_coor1r;

size_P_sh=size(P_sh);
    
    
    
    %single_bridge_c3(laoz,1) P_sh midfrag_struc midfrag_alpha(laoz,1)
    %single_midfrag_point_atom(laoz,1)
        


%function out = fragposegen(frag_chos,Heatmap, single_bridge_c3, P_sh, midfrag_alpha, single_midfrag_point_atom, centroidb )

frag_chos=midfrag_struc;
size_frag_chos=size(frag_chos);
%frag_chos(:,7)=1:size_frag_chos(1,1);

size_P_sh=size(P_sh);


c3_num=0;
c2_num=0;
c1_num=0;
car_num=0;
o3_num=0;
o2_num=0;
n2_num=0;
nar_num=0;
nam_num=0;
npl3_num=0;
s_num=0;
f_num=0;
cl_num=0;
br_num=0;
n4_num=0;


for lz=1:1:size_frag_chos(1,1)
    
    if frag_chos(lz,4)==4
        car_num=car_num+1;  
    elseif frag_chos(lz,4)==5
        o3_num=o3_num+1;      
    elseif (frag_chos(lz,4)==6)|(frag_chos(lz,4)==25)
        o2_num=o2_num+1;                     
    elseif (frag_chos(lz,4)==12|frag_chos(lz,4)==N_4)&frag_chos(lz,8)>0
        nam_num=nam_num+1;              
    elseif frag_chos(lz,4)==13&frag_chos(lz,8)>0
        npl3_num=npl3_num+1;                
    elseif (frag_chos(lz,4)>=14)&(frag_chos(lz,4)<=17)
        s_num=s_num+1;
    elseif frag_chos(lz,4)==19
        cl_num=cl_num+1;
    elseif frag_chos(lz,4)==20
        br_num=br_num+1;
    elseif frag_chos(lz,4)==26
        n4_num=n4_num+1;    
    end
end




o2_atom=[];
nam_atom=[];
npl3_atom=[];
o3_atom=[];
nar_atom=[];
n2_atom=[];
c3_atom=[];
c2_atom=[];
car_atom=[];
n4_atom=[];


lia=1;
lib=1;
lic=1;
lid=1;
lie=1;
lif=1;
liff=1;
lifff=1;
liffff=1;
lih=1;

for i=1:1:size_frag_chos(1,1)
    if frag_chos(i,4)==O_2
        o2_atom(lia,:)=frag_chos(i,:);
        lia=lia+1;
    elseif (frag_chos(i,4)==N_am|frag_chos(i,4)==N_4)&frag_chos(i,8)>0
        nam_atom(lib,:)=frag_chos(i,:);
        lib=lib+1;
    elseif frag_chos(i,4)==N_pl3&frag_chos(i,8)>0
        npl3_atom(lic,:)=frag_chos(i,:);
        lic=lic+1;
    elseif frag_chos(i,4)==O_3
        o3_atom(lid,:)=frag_chos(i,:);
        lid=lid+1;    
    elseif frag_chos(i,4)==Cl
        cl_atom(lif,:)=frag_chos(i,:);
        lif=lif+1; 
    elseif frag_chos(i,4)==Br
        br_atom(liff,:)=frag_chos(i,:);
        liff=liff+1; 

    elseif frag_chos(i,4)==C_ar
        car_atom(liffff,:)=frag_chos(i,:);
        liffff=liffff+1; 
    elseif frag_chos(i,4)==N_4
        n4_atom(lih,:)=frag_chos(i,:);
        lih=lih+1; 

    end
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%clear best_struc;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
cxt=0;

   sing_alph_o2_combo=[];     
if o2_num>=1
    %sing_alph_o2_combo=[1 1 1 1 1 1];
hug1=1;
%for i=1:1:o2_num-2
%    for x=1:1:size_grid_o2_final(1,1)
%            for j=i+1:1:o2_num-1
                for y=1:1:size_P_sh(1,1)
                    %if (abs(norm(single_bridge_c3(1,1:3)-o2_atom(j,1:3))-norm(grid_o2_final(x,1:3)-grid_o2_final(y,1:3)))<=0.5)&grid_o2_final(x,4)~=grid_o2_final(y,4)
                        for k=1:1:o2_num
                            for z=1:1:size_grid_o2_final(1,1)
                                if (abs(norm(single_midfrag_point_atom_c(zlao,1:3)-o2_atom(k,1:3))-norm(single_bridge_c3(laoz,1:3)-grid_o2_final(z,1:3)))<=0.5)&(abs(norm(midfrag_alpha(zlao,1:3)-o2_atom(k,1:3))-norm(P_sh(y,1:3)-grid_o2_final(z,1:3)))<=0.5)&(abs(norm(old_input_alpha(laoz,1:3)-o2_atom(k,1:3))-norm(input_alpha(laoz,1:3)-grid_o2_final(z,1:3)))<=0.5)

                                    sing_alph_o2_combo(hug1,1)=laoz;
                                    sing_alph_o2_combo(hug1,2)=y;
                                    sing_alph_o2_combo(hug1,3)=z;
                                    
                                    sing_alph_o2_combo(hug1,4)=zlao;
                                    sing_alph_o2_combo(hug1,5)=zlao;
                                    sing_alph_o2_combo(hug1,6)=k;
                                    hug1=hug1+1;
                                end
                            end
                        end
                    %end
                end
%            end
%    end
%end



size_sing_alph_o2_combo=size(sing_alph_o2_combo);


end




   sing_alph_o3_combo=[];     
if o3_num>=1
    %sing_alph_o3_combo=[1 1 1 1 1 1];
hug1=1;
%for i=1:1:o3_num-2
%    for x=1:1:size_grid_o3_final(1,1)
%            for j=i+1:1:o3_num-1
                for y=1:1:size_P_sh(1,1)
                    %if (abs(norm(single_bridge_c3(1,1:3)-o3_atom(j,1:3))-norm(grid_o3_final(x,1:3)-grid_o3_final(y,1:3)))<=0.5)&grid_o3_final(x,4)~=grid_o3_final(y,4)
                        for k=1:1:o3_num
                            for z=1:1:size_grid_o3_final(1,1)
                                if (abs(norm(single_midfrag_point_atom_c(zlao,1:3)-o3_atom(k,1:3))-norm(single_bridge_c3(laoz,1:3)-grid_o3_final(z,1:3)))<=0.5)&(abs(norm(midfrag_alpha(zlao,1:3)-o3_atom(k,1:3))-norm(P_sh(y,1:3)-grid_o3_final(z,1:3)))<=0.5)&(abs(norm(old_input_alpha(laoz,1:3)-o3_atom(k,1:3))-norm(input_alpha(laoz,1:3)-grid_o3_final(z,1:3)))<=0.5)

                                    sing_alph_o3_combo(hug1,1)=laoz;
                                    sing_alph_o3_combo(hug1,2)=y;
                                    sing_alph_o3_combo(hug1,3)=z;
                                    
                                    sing_alph_o3_combo(hug1,4)=zlao;
                                    sing_alph_o3_combo(hug1,5)=zlao;
                                    sing_alph_o3_combo(hug1,6)=k;
                                    hug1=hug1+1;
                                end
                            end
                        end
                    %end
                end
%            end
%    end
%end



size_sing_alph_o3_combo=size(sing_alph_o3_combo);


end



   sing_alph_nam_combo=[];     
if nam_num>=1
    %sing_alph_nam_combo=[1 1 1 1 1 1];
hug1=1;
%for i=1:1:nam_num-2
%    for x=1:1:size_grid_nam_final(1,1)
%            for j=i+1:1:nam_num-1
                for y=1:1:size_P_sh(1,1)
                    %if (abs(norm(single_bridge_c3(1,1:3)-nam_atom(j,1:3))-norm(grid_nam_final(x,1:3)-grid_nam_final(y,1:3)))<=0.5)&grid_nam_final(x,4)~=grid_nam_final(y,4)
                        for k=1:1:nam_num
                            for z=1:1:size_grid_nam_final(1,1)
                                if (abs(norm(single_midfrag_point_atom_c(zlao,1:3)-nam_atom(k,1:3))-norm(single_bridge_c3(laoz,1:3)-grid_nam_final(z,1:3)))<=0.5)&(abs(norm(midfrag_alpha(zlao,1:3)-nam_atom(k,1:3))-norm(P_sh(y,1:3)-grid_nam_final(z,1:3)))<=0.5)&(abs(norm(old_input_alpha(laoz,1:3)-nam_atom(k,1:3))-norm(input_alpha(laoz,1:3)-grid_nam_final(z,1:3)))<=0.5)

                                    sing_alph_nam_combo(hug1,1)=laoz;
                                    sing_alph_nam_combo(hug1,2)=y;
                                    sing_alph_nam_combo(hug1,3)=z;
                                    
                                    sing_alph_nam_combo(hug1,4)=zlao;
                                    sing_alph_nam_combo(hug1,5)=zlao;
                                    sing_alph_nam_combo(hug1,6)=k;
                                    hug1=hug1+1;
                                end
                            end
                        end
                    %end
                end
%            end
%    end
%end



size_sing_alph_nam_combo=size(sing_alph_nam_combo);


end



   sing_alph_npl3_combo=[];     
if npl3_num>=1
    %sing_alph_npl3_combo=[1 1 1 1 1 1];
hug1=1;
%for i=1:1:npl3_num-2
%    for x=1:1:size_grid_npl3_final(1,1)
%            for j=i+1:1:npl3_num-1
                for y=1:1:size_P_sh(1,1)
                    %if (abs(norm(single_bridge_c3(1,1:3)-npl3_atom(j,1:3))-norm(grid_npl3_final(x,1:3)-grid_npl3_final(y,1:3)))<=0.5)&grid_npl3_final(x,4)~=grid_npl3_final(y,4)
                        for k=1:1:npl3_num
                            for z=1:1:size_grid_npl3_final(1,1)
                                if (abs(norm(single_midfrag_point_atom_c(zlao,1:3)-npl3_atom(k,1:3))-norm(single_bridge_c3(laoz,1:3)-grid_npl3_final(z,1:3)))<=0.5)&(abs(norm(midfrag_alpha(zlao,1:3)-npl3_atom(k,1:3))-norm(P_sh(y,1:3)-grid_npl3_final(z,1:3)))<=0.5)&(abs(norm(old_input_alpha(laoz,1:3)-npl3_atom(k,1:3))-norm(input_alpha(laoz,1:3)-grid_npl3_final(z,1:3)))<=0.5)

                                    sing_alph_npl3_combo(hug1,1)=laoz;
                                    sing_alph_npl3_combo(hug1,2)=y;
                                    sing_alph_npl3_combo(hug1,3)=z;
                                    
                                    sing_alph_npl3_combo(hug1,4)=zlao;
                                    sing_alph_npl3_combo(hug1,5)=zlao;
                                    sing_alph_npl3_combo(hug1,6)=k;
                                    hug1=hug1+1;
                                end
                            end
                        end
                    %end
                end
%            end
%    end
%end



size_sing_alph_npl3_combo=size(sing_alph_npl3_combo);


end



   sing_alph_n4_combo=[];     
if n4_num>=1
    %sing_alph_n4_combo=[1 1 1 1 1 1];
hug1=1;
%for i=1:1:n4_num-2
%    for x=1:1:size_grid_n4_final(1,1)
%            for j=i+1:1:n4_num-1
                for y=1:1:size_P_sh(1,1)
                    %if (abs(norm(single_bridge_c3(1,1:3)-n4_atom(j,1:3))-norm(grid_n4_final(x,1:3)-grid_n4_final(y,1:3)))<=0.5)&grid_n4_final(x,4)~=grid_n4_final(y,4)
                        for k=1:1:n4_num
                            for z=1:1:size_grid_n4_final(1,1)
                                if (abs(norm(single_midfrag_point_atom_c(zlao,1:3)-n4_atom(k,1:3))-norm(single_bridge_c3(laoz,1:3)-grid_n4_final(z,1:3)))<=0.5)&(abs(norm(midfrag_alpha(zlao,1:3)-n4_atom(k,1:3))-norm(P_sh(y,1:3)-grid_n4_final(z,1:3)))<=0.5)&(abs(norm(old_input_alpha(laoz,1:3)-n4_atom(k,1:3))-norm(input_alpha(laoz,1:3)-grid_n4_final(z,1:3)))<=0.5)

                                    sing_alph_n4_combo(hug1,1)=laoz;
                                    sing_alph_n4_combo(hug1,2)=y;
                                    sing_alph_n4_combo(hug1,3)=z;
                                    
                                    sing_alph_n4_combo(hug1,4)=zlao;
                                    sing_alph_n4_combo(hug1,5)=zlao;
                                    sing_alph_n4_combo(hug1,6)=k;
                                    hug1=hug1+1;
                                end
                            end
                        end
                    %end
                end
%            end
%    end
%end



size_sing_alph_n4_combo=size(sing_alph_n4_combo);


end


   sing_alph_car_combo=[];     
if car_num>=1
    %sing_alph_car_combo=[1 1 1 1 1 1];
hug1=1;
%for i=1:1:car_num-2
%    for x=1:1:size_grid_car_final(1,1)
%            for j=i+1:1:car_num-1
                for y=1:1:size_P_sh(1,1)
                    %if (abs(norm(single_bridge_c3(1,1:3)-car_atom(j,1:3))-norm(grid_car_final(x,1:3)-grid_car_final(y,1:3)))<=0.5)&grid_car_final(x,4)~=grid_car_final(y,4)
                        for k=1:1:car_num
                            for z=1:1:size_grid_car_final(1,1)
                                if (abs(norm(single_midfrag_point_atom_c(zlao,1:3)-car_atom(k,1:3))-norm(single_bridge_c3(laoz,1:3)-grid_car_final(z,1:3)))<=0.5)&(abs(norm(midfrag_alpha(zlao,1:3)-car_atom(k,1:3))-norm(P_sh(y,1:3)-grid_car_final(z,1:3)))<=0.5)&(abs(norm(old_input_alpha(laoz,1:3)-car_atom(k,1:3))-norm(input_alpha(laoz,1:3)-grid_car_final(z,1:3)))<=0.5)

                                    sing_alph_car_combo(hug1,1)=laoz;
                                    sing_alph_car_combo(hug1,2)=y;
                                    sing_alph_car_combo(hug1,3)=z;
                                    
                                    sing_alph_car_combo(hug1,4)=zlao;
                                    sing_alph_car_combo(hug1,5)=zlao;
                                    sing_alph_car_combo(hug1,6)=k;
                                    hug1=hug1+1;
                                end
                            end
                        end
                    %end
                end
%            end
%    end
%end



size_sing_alph_car_combo=size(sing_alph_car_combo);


end








if length(sing_alph_o2_combo)~=0
    size_sing_alph_o2_combo=size(sing_alph_o2_combo);
    for daz=1:1:size_sing_alph_o2_combo(1,1)
        modia=laoz;
        modi1=single_midfrag_point_atom_c(sing_alph_o2_combo(daz,4),9);
        modib=sing_alph_o2_combo(daz,2);
        modi2=midfrag_alpha(sing_alph_o2_combo(daz,5),9);
        modic=sing_alph_o2_combo(daz,3);
        modi3=o2_atom(sing_alph_o2_combo(daz,6),9);
        
        
stan_frag_chos=single_bridge_c3;
%stan_frag_chos(modib+20,:)=grid_npl3_final(modib,:);
%stan_frag_chos(modic+40,:)=grid_o3_final(modic,:);


x_move=stan_frag_chos(modia,1)-frag_chos(modi1,1);
y_move=stan_frag_chos(modia,2)-frag_chos(modi1,2);
z_move=stan_frag_chos(modia,3)-frag_chos(modi1,3);
clear frag_chos_stage1;
for i=1:1:size_frag_chos(1,1)
    frag_chos_stage1(i,1)=frag_chos(i,1)+x_move;
    frag_chos_stage1(i,2)=frag_chos(i,2)+y_move;
    frag_chos_stage1(i,3)=frag_chos(i,3)+z_move;
    frag_chos_stage1(i,4:13)=frag_chos(i,4:13);
end

size_frag_chos_stage1=size(frag_chos_stage1);

%frag_chos_pointer(1,:)=frag_chos_stage1(3,:);
N_pointer=frag_chos_stage1(modi2,:);
C_pointer=frag_chos_stage1(modi3,:);

    
%stan_frag_chos_pointer(1,:)=stan_frag_chos(modi,:);
stan_N_pointer=P_sh(modib,:);
stan_C_pointer=grid_o2_final(modic,:);

size_frag_chos_pointer=size(frag_chos_pointer);

size_stan_frag_chos_pointer=size(stan_frag_chos_pointer);

frag_chos_stage3=rota51(N_pointer,C_pointer,stan_frag_chos,stan_N_pointer,stan_C_pointer,frag_chos_stage1,modia,modib,modic,modi1,modi2,modi3,centroidb);
size_frag_chos_stage3=size(frag_chos_stage3);
if size_frag_chos_stage3(1,1)<size_frag_chos(1,1)
    continue
elseif size_frag_chos_stage3(1,2)==size_frag_chos(1,2)
    test_stage3=frag_chos_stage3;
    distj=judge_dist(test_stage3,frag_chos_stage1);
    atom_old=input_struc_1;
    colli=collision_jug1(atom_old,test_stage3,laoz,single_bridge_c3,input_alpha,single_midfrag_point_atom_c(sing_alph_o2_combo(daz,4),:),midfrag_alpha(sing_alph_o2_combo(daz,4),:));
    if distj==1&colli==1
        eval( strcat(  'mid_best_struc_',num2str(frag1),'_',num2str(frag2),'=frag_chos_stage3;')  );
        Ind_table(frag2,1)=frag1;
        Ind_table(frag2,frn)=ifr2;
        frag2=frag2+1;
    end
elseif size_frag_chos_stage3(1,2)==2*size_frag_chos(1,2)
    test_stage3_1=frag_chos_stage3(:,1:13);
    test_stage3_2=frag_chos_stage3(:,14:26);
    distj1=judge_dist(test_stage3_1,frag_chos_stage1);
    distj2=judge_dist(test_stage3_2,frag_chos_stage1);
    atom_old=input_struc_1;
    colli1=collision_jug1(atom_old,test_stage3_1,laoz,single_bridge_c3,input_alpha,single_midfrag_point_atom_c(sing_alph_o2_combo(daz,4),:),midfrag_alpha(sing_alph_o2_combo(daz,4),:));
    colli2=collision_jug1(atom_old,test_stage3_2,laoz,single_bridge_c3,input_alpha,single_midfrag_point_atom_c(sing_alph_o2_combo(daz,4),:),midfrag_alpha(sing_alph_o2_combo(daz,4),:));

    if (distj1==1&colli1==1)&(distj2~=1|colli2~=1)
        eval( strcat(  'mid_best_struc_',num2str(frag1),'_',num2str(frag2),'=frag_chos_stage3(:,1:13);')  );
        Ind_table(frag2,1)=frag1;
        Ind_table(frag2,frn)=ifr2;
        frag2=frag2+1;
        
        
    elseif (distj2==1&colli2==1)&(distj1~=1|colli1~=1)
        eval( strcat(  'mid_best_struc_',num2str(frag1),'_',num2str(frag2),'=frag_chos_stage3(:,14:26);')  );
        Ind_table(frag2,1)=frag1;
        Ind_table(frag2,frn)=ifr2;
        frag2=frag2+1;
        
    elseif  distj1==1&distj2==1&colli1==1&colli2==1
        eval( strcat(  'mid_best_struc_',num2str(frag1),'_',num2str(frag2),'=frag_chos_stage3(:,1:13);')  );
        Ind_table(frag2,1)=frag1;
        Ind_table(frag2,frn)=ifr2;
        frag2=frag2+1;
        eval( strcat(  'mid_best_struc_',num2str(frag1),'_',num2str(frag2),'=frag_chos_stage3(:,14:26);')  );
        Ind_table(frag2,1)=frag1;
        Ind_table(frag2,frn)=ifr2;
        frag2=frag2+1;
    end
    
end




%colli=collision_jug12(protein_dir,frag_chos_stage3(:,1:3));
%if colli==0
%    continue
%end
    

%out(frag2,1)=kecsamt1(protein_dir,frag_chos_dir,frag_chos_stage3(:,1:3));

%eval( strcat(  'best_struc',num2str(frag2),'=o2_npl3_o3_struc_',num2str(daz),'_1;')  );
%clear input;
%eval( strcat(  'input_struc=best_struc',num2str(frag2),'_1;')  );
%pp=lig_output(input_struc,frag_chos_dir,frag2);   

%if frag2==1
%    %eval( strcat(  'best_struc',num2str(frag2),'=o2_o2_npl3_struc_',num2str(daz),'_1;')  );
%    best_score=out(frag2,1);
%    frag2=frag2+1;
%elseif (frag2>1)&(out(frag2,1)==max(out))
%    %eval( strcat(  'best_struc',num2str(frag2),'=o2_o2_npl3_struc_',num2str(daz),'_1;')  );
%    best_score=out(frag2,1);
%    frag2=frag2+1;
%end


        clear functions;
        pack
    end
    
end
     






if length(sing_alph_o3_combo)~=0
    size_sing_alph_o3_combo=size(sing_alph_o3_combo);
    for daz=1:1:size_sing_alph_o3_combo(1,1)
        modia=laoz;
        modi1=single_midfrag_point_atom_c(sing_alph_o3_combo(daz,4),9);
        modib=sing_alph_o3_combo(daz,2);
        modi2=midfrag_alpha(sing_alph_o3_combo(daz,5),9);
        modic=sing_alph_o3_combo(daz,3);
        modi3=o3_atom(sing_alph_o3_combo(daz,6),9);
        
        
stan_frag_chos=single_bridge_c3;
%stan_frag_chos(modib+20,:)=grid_npl3_final(modib,:);
%stan_frag_chos(modic+40,:)=grid_o3_final(modic,:);


x_move=stan_frag_chos(modia,1)-frag_chos(modi1,1);
y_move=stan_frag_chos(modia,2)-frag_chos(modi1,2);
z_move=stan_frag_chos(modia,3)-frag_chos(modi1,3);
clear frag_chos_stage1;
for i=1:1:size_frag_chos(1,1)
    frag_chos_stage1(i,1)=frag_chos(i,1)+x_move;
    frag_chos_stage1(i,2)=frag_chos(i,2)+y_move;
    frag_chos_stage1(i,3)=frag_chos(i,3)+z_move;
    frag_chos_stage1(i,4:13)=frag_chos(i,4:13);
end

size_frag_chos_stage1=size(frag_chos_stage1);

%frag_chos_pointer(1,:)=frag_chos_stage1(3,:);
N_pointer=frag_chos_stage1(modi2,:);
C_pointer=frag_chos_stage1(modi3,:);

    
%stan_frag_chos_pointer(1,:)=stan_frag_chos(modi,:);
stan_N_pointer=P_sh(modib,:);
stan_C_pointer=grid_o3_final(modic,:);

size_frag_chos_pointer=size(frag_chos_pointer);

size_stan_frag_chos_pointer=size(stan_frag_chos_pointer);

frag_chos_stage3=rota51(N_pointer,C_pointer,stan_frag_chos,stan_N_pointer,stan_C_pointer,frag_chos_stage1,modia,modib,modic,modi1,modi2,modi3,protein_frame);
size_frag_chos_stage3=size(frag_chos_stage3);
if size_frag_chos_stage3(1,1)<size_frag_chos(1,1)
    continue
elseif size_frag_chos_stage3(1,2)==size_frag_chos(1,2)
    test_stage3=frag_chos_stage3;
    distj=judge_dist(test_stage3,frag_chos_stage1);
    atom_old=input_struc_1;
    colli=collision_jug1(atom_old,test_stage3,laoz,single_bridge_c3,input_alpha,single_midfrag_point_atom_c(sing_alph_o3_combo(daz,4),:),midfrag_alpha(sing_alph_o3_combo(daz,4),:));
    if distj==1&colli==1
        eval( strcat(  'mid_best_struc_',num2str(frag1),'_',num2str(frag2),'=frag_chos_stage3;')  );
        Ind_table(frag2,1)=frag1;
        Ind_table(frag2,frn)=ifr2;
        frag2=frag2+1;
    end
elseif size_frag_chos_stage3(1,2)==2*size_frag_chos(1,2)
    test_stage3_1=frag_chos_stage3(:,1:13);
    test_stage3_2=frag_chos_stage3(:,14:26);
    distj1=judge_dist(test_stage3_1,frag_chos_stage1);
    distj2=judge_dist(test_stage3_2,frag_chos_stage1);
    atom_old=input_struc_1;
    colli1=collision_jug1(atom_old,test_stage3_1,laoz,single_bridge_c3,input_alpha,single_midfrag_point_atom_c(sing_alph_o3_combo(daz,4),:),midfrag_alpha(sing_alph_o3_combo(daz,4),:));
    colli2=collision_jug1(atom_old,test_stage3_2,laoz,single_bridge_c3,input_alpha,single_midfrag_point_atom_c(sing_alph_o3_combo(daz,4),:),midfrag_alpha(sing_alph_o3_combo(daz,4),:));

    if (distj1==1&colli1==1)&(distj2~=1|colli2~=1)
        eval( strcat(  'mid_best_struc_',num2str(frag1),'_',num2str(frag2),'=frag_chos_stage3(:,1:13);')  );
        Ind_table(frag2,1)=frag1;
        Ind_table(frag2,frn)=ifr2;
        frag2=frag2+1;
        
        
    elseif (distj2==1&colli2==1)&(distj1~=1|colli1~=1)
        eval( strcat(  'mid_best_struc_',num2str(frag1),'_',num2str(frag2),'=frag_chos_stage3(:,14:26);')  );
        Ind_table(frag2,1)=frag1;
        Ind_table(frag2,frn)=ifr2;
        frag2=frag2+1;
        
    elseif  distj1==1&distj2==1&colli1==1&colli2==1
        eval( strcat(  'mid_best_struc_',num2str(frag1),'_',num2str(frag2),'=frag_chos_stage3(:,1:13);')  );
        Ind_table(frag2,1)=frag1;
        Ind_table(frag2,frn)=ifr2;
        frag2=frag2+1;
        eval( strcat(  'mid_best_struc_',num2str(frag1),'_',num2str(frag2),'=frag_chos_stage3(:,14:26);')  );
        Ind_table(frag2,1)=frag1;
        Ind_table(frag2,frn)=ifr2;
        frag2=frag2+1;
    end
    
end



        clear functions;
        pack
    end
    
end
     




if length(sing_alph_nam_combo)~=0
    size_sing_alph_nam_combo=size(sing_alph_nam_combo);
    for daz=1:1:size_sing_alph_nam_combo(1,1)
        modia=laoz;
        modi1=single_midfrag_point_atom_c(sing_alph_nam_combo(daz,4),9);
        modib=sing_alph_nam_combo(daz,2);
        modi2=midfrag_alpha(sing_alph_nam_combo(daz,5),9);
        modic=sing_alph_nam_combo(daz,3);
        modi3=nam_atom(sing_alph_nam_combo(daz,6),9);
        
        
stan_frag_chos=single_bridge_c3;
%stan_frag_chos(modib+20,:)=grid_npl3_final(modib,:);
%stan_frag_chos(modic+40,:)=grid_nam_final(modic,:);


x_move=stan_frag_chos(modia,1)-frag_chos(modi1,1);
y_move=stan_frag_chos(modia,2)-frag_chos(modi1,2);
z_move=stan_frag_chos(modia,3)-frag_chos(modi1,3);
clear frag_chos_stage1;
for i=1:1:size_frag_chos(1,1)
    frag_chos_stage1(i,1)=frag_chos(i,1)+x_move;
    frag_chos_stage1(i,2)=frag_chos(i,2)+y_move;
    frag_chos_stage1(i,3)=frag_chos(i,3)+z_move;
    frag_chos_stage1(i,4:13)=frag_chos(i,4:13);
end

size_frag_chos_stage1=size(frag_chos_stage1);

%frag_chos_pointer(1,:)=frag_chos_stage1(3,:);
N_pointer=frag_chos_stage1(modi2,:);
C_pointer=frag_chos_stage1(modi3,:);

    
%stan_frag_chos_pointer(1,:)=stan_frag_chos(modi,:);
stan_N_pointer=P_sh(modib,:);
stan_C_pointer=grid_nam_final(modic,:);

size_frag_chos_pointer=size(frag_chos_pointer);

size_stan_frag_chos_pointer=size(stan_frag_chos_pointer);

frag_chos_stage3=rota51(N_pointer,C_pointer,stan_frag_chos,stan_N_pointer,stan_C_pointer,frag_chos_stage1,modia,modib,modic,modi1,modi2,modi3,protein_frame);
size_frag_chos_stage3=size(frag_chos_stage3);
if size_frag_chos_stage3(1,1)<size_frag_chos(1,1)
    continue
elseif size_frag_chos_stage3(1,2)==size_frag_chos(1,2)
    test_stage3=frag_chos_stage3;
    distj=judge_dist(test_stage3,frag_chos_stage1);
    atom_old=input_struc_1;
    colli=collision_jug1(atom_old,test_stage3,laoz,single_bridge_c3,input_alpha,single_midfrag_point_atom_c(sing_alph_nam_combo(daz,4),:),midfrag_alpha(sing_alph_nam_combo(daz,4),:));
    if distj==1&colli==1
        eval( strcat(  'mid_best_struc_',num2str(frag1),'_',num2str(frag2),'=frag_chos_stage3;')  );
        Ind_table(frag2,1)=frag1;
        Ind_table(frag2,frn)=ifr2;
        frag2=frag2+1;
    end
elseif size_frag_chos_stage3(1,2)==2*size_frag_chos(1,2)
    test_stage3_1=frag_chos_stage3(:,1:13);
    test_stage3_2=frag_chos_stage3(:,14:26);
    distj1=judge_dist(test_stage3_1,frag_chos_stage1);
    distj2=judge_dist(test_stage3_2,frag_chos_stage1);
    atom_old=input_struc_1;
    colli1=collision_jug1(atom_old,test_stage3_1,laoz,single_bridge_c3,input_alpha,single_midfrag_point_atom_c(sing_alph_nam_combo(daz,4),:),midfrag_alpha(sing_alph_nam_combo(daz,4),:));
    colli2=collision_jug1(atom_old,test_stage3_2,laoz,single_bridge_c3,input_alpha,single_midfrag_point_atom_c(sing_alph_nam_combo(daz,4),:),midfrag_alpha(sing_alph_nam_combo(daz,4),:));

    if (distj1==1&colli1==1)&(distj2~=1|colli2~=1)
        eval( strcat(  'mid_best_struc_',num2str(frag1),'_',num2str(frag2),'=frag_chos_stage3(:,1:13);')  );
        Ind_table(frag2,1)=frag1;
        Ind_table(frag2,frn)=ifr2;
        frag2=frag2+1;
        
        
    elseif (distj2==1&colli2==1)&(distj1~=1|colli1~=1)
        eval( strcat(  'mid_best_struc_',num2str(frag1),'_',num2str(frag2),'=frag_chos_stage3(:,14:26);')  );
        Ind_table(frag2,1)=frag1;
        Ind_table(frag2,frn)=ifr2;
        frag2=frag2+1;
        
    elseif  distj1==1&distj2==1&colli1==1&colli2==1
        eval( strcat(  'mid_best_struc_',num2str(frag1),'_',num2str(frag2),'=frag_chos_stage3(:,1:13);')  );
        Ind_table(frag2,1)=frag1;
        Ind_table(frag2,frn)=ifr2;
        frag2=frag2+1;
        eval( strcat(  'mid_best_struc_',num2str(frag1),'_',num2str(frag2),'=frag_chos_stage3(:,14:26);')  );
        Ind_table(frag2,1)=frag1;
        Ind_table(frag2,frn)=ifr2;
        frag2=frag2+1;
    end
    
end


        clear functions;
        pack
    end
    
end
     




if length(sing_alph_npl3_combo)~=0
    size_sing_alph_npl3_combo=size(sing_alph_npl3_combo);
    for daz=1:1:size_sing_alph_npl3_combo(1,1)
        modia=laoz;
        modi1=single_midfrag_point_atom_c(sing_alph_npl3_combo(daz,4),9);
        modib=sing_alph_npl3_combo(daz,2);
        modi2=midfrag_alpha(sing_alph_npl3_combo(daz,5),9);
        modic=sing_alph_npl3_combo(daz,3);
        modi3=npl3_atom(sing_alph_npl3_combo(daz,6),9);
        
        
stan_frag_chos=single_bridge_c3;
%stan_frag_chos(modib+20,:)=grid_npl3_final(modib,:);
%stan_frag_chos(modic+40,:)=grid_npl3_final(modic,:);


x_move=stan_frag_chos(modia,1)-frag_chos(modi1,1);
y_move=stan_frag_chos(modia,2)-frag_chos(modi1,2);
z_move=stan_frag_chos(modia,3)-frag_chos(modi1,3);
clear frag_chos_stage1;
for i=1:1:size_frag_chos(1,1)
    frag_chos_stage1(i,1)=frag_chos(i,1)+x_move;
    frag_chos_stage1(i,2)=frag_chos(i,2)+y_move;
    frag_chos_stage1(i,3)=frag_chos(i,3)+z_move;
    frag_chos_stage1(i,4:13)=frag_chos(i,4:13);
end

size_frag_chos_stage1=size(frag_chos_stage1);

%frag_chos_pointer(1,:)=frag_chos_stage1(3,:);
N_pointer=frag_chos_stage1(modi2,:);
C_pointer=frag_chos_stage1(modi3,:);

    
%stan_frag_chos_pointer(1,:)=stan_frag_chos(modi,:);
stan_N_pointer=P_sh(modib,:);
stan_C_pointer=grid_npl3_final(modic,:);

size_frag_chos_pointer=size(frag_chos_pointer);

size_stan_frag_chos_pointer=size(stan_frag_chos_pointer);

frag_chos_stage3=rota51(N_pointer,C_pointer,stan_frag_chos,stan_N_pointer,stan_C_pointer,frag_chos_stage1,modia,modib,modic,modi1,modi2,modi3,protein_frame);
size_frag_chos_stage3=size(frag_chos_stage3);
if size_frag_chos_stage3(1,1)<size_frag_chos(1,1)
    continue
elseif size_frag_chos_stage3(1,2)==size_frag_chos(1,2)
    test_stage3=frag_chos_stage3;
    distj=judge_dist(test_stage3,frag_chos_stage1);
    atom_old=input_struc_1;
    colli=collision_jug1(atom_old,test_stage3,laoz,single_bridge_c3,input_alpha,single_midfrag_point_atom_c(sing_alph_npl3_combo(daz,4),:),midfrag_alpha(sing_alph_npl3_combo(daz,4),:));
    if distj==1&colli==1
        eval( strcat(  'mid_best_struc_',num2str(frag1),'_',num2str(frag2),'=frag_chos_stage3;')  );
        Ind_table(frag2,1)=frag1;
        Ind_table(frag2,frn)=ifr2;
        frag2=frag2+1;
    end
elseif size_frag_chos_stage3(1,2)==2*size_frag_chos(1,2)
    test_stage3_1=frag_chos_stage3(:,1:13);
    test_stage3_2=frag_chos_stage3(:,14:26);
    distj1=judge_dist(test_stage3_1,frag_chos_stage1);
    distj2=judge_dist(test_stage3_2,frag_chos_stage1);
    atom_old=input_struc_1;
    colli1=collision_jug1(atom_old,test_stage3_1,laoz,single_bridge_c3,input_alpha,single_midfrag_point_atom_c(sing_alph_npl3_combo(daz,4),:),midfrag_alpha(sing_alph_npl3_combo(daz,4),:));
    colli2=collision_jug1(atom_old,test_stage3_2,laoz,single_bridge_c3,input_alpha,single_midfrag_point_atom_c(sing_alph_npl3_combo(daz,4),:),midfrag_alpha(sing_alph_npl3_combo(daz,4),:));

    if (distj1==1&colli1==1)&(distj2~=1|colli2~=1)
        eval( strcat(  'mid_best_struc_',num2str(frag1),'_',num2str(frag2),'=frag_chos_stage3(:,1:13);')  );
        Ind_table(frag2,1)=frag1;
        Ind_table(frag2,frn)=ifr2;
        frag2=frag2+1;
        
        
    elseif (distj2==1&colli2==1)&(distj1~=1|colli1~=1)
        eval( strcat(  'mid_best_struc_',num2str(frag1),'_',num2str(frag2),'=frag_chos_stage3(:,14:26);')  );
        Ind_table(frag2,1)=frag1;
        Ind_table(frag2,frn)=ifr2;
        frag2=frag2+1;
        
    elseif  distj1==1&distj2==1&colli1==1&colli2==1
        eval( strcat(  'mid_best_struc_',num2str(frag1),'_',num2str(frag2),'=frag_chos_stage3(:,1:13);')  );
        Ind_table(frag2,1)=frag1;
        Ind_table(frag2,frn)=ifr2;
        frag2=frag2+1;
        eval( strcat(  'mid_best_struc_',num2str(frag1),'_',num2str(frag2),'=frag_chos_stage3(:,14:26);')  );
        Ind_table(frag2,1)=frag1;
        Ind_table(frag2,frn)=ifr2;
        frag2=frag2+1;
    end
    
end



        clear functions;
        pack
    end
    
end
     








if length(sing_alph_n4_combo)~=0
    size_sing_alph_n4_combo=size(sing_alph_n4_combo);
    for daz=1:1:size_sing_alph_n4_combo(1,1)
        modia=laoz;
        modi1=single_midfrag_point_atom_c(sing_alph_n4_combo(daz,4),9);
        modib=sing_alph_n4_combo(daz,2);
        modi2=midfrag_alpha(sing_alph_n4_combo(daz,5),9);
        modic=sing_alph_n4_combo(daz,3);
        modi3=n4_atom(sing_alph_n4_combo(daz,6),9);
        
        
stan_frag_chos=single_bridge_c3;
%stan_frag_chos(modib+20,:)=grid_npl3_final(modib,:);
%stan_frag_chos(modic+40,:)=grid_n4_final(modic,:);


x_move=stan_frag_chos(modia,1)-frag_chos(modi1,1);
y_move=stan_frag_chos(modia,2)-frag_chos(modi1,2);
z_move=stan_frag_chos(modia,3)-frag_chos(modi1,3);
clear frag_chos_stage1;
for i=1:1:size_frag_chos(1,1)
    frag_chos_stage1(i,1)=frag_chos(i,1)+x_move;
    frag_chos_stage1(i,2)=frag_chos(i,2)+y_move;
    frag_chos_stage1(i,3)=frag_chos(i,3)+z_move;
    frag_chos_stage1(i,4:13)=frag_chos(i,4:13);
end

size_frag_chos_stage1=size(frag_chos_stage1);

%frag_chos_pointer(1,:)=frag_chos_stage1(3,:);
N_pointer=frag_chos_stage1(modi2,:);
C_pointer=frag_chos_stage1(modi3,:);

    
%stan_frag_chos_pointer(1,:)=stan_frag_chos(modi,:);
stan_N_pointer=P_sh(modib,:);
stan_C_pointer=grid_n4_final(modic,:);

size_frag_chos_pointer=size(frag_chos_pointer);

size_stan_frag_chos_pointer=size(stan_frag_chos_pointer);

frag_chos_stage3=rota51(N_pointer,C_pointer,stan_frag_chos,stan_N_pointer,stan_C_pointer,frag_chos_stage1,modia,modib,modic,modi1,modi2,modi3,protein_frame);
size_frag_chos_stage3=size(frag_chos_stage3);
if size_frag_chos_stage3(1,1)<size_frag_chos(1,1)
    continue
elseif size_frag_chos_stage3(1,2)==size_frag_chos(1,2)
    test_stage3=frag_chos_stage3;
    distj=judge_dist(test_stage3,frag_chos_stage1);
    atom_old=input_struc_1;
    colli=collision_jug1(atom_old,test_stage3,laoz,single_bridge_c3,input_alpha,single_midfrag_point_atom_c(sing_alph_n4_combo(daz,4),:),midfrag_alpha(sing_alph_n4_combo(daz,4),:));
    if distj==1&colli==1
        eval( strcat(  'mid_best_struc_',num2str(frag1),'_',num2str(frag2),'=frag_chos_stage3;')  );
        Ind_table(frag2,1)=frag1;
        Ind_table(frag2,frn)=ifr2;
        frag2=frag2+1;
    end
elseif size_frag_chos_stage3(1,2)==2*size_frag_chos(1,2)
    test_stage3_1=frag_chos_stage3(:,1:13);
    test_stage3_2=frag_chos_stage3(:,14:26);
    distj1=judge_dist(test_stage3_1,frag_chos_stage1);
    distj2=judge_dist(test_stage3_2,frag_chos_stage1);
    atom_old=input_struc_1;
    colli1=collision_jug1(atom_old,test_stage3_1,laoz,single_bridge_c3,input_alpha,single_midfrag_point_atom_c(sing_alph_n4_combo(daz,4),:),midfrag_alpha(sing_alph_n4_combo(daz,4),:));
    colli2=collision_jug1(atom_old,test_stage3_2,laoz,single_bridge_c3,input_alpha,single_midfrag_point_atom_c(sing_alph_n4_combo(daz,4),:),midfrag_alpha(sing_alph_n4_combo(daz,4),:));

    if (distj1==1&colli1==1)&(distj2~=1|colli2~=1)
        eval( strcat(  'mid_best_struc_',num2str(frag1),'_',num2str(frag2),'=frag_chos_stage3(:,1:13);')  );
        Ind_table(frag2,1)=frag1;
        Ind_table(frag2,frn)=ifr2;
        frag2=frag2+1;
        
        
    elseif (distj2==1&colli2==1)&(distj1~=1|colli1~=1)
        eval( strcat(  'mid_best_struc_',num2str(frag1),'_',num2str(frag2),'=frag_chos_stage3(:,14:26);')  );
        Ind_table(frag2,1)=frag1;
        Ind_table(frag2,frn)=ifr2;
        frag2=frag2+1;
        
    elseif  distj1==1&distj2==1&colli1==1&colli2==1
        eval( strcat(  'mid_best_struc_',num2str(frag1),'_',num2str(frag2),'=frag_chos_stage3(:,1:13);')  );
        Ind_table(frag2,1)=frag1;
        Ind_table(frag2,frn)=ifr2;
        frag2=frag2+1;
        eval( strcat(  'mid_best_struc_',num2str(frag1),'_',num2str(frag2),'=frag_chos_stage3(:,14:26);')  );
        Ind_table(frag2,1)=frag1;
        Ind_table(frag2,frn)=ifr2;
        frag2=frag2+1;
    end
    
end


        clear functions;
        pack
    end
    
end
     






if length(sing_alph_car_combo)~=0
    size_sing_alph_car_combo=size(sing_alph_car_combo);
    for daz=1:1:size_sing_alph_car_combo(1,1)
        modia=laoz;
        modi1=single_midfrag_point_atom_c(sing_alph_car_combo(daz,4),9);
        modib=sing_alph_car_combo(daz,2);
        modi2=midfrag_alpha(sing_alph_car_combo(daz,5),9);
        modic=sing_alph_car_combo(daz,3);
        modi3=car_atom(sing_alph_car_combo(daz,6),9);
        
        
stan_frag_chos=single_bridge_c3;
%stan_frag_chos(modib+20,:)=grid_npl3_final(modib,:);
%stan_frag_chos(modic+40,:)=grid_car_final(modic,:);


x_move=stan_frag_chos(modia,1)-frag_chos(modi1,1);
y_move=stan_frag_chos(modia,2)-frag_chos(modi1,2);
z_move=stan_frag_chos(modia,3)-frag_chos(modi1,3);
clear frag_chos_stage1;
for i=1:1:size_frag_chos(1,1)
    frag_chos_stage1(i,1)=frag_chos(i,1)+x_move;
    frag_chos_stage1(i,2)=frag_chos(i,2)+y_move;
    frag_chos_stage1(i,3)=frag_chos(i,3)+z_move;
    frag_chos_stage1(i,4:13)=frag_chos(i,4:13);
end

size_frag_chos_stage1=size(frag_chos_stage1);

%frag_chos_pointer(1,:)=frag_chos_stage1(3,:);
N_pointer=frag_chos_stage1(modi2,:);
C_pointer=frag_chos_stage1(modi3,:);

    
%stan_frag_chos_pointer(1,:)=stan_frag_chos(modi,:);
stan_N_pointer=P_sh(modib,:);
stan_C_pointer=grid_car_final(modic,:);

size_frag_chos_pointer=size(frag_chos_pointer);

size_stan_frag_chos_pointer=size(stan_frag_chos_pointer);

frag_chos_stage3=rota51(N_pointer,C_pointer,stan_frag_chos,stan_N_pointer,stan_C_pointer,frag_chos_stage1,modia,modib,modic,modi1,modi2,modi3,protein_frame);
size_frag_chos_stage3=size(frag_chos_stage3);
if size_frag_chos_stage3(1,1)<size_frag_chos(1,1)
    continue
elseif size_frag_chos_stage3(1,2)==size_frag_chos(1,2)
    test_stage3=frag_chos_stage3;
    distj=judge_dist(test_stage3,frag_chos_stage1);
    atom_old=input_struc_1;
    colli=collision_jug1(atom_old,test_stage3,laoz,single_bridge_c3,input_alpha,single_midfrag_point_atom_c(sing_alph_car_combo(daz,4),:),midfrag_alpha(sing_alph_car_combo(daz,4),:));
    if distj==1&colli==1
        eval( strcat(  'mid_best_struc_',num2str(frag1),'_',num2str(frag2),'=frag_chos_stage3;')  );
        Ind_table(frag2,1)=frag1;
        Ind_table(frag2,frn)=ifr2;
        frag2=frag2+1;
    end
elseif size_frag_chos_stage3(1,2)==2*size_frag_chos(1,2)
    test_stage3_1=frag_chos_stage3(:,1:13);
    test_stage3_2=frag_chos_stage3(:,14:26);
    distj1=judge_dist(test_stage3_1,frag_chos_stage1);
    distj2=judge_dist(test_stage3_2,frag_chos_stage1);
    atom_old=input_struc_1;
    colli1=collision_jug1(atom_old,test_stage3_1,laoz,single_bridge_c3,input_alpha,single_midfrag_point_atom_c(sing_alph_car_combo(daz,4),:),midfrag_alpha(sing_alph_car_combo(daz,4),:));
    colli2=collision_jug1(atom_old,test_stage3_2,laoz,single_bridge_c3,input_alpha,single_midfrag_point_atom_c(sing_alph_car_combo(daz,4),:),midfrag_alpha(sing_alph_car_combo(daz,4),:));

    if (distj1==1&colli1==1)&(distj2~=1|colli2~=1)
        eval( strcat(  'mid_best_struc_',num2str(frag1),'_',num2str(frag2),'=frag_chos_stage3(:,1:13);')  );
        Ind_table(frag2,1)=frag1;
        Ind_table(frag2,frn)=ifr2;
        frag2=frag2+1;
        
        
    elseif (distj2==1&colli2==1)&(distj1~=1|colli1~=1)
        eval( strcat(  'mid_best_struc_',num2str(frag1),'_',num2str(frag2),'=frag_chos_stage3(:,14:26);')  );
        Ind_table(frag2,1)=frag1;
        Ind_table(frag2,frn)=ifr2;
        frag2=frag2+1;
        
    elseif  distj1==1&distj2==1&colli1==1&colli2==1
        eval( strcat(  'mid_best_struc_',num2str(frag1),'_',num2str(frag2),'=frag_chos_stage3(:,1:13);')  );
        Ind_table(frag2,1)=frag1;
        Ind_table(frag2,frn)=ifr2;
        frag2=frag2+1;
        eval( strcat(  'mid_best_struc_',num2str(frag1),'_',num2str(frag2),'=frag_chos_stage3(:,14:26);')  );
        Ind_table(frag2,1)=frag1;
        Ind_table(frag2,frn)=ifr2;
        frag2=frag2+1;
    end
    
end

        clear functions;
        pack
    end
    
end
     


     



    end %zlao end
end % laoz end




    end

end






frn=frn+1;
end

toc
