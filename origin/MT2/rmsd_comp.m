function out = rmsd_comp(comp1,comp2)

size_comp1=size(comp1);

for i=1:1:size_comp1(1,1)
    d1(i,1)=norm(comp1(i,1:3)-comp2(i,1:3));
end

da=d1.*d1;

out=sqrt(sum(da)/size_comp1(1,1));
