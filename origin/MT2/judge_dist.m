function out = judge_dist(test_stage3,frag_chos_stage1)

size_test_stage3=size(test_stage3);
size_frag_chos_stage1=size(frag_chos_stage1);

for i=2:1:size_test_stage3(1,1)
    dist_new(i,1)=norm(test_stage3(i,1:3)-test_stage3(1,1:3));
    dist_old(i,1)=norm(frag_chos_stage1(i,1:3)-frag_chos_stage1(1,1:3));
end

if sum(abs(dist_old-dist_new))<=0.1*size_test_stage3(1,1)
    out=1;
elseif sum(abs(dist_old-dist_new))>0.1*size_test_stage3(1,1)
    out=0;
end
    



